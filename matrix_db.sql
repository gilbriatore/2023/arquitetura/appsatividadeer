CREATE DATABASE  IF NOT EXISTS `matrix` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `matrix`;
-- MySQL dump 10.13  Distrib 8.0.34, for Win64 (x86_64)
--
-- Host: localhost    Database: matrix
-- ------------------------------------------------------
-- Server version	8.0.34

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `atividade`
--

DROP TABLE IF EXISTS `atividade`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `atividade` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1500 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='A Atividade.\\n@author Geucimar\\n@version 1.0\\n@created 19-out-2023 16:48:38';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `atividade`
--

LOCK TABLES `atividade` WRITE;
/*!40000 ALTER TABLE `atividade` DISABLE KEYS */;
INSERT INTO `atividade` VALUES (1,'amongst ack'),(2,'street'),(3,'vaporise ricochet beneath'),(4,'like easily left'),(5,'flu'),(6,'phooey youthful'),(7,'unknown meh'),(8,'ugh'),(9,'approval long-term about'),(10,'astride modulo rust');
/*!40000 ALTER TABLE `atividade` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bloom`
--

DROP TABLE IF EXISTS `bloom`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `bloom` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nivel` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1500 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='@author Geucimar\\n@version 1.0\\n@created 19-out-2023 16:48:38';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bloom`
--

LOCK TABLES `bloom` WRITE;
/*!40000 ALTER TABLE `bloom` DISABLE KEYS */;
INSERT INTO `bloom` VALUES (1,'with'),(2,'uselessly'),(3,'pole ack'),(4,'miniaturize'),(5,'lag'),(6,'hm'),(7,'nail jolly spiritual'),(8,'awkwardly'),(9,'ping bathhouse despite'),(10,'against gen');
/*!40000 ALTER TABLE `bloom` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `curso`
--

DROP TABLE IF EXISTS `curso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `curso` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) DEFAULT NULL,
  `carga_horas_min_curso` int DEFAULT NULL,
  `carga_horas_min_estagio` int DEFAULT NULL,
  `perc_max_estagio_ac` int DEFAULT NULL,
  `perc_max_atividade_distancia` int DEFAULT NULL,
  `escola_id` int DEFAULT NULL,
  `tipo_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_curso__escola_id` (`escola_id`),
  KEY `fk_curso__tipo_id` (`tipo_id`),
  CONSTRAINT `fk_curso__escola_id` FOREIGN KEY (`escola_id`) REFERENCES `escola` (`id`),
  CONSTRAINT `fk_curso__tipo_id` FOREIGN KEY (`tipo_id`) REFERENCES `tipo_de_curso` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1500 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='@author Geucimar\\n@version 1.0\\n@created 19-out-2023 16:48:38';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `curso`
--

LOCK TABLES `curso` WRITE;
/*!40000 ALTER TABLE `curso` DISABLE KEYS */;
INSERT INTO `curso` VALUES (1,'woot rude',14073,23191,5146,19956,NULL,NULL),(2,'towel detect insidious',19798,3505,28525,4254,NULL,NULL),(3,'disguised fervently dirty',22475,27631,6312,24323,NULL,NULL),(4,'across',23938,21505,12950,108,NULL,NULL),(5,'smuggling measly',19016,12659,957,16558,NULL,NULL),(6,'however aside',2722,28648,27970,30214,NULL,NULL),(7,'meh jubilantly er',14047,29658,21847,462,NULL,NULL),(8,'hence um',4983,26160,30480,4088,NULL,NULL),(9,'yuck skeletal',412,14960,25533,21559,NULL,NULL),(10,'iconify majestically',10446,19906,4170,23937,NULL,NULL);
/*!40000 ALTER TABLE `curso` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `databasechangelog`
--

DROP TABLE IF EXISTS `databasechangelog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `databasechangelog` (
  `ID` varchar(255) NOT NULL,
  `AUTHOR` varchar(255) NOT NULL,
  `FILENAME` varchar(255) NOT NULL,
  `DATEEXECUTED` datetime NOT NULL,
  `ORDEREXECUTED` int NOT NULL,
  `EXECTYPE` varchar(10) NOT NULL,
  `MD5SUM` varchar(35) DEFAULT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `COMMENTS` varchar(255) DEFAULT NULL,
  `TAG` varchar(255) DEFAULT NULL,
  `LIQUIBASE` varchar(20) DEFAULT NULL,
  `CONTEXTS` varchar(255) DEFAULT NULL,
  `LABELS` varchar(255) DEFAULT NULL,
  `DEPLOYMENT_ID` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `databasechangelog`
--

LOCK TABLES `databasechangelog` WRITE;
/*!40000 ALTER TABLE `databasechangelog` DISABLE KEYS */;
INSERT INTO `databasechangelog` VALUES ('00000000000001','jhipster','config/liquibase/changelog/00000000000000_initial_schema.xml','2023-10-23 09:30:29',1,'EXECUTED','9:c7fb86a72d1815e00aff2981ae71c464','createTable tableName=jhi_user; createTable tableName=jhi_authority; createTable tableName=jhi_user_authority; addPrimaryKey tableName=jhi_user_authority; addForeignKeyConstraint baseTableName=jhi_user_authority, constraintName=fk_authority_name, ...','',NULL,'4.24.0',NULL,NULL,'8064229230'),('20231023124332-1','jhipster','config/liquibase/changelog/20231023124332_added_entity_Atividade.xml','2023-10-23 09:45:28',2,'EXECUTED','9:ad52d7ffb3c3cfb5e38216aed910ae39','createTable tableName=atividade','',NULL,'4.24.0',NULL,NULL,'8065128307'),('20231023124332-1-data','jhipster','config/liquibase/changelog/20231023124332_added_entity_Atividade.xml','2023-10-23 09:45:28',3,'EXECUTED','9:eee98398984d61dee1d6c060c1f157e2','loadData tableName=atividade','',NULL,'4.24.0','faker',NULL,'8065128307'),('20231023124333-1','jhipster','config/liquibase/changelog/20231023124333_added_entity_Bloom.xml','2023-10-23 09:45:28',4,'EXECUTED','9:28ef3c3d80af851b8f6b65103d7fbedc','createTable tableName=bloom','',NULL,'4.24.0',NULL,NULL,'8065128307'),('20231023124333-1-data','jhipster','config/liquibase/changelog/20231023124333_added_entity_Bloom.xml','2023-10-23 09:45:28',5,'EXECUTED','9:b3d191df266fe33d7b96b24bcf63f8b1','loadData tableName=bloom','',NULL,'4.24.0','faker',NULL,'8065128307'),('20231023124334-1','jhipster','config/liquibase/changelog/20231023124334_added_entity_Curso.xml','2023-10-23 09:45:28',6,'EXECUTED','9:4e11efdc1186ec62e745bea41387c3ce','createTable tableName=curso','',NULL,'4.24.0',NULL,NULL,'8065128307'),('20231023124334-1-data','jhipster','config/liquibase/changelog/20231023124334_added_entity_Curso.xml','2023-10-23 09:45:28',7,'EXECUTED','9:ccfd62879eff02260b96ada20f5e2e33','loadData tableName=curso','',NULL,'4.24.0','faker',NULL,'8065128307'),('20231023124335-1','jhipster','config/liquibase/changelog/20231023124335_added_entity_Escola.xml','2023-10-23 09:45:28',8,'EXECUTED','9:a7c20a6d0b27cacaffae294d0e2499b0','createTable tableName=escola','',NULL,'4.24.0',NULL,NULL,'8065128307'),('20231023124335-1-data','jhipster','config/liquibase/changelog/20231023124335_added_entity_Escola.xml','2023-10-23 09:45:28',9,'EXECUTED','9:63669451addc8078e186ca8c20018730','loadData tableName=escola','',NULL,'4.24.0','faker',NULL,'8065128307'),('20231023124336-1','jhipster','config/liquibase/changelog/20231023124336_added_entity_Matriz.xml','2023-10-23 09:45:28',10,'EXECUTED','9:f50ef1b2c610a297fb509ad1ff149131','createTable tableName=matriz','',NULL,'4.24.0',NULL,NULL,'8065128307'),('20231023124336-1-data','jhipster','config/liquibase/changelog/20231023124336_added_entity_Matriz.xml','2023-10-23 09:45:28',11,'EXECUTED','9:3ea48d54565a40a3169c85c2b4ba66d8','loadData tableName=matriz','',NULL,'4.24.0','faker',NULL,'8065128307'),('20231023124337-1','jhipster','config/liquibase/changelog/20231023124337_added_entity_Modalidade.xml','2023-10-23 09:45:28',12,'EXECUTED','9:034040291240259dcbcda8908213afcf','createTable tableName=modalidade','',NULL,'4.24.0',NULL,NULL,'8065128307'),('20231023124337-1-data','jhipster','config/liquibase/changelog/20231023124337_added_entity_Modalidade.xml','2023-10-23 09:45:28',13,'EXECUTED','9:30edbba18fa7d069d37d9f3f3121c306','loadData tableName=modalidade','',NULL,'4.24.0','faker',NULL,'8065128307'),('20231023124338-1','jhipster','config/liquibase/changelog/20231023124338_added_entity_TipoDeCurso.xml','2023-10-23 09:45:28',14,'EXECUTED','9:a9e84ae28b2cf1100419994e71b660e6','createTable tableName=tipo_de_curso','',NULL,'4.24.0',NULL,NULL,'8065128307'),('20231023124338-1-data','jhipster','config/liquibase/changelog/20231023124338_added_entity_TipoDeCurso.xml','2023-10-23 09:45:28',15,'EXECUTED','9:dab6a6fce76a297cf252fdd64d44a32c','loadData tableName=tipo_de_curso','',NULL,'4.24.0','faker',NULL,'8065128307'),('20231023124339-1','jhipster','config/liquibase/changelog/20231023124339_added_entity_Unidade.xml','2023-10-23 09:45:28',16,'EXECUTED','9:65b957866cdd92f93c6898f781a13ce4','createTable tableName=unidade','',NULL,'4.24.0',NULL,NULL,'8065128307'),('20231023124339-1-data','jhipster','config/liquibase/changelog/20231023124339_added_entity_Unidade.xml','2023-10-23 09:45:28',17,'EXECUTED','9:073821735d739c7b4978964783709cfc','loadData tableName=unidade','',NULL,'4.24.0','faker',NULL,'8065128307'),('20231023124334-2','jhipster','config/liquibase/changelog/20231023124334_added_entity_constraints_Curso.xml','2023-10-23 09:45:28',18,'EXECUTED','9:b1d6a82bd982a0732bae8ea977b0b803','addForeignKeyConstraint baseTableName=curso, constraintName=fk_curso__escola_id, referencedTableName=escola; addForeignKeyConstraint baseTableName=curso, constraintName=fk_curso__tipo_id, referencedTableName=tipo_de_curso','',NULL,'4.24.0',NULL,NULL,'8065128307');
/*!40000 ALTER TABLE `databasechangelog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `databasechangeloglock`
--

DROP TABLE IF EXISTS `databasechangeloglock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `databasechangeloglock` (
  `ID` int NOT NULL,
  `LOCKED` tinyint(1) NOT NULL,
  `LOCKGRANTED` datetime DEFAULT NULL,
  `LOCKEDBY` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `databasechangeloglock`
--

LOCK TABLES `databasechangeloglock` WRITE;
/*!40000 ALTER TABLE `databasechangeloglock` DISABLE KEYS */;
INSERT INTO `databasechangeloglock` VALUES (1,0,NULL,NULL);
/*!40000 ALTER TABLE `databasechangeloglock` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `escola`
--

DROP TABLE IF EXISTS `escola`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `escola` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1500 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='@author Geucimar\\n@version 1.0\\n@created 19-out-2023 16:48:38';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `escola`
--

LOCK TABLES `escola` WRITE;
/*!40000 ALTER TABLE `escola` DISABLE KEYS */;
INSERT INTO `escola` VALUES (1,'down er'),(2,'ripen'),(3,'to willing nor'),(4,'campaigning via geez'),(5,'ferociously'),(6,'unaccountably'),(7,'vivid afore'),(8,'ah drat'),(9,'step-grandfather'),(10,'acclaimed regularly psst');
/*!40000 ALTER TABLE `escola` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jhi_authority`
--

DROP TABLE IF EXISTS `jhi_authority`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `jhi_authority` (
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jhi_authority`
--

LOCK TABLES `jhi_authority` WRITE;
/*!40000 ALTER TABLE `jhi_authority` DISABLE KEYS */;
INSERT INTO `jhi_authority` VALUES ('ROLE_ADMIN'),('ROLE_USER');
/*!40000 ALTER TABLE `jhi_authority` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jhi_user`
--

DROP TABLE IF EXISTS `jhi_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `jhi_user` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `login` varchar(50) NOT NULL,
  `password_hash` varchar(60) NOT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `email` varchar(191) DEFAULT NULL,
  `image_url` varchar(256) DEFAULT NULL,
  `activated` tinyint(1) NOT NULL,
  `lang_key` varchar(10) DEFAULT NULL,
  `activation_key` varchar(20) DEFAULT NULL,
  `reset_key` varchar(20) DEFAULT NULL,
  `created_by` varchar(50) NOT NULL,
  `created_date` timestamp NULL,
  `reset_date` timestamp NULL DEFAULT NULL,
  `last_modified_by` varchar(50) DEFAULT NULL,
  `last_modified_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_user_login` (`login`),
  UNIQUE KEY `ux_user_email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=1050 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jhi_user`
--

LOCK TABLES `jhi_user` WRITE;
/*!40000 ALTER TABLE `jhi_user` DISABLE KEYS */;
INSERT INTO `jhi_user` VALUES (1,'admin','$2a$10$gSAhZrxMllrbgj/kkK9UceBPpChGWJA7SYIb1Mqo.n5aNLq1/oRrC','Administrator','Administrator','admin@localhost','',1,'pt-br',NULL,NULL,'system',NULL,NULL,'system',NULL),(2,'user','$2a$10$VEjxo0jq2YG9Rbk2HmX9S.k1uZBGYUHdUcid3g/vfiEl7lwWgOH/K','User','User','user@localhost','',1,'pt-br',NULL,NULL,'system',NULL,NULL,'system',NULL);
/*!40000 ALTER TABLE `jhi_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jhi_user_authority`
--

DROP TABLE IF EXISTS `jhi_user_authority`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `jhi_user_authority` (
  `user_id` bigint NOT NULL,
  `authority_name` varchar(50) NOT NULL,
  PRIMARY KEY (`user_id`,`authority_name`),
  KEY `fk_authority_name` (`authority_name`),
  CONSTRAINT `fk_authority_name` FOREIGN KEY (`authority_name`) REFERENCES `jhi_authority` (`name`),
  CONSTRAINT `fk_user_id` FOREIGN KEY (`user_id`) REFERENCES `jhi_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jhi_user_authority`
--

LOCK TABLES `jhi_user_authority` WRITE;
/*!40000 ALTER TABLE `jhi_user_authority` DISABLE KEYS */;
INSERT INTO `jhi_user_authority` VALUES (1,'ROLE_ADMIN'),(1,'ROLE_USER'),(2,'ROLE_USER');
/*!40000 ALTER TABLE `jhi_user_authority` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `matriz`
--

DROP TABLE IF EXISTS `matriz`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `matriz` (
  `id` int NOT NULL AUTO_INCREMENT,
  `codigo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1500 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='@author Geucimar\\n@version 1.0\\n@created 19-out-2023 16:48:38';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `matriz`
--

LOCK TABLES `matriz` WRITE;
/*!40000 ALTER TABLE `matriz` DISABLE KEYS */;
INSERT INTO `matriz` VALUES (1,'fugato incidentally cultured'),(2,'between ha'),(3,'paint'),(4,'unto yesterday'),(5,'questionably wasteful'),(6,'defensive'),(7,'discuss furthermore robust'),(8,'inside because'),(9,'faint tomorrow adventurously'),(10,'to lather openly');
/*!40000 ALTER TABLE `matriz` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modalidade`
--

DROP TABLE IF EXISTS `modalidade`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `modalidade` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1500 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='@author Geucimar\\n@version 1.0\\n@created 19-out-2023 16:48:38';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modalidade`
--

LOCK TABLES `modalidade` WRITE;
/*!40000 ALTER TABLE `modalidade` DISABLE KEYS */;
INSERT INTO `modalidade` VALUES (1,'ah'),(2,'threaten accordion boldly'),(3,'ambiguity ick'),(4,'worth'),(5,'upon'),(6,'mink whisper morphology'),(7,'below'),(8,'guilt about'),(9,'phooey sadly'),(10,'delightfully pro');
/*!40000 ALTER TABLE `modalidade` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_de_curso`
--

DROP TABLE IF EXISTS `tipo_de_curso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tipo_de_curso` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1500 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='@author Geucimar\\n@version 1.0\\n@created 19-out-2023 16:48:39';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_de_curso`
--

LOCK TABLES `tipo_de_curso` WRITE;
/*!40000 ALTER TABLE `tipo_de_curso` DISABLE KEYS */;
INSERT INTO `tipo_de_curso` VALUES (1,'reconstruct'),(2,'burdensome income ugh'),(3,'in'),(4,'although for notwithstanding'),(5,'zowie concerning flume'),(6,'faithfully'),(7,'arrogantly fracture'),(8,'pfft'),(9,'greatly dig about'),(10,'neatly');
/*!40000 ALTER TABLE `tipo_de_curso` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `unidade`
--

DROP TABLE IF EXISTS `unidade`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `unidade` (
  `id` int NOT NULL AUTO_INCREMENT,
  `codigo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1500 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='@author Geucimar\\n@version 1.0\\n@created 19-out-2023 16:48:39';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `unidade`
--

LOCK TABLES `unidade` WRITE;
/*!40000 ALTER TABLE `unidade` DISABLE KEYS */;
INSERT INTO `unidade` VALUES (1,'indolent'),(2,'officially'),(3,'needily extra-small'),(4,'complete phooey or'),(5,'insubstantial spell'),(6,'bah mope'),(7,'insecure represent naturally'),(8,'interview aw if'),(9,'whoever'),(10,'knowledgeably ack joyous');
/*!40000 ALTER TABLE `unidade` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-10-23 10:51:33
