package br.edu.up.matrix.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import br.edu.up.matrix.IntegrationTest;
import br.edu.up.matrix.domain.Conhecimento;
import br.edu.up.matrix.repository.ConhecimentoRepository;
import jakarta.persistence.EntityManager;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link ConhecimentoResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class ConhecimentoResourceIT {

    private static final String DEFAULT_DESCRICAO = "AAAAAAAAAA";
    private static final String UPDATED_DESCRICAO = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/conhecimentos";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicInteger count = new AtomicInteger(random.nextInt() + (2 * Short.MAX_VALUE));

    @Autowired
    private ConhecimentoRepository conhecimentoRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restConhecimentoMockMvc;

    private Conhecimento conhecimento;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Conhecimento createEntity(EntityManager em) {
        Conhecimento conhecimento = new Conhecimento().descricao(DEFAULT_DESCRICAO);
        return conhecimento;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Conhecimento createUpdatedEntity(EntityManager em) {
        Conhecimento conhecimento = new Conhecimento().descricao(UPDATED_DESCRICAO);
        return conhecimento;
    }

    @BeforeEach
    public void initTest() {
        conhecimento = createEntity(em);
    }

    @Test
    @Transactional
    void createConhecimento() throws Exception {
        int databaseSizeBeforeCreate = conhecimentoRepository.findAll().size();
        // Create the Conhecimento
        restConhecimentoMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(conhecimento)))
            .andExpect(status().isCreated());

        // Validate the Conhecimento in the database
        List<Conhecimento> conhecimentoList = conhecimentoRepository.findAll();
        assertThat(conhecimentoList).hasSize(databaseSizeBeforeCreate + 1);
        Conhecimento testConhecimento = conhecimentoList.get(conhecimentoList.size() - 1);
        assertThat(testConhecimento.getDescricao()).isEqualTo(DEFAULT_DESCRICAO);
    }

    @Test
    @Transactional
    void createConhecimentoWithExistingId() throws Exception {
        // Create the Conhecimento with an existing ID
        conhecimento.setId(1);

        int databaseSizeBeforeCreate = conhecimentoRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restConhecimentoMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(conhecimento)))
            .andExpect(status().isBadRequest());

        // Validate the Conhecimento in the database
        List<Conhecimento> conhecimentoList = conhecimentoRepository.findAll();
        assertThat(conhecimentoList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllConhecimentos() throws Exception {
        // Initialize the database
        conhecimentoRepository.saveAndFlush(conhecimento);

        // Get all the conhecimentoList
        restConhecimentoMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(conhecimento.getId().intValue())))
            .andExpect(jsonPath("$.[*].descricao").value(hasItem(DEFAULT_DESCRICAO)));
    }

    @Test
    @Transactional
    void getConhecimento() throws Exception {
        // Initialize the database
        conhecimentoRepository.saveAndFlush(conhecimento);

        // Get the conhecimento
        restConhecimentoMockMvc
            .perform(get(ENTITY_API_URL_ID, conhecimento.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(conhecimento.getId().intValue()))
            .andExpect(jsonPath("$.descricao").value(DEFAULT_DESCRICAO));
    }

    @Test
    @Transactional
    void getNonExistingConhecimento() throws Exception {
        // Get the conhecimento
        restConhecimentoMockMvc.perform(get(ENTITY_API_URL_ID, Integer.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingConhecimento() throws Exception {
        // Initialize the database
        conhecimentoRepository.saveAndFlush(conhecimento);

        int databaseSizeBeforeUpdate = conhecimentoRepository.findAll().size();

        // Update the conhecimento
        Conhecimento updatedConhecimento = conhecimentoRepository.findById(conhecimento.getId()).orElseThrow();
        // Disconnect from session so that the updates on updatedConhecimento are not directly saved in db
        em.detach(updatedConhecimento);
        updatedConhecimento.descricao(UPDATED_DESCRICAO);

        restConhecimentoMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedConhecimento.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedConhecimento))
            )
            .andExpect(status().isOk());

        // Validate the Conhecimento in the database
        List<Conhecimento> conhecimentoList = conhecimentoRepository.findAll();
        assertThat(conhecimentoList).hasSize(databaseSizeBeforeUpdate);
        Conhecimento testConhecimento = conhecimentoList.get(conhecimentoList.size() - 1);
        assertThat(testConhecimento.getDescricao()).isEqualTo(UPDATED_DESCRICAO);
    }

    @Test
    @Transactional
    void putNonExistingConhecimento() throws Exception {
        int databaseSizeBeforeUpdate = conhecimentoRepository.findAll().size();
        conhecimento.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restConhecimentoMockMvc
            .perform(
                put(ENTITY_API_URL_ID, conhecimento.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(conhecimento))
            )
            .andExpect(status().isBadRequest());

        // Validate the Conhecimento in the database
        List<Conhecimento> conhecimentoList = conhecimentoRepository.findAll();
        assertThat(conhecimentoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchConhecimento() throws Exception {
        int databaseSizeBeforeUpdate = conhecimentoRepository.findAll().size();
        conhecimento.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restConhecimentoMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(conhecimento))
            )
            .andExpect(status().isBadRequest());

        // Validate the Conhecimento in the database
        List<Conhecimento> conhecimentoList = conhecimentoRepository.findAll();
        assertThat(conhecimentoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamConhecimento() throws Exception {
        int databaseSizeBeforeUpdate = conhecimentoRepository.findAll().size();
        conhecimento.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restConhecimentoMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(conhecimento)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Conhecimento in the database
        List<Conhecimento> conhecimentoList = conhecimentoRepository.findAll();
        assertThat(conhecimentoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateConhecimentoWithPatch() throws Exception {
        // Initialize the database
        conhecimentoRepository.saveAndFlush(conhecimento);

        int databaseSizeBeforeUpdate = conhecimentoRepository.findAll().size();

        // Update the conhecimento using partial update
        Conhecimento partialUpdatedConhecimento = new Conhecimento();
        partialUpdatedConhecimento.setId(conhecimento.getId());

        restConhecimentoMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedConhecimento.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedConhecimento))
            )
            .andExpect(status().isOk());

        // Validate the Conhecimento in the database
        List<Conhecimento> conhecimentoList = conhecimentoRepository.findAll();
        assertThat(conhecimentoList).hasSize(databaseSizeBeforeUpdate);
        Conhecimento testConhecimento = conhecimentoList.get(conhecimentoList.size() - 1);
        assertThat(testConhecimento.getDescricao()).isEqualTo(DEFAULT_DESCRICAO);
    }

    @Test
    @Transactional
    void fullUpdateConhecimentoWithPatch() throws Exception {
        // Initialize the database
        conhecimentoRepository.saveAndFlush(conhecimento);

        int databaseSizeBeforeUpdate = conhecimentoRepository.findAll().size();

        // Update the conhecimento using partial update
        Conhecimento partialUpdatedConhecimento = new Conhecimento();
        partialUpdatedConhecimento.setId(conhecimento.getId());

        partialUpdatedConhecimento.descricao(UPDATED_DESCRICAO);

        restConhecimentoMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedConhecimento.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedConhecimento))
            )
            .andExpect(status().isOk());

        // Validate the Conhecimento in the database
        List<Conhecimento> conhecimentoList = conhecimentoRepository.findAll();
        assertThat(conhecimentoList).hasSize(databaseSizeBeforeUpdate);
        Conhecimento testConhecimento = conhecimentoList.get(conhecimentoList.size() - 1);
        assertThat(testConhecimento.getDescricao()).isEqualTo(UPDATED_DESCRICAO);
    }

    @Test
    @Transactional
    void patchNonExistingConhecimento() throws Exception {
        int databaseSizeBeforeUpdate = conhecimentoRepository.findAll().size();
        conhecimento.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restConhecimentoMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, conhecimento.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(conhecimento))
            )
            .andExpect(status().isBadRequest());

        // Validate the Conhecimento in the database
        List<Conhecimento> conhecimentoList = conhecimentoRepository.findAll();
        assertThat(conhecimentoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchConhecimento() throws Exception {
        int databaseSizeBeforeUpdate = conhecimentoRepository.findAll().size();
        conhecimento.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restConhecimentoMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(conhecimento))
            )
            .andExpect(status().isBadRequest());

        // Validate the Conhecimento in the database
        List<Conhecimento> conhecimentoList = conhecimentoRepository.findAll();
        assertThat(conhecimentoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamConhecimento() throws Exception {
        int databaseSizeBeforeUpdate = conhecimentoRepository.findAll().size();
        conhecimento.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restConhecimentoMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(conhecimento))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Conhecimento in the database
        List<Conhecimento> conhecimentoList = conhecimentoRepository.findAll();
        assertThat(conhecimentoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteConhecimento() throws Exception {
        // Initialize the database
        conhecimentoRepository.saveAndFlush(conhecimento);

        int databaseSizeBeforeDelete = conhecimentoRepository.findAll().size();

        // Delete the conhecimento
        restConhecimentoMockMvc
            .perform(delete(ENTITY_API_URL_ID, conhecimento.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Conhecimento> conhecimentoList = conhecimentoRepository.findAll();
        assertThat(conhecimentoList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
