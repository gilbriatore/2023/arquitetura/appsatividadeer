package br.edu.up.matrix.domain;

import static org.assertj.core.api.Assertions.assertThat;

import br.edu.up.matrix.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class DCNTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DCN.class);
        DCN dCN1 = new DCN();
        dCN1.setId(1);
        DCN dCN2 = new DCN();
        dCN2.setId(dCN1.getId());
        assertThat(dCN1).isEqualTo(dCN2);
        dCN2.setId(2);
        assertThat(dCN1).isNotEqualTo(dCN2);
        dCN1.setId(null);
        assertThat(dCN1).isNotEqualTo(dCN2);
    }
}
