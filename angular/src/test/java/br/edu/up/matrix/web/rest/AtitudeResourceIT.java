package br.edu.up.matrix.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import br.edu.up.matrix.IntegrationTest;
import br.edu.up.matrix.domain.Atitude;
import br.edu.up.matrix.repository.AtitudeRepository;
import jakarta.persistence.EntityManager;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link AtitudeResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class AtitudeResourceIT {

    private static final String DEFAULT_DESCRICAO = "AAAAAAAAAA";
    private static final String UPDATED_DESCRICAO = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/atitudes";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicInteger count = new AtomicInteger(random.nextInt() + (2 * Short.MAX_VALUE));

    @Autowired
    private AtitudeRepository atitudeRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restAtitudeMockMvc;

    private Atitude atitude;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Atitude createEntity(EntityManager em) {
        Atitude atitude = new Atitude().descricao(DEFAULT_DESCRICAO);
        return atitude;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Atitude createUpdatedEntity(EntityManager em) {
        Atitude atitude = new Atitude().descricao(UPDATED_DESCRICAO);
        return atitude;
    }

    @BeforeEach
    public void initTest() {
        atitude = createEntity(em);
    }

    @Test
    @Transactional
    void createAtitude() throws Exception {
        int databaseSizeBeforeCreate = atitudeRepository.findAll().size();
        // Create the Atitude
        restAtitudeMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(atitude)))
            .andExpect(status().isCreated());

        // Validate the Atitude in the database
        List<Atitude> atitudeList = atitudeRepository.findAll();
        assertThat(atitudeList).hasSize(databaseSizeBeforeCreate + 1);
        Atitude testAtitude = atitudeList.get(atitudeList.size() - 1);
        assertThat(testAtitude.getDescricao()).isEqualTo(DEFAULT_DESCRICAO);
    }

    @Test
    @Transactional
    void createAtitudeWithExistingId() throws Exception {
        // Create the Atitude with an existing ID
        atitude.setId(1);

        int databaseSizeBeforeCreate = atitudeRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restAtitudeMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(atitude)))
            .andExpect(status().isBadRequest());

        // Validate the Atitude in the database
        List<Atitude> atitudeList = atitudeRepository.findAll();
        assertThat(atitudeList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllAtitudes() throws Exception {
        // Initialize the database
        atitudeRepository.saveAndFlush(atitude);

        // Get all the atitudeList
        restAtitudeMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(atitude.getId().intValue())))
            .andExpect(jsonPath("$.[*].descricao").value(hasItem(DEFAULT_DESCRICAO)));
    }

    @Test
    @Transactional
    void getAtitude() throws Exception {
        // Initialize the database
        atitudeRepository.saveAndFlush(atitude);

        // Get the atitude
        restAtitudeMockMvc
            .perform(get(ENTITY_API_URL_ID, atitude.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(atitude.getId().intValue()))
            .andExpect(jsonPath("$.descricao").value(DEFAULT_DESCRICAO));
    }

    @Test
    @Transactional
    void getNonExistingAtitude() throws Exception {
        // Get the atitude
        restAtitudeMockMvc.perform(get(ENTITY_API_URL_ID, Integer.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingAtitude() throws Exception {
        // Initialize the database
        atitudeRepository.saveAndFlush(atitude);

        int databaseSizeBeforeUpdate = atitudeRepository.findAll().size();

        // Update the atitude
        Atitude updatedAtitude = atitudeRepository.findById(atitude.getId()).orElseThrow();
        // Disconnect from session so that the updates on updatedAtitude are not directly saved in db
        em.detach(updatedAtitude);
        updatedAtitude.descricao(UPDATED_DESCRICAO);

        restAtitudeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedAtitude.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedAtitude))
            )
            .andExpect(status().isOk());

        // Validate the Atitude in the database
        List<Atitude> atitudeList = atitudeRepository.findAll();
        assertThat(atitudeList).hasSize(databaseSizeBeforeUpdate);
        Atitude testAtitude = atitudeList.get(atitudeList.size() - 1);
        assertThat(testAtitude.getDescricao()).isEqualTo(UPDATED_DESCRICAO);
    }

    @Test
    @Transactional
    void putNonExistingAtitude() throws Exception {
        int databaseSizeBeforeUpdate = atitudeRepository.findAll().size();
        atitude.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAtitudeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, atitude.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(atitude))
            )
            .andExpect(status().isBadRequest());

        // Validate the Atitude in the database
        List<Atitude> atitudeList = atitudeRepository.findAll();
        assertThat(atitudeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchAtitude() throws Exception {
        int databaseSizeBeforeUpdate = atitudeRepository.findAll().size();
        atitude.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restAtitudeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(atitude))
            )
            .andExpect(status().isBadRequest());

        // Validate the Atitude in the database
        List<Atitude> atitudeList = atitudeRepository.findAll();
        assertThat(atitudeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamAtitude() throws Exception {
        int databaseSizeBeforeUpdate = atitudeRepository.findAll().size();
        atitude.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restAtitudeMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(atitude)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Atitude in the database
        List<Atitude> atitudeList = atitudeRepository.findAll();
        assertThat(atitudeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateAtitudeWithPatch() throws Exception {
        // Initialize the database
        atitudeRepository.saveAndFlush(atitude);

        int databaseSizeBeforeUpdate = atitudeRepository.findAll().size();

        // Update the atitude using partial update
        Atitude partialUpdatedAtitude = new Atitude();
        partialUpdatedAtitude.setId(atitude.getId());

        restAtitudeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedAtitude.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedAtitude))
            )
            .andExpect(status().isOk());

        // Validate the Atitude in the database
        List<Atitude> atitudeList = atitudeRepository.findAll();
        assertThat(atitudeList).hasSize(databaseSizeBeforeUpdate);
        Atitude testAtitude = atitudeList.get(atitudeList.size() - 1);
        assertThat(testAtitude.getDescricao()).isEqualTo(DEFAULT_DESCRICAO);
    }

    @Test
    @Transactional
    void fullUpdateAtitudeWithPatch() throws Exception {
        // Initialize the database
        atitudeRepository.saveAndFlush(atitude);

        int databaseSizeBeforeUpdate = atitudeRepository.findAll().size();

        // Update the atitude using partial update
        Atitude partialUpdatedAtitude = new Atitude();
        partialUpdatedAtitude.setId(atitude.getId());

        partialUpdatedAtitude.descricao(UPDATED_DESCRICAO);

        restAtitudeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedAtitude.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedAtitude))
            )
            .andExpect(status().isOk());

        // Validate the Atitude in the database
        List<Atitude> atitudeList = atitudeRepository.findAll();
        assertThat(atitudeList).hasSize(databaseSizeBeforeUpdate);
        Atitude testAtitude = atitudeList.get(atitudeList.size() - 1);
        assertThat(testAtitude.getDescricao()).isEqualTo(UPDATED_DESCRICAO);
    }

    @Test
    @Transactional
    void patchNonExistingAtitude() throws Exception {
        int databaseSizeBeforeUpdate = atitudeRepository.findAll().size();
        atitude.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAtitudeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, atitude.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(atitude))
            )
            .andExpect(status().isBadRequest());

        // Validate the Atitude in the database
        List<Atitude> atitudeList = atitudeRepository.findAll();
        assertThat(atitudeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchAtitude() throws Exception {
        int databaseSizeBeforeUpdate = atitudeRepository.findAll().size();
        atitude.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restAtitudeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(atitude))
            )
            .andExpect(status().isBadRequest());

        // Validate the Atitude in the database
        List<Atitude> atitudeList = atitudeRepository.findAll();
        assertThat(atitudeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamAtitude() throws Exception {
        int databaseSizeBeforeUpdate = atitudeRepository.findAll().size();
        atitude.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restAtitudeMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(atitude)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Atitude in the database
        List<Atitude> atitudeList = atitudeRepository.findAll();
        assertThat(atitudeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteAtitude() throws Exception {
        // Initialize the database
        atitudeRepository.saveAndFlush(atitude);

        int databaseSizeBeforeDelete = atitudeRepository.findAll().size();

        // Delete the atitude
        restAtitudeMockMvc
            .perform(delete(ENTITY_API_URL_ID, atitude.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Atitude> atitudeList = atitudeRepository.findAll();
        assertThat(atitudeList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
