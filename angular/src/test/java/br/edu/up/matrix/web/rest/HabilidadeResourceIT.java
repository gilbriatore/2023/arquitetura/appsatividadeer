package br.edu.up.matrix.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import br.edu.up.matrix.IntegrationTest;
import br.edu.up.matrix.domain.Habilidade;
import br.edu.up.matrix.repository.HabilidadeRepository;
import jakarta.persistence.EntityManager;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link HabilidadeResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class HabilidadeResourceIT {

    private static final String DEFAULT_DESCRICAO = "AAAAAAAAAA";
    private static final String UPDATED_DESCRICAO = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/habilidades";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicInteger count = new AtomicInteger(random.nextInt() + (2 * Short.MAX_VALUE));

    @Autowired
    private HabilidadeRepository habilidadeRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restHabilidadeMockMvc;

    private Habilidade habilidade;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Habilidade createEntity(EntityManager em) {
        Habilidade habilidade = new Habilidade().descricao(DEFAULT_DESCRICAO);
        return habilidade;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Habilidade createUpdatedEntity(EntityManager em) {
        Habilidade habilidade = new Habilidade().descricao(UPDATED_DESCRICAO);
        return habilidade;
    }

    @BeforeEach
    public void initTest() {
        habilidade = createEntity(em);
    }

    @Test
    @Transactional
    void createHabilidade() throws Exception {
        int databaseSizeBeforeCreate = habilidadeRepository.findAll().size();
        // Create the Habilidade
        restHabilidadeMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(habilidade)))
            .andExpect(status().isCreated());

        // Validate the Habilidade in the database
        List<Habilidade> habilidadeList = habilidadeRepository.findAll();
        assertThat(habilidadeList).hasSize(databaseSizeBeforeCreate + 1);
        Habilidade testHabilidade = habilidadeList.get(habilidadeList.size() - 1);
        assertThat(testHabilidade.getDescricao()).isEqualTo(DEFAULT_DESCRICAO);
    }

    @Test
    @Transactional
    void createHabilidadeWithExistingId() throws Exception {
        // Create the Habilidade with an existing ID
        habilidade.setId(1);

        int databaseSizeBeforeCreate = habilidadeRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restHabilidadeMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(habilidade)))
            .andExpect(status().isBadRequest());

        // Validate the Habilidade in the database
        List<Habilidade> habilidadeList = habilidadeRepository.findAll();
        assertThat(habilidadeList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllHabilidades() throws Exception {
        // Initialize the database
        habilidadeRepository.saveAndFlush(habilidade);

        // Get all the habilidadeList
        restHabilidadeMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(habilidade.getId().intValue())))
            .andExpect(jsonPath("$.[*].descricao").value(hasItem(DEFAULT_DESCRICAO)));
    }

    @Test
    @Transactional
    void getHabilidade() throws Exception {
        // Initialize the database
        habilidadeRepository.saveAndFlush(habilidade);

        // Get the habilidade
        restHabilidadeMockMvc
            .perform(get(ENTITY_API_URL_ID, habilidade.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(habilidade.getId().intValue()))
            .andExpect(jsonPath("$.descricao").value(DEFAULT_DESCRICAO));
    }

    @Test
    @Transactional
    void getNonExistingHabilidade() throws Exception {
        // Get the habilidade
        restHabilidadeMockMvc.perform(get(ENTITY_API_URL_ID, Integer.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingHabilidade() throws Exception {
        // Initialize the database
        habilidadeRepository.saveAndFlush(habilidade);

        int databaseSizeBeforeUpdate = habilidadeRepository.findAll().size();

        // Update the habilidade
        Habilidade updatedHabilidade = habilidadeRepository.findById(habilidade.getId()).orElseThrow();
        // Disconnect from session so that the updates on updatedHabilidade are not directly saved in db
        em.detach(updatedHabilidade);
        updatedHabilidade.descricao(UPDATED_DESCRICAO);

        restHabilidadeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedHabilidade.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedHabilidade))
            )
            .andExpect(status().isOk());

        // Validate the Habilidade in the database
        List<Habilidade> habilidadeList = habilidadeRepository.findAll();
        assertThat(habilidadeList).hasSize(databaseSizeBeforeUpdate);
        Habilidade testHabilidade = habilidadeList.get(habilidadeList.size() - 1);
        assertThat(testHabilidade.getDescricao()).isEqualTo(UPDATED_DESCRICAO);
    }

    @Test
    @Transactional
    void putNonExistingHabilidade() throws Exception {
        int databaseSizeBeforeUpdate = habilidadeRepository.findAll().size();
        habilidade.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restHabilidadeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, habilidade.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(habilidade))
            )
            .andExpect(status().isBadRequest());

        // Validate the Habilidade in the database
        List<Habilidade> habilidadeList = habilidadeRepository.findAll();
        assertThat(habilidadeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchHabilidade() throws Exception {
        int databaseSizeBeforeUpdate = habilidadeRepository.findAll().size();
        habilidade.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restHabilidadeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(habilidade))
            )
            .andExpect(status().isBadRequest());

        // Validate the Habilidade in the database
        List<Habilidade> habilidadeList = habilidadeRepository.findAll();
        assertThat(habilidadeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamHabilidade() throws Exception {
        int databaseSizeBeforeUpdate = habilidadeRepository.findAll().size();
        habilidade.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restHabilidadeMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(habilidade)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Habilidade in the database
        List<Habilidade> habilidadeList = habilidadeRepository.findAll();
        assertThat(habilidadeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateHabilidadeWithPatch() throws Exception {
        // Initialize the database
        habilidadeRepository.saveAndFlush(habilidade);

        int databaseSizeBeforeUpdate = habilidadeRepository.findAll().size();

        // Update the habilidade using partial update
        Habilidade partialUpdatedHabilidade = new Habilidade();
        partialUpdatedHabilidade.setId(habilidade.getId());

        restHabilidadeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedHabilidade.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedHabilidade))
            )
            .andExpect(status().isOk());

        // Validate the Habilidade in the database
        List<Habilidade> habilidadeList = habilidadeRepository.findAll();
        assertThat(habilidadeList).hasSize(databaseSizeBeforeUpdate);
        Habilidade testHabilidade = habilidadeList.get(habilidadeList.size() - 1);
        assertThat(testHabilidade.getDescricao()).isEqualTo(DEFAULT_DESCRICAO);
    }

    @Test
    @Transactional
    void fullUpdateHabilidadeWithPatch() throws Exception {
        // Initialize the database
        habilidadeRepository.saveAndFlush(habilidade);

        int databaseSizeBeforeUpdate = habilidadeRepository.findAll().size();

        // Update the habilidade using partial update
        Habilidade partialUpdatedHabilidade = new Habilidade();
        partialUpdatedHabilidade.setId(habilidade.getId());

        partialUpdatedHabilidade.descricao(UPDATED_DESCRICAO);

        restHabilidadeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedHabilidade.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedHabilidade))
            )
            .andExpect(status().isOk());

        // Validate the Habilidade in the database
        List<Habilidade> habilidadeList = habilidadeRepository.findAll();
        assertThat(habilidadeList).hasSize(databaseSizeBeforeUpdate);
        Habilidade testHabilidade = habilidadeList.get(habilidadeList.size() - 1);
        assertThat(testHabilidade.getDescricao()).isEqualTo(UPDATED_DESCRICAO);
    }

    @Test
    @Transactional
    void patchNonExistingHabilidade() throws Exception {
        int databaseSizeBeforeUpdate = habilidadeRepository.findAll().size();
        habilidade.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restHabilidadeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, habilidade.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(habilidade))
            )
            .andExpect(status().isBadRequest());

        // Validate the Habilidade in the database
        List<Habilidade> habilidadeList = habilidadeRepository.findAll();
        assertThat(habilidadeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchHabilidade() throws Exception {
        int databaseSizeBeforeUpdate = habilidadeRepository.findAll().size();
        habilidade.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restHabilidadeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(habilidade))
            )
            .andExpect(status().isBadRequest());

        // Validate the Habilidade in the database
        List<Habilidade> habilidadeList = habilidadeRepository.findAll();
        assertThat(habilidadeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamHabilidade() throws Exception {
        int databaseSizeBeforeUpdate = habilidadeRepository.findAll().size();
        habilidade.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restHabilidadeMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(habilidade))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Habilidade in the database
        List<Habilidade> habilidadeList = habilidadeRepository.findAll();
        assertThat(habilidadeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteHabilidade() throws Exception {
        // Initialize the database
        habilidadeRepository.saveAndFlush(habilidade);

        int databaseSizeBeforeDelete = habilidadeRepository.findAll().size();

        // Delete the habilidade
        restHabilidadeMockMvc
            .perform(delete(ENTITY_API_URL_ID, habilidade.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Habilidade> habilidadeList = habilidadeRepository.findAll();
        assertThat(habilidadeList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
