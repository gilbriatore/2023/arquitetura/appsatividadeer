package br.edu.up.matrix.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import br.edu.up.matrix.IntegrationTest;
import br.edu.up.matrix.domain.Papel;
import br.edu.up.matrix.repository.PapelRepository;
import jakarta.persistence.EntityManager;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link PapelResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class PapelResourceIT {

    private static final String DEFAULT_DESCRICAO = "AAAAAAAAAA";
    private static final String UPDATED_DESCRICAO = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/papels";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicInteger count = new AtomicInteger(random.nextInt() + (2 * Short.MAX_VALUE));

    @Autowired
    private PapelRepository papelRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restPapelMockMvc;

    private Papel papel;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Papel createEntity(EntityManager em) {
        Papel papel = new Papel().descricao(DEFAULT_DESCRICAO);
        return papel;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Papel createUpdatedEntity(EntityManager em) {
        Papel papel = new Papel().descricao(UPDATED_DESCRICAO);
        return papel;
    }

    @BeforeEach
    public void initTest() {
        papel = createEntity(em);
    }

    @Test
    @Transactional
    void createPapel() throws Exception {
        int databaseSizeBeforeCreate = papelRepository.findAll().size();
        // Create the Papel
        restPapelMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(papel)))
            .andExpect(status().isCreated());

        // Validate the Papel in the database
        List<Papel> papelList = papelRepository.findAll();
        assertThat(papelList).hasSize(databaseSizeBeforeCreate + 1);
        Papel testPapel = papelList.get(papelList.size() - 1);
        assertThat(testPapel.getDescricao()).isEqualTo(DEFAULT_DESCRICAO);
    }

    @Test
    @Transactional
    void createPapelWithExistingId() throws Exception {
        // Create the Papel with an existing ID
        papel.setId(1);

        int databaseSizeBeforeCreate = papelRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restPapelMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(papel)))
            .andExpect(status().isBadRequest());

        // Validate the Papel in the database
        List<Papel> papelList = papelRepository.findAll();
        assertThat(papelList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllPapels() throws Exception {
        // Initialize the database
        papelRepository.saveAndFlush(papel);

        // Get all the papelList
        restPapelMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(papel.getId().intValue())))
            .andExpect(jsonPath("$.[*].descricao").value(hasItem(DEFAULT_DESCRICAO)));
    }

    @Test
    @Transactional
    void getPapel() throws Exception {
        // Initialize the database
        papelRepository.saveAndFlush(papel);

        // Get the papel
        restPapelMockMvc
            .perform(get(ENTITY_API_URL_ID, papel.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(papel.getId().intValue()))
            .andExpect(jsonPath("$.descricao").value(DEFAULT_DESCRICAO));
    }

    @Test
    @Transactional
    void getNonExistingPapel() throws Exception {
        // Get the papel
        restPapelMockMvc.perform(get(ENTITY_API_URL_ID, Integer.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingPapel() throws Exception {
        // Initialize the database
        papelRepository.saveAndFlush(papel);

        int databaseSizeBeforeUpdate = papelRepository.findAll().size();

        // Update the papel
        Papel updatedPapel = papelRepository.findById(papel.getId()).orElseThrow();
        // Disconnect from session so that the updates on updatedPapel are not directly saved in db
        em.detach(updatedPapel);
        updatedPapel.descricao(UPDATED_DESCRICAO);

        restPapelMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedPapel.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedPapel))
            )
            .andExpect(status().isOk());

        // Validate the Papel in the database
        List<Papel> papelList = papelRepository.findAll();
        assertThat(papelList).hasSize(databaseSizeBeforeUpdate);
        Papel testPapel = papelList.get(papelList.size() - 1);
        assertThat(testPapel.getDescricao()).isEqualTo(UPDATED_DESCRICAO);
    }

    @Test
    @Transactional
    void putNonExistingPapel() throws Exception {
        int databaseSizeBeforeUpdate = papelRepository.findAll().size();
        papel.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPapelMockMvc
            .perform(
                put(ENTITY_API_URL_ID, papel.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(papel))
            )
            .andExpect(status().isBadRequest());

        // Validate the Papel in the database
        List<Papel> papelList = papelRepository.findAll();
        assertThat(papelList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchPapel() throws Exception {
        int databaseSizeBeforeUpdate = papelRepository.findAll().size();
        papel.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPapelMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(papel))
            )
            .andExpect(status().isBadRequest());

        // Validate the Papel in the database
        List<Papel> papelList = papelRepository.findAll();
        assertThat(papelList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamPapel() throws Exception {
        int databaseSizeBeforeUpdate = papelRepository.findAll().size();
        papel.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPapelMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(papel)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Papel in the database
        List<Papel> papelList = papelRepository.findAll();
        assertThat(papelList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdatePapelWithPatch() throws Exception {
        // Initialize the database
        papelRepository.saveAndFlush(papel);

        int databaseSizeBeforeUpdate = papelRepository.findAll().size();

        // Update the papel using partial update
        Papel partialUpdatedPapel = new Papel();
        partialUpdatedPapel.setId(papel.getId());

        partialUpdatedPapel.descricao(UPDATED_DESCRICAO);

        restPapelMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedPapel.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedPapel))
            )
            .andExpect(status().isOk());

        // Validate the Papel in the database
        List<Papel> papelList = papelRepository.findAll();
        assertThat(papelList).hasSize(databaseSizeBeforeUpdate);
        Papel testPapel = papelList.get(papelList.size() - 1);
        assertThat(testPapel.getDescricao()).isEqualTo(UPDATED_DESCRICAO);
    }

    @Test
    @Transactional
    void fullUpdatePapelWithPatch() throws Exception {
        // Initialize the database
        papelRepository.saveAndFlush(papel);

        int databaseSizeBeforeUpdate = papelRepository.findAll().size();

        // Update the papel using partial update
        Papel partialUpdatedPapel = new Papel();
        partialUpdatedPapel.setId(papel.getId());

        partialUpdatedPapel.descricao(UPDATED_DESCRICAO);

        restPapelMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedPapel.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedPapel))
            )
            .andExpect(status().isOk());

        // Validate the Papel in the database
        List<Papel> papelList = papelRepository.findAll();
        assertThat(papelList).hasSize(databaseSizeBeforeUpdate);
        Papel testPapel = papelList.get(papelList.size() - 1);
        assertThat(testPapel.getDescricao()).isEqualTo(UPDATED_DESCRICAO);
    }

    @Test
    @Transactional
    void patchNonExistingPapel() throws Exception {
        int databaseSizeBeforeUpdate = papelRepository.findAll().size();
        papel.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPapelMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, papel.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(papel))
            )
            .andExpect(status().isBadRequest());

        // Validate the Papel in the database
        List<Papel> papelList = papelRepository.findAll();
        assertThat(papelList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchPapel() throws Exception {
        int databaseSizeBeforeUpdate = papelRepository.findAll().size();
        papel.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPapelMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(papel))
            )
            .andExpect(status().isBadRequest());

        // Validate the Papel in the database
        List<Papel> papelList = papelRepository.findAll();
        assertThat(papelList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamPapel() throws Exception {
        int databaseSizeBeforeUpdate = papelRepository.findAll().size();
        papel.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPapelMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(papel)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Papel in the database
        List<Papel> papelList = papelRepository.findAll();
        assertThat(papelList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deletePapel() throws Exception {
        // Initialize the database
        papelRepository.saveAndFlush(papel);

        int databaseSizeBeforeDelete = papelRepository.findAll().size();

        // Delete the papel
        restPapelMockMvc
            .perform(delete(ENTITY_API_URL_ID, papel.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Papel> papelList = papelRepository.findAll();
        assertThat(papelList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
