package br.edu.up.matrix.domain;

import static org.assertj.core.api.Assertions.assertThat;

import br.edu.up.matrix.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class AtitudeTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Atitude.class);
        Atitude atitude1 = new Atitude();
        atitude1.setId(1);
        Atitude atitude2 = new Atitude();
        atitude2.setId(atitude1.getId());
        assertThat(atitude1).isEqualTo(atitude2);
        atitude2.setId(2);
        assertThat(atitude1).isNotEqualTo(atitude2);
        atitude1.setId(null);
        assertThat(atitude1).isNotEqualTo(atitude2);
    }
}
