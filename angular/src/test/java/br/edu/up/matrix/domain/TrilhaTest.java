package br.edu.up.matrix.domain;

import static org.assertj.core.api.Assertions.assertThat;

import br.edu.up.matrix.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class TrilhaTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Trilha.class);
        Trilha trilha1 = new Trilha();
        trilha1.setId(1);
        Trilha trilha2 = new Trilha();
        trilha2.setId(trilha1.getId());
        assertThat(trilha1).isEqualTo(trilha2);
        trilha2.setId(2);
        assertThat(trilha1).isNotEqualTo(trilha2);
        trilha1.setId(null);
        assertThat(trilha1).isNotEqualTo(trilha2);
    }
}
