package br.edu.up.matrix.domain;

import static org.assertj.core.api.Assertions.assertThat;

import br.edu.up.matrix.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class ConteudoTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Conteudo.class);
        Conteudo conteudo1 = new Conteudo();
        conteudo1.setId(1);
        Conteudo conteudo2 = new Conteudo();
        conteudo2.setId(conteudo1.getId());
        assertThat(conteudo1).isEqualTo(conteudo2);
        conteudo2.setId(2);
        assertThat(conteudo1).isNotEqualTo(conteudo2);
        conteudo1.setId(null);
        assertThat(conteudo1).isNotEqualTo(conteudo2);
    }
}
