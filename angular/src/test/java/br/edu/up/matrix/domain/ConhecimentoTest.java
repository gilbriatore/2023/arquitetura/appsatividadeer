package br.edu.up.matrix.domain;

import static org.assertj.core.api.Assertions.assertThat;

import br.edu.up.matrix.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class ConhecimentoTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Conhecimento.class);
        Conhecimento conhecimento1 = new Conhecimento();
        conhecimento1.setId(1);
        Conhecimento conhecimento2 = new Conhecimento();
        conhecimento2.setId(conhecimento1.getId());
        assertThat(conhecimento1).isEqualTo(conhecimento2);
        conhecimento2.setId(2);
        assertThat(conhecimento1).isNotEqualTo(conhecimento2);
        conhecimento1.setId(null);
        assertThat(conhecimento1).isNotEqualTo(conhecimento2);
    }
}
