package br.edu.up.matrix.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import br.edu.up.matrix.IntegrationTest;
import br.edu.up.matrix.domain.DCN;
import br.edu.up.matrix.repository.DCNRepository;
import jakarta.persistence.EntityManager;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link DCNResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class DCNResourceIT {

    private static final String DEFAULT_DESCRICAO = "AAAAAAAAAA";
    private static final String UPDATED_DESCRICAO = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/dcns";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicInteger count = new AtomicInteger(random.nextInt() + (2 * Short.MAX_VALUE));

    @Autowired
    private DCNRepository dCNRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restDCNMockMvc;

    private DCN dCN;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DCN createEntity(EntityManager em) {
        DCN dCN = new DCN().descricao(DEFAULT_DESCRICAO);
        return dCN;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DCN createUpdatedEntity(EntityManager em) {
        DCN dCN = new DCN().descricao(UPDATED_DESCRICAO);
        return dCN;
    }

    @BeforeEach
    public void initTest() {
        dCN = createEntity(em);
    }

    @Test
    @Transactional
    void createDCN() throws Exception {
        int databaseSizeBeforeCreate = dCNRepository.findAll().size();
        // Create the DCN
        restDCNMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(dCN)))
            .andExpect(status().isCreated());

        // Validate the DCN in the database
        List<DCN> dCNList = dCNRepository.findAll();
        assertThat(dCNList).hasSize(databaseSizeBeforeCreate + 1);
        DCN testDCN = dCNList.get(dCNList.size() - 1);
        assertThat(testDCN.getDescricao()).isEqualTo(DEFAULT_DESCRICAO);
    }

    @Test
    @Transactional
    void createDCNWithExistingId() throws Exception {
        // Create the DCN with an existing ID
        dCN.setId(1);

        int databaseSizeBeforeCreate = dCNRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restDCNMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(dCN)))
            .andExpect(status().isBadRequest());

        // Validate the DCN in the database
        List<DCN> dCNList = dCNRepository.findAll();
        assertThat(dCNList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllDCNS() throws Exception {
        // Initialize the database
        dCNRepository.saveAndFlush(dCN);

        // Get all the dCNList
        restDCNMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dCN.getId().intValue())))
            .andExpect(jsonPath("$.[*].descricao").value(hasItem(DEFAULT_DESCRICAO)));
    }

    @Test
    @Transactional
    void getDCN() throws Exception {
        // Initialize the database
        dCNRepository.saveAndFlush(dCN);

        // Get the dCN
        restDCNMockMvc
            .perform(get(ENTITY_API_URL_ID, dCN.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(dCN.getId().intValue()))
            .andExpect(jsonPath("$.descricao").value(DEFAULT_DESCRICAO));
    }

    @Test
    @Transactional
    void getNonExistingDCN() throws Exception {
        // Get the dCN
        restDCNMockMvc.perform(get(ENTITY_API_URL_ID, Integer.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingDCN() throws Exception {
        // Initialize the database
        dCNRepository.saveAndFlush(dCN);

        int databaseSizeBeforeUpdate = dCNRepository.findAll().size();

        // Update the dCN
        DCN updatedDCN = dCNRepository.findById(dCN.getId()).orElseThrow();
        // Disconnect from session so that the updates on updatedDCN are not directly saved in db
        em.detach(updatedDCN);
        updatedDCN.descricao(UPDATED_DESCRICAO);

        restDCNMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedDCN.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedDCN))
            )
            .andExpect(status().isOk());

        // Validate the DCN in the database
        List<DCN> dCNList = dCNRepository.findAll();
        assertThat(dCNList).hasSize(databaseSizeBeforeUpdate);
        DCN testDCN = dCNList.get(dCNList.size() - 1);
        assertThat(testDCN.getDescricao()).isEqualTo(UPDATED_DESCRICAO);
    }

    @Test
    @Transactional
    void putNonExistingDCN() throws Exception {
        int databaseSizeBeforeUpdate = dCNRepository.findAll().size();
        dCN.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDCNMockMvc
            .perform(
                put(ENTITY_API_URL_ID, dCN.getId()).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(dCN))
            )
            .andExpect(status().isBadRequest());

        // Validate the DCN in the database
        List<DCN> dCNList = dCNRepository.findAll();
        assertThat(dCNList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchDCN() throws Exception {
        int databaseSizeBeforeUpdate = dCNRepository.findAll().size();
        dCN.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDCNMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(dCN))
            )
            .andExpect(status().isBadRequest());

        // Validate the DCN in the database
        List<DCN> dCNList = dCNRepository.findAll();
        assertThat(dCNList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamDCN() throws Exception {
        int databaseSizeBeforeUpdate = dCNRepository.findAll().size();
        dCN.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDCNMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(dCN)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the DCN in the database
        List<DCN> dCNList = dCNRepository.findAll();
        assertThat(dCNList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateDCNWithPatch() throws Exception {
        // Initialize the database
        dCNRepository.saveAndFlush(dCN);

        int databaseSizeBeforeUpdate = dCNRepository.findAll().size();

        // Update the dCN using partial update
        DCN partialUpdatedDCN = new DCN();
        partialUpdatedDCN.setId(dCN.getId());

        restDCNMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedDCN.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedDCN))
            )
            .andExpect(status().isOk());

        // Validate the DCN in the database
        List<DCN> dCNList = dCNRepository.findAll();
        assertThat(dCNList).hasSize(databaseSizeBeforeUpdate);
        DCN testDCN = dCNList.get(dCNList.size() - 1);
        assertThat(testDCN.getDescricao()).isEqualTo(DEFAULT_DESCRICAO);
    }

    @Test
    @Transactional
    void fullUpdateDCNWithPatch() throws Exception {
        // Initialize the database
        dCNRepository.saveAndFlush(dCN);

        int databaseSizeBeforeUpdate = dCNRepository.findAll().size();

        // Update the dCN using partial update
        DCN partialUpdatedDCN = new DCN();
        partialUpdatedDCN.setId(dCN.getId());

        partialUpdatedDCN.descricao(UPDATED_DESCRICAO);

        restDCNMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedDCN.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedDCN))
            )
            .andExpect(status().isOk());

        // Validate the DCN in the database
        List<DCN> dCNList = dCNRepository.findAll();
        assertThat(dCNList).hasSize(databaseSizeBeforeUpdate);
        DCN testDCN = dCNList.get(dCNList.size() - 1);
        assertThat(testDCN.getDescricao()).isEqualTo(UPDATED_DESCRICAO);
    }

    @Test
    @Transactional
    void patchNonExistingDCN() throws Exception {
        int databaseSizeBeforeUpdate = dCNRepository.findAll().size();
        dCN.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDCNMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, dCN.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(dCN))
            )
            .andExpect(status().isBadRequest());

        // Validate the DCN in the database
        List<DCN> dCNList = dCNRepository.findAll();
        assertThat(dCNList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchDCN() throws Exception {
        int databaseSizeBeforeUpdate = dCNRepository.findAll().size();
        dCN.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDCNMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(dCN))
            )
            .andExpect(status().isBadRequest());

        // Validate the DCN in the database
        List<DCN> dCNList = dCNRepository.findAll();
        assertThat(dCNList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamDCN() throws Exception {
        int databaseSizeBeforeUpdate = dCNRepository.findAll().size();
        dCN.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDCNMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(dCN)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the DCN in the database
        List<DCN> dCNList = dCNRepository.findAll();
        assertThat(dCNList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteDCN() throws Exception {
        // Initialize the database
        dCNRepository.saveAndFlush(dCN);

        int databaseSizeBeforeDelete = dCNRepository.findAll().size();

        // Delete the dCN
        restDCNMockMvc.perform(delete(ENTITY_API_URL_ID, dCN.getId()).accept(MediaType.APPLICATION_JSON)).andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<DCN> dCNList = dCNRepository.findAll();
        assertThat(dCNList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
