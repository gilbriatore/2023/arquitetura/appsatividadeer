package br.edu.up.matrix.domain;

import static org.assertj.core.api.Assertions.assertThat;

import br.edu.up.matrix.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class PapelTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Papel.class);
        Papel papel1 = new Papel();
        papel1.setId(1);
        Papel papel2 = new Papel();
        papel2.setId(papel1.getId());
        assertThat(papel1).isEqualTo(papel2);
        papel2.setId(2);
        assertThat(papel1).isNotEqualTo(papel2);
        papel1.setId(null);
        assertThat(papel1).isNotEqualTo(papel2);
    }
}
