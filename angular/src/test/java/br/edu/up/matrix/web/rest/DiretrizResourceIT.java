package br.edu.up.matrix.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import br.edu.up.matrix.IntegrationTest;
import br.edu.up.matrix.domain.Diretriz;
import br.edu.up.matrix.repository.DiretrizRepository;
import jakarta.persistence.EntityManager;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link DiretrizResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class DiretrizResourceIT {

    private static final String DEFAULT_DESCRICAO = "AAAAAAAAAA";
    private static final String UPDATED_DESCRICAO = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/diretrizs";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicInteger count = new AtomicInteger(random.nextInt() + (2 * Short.MAX_VALUE));

    @Autowired
    private DiretrizRepository diretrizRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restDiretrizMockMvc;

    private Diretriz diretriz;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Diretriz createEntity(EntityManager em) {
        Diretriz diretriz = new Diretriz().descricao(DEFAULT_DESCRICAO);
        return diretriz;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Diretriz createUpdatedEntity(EntityManager em) {
        Diretriz diretriz = new Diretriz().descricao(UPDATED_DESCRICAO);
        return diretriz;
    }

    @BeforeEach
    public void initTest() {
        diretriz = createEntity(em);
    }

    @Test
    @Transactional
    void createDiretriz() throws Exception {
        int databaseSizeBeforeCreate = diretrizRepository.findAll().size();
        // Create the Diretriz
        restDiretrizMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(diretriz)))
            .andExpect(status().isCreated());

        // Validate the Diretriz in the database
        List<Diretriz> diretrizList = diretrizRepository.findAll();
        assertThat(diretrizList).hasSize(databaseSizeBeforeCreate + 1);
        Diretriz testDiretriz = diretrizList.get(diretrizList.size() - 1);
        assertThat(testDiretriz.getDescricao()).isEqualTo(DEFAULT_DESCRICAO);
    }

    @Test
    @Transactional
    void createDiretrizWithExistingId() throws Exception {
        // Create the Diretriz with an existing ID
        diretriz.setId(1);

        int databaseSizeBeforeCreate = diretrizRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restDiretrizMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(diretriz)))
            .andExpect(status().isBadRequest());

        // Validate the Diretriz in the database
        List<Diretriz> diretrizList = diretrizRepository.findAll();
        assertThat(diretrizList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllDiretrizs() throws Exception {
        // Initialize the database
        diretrizRepository.saveAndFlush(diretriz);

        // Get all the diretrizList
        restDiretrizMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(diretriz.getId().intValue())))
            .andExpect(jsonPath("$.[*].descricao").value(hasItem(DEFAULT_DESCRICAO)));
    }

    @Test
    @Transactional
    void getDiretriz() throws Exception {
        // Initialize the database
        diretrizRepository.saveAndFlush(diretriz);

        // Get the diretriz
        restDiretrizMockMvc
            .perform(get(ENTITY_API_URL_ID, diretriz.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(diretriz.getId().intValue()))
            .andExpect(jsonPath("$.descricao").value(DEFAULT_DESCRICAO));
    }

    @Test
    @Transactional
    void getNonExistingDiretriz() throws Exception {
        // Get the diretriz
        restDiretrizMockMvc.perform(get(ENTITY_API_URL_ID, Integer.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingDiretriz() throws Exception {
        // Initialize the database
        diretrizRepository.saveAndFlush(diretriz);

        int databaseSizeBeforeUpdate = diretrizRepository.findAll().size();

        // Update the diretriz
        Diretriz updatedDiretriz = diretrizRepository.findById(diretriz.getId()).orElseThrow();
        // Disconnect from session so that the updates on updatedDiretriz are not directly saved in db
        em.detach(updatedDiretriz);
        updatedDiretriz.descricao(UPDATED_DESCRICAO);

        restDiretrizMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedDiretriz.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedDiretriz))
            )
            .andExpect(status().isOk());

        // Validate the Diretriz in the database
        List<Diretriz> diretrizList = diretrizRepository.findAll();
        assertThat(diretrizList).hasSize(databaseSizeBeforeUpdate);
        Diretriz testDiretriz = diretrizList.get(diretrizList.size() - 1);
        assertThat(testDiretriz.getDescricao()).isEqualTo(UPDATED_DESCRICAO);
    }

    @Test
    @Transactional
    void putNonExistingDiretriz() throws Exception {
        int databaseSizeBeforeUpdate = diretrizRepository.findAll().size();
        diretriz.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDiretrizMockMvc
            .perform(
                put(ENTITY_API_URL_ID, diretriz.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(diretriz))
            )
            .andExpect(status().isBadRequest());

        // Validate the Diretriz in the database
        List<Diretriz> diretrizList = diretrizRepository.findAll();
        assertThat(diretrizList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchDiretriz() throws Exception {
        int databaseSizeBeforeUpdate = diretrizRepository.findAll().size();
        diretriz.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDiretrizMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(diretriz))
            )
            .andExpect(status().isBadRequest());

        // Validate the Diretriz in the database
        List<Diretriz> diretrizList = diretrizRepository.findAll();
        assertThat(diretrizList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamDiretriz() throws Exception {
        int databaseSizeBeforeUpdate = diretrizRepository.findAll().size();
        diretriz.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDiretrizMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(diretriz)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Diretriz in the database
        List<Diretriz> diretrizList = diretrizRepository.findAll();
        assertThat(diretrizList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateDiretrizWithPatch() throws Exception {
        // Initialize the database
        diretrizRepository.saveAndFlush(diretriz);

        int databaseSizeBeforeUpdate = diretrizRepository.findAll().size();

        // Update the diretriz using partial update
        Diretriz partialUpdatedDiretriz = new Diretriz();
        partialUpdatedDiretriz.setId(diretriz.getId());

        partialUpdatedDiretriz.descricao(UPDATED_DESCRICAO);

        restDiretrizMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedDiretriz.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedDiretriz))
            )
            .andExpect(status().isOk());

        // Validate the Diretriz in the database
        List<Diretriz> diretrizList = diretrizRepository.findAll();
        assertThat(diretrizList).hasSize(databaseSizeBeforeUpdate);
        Diretriz testDiretriz = diretrizList.get(diretrizList.size() - 1);
        assertThat(testDiretriz.getDescricao()).isEqualTo(UPDATED_DESCRICAO);
    }

    @Test
    @Transactional
    void fullUpdateDiretrizWithPatch() throws Exception {
        // Initialize the database
        diretrizRepository.saveAndFlush(diretriz);

        int databaseSizeBeforeUpdate = diretrizRepository.findAll().size();

        // Update the diretriz using partial update
        Diretriz partialUpdatedDiretriz = new Diretriz();
        partialUpdatedDiretriz.setId(diretriz.getId());

        partialUpdatedDiretriz.descricao(UPDATED_DESCRICAO);

        restDiretrizMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedDiretriz.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedDiretriz))
            )
            .andExpect(status().isOk());

        // Validate the Diretriz in the database
        List<Diretriz> diretrizList = diretrizRepository.findAll();
        assertThat(diretrizList).hasSize(databaseSizeBeforeUpdate);
        Diretriz testDiretriz = diretrizList.get(diretrizList.size() - 1);
        assertThat(testDiretriz.getDescricao()).isEqualTo(UPDATED_DESCRICAO);
    }

    @Test
    @Transactional
    void patchNonExistingDiretriz() throws Exception {
        int databaseSizeBeforeUpdate = diretrizRepository.findAll().size();
        diretriz.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDiretrizMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, diretriz.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(diretriz))
            )
            .andExpect(status().isBadRequest());

        // Validate the Diretriz in the database
        List<Diretriz> diretrizList = diretrizRepository.findAll();
        assertThat(diretrizList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchDiretriz() throws Exception {
        int databaseSizeBeforeUpdate = diretrizRepository.findAll().size();
        diretriz.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDiretrizMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(diretriz))
            )
            .andExpect(status().isBadRequest());

        // Validate the Diretriz in the database
        List<Diretriz> diretrizList = diretrizRepository.findAll();
        assertThat(diretrizList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamDiretriz() throws Exception {
        int databaseSizeBeforeUpdate = diretrizRepository.findAll().size();
        diretriz.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDiretrizMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(diretriz)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Diretriz in the database
        List<Diretriz> diretrizList = diretrizRepository.findAll();
        assertThat(diretrizList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteDiretriz() throws Exception {
        // Initialize the database
        diretrizRepository.saveAndFlush(diretriz);

        int databaseSizeBeforeDelete = diretrizRepository.findAll().size();

        // Delete the diretriz
        restDiretrizMockMvc
            .perform(delete(ENTITY_API_URL_ID, diretriz.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Diretriz> diretrizList = diretrizRepository.findAll();
        assertThat(diretrizList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
