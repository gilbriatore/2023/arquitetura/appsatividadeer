package br.edu.up.matrix.domain;

import static org.assertj.core.api.Assertions.assertThat;

import br.edu.up.matrix.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class DiretrizTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Diretriz.class);
        Diretriz diretriz1 = new Diretriz();
        diretriz1.setId(1);
        Diretriz diretriz2 = new Diretriz();
        diretriz2.setId(diretriz1.getId());
        assertThat(diretriz1).isEqualTo(diretriz2);
        diretriz2.setId(2);
        assertThat(diretriz1).isNotEqualTo(diretriz2);
        diretriz1.setId(null);
        assertThat(diretriz1).isNotEqualTo(diretriz2);
    }
}
