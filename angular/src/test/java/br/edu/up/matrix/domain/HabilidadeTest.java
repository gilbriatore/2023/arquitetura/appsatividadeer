package br.edu.up.matrix.domain;

import static org.assertj.core.api.Assertions.assertThat;

import br.edu.up.matrix.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class HabilidadeTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Habilidade.class);
        Habilidade habilidade1 = new Habilidade();
        habilidade1.setId(1);
        Habilidade habilidade2 = new Habilidade();
        habilidade2.setId(habilidade1.getId());
        assertThat(habilidade1).isEqualTo(habilidade2);
        habilidade2.setId(2);
        assertThat(habilidade1).isNotEqualTo(habilidade2);
        habilidade1.setId(null);
        assertThat(habilidade1).isNotEqualTo(habilidade2);
    }
}
