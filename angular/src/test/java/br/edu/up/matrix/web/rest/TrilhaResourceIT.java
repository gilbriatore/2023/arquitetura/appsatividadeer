package br.edu.up.matrix.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import br.edu.up.matrix.IntegrationTest;
import br.edu.up.matrix.domain.Trilha;
import br.edu.up.matrix.repository.TrilhaRepository;
import jakarta.persistence.EntityManager;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link TrilhaResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class TrilhaResourceIT {

    private static final String ENTITY_API_URL = "/api/trilhas";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicInteger count = new AtomicInteger(random.nextInt() + (2 * Short.MAX_VALUE));

    @Autowired
    private TrilhaRepository trilhaRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restTrilhaMockMvc;

    private Trilha trilha;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Trilha createEntity(EntityManager em) {
        Trilha trilha = new Trilha();
        return trilha;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Trilha createUpdatedEntity(EntityManager em) {
        Trilha trilha = new Trilha();
        return trilha;
    }

    @BeforeEach
    public void initTest() {
        trilha = createEntity(em);
    }

    @Test
    @Transactional
    void createTrilha() throws Exception {
        int databaseSizeBeforeCreate = trilhaRepository.findAll().size();
        // Create the Trilha
        restTrilhaMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(trilha)))
            .andExpect(status().isCreated());

        // Validate the Trilha in the database
        List<Trilha> trilhaList = trilhaRepository.findAll();
        assertThat(trilhaList).hasSize(databaseSizeBeforeCreate + 1);
        Trilha testTrilha = trilhaList.get(trilhaList.size() - 1);
    }

    @Test
    @Transactional
    void createTrilhaWithExistingId() throws Exception {
        // Create the Trilha with an existing ID
        trilha.setId(1);

        int databaseSizeBeforeCreate = trilhaRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restTrilhaMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(trilha)))
            .andExpect(status().isBadRequest());

        // Validate the Trilha in the database
        List<Trilha> trilhaList = trilhaRepository.findAll();
        assertThat(trilhaList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllTrilhas() throws Exception {
        // Initialize the database
        trilhaRepository.saveAndFlush(trilha);

        // Get all the trilhaList
        restTrilhaMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(trilha.getId().intValue())));
    }

    @Test
    @Transactional
    void getTrilha() throws Exception {
        // Initialize the database
        trilhaRepository.saveAndFlush(trilha);

        // Get the trilha
        restTrilhaMockMvc
            .perform(get(ENTITY_API_URL_ID, trilha.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(trilha.getId().intValue()));
    }

    @Test
    @Transactional
    void getNonExistingTrilha() throws Exception {
        // Get the trilha
        restTrilhaMockMvc.perform(get(ENTITY_API_URL_ID, Integer.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingTrilha() throws Exception {
        // Initialize the database
        trilhaRepository.saveAndFlush(trilha);

        int databaseSizeBeforeUpdate = trilhaRepository.findAll().size();

        // Update the trilha
        Trilha updatedTrilha = trilhaRepository.findById(trilha.getId()).orElseThrow();
        // Disconnect from session so that the updates on updatedTrilha are not directly saved in db
        em.detach(updatedTrilha);

        restTrilhaMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedTrilha.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedTrilha))
            )
            .andExpect(status().isOk());

        // Validate the Trilha in the database
        List<Trilha> trilhaList = trilhaRepository.findAll();
        assertThat(trilhaList).hasSize(databaseSizeBeforeUpdate);
        Trilha testTrilha = trilhaList.get(trilhaList.size() - 1);
    }

    @Test
    @Transactional
    void putNonExistingTrilha() throws Exception {
        int databaseSizeBeforeUpdate = trilhaRepository.findAll().size();
        trilha.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTrilhaMockMvc
            .perform(
                put(ENTITY_API_URL_ID, trilha.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(trilha))
            )
            .andExpect(status().isBadRequest());

        // Validate the Trilha in the database
        List<Trilha> trilhaList = trilhaRepository.findAll();
        assertThat(trilhaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchTrilha() throws Exception {
        int databaseSizeBeforeUpdate = trilhaRepository.findAll().size();
        trilha.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restTrilhaMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(trilha))
            )
            .andExpect(status().isBadRequest());

        // Validate the Trilha in the database
        List<Trilha> trilhaList = trilhaRepository.findAll();
        assertThat(trilhaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamTrilha() throws Exception {
        int databaseSizeBeforeUpdate = trilhaRepository.findAll().size();
        trilha.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restTrilhaMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(trilha)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Trilha in the database
        List<Trilha> trilhaList = trilhaRepository.findAll();
        assertThat(trilhaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateTrilhaWithPatch() throws Exception {
        // Initialize the database
        trilhaRepository.saveAndFlush(trilha);

        int databaseSizeBeforeUpdate = trilhaRepository.findAll().size();

        // Update the trilha using partial update
        Trilha partialUpdatedTrilha = new Trilha();
        partialUpdatedTrilha.setId(trilha.getId());

        restTrilhaMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedTrilha.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedTrilha))
            )
            .andExpect(status().isOk());

        // Validate the Trilha in the database
        List<Trilha> trilhaList = trilhaRepository.findAll();
        assertThat(trilhaList).hasSize(databaseSizeBeforeUpdate);
        Trilha testTrilha = trilhaList.get(trilhaList.size() - 1);
    }

    @Test
    @Transactional
    void fullUpdateTrilhaWithPatch() throws Exception {
        // Initialize the database
        trilhaRepository.saveAndFlush(trilha);

        int databaseSizeBeforeUpdate = trilhaRepository.findAll().size();

        // Update the trilha using partial update
        Trilha partialUpdatedTrilha = new Trilha();
        partialUpdatedTrilha.setId(trilha.getId());

        restTrilhaMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedTrilha.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedTrilha))
            )
            .andExpect(status().isOk());

        // Validate the Trilha in the database
        List<Trilha> trilhaList = trilhaRepository.findAll();
        assertThat(trilhaList).hasSize(databaseSizeBeforeUpdate);
        Trilha testTrilha = trilhaList.get(trilhaList.size() - 1);
    }

    @Test
    @Transactional
    void patchNonExistingTrilha() throws Exception {
        int databaseSizeBeforeUpdate = trilhaRepository.findAll().size();
        trilha.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTrilhaMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, trilha.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(trilha))
            )
            .andExpect(status().isBadRequest());

        // Validate the Trilha in the database
        List<Trilha> trilhaList = trilhaRepository.findAll();
        assertThat(trilhaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchTrilha() throws Exception {
        int databaseSizeBeforeUpdate = trilhaRepository.findAll().size();
        trilha.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restTrilhaMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(trilha))
            )
            .andExpect(status().isBadRequest());

        // Validate the Trilha in the database
        List<Trilha> trilhaList = trilhaRepository.findAll();
        assertThat(trilhaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamTrilha() throws Exception {
        int databaseSizeBeforeUpdate = trilhaRepository.findAll().size();
        trilha.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restTrilhaMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(trilha)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Trilha in the database
        List<Trilha> trilhaList = trilhaRepository.findAll();
        assertThat(trilhaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteTrilha() throws Exception {
        // Initialize the database
        trilhaRepository.saveAndFlush(trilha);

        int databaseSizeBeforeDelete = trilhaRepository.findAll().size();

        // Delete the trilha
        restTrilhaMockMvc
            .perform(delete(ENTITY_API_URL_ID, trilha.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Trilha> trilhaList = trilhaRepository.findAll();
        assertThat(trilhaList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
