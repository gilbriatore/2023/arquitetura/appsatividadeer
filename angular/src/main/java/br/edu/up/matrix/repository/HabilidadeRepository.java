package br.edu.up.matrix.repository;

import br.edu.up.matrix.domain.Habilidade;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the Habilidade entity.
 */
@SuppressWarnings("unused")
@Repository
public interface HabilidadeRepository extends JpaRepository<Habilidade, Integer> {}
