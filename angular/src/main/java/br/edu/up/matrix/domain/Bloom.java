package br.edu.up.matrix.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Bloom.
 */
@Entity
@Table(name = "bloom")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Bloom implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "nivel")
    private String nivel;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "bloom")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "competencia", "bloom" }, allowSetters = true)
    private Set<Habilidade> habilidades = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "bloom")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "conteudos", "competencia", "bloom" }, allowSetters = true)
    private Set<Conhecimento> conhecimentos = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Integer getId() {
        return this.id;
    }

    public Bloom id(Integer id) {
        this.setId(id);
        return this;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNivel() {
        return this.nivel;
    }

    public Bloom nivel(String nivel) {
        this.setNivel(nivel);
        return this;
    }

    public void setNivel(String nivel) {
        this.nivel = nivel;
    }

    public Set<Habilidade> getHabilidades() {
        return this.habilidades;
    }

    public void setHabilidades(Set<Habilidade> habilidades) {
        if (this.habilidades != null) {
            this.habilidades.forEach(i -> i.setBloom(null));
        }
        if (habilidades != null) {
            habilidades.forEach(i -> i.setBloom(this));
        }
        this.habilidades = habilidades;
    }

    public Bloom habilidades(Set<Habilidade> habilidades) {
        this.setHabilidades(habilidades);
        return this;
    }

    public Bloom addHabilidade(Habilidade habilidade) {
        this.habilidades.add(habilidade);
        habilidade.setBloom(this);
        return this;
    }

    public Bloom removeHabilidade(Habilidade habilidade) {
        this.habilidades.remove(habilidade);
        habilidade.setBloom(null);
        return this;
    }

    public Set<Conhecimento> getConhecimentos() {
        return this.conhecimentos;
    }

    public void setConhecimentos(Set<Conhecimento> conhecimentos) {
        if (this.conhecimentos != null) {
            this.conhecimentos.forEach(i -> i.setBloom(null));
        }
        if (conhecimentos != null) {
            conhecimentos.forEach(i -> i.setBloom(this));
        }
        this.conhecimentos = conhecimentos;
    }

    public Bloom conhecimentos(Set<Conhecimento> conhecimentos) {
        this.setConhecimentos(conhecimentos);
        return this;
    }

    public Bloom addConhecimento(Conhecimento conhecimento) {
        this.conhecimentos.add(conhecimento);
        conhecimento.setBloom(this);
        return this;
    }

    public Bloom removeConhecimento(Conhecimento conhecimento) {
        this.conhecimentos.remove(conhecimento);
        conhecimento.setBloom(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Bloom)) {
            return false;
        }
        return getId() != null && getId().equals(((Bloom) o).getId());
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Bloom{" +
            "id=" + getId() +
            ", nivel='" + getNivel() + "'" +
            "}";
    }
}
