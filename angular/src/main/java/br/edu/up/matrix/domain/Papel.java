package br.edu.up.matrix.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Papel.
 */
@Entity
@Table(name = "papel")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Papel implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "descricao")
    private String descricao;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "papel")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "papel", "unidade" }, allowSetters = true)
    private Set<Atividade> atividades = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "papel")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "atitudes", "habilidades", "conhecimentos", "diretriz", "papel" }, allowSetters = true)
    private Set<Competencia> competencias = new HashSet<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties(value = { "papels", "cursos" }, allowSetters = true)
    private Perfil perfil;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Integer getId() {
        return this.id;
    }

    public Papel id(Integer id) {
        this.setId(id);
        return this;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescricao() {
        return this.descricao;
    }

    public Papel descricao(String descricao) {
        this.setDescricao(descricao);
        return this;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Set<Atividade> getAtividades() {
        return this.atividades;
    }

    public void setAtividades(Set<Atividade> atividades) {
        if (this.atividades != null) {
            this.atividades.forEach(i -> i.setPapel(null));
        }
        if (atividades != null) {
            atividades.forEach(i -> i.setPapel(this));
        }
        this.atividades = atividades;
    }

    public Papel atividades(Set<Atividade> atividades) {
        this.setAtividades(atividades);
        return this;
    }

    public Papel addAtividade(Atividade atividade) {
        this.atividades.add(atividade);
        atividade.setPapel(this);
        return this;
    }

    public Papel removeAtividade(Atividade atividade) {
        this.atividades.remove(atividade);
        atividade.setPapel(null);
        return this;
    }

    public Set<Competencia> getCompetencias() {
        return this.competencias;
    }

    public void setCompetencias(Set<Competencia> competencias) {
        if (this.competencias != null) {
            this.competencias.forEach(i -> i.setPapel(null));
        }
        if (competencias != null) {
            competencias.forEach(i -> i.setPapel(this));
        }
        this.competencias = competencias;
    }

    public Papel competencias(Set<Competencia> competencias) {
        this.setCompetencias(competencias);
        return this;
    }

    public Papel addCompetencia(Competencia competencia) {
        this.competencias.add(competencia);
        competencia.setPapel(this);
        return this;
    }

    public Papel removeCompetencia(Competencia competencia) {
        this.competencias.remove(competencia);
        competencia.setPapel(null);
        return this;
    }

    public Perfil getPerfil() {
        return this.perfil;
    }

    public void setPerfil(Perfil perfil) {
        this.perfil = perfil;
    }

    public Papel perfil(Perfil perfil) {
        this.setPerfil(perfil);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Papel)) {
            return false;
        }
        return getId() != null && getId().equals(((Papel) o).getId());
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Papel{" +
            "id=" + getId() +
            ", descricao='" + getDescricao() + "'" +
            "}";
    }
}
