package br.edu.up.matrix.web.rest;

import br.edu.up.matrix.domain.Habilidade;
import br.edu.up.matrix.repository.HabilidadeRepository;
import br.edu.up.matrix.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link br.edu.up.matrix.domain.Habilidade}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class HabilidadeResource {

    private final Logger log = LoggerFactory.getLogger(HabilidadeResource.class);

    private static final String ENTITY_NAME = "habilidade";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final HabilidadeRepository habilidadeRepository;

    public HabilidadeResource(HabilidadeRepository habilidadeRepository) {
        this.habilidadeRepository = habilidadeRepository;
    }

    /**
     * {@code POST  /habilidades} : Create a new habilidade.
     *
     * @param habilidade the habilidade to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new habilidade, or with status {@code 400 (Bad Request)} if the habilidade has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/habilidades")
    public ResponseEntity<Habilidade> createHabilidade(@RequestBody Habilidade habilidade) throws URISyntaxException {
        log.debug("REST request to save Habilidade : {}", habilidade);
        if (habilidade.getId() != null) {
            throw new BadRequestAlertException("A new habilidade cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Habilidade result = habilidadeRepository.save(habilidade);
        return ResponseEntity
            .created(new URI("/api/habilidades/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /habilidades/:id} : Updates an existing habilidade.
     *
     * @param id the id of the habilidade to save.
     * @param habilidade the habilidade to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated habilidade,
     * or with status {@code 400 (Bad Request)} if the habilidade is not valid,
     * or with status {@code 500 (Internal Server Error)} if the habilidade couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/habilidades/{id}")
    public ResponseEntity<Habilidade> updateHabilidade(
        @PathVariable(value = "id", required = false) final Integer id,
        @RequestBody Habilidade habilidade
    ) throws URISyntaxException {
        log.debug("REST request to update Habilidade : {}, {}", id, habilidade);
        if (habilidade.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, habilidade.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!habilidadeRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Habilidade result = habilidadeRepository.save(habilidade);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, habilidade.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /habilidades/:id} : Partial updates given fields of an existing habilidade, field will ignore if it is null
     *
     * @param id the id of the habilidade to save.
     * @param habilidade the habilidade to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated habilidade,
     * or with status {@code 400 (Bad Request)} if the habilidade is not valid,
     * or with status {@code 404 (Not Found)} if the habilidade is not found,
     * or with status {@code 500 (Internal Server Error)} if the habilidade couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/habilidades/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<Habilidade> partialUpdateHabilidade(
        @PathVariable(value = "id", required = false) final Integer id,
        @RequestBody Habilidade habilidade
    ) throws URISyntaxException {
        log.debug("REST request to partial update Habilidade partially : {}, {}", id, habilidade);
        if (habilidade.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, habilidade.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!habilidadeRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<Habilidade> result = habilidadeRepository
            .findById(habilidade.getId())
            .map(existingHabilidade -> {
                if (habilidade.getDescricao() != null) {
                    existingHabilidade.setDescricao(habilidade.getDescricao());
                }

                return existingHabilidade;
            })
            .map(habilidadeRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, habilidade.getId().toString())
        );
    }

    /**
     * {@code GET  /habilidades} : get all the habilidades.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of habilidades in body.
     */
    @GetMapping("/habilidades")
    public List<Habilidade> getAllHabilidades() {
        log.debug("REST request to get all Habilidades");
        return habilidadeRepository.findAll();
    }

    /**
     * {@code GET  /habilidades/:id} : get the "id" habilidade.
     *
     * @param id the id of the habilidade to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the habilidade, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/habilidades/{id}")
    public ResponseEntity<Habilidade> getHabilidade(@PathVariable Integer id) {
        log.debug("REST request to get Habilidade : {}", id);
        Optional<Habilidade> habilidade = habilidadeRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(habilidade);
    }

    /**
     * {@code DELETE  /habilidades/:id} : delete the "id" habilidade.
     *
     * @param id the id of the habilidade to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/habilidades/{id}")
    public ResponseEntity<Void> deleteHabilidade(@PathVariable Integer id) {
        log.debug("REST request to delete Habilidade : {}", id);
        habilidadeRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
