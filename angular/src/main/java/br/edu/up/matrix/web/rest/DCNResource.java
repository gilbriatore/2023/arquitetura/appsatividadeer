package br.edu.up.matrix.web.rest;

import br.edu.up.matrix.domain.DCN;
import br.edu.up.matrix.repository.DCNRepository;
import br.edu.up.matrix.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link br.edu.up.matrix.domain.DCN}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class DCNResource {

    private final Logger log = LoggerFactory.getLogger(DCNResource.class);

    private static final String ENTITY_NAME = "dCN";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final DCNRepository dCNRepository;

    public DCNResource(DCNRepository dCNRepository) {
        this.dCNRepository = dCNRepository;
    }

    /**
     * {@code POST  /dcns} : Create a new dCN.
     *
     * @param dCN the dCN to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new dCN, or with status {@code 400 (Bad Request)} if the dCN has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/dcns")
    public ResponseEntity<DCN> createDCN(@RequestBody DCN dCN) throws URISyntaxException {
        log.debug("REST request to save DCN : {}", dCN);
        if (dCN.getId() != null) {
            throw new BadRequestAlertException("A new dCN cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DCN result = dCNRepository.save(dCN);
        return ResponseEntity
            .created(new URI("/api/dcns/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /dcns/:id} : Updates an existing dCN.
     *
     * @param id the id of the dCN to save.
     * @param dCN the dCN to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated dCN,
     * or with status {@code 400 (Bad Request)} if the dCN is not valid,
     * or with status {@code 500 (Internal Server Error)} if the dCN couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/dcns/{id}")
    public ResponseEntity<DCN> updateDCN(@PathVariable(value = "id", required = false) final Integer id, @RequestBody DCN dCN)
        throws URISyntaxException {
        log.debug("REST request to update DCN : {}, {}", id, dCN);
        if (dCN.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, dCN.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!dCNRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        DCN result = dCNRepository.save(dCN);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, dCN.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /dcns/:id} : Partial updates given fields of an existing dCN, field will ignore if it is null
     *
     * @param id the id of the dCN to save.
     * @param dCN the dCN to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated dCN,
     * or with status {@code 400 (Bad Request)} if the dCN is not valid,
     * or with status {@code 404 (Not Found)} if the dCN is not found,
     * or with status {@code 500 (Internal Server Error)} if the dCN couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/dcns/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<DCN> partialUpdateDCN(@PathVariable(value = "id", required = false) final Integer id, @RequestBody DCN dCN)
        throws URISyntaxException {
        log.debug("REST request to partial update DCN partially : {}, {}", id, dCN);
        if (dCN.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, dCN.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!dCNRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<DCN> result = dCNRepository
            .findById(dCN.getId())
            .map(existingDCN -> {
                if (dCN.getDescricao() != null) {
                    existingDCN.setDescricao(dCN.getDescricao());
                }

                return existingDCN;
            })
            .map(dCNRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, dCN.getId().toString())
        );
    }

    /**
     * {@code GET  /dcns} : get all the dCNS.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of dCNS in body.
     */
    @GetMapping("/dcns")
    public List<DCN> getAllDCNS() {
        log.debug("REST request to get all DCNS");
        return dCNRepository.findAll();
    }

    /**
     * {@code GET  /dcns/:id} : get the "id" dCN.
     *
     * @param id the id of the dCN to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the dCN, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/dcns/{id}")
    public ResponseEntity<DCN> getDCN(@PathVariable Integer id) {
        log.debug("REST request to get DCN : {}", id);
        Optional<DCN> dCN = dCNRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(dCN);
    }

    /**
     * {@code DELETE  /dcns/:id} : delete the "id" dCN.
     *
     * @param id the id of the dCN to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/dcns/{id}")
    public ResponseEntity<Void> deleteDCN(@PathVariable Integer id) {
        log.debug("REST request to delete DCN : {}", id);
        dCNRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
