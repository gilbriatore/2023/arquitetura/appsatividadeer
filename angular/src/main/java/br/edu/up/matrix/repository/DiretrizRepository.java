package br.edu.up.matrix.repository;

import br.edu.up.matrix.domain.Diretriz;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the Diretriz entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DiretrizRepository extends JpaRepository<Diretriz, Integer> {}
