package br.edu.up.matrix.web.rest;

import br.edu.up.matrix.domain.Periodo;
import br.edu.up.matrix.repository.PeriodoRepository;
import br.edu.up.matrix.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link br.edu.up.matrix.domain.Periodo}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class PeriodoResource {

    private final Logger log = LoggerFactory.getLogger(PeriodoResource.class);

    private static final String ENTITY_NAME = "periodo";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PeriodoRepository periodoRepository;

    public PeriodoResource(PeriodoRepository periodoRepository) {
        this.periodoRepository = periodoRepository;
    }

    /**
     * {@code POST  /periodos} : Create a new periodo.
     *
     * @param periodo the periodo to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new periodo, or with status {@code 400 (Bad Request)} if the periodo has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/periodos")
    public ResponseEntity<Periodo> createPeriodo(@RequestBody Periodo periodo) throws URISyntaxException {
        log.debug("REST request to save Periodo : {}", periodo);
        if (periodo.getId() != null) {
            throw new BadRequestAlertException("A new periodo cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Periodo result = periodoRepository.save(periodo);
        return ResponseEntity
            .created(new URI("/api/periodos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /periodos/:id} : Updates an existing periodo.
     *
     * @param id the id of the periodo to save.
     * @param periodo the periodo to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated periodo,
     * or with status {@code 400 (Bad Request)} if the periodo is not valid,
     * or with status {@code 500 (Internal Server Error)} if the periodo couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/periodos/{id}")
    public ResponseEntity<Periodo> updatePeriodo(
        @PathVariable(value = "id", required = false) final Integer id,
        @RequestBody Periodo periodo
    ) throws URISyntaxException {
        log.debug("REST request to update Periodo : {}, {}", id, periodo);
        if (periodo.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, periodo.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!periodoRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Periodo result = periodoRepository.save(periodo);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, periodo.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /periodos/:id} : Partial updates given fields of an existing periodo, field will ignore if it is null
     *
     * @param id the id of the periodo to save.
     * @param periodo the periodo to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated periodo,
     * or with status {@code 400 (Bad Request)} if the periodo is not valid,
     * or with status {@code 404 (Not Found)} if the periodo is not found,
     * or with status {@code 500 (Internal Server Error)} if the periodo couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/periodos/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<Periodo> partialUpdatePeriodo(
        @PathVariable(value = "id", required = false) final Integer id,
        @RequestBody Periodo periodo
    ) throws URISyntaxException {
        log.debug("REST request to partial update Periodo partially : {}, {}", id, periodo);
        if (periodo.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, periodo.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!periodoRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<Periodo> result = periodoRepository
            .findById(periodo.getId())
            .map(existingPeriodo -> {
                if (periodo.getDescricao() != null) {
                    existingPeriodo.setDescricao(periodo.getDescricao());
                }

                return existingPeriodo;
            })
            .map(periodoRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, periodo.getId().toString())
        );
    }

    /**
     * {@code GET  /periodos} : get all the periodos.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of periodos in body.
     */
    @GetMapping("/periodos")
    public List<Periodo> getAllPeriodos() {
        log.debug("REST request to get all Periodos");
        return periodoRepository.findAll();
    }

    /**
     * {@code GET  /periodos/:id} : get the "id" periodo.
     *
     * @param id the id of the periodo to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the periodo, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/periodos/{id}")
    public ResponseEntity<Periodo> getPeriodo(@PathVariable Integer id) {
        log.debug("REST request to get Periodo : {}", id);
        Optional<Periodo> periodo = periodoRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(periodo);
    }

    /**
     * {@code DELETE  /periodos/:id} : delete the "id" periodo.
     *
     * @param id the id of the periodo to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/periodos/{id}")
    public ResponseEntity<Void> deletePeriodo(@PathVariable Integer id) {
        log.debug("REST request to delete Periodo : {}", id);
        periodoRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
