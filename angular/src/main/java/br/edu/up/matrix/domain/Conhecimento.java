package br.edu.up.matrix.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Conhecimento.
 */
@Entity
@Table(name = "conhecimento")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Conhecimento implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "descricao")
    private String descricao;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "conhecimento")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "conteudos", "trilhas", "conhecimento", "geral" }, allowSetters = true)
    private Set<Conteudo> conteudos = new HashSet<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties(value = { "atitudes", "habilidades", "conhecimentos", "diretriz", "papel" }, allowSetters = true)
    private Competencia competencia;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties(value = { "habilidades", "conhecimentos" }, allowSetters = true)
    private Bloom bloom;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Integer getId() {
        return this.id;
    }

    public Conhecimento id(Integer id) {
        this.setId(id);
        return this;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescricao() {
        return this.descricao;
    }

    public Conhecimento descricao(String descricao) {
        this.setDescricao(descricao);
        return this;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Set<Conteudo> getConteudos() {
        return this.conteudos;
    }

    public void setConteudos(Set<Conteudo> conteudos) {
        if (this.conteudos != null) {
            this.conteudos.forEach(i -> i.setConhecimento(null));
        }
        if (conteudos != null) {
            conteudos.forEach(i -> i.setConhecimento(this));
        }
        this.conteudos = conteudos;
    }

    public Conhecimento conteudos(Set<Conteudo> conteudos) {
        this.setConteudos(conteudos);
        return this;
    }

    public Conhecimento addConteudo(Conteudo conteudo) {
        this.conteudos.add(conteudo);
        conteudo.setConhecimento(this);
        return this;
    }

    public Conhecimento removeConteudo(Conteudo conteudo) {
        this.conteudos.remove(conteudo);
        conteudo.setConhecimento(null);
        return this;
    }

    public Competencia getCompetencia() {
        return this.competencia;
    }

    public void setCompetencia(Competencia competencia) {
        this.competencia = competencia;
    }

    public Conhecimento competencia(Competencia competencia) {
        this.setCompetencia(competencia);
        return this;
    }

    public Bloom getBloom() {
        return this.bloom;
    }

    public void setBloom(Bloom bloom) {
        this.bloom = bloom;
    }

    public Conhecimento bloom(Bloom bloom) {
        this.setBloom(bloom);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Conhecimento)) {
            return false;
        }
        return getId() != null && getId().equals(((Conhecimento) o).getId());
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Conhecimento{" +
            "id=" + getId() +
            ", descricao='" + getDescricao() + "'" +
            "}";
    }
}
