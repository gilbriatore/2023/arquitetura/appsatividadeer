package br.edu.up.matrix.repository;

import br.edu.up.matrix.domain.Trilha;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the Trilha entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TrilhaRepository extends JpaRepository<Trilha, Integer> {}
