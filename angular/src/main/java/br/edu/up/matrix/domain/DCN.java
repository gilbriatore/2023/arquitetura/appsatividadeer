package br.edu.up.matrix.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A DCN.
 */
@Entity
@Table(name = "dcn")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class DCN implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "descricao")
    private String descricao;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "dCN")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "competencias", "dCN" }, allowSetters = true)
    private Set<Diretriz> diretrizs = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Integer getId() {
        return this.id;
    }

    public DCN id(Integer id) {
        this.setId(id);
        return this;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescricao() {
        return this.descricao;
    }

    public DCN descricao(String descricao) {
        this.setDescricao(descricao);
        return this;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Set<Diretriz> getDiretrizs() {
        return this.diretrizs;
    }

    public void setDiretrizs(Set<Diretriz> diretrizs) {
        if (this.diretrizs != null) {
            this.diretrizs.forEach(i -> i.setDCN(null));
        }
        if (diretrizs != null) {
            diretrizs.forEach(i -> i.setDCN(this));
        }
        this.diretrizs = diretrizs;
    }

    public DCN diretrizs(Set<Diretriz> diretrizs) {
        this.setDiretrizs(diretrizs);
        return this;
    }

    public DCN addDiretriz(Diretriz diretriz) {
        this.diretrizs.add(diretriz);
        diretriz.setDCN(this);
        return this;
    }

    public DCN removeDiretriz(Diretriz diretriz) {
        this.diretrizs.remove(diretriz);
        diretriz.setDCN(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DCN)) {
            return false;
        }
        return getId() != null && getId().equals(((DCN) o).getId());
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "DCN{" +
            "id=" + getId() +
            ", descricao='" + getDescricao() + "'" +
            "}";
    }
}
