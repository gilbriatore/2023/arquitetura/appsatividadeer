package br.edu.up.matrix.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Competencia.
 */
@Entity
@Table(name = "competencia")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Competencia implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "descricao")
    private String descricao;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "competencia")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "competencia" }, allowSetters = true)
    private Set<Atitude> atitudes = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "competencia")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "competencia", "bloom" }, allowSetters = true)
    private Set<Habilidade> habilidades = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "competencia")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "conteudos", "competencia", "bloom" }, allowSetters = true)
    private Set<Conhecimento> conhecimentos = new HashSet<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties(value = { "competencias", "dCN" }, allowSetters = true)
    private Diretriz diretriz;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties(value = { "atividades", "competencias", "perfil" }, allowSetters = true)
    private Papel papel;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Integer getId() {
        return this.id;
    }

    public Competencia id(Integer id) {
        this.setId(id);
        return this;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescricao() {
        return this.descricao;
    }

    public Competencia descricao(String descricao) {
        this.setDescricao(descricao);
        return this;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Set<Atitude> getAtitudes() {
        return this.atitudes;
    }

    public void setAtitudes(Set<Atitude> atitudes) {
        if (this.atitudes != null) {
            this.atitudes.forEach(i -> i.setCompetencia(null));
        }
        if (atitudes != null) {
            atitudes.forEach(i -> i.setCompetencia(this));
        }
        this.atitudes = atitudes;
    }

    public Competencia atitudes(Set<Atitude> atitudes) {
        this.setAtitudes(atitudes);
        return this;
    }

    public Competencia addAtitude(Atitude atitude) {
        this.atitudes.add(atitude);
        atitude.setCompetencia(this);
        return this;
    }

    public Competencia removeAtitude(Atitude atitude) {
        this.atitudes.remove(atitude);
        atitude.setCompetencia(null);
        return this;
    }

    public Set<Habilidade> getHabilidades() {
        return this.habilidades;
    }

    public void setHabilidades(Set<Habilidade> habilidades) {
        if (this.habilidades != null) {
            this.habilidades.forEach(i -> i.setCompetencia(null));
        }
        if (habilidades != null) {
            habilidades.forEach(i -> i.setCompetencia(this));
        }
        this.habilidades = habilidades;
    }

    public Competencia habilidades(Set<Habilidade> habilidades) {
        this.setHabilidades(habilidades);
        return this;
    }

    public Competencia addHabilidade(Habilidade habilidade) {
        this.habilidades.add(habilidade);
        habilidade.setCompetencia(this);
        return this;
    }

    public Competencia removeHabilidade(Habilidade habilidade) {
        this.habilidades.remove(habilidade);
        habilidade.setCompetencia(null);
        return this;
    }

    public Set<Conhecimento> getConhecimentos() {
        return this.conhecimentos;
    }

    public void setConhecimentos(Set<Conhecimento> conhecimentos) {
        if (this.conhecimentos != null) {
            this.conhecimentos.forEach(i -> i.setCompetencia(null));
        }
        if (conhecimentos != null) {
            conhecimentos.forEach(i -> i.setCompetencia(this));
        }
        this.conhecimentos = conhecimentos;
    }

    public Competencia conhecimentos(Set<Conhecimento> conhecimentos) {
        this.setConhecimentos(conhecimentos);
        return this;
    }

    public Competencia addConhecimento(Conhecimento conhecimento) {
        this.conhecimentos.add(conhecimento);
        conhecimento.setCompetencia(this);
        return this;
    }

    public Competencia removeConhecimento(Conhecimento conhecimento) {
        this.conhecimentos.remove(conhecimento);
        conhecimento.setCompetencia(null);
        return this;
    }

    public Diretriz getDiretriz() {
        return this.diretriz;
    }

    public void setDiretriz(Diretriz diretriz) {
        this.diretriz = diretriz;
    }

    public Competencia diretriz(Diretriz diretriz) {
        this.setDiretriz(diretriz);
        return this;
    }

    public Papel getPapel() {
        return this.papel;
    }

    public void setPapel(Papel papel) {
        this.papel = papel;
    }

    public Competencia papel(Papel papel) {
        this.setPapel(papel);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Competencia)) {
            return false;
        }
        return getId() != null && getId().equals(((Competencia) o).getId());
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Competencia{" +
            "id=" + getId() +
            ", descricao='" + getDescricao() + "'" +
            "}";
    }
}
