package br.edu.up.matrix.repository;

import br.edu.up.matrix.domain.Atitude;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the Atitude entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AtitudeRepository extends JpaRepository<Atitude, Integer> {}
