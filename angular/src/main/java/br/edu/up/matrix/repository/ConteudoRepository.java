package br.edu.up.matrix.repository;

import br.edu.up.matrix.domain.Conteudo;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the Conteudo entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ConteudoRepository extends JpaRepository<Conteudo, Integer> {}
