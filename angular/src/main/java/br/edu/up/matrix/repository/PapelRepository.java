package br.edu.up.matrix.repository;

import br.edu.up.matrix.domain.Papel;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the Papel entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PapelRepository extends JpaRepository<Papel, Integer> {}
