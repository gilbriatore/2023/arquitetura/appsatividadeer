package br.edu.up.matrix.config;

import java.time.Duration;
import org.ehcache.config.builders.*;
import org.ehcache.jsr107.Eh107Configuration;
import org.hibernate.cache.jcache.ConfigSettings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer;
import org.springframework.boot.autoconfigure.orm.jpa.HibernatePropertiesCustomizer;
import org.springframework.boot.info.BuildProperties;
import org.springframework.boot.info.GitProperties;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.context.annotation.*;
import tech.jhipster.config.JHipsterProperties;
import tech.jhipster.config.cache.PrefixedKeyGenerator;

@Configuration
@EnableCaching
public class CacheConfiguration {

    private GitProperties gitProperties;
    private BuildProperties buildProperties;
    private final javax.cache.configuration.Configuration<Object, Object> jcacheConfiguration;

    public CacheConfiguration(JHipsterProperties jHipsterProperties) {
        JHipsterProperties.Cache.Ehcache ehcache = jHipsterProperties.getCache().getEhcache();

        jcacheConfiguration =
            Eh107Configuration.fromEhcacheCacheConfiguration(
                CacheConfigurationBuilder
                    .newCacheConfigurationBuilder(Object.class, Object.class, ResourcePoolsBuilder.heap(ehcache.getMaxEntries()))
                    .withExpiry(ExpiryPolicyBuilder.timeToLiveExpiration(Duration.ofSeconds(ehcache.getTimeToLiveSeconds())))
                    .build()
            );
    }

    @Bean
    public HibernatePropertiesCustomizer hibernatePropertiesCustomizer(javax.cache.CacheManager cacheManager) {
        return hibernateProperties -> hibernateProperties.put(ConfigSettings.CACHE_MANAGER, cacheManager);
    }

    @Bean
    public JCacheManagerCustomizer cacheManagerCustomizer() {
        return cm -> {
            createCache(cm, br.edu.up.matrix.repository.UserRepository.USERS_BY_LOGIN_CACHE);
            createCache(cm, br.edu.up.matrix.repository.UserRepository.USERS_BY_EMAIL_CACHE);
            createCache(cm, br.edu.up.matrix.domain.User.class.getName());
            createCache(cm, br.edu.up.matrix.domain.Authority.class.getName());
            createCache(cm, br.edu.up.matrix.domain.User.class.getName() + ".authorities");
            createCache(cm, br.edu.up.matrix.domain.Atividade.class.getName());
            createCache(cm, br.edu.up.matrix.domain.Bloom.class.getName());
            createCache(cm, br.edu.up.matrix.domain.Bloom.class.getName() + ".habilidades");
            createCache(cm, br.edu.up.matrix.domain.Bloom.class.getName() + ".conhecimentos");
            createCache(cm, br.edu.up.matrix.domain.Curso.class.getName());
            createCache(cm, br.edu.up.matrix.domain.Curso.class.getName() + ".matrizs");
            createCache(cm, br.edu.up.matrix.domain.Escola.class.getName());
            createCache(cm, br.edu.up.matrix.domain.Escola.class.getName() + ".cursos");
            createCache(cm, br.edu.up.matrix.domain.Matriz.class.getName());
            createCache(cm, br.edu.up.matrix.domain.Matriz.class.getName() + ".periodos");
            createCache(cm, br.edu.up.matrix.domain.Modalidade.class.getName());
            createCache(cm, br.edu.up.matrix.domain.Modalidade.class.getName() + ".cursos");
            createCache(cm, br.edu.up.matrix.domain.TipoDeCurso.class.getName());
            createCache(cm, br.edu.up.matrix.domain.TipoDeCurso.class.getName() + ".tipos");
            createCache(cm, br.edu.up.matrix.domain.Unidade.class.getName());
            createCache(cm, br.edu.up.matrix.domain.Unidade.class.getName() + ".atividades");
            createCache(cm, br.edu.up.matrix.domain.Atitude.class.getName());
            createCache(cm, br.edu.up.matrix.domain.Conhecimento.class.getName());
            createCache(cm, br.edu.up.matrix.domain.Conhecimento.class.getName() + ".conteudos");
            createCache(cm, br.edu.up.matrix.domain.Competencia.class.getName());
            createCache(cm, br.edu.up.matrix.domain.Competencia.class.getName() + ".atitudes");
            createCache(cm, br.edu.up.matrix.domain.Competencia.class.getName() + ".habilidades");
            createCache(cm, br.edu.up.matrix.domain.Competencia.class.getName() + ".conhecimentos");
            createCache(cm, br.edu.up.matrix.domain.Conteudo.class.getName());
            createCache(cm, br.edu.up.matrix.domain.Conteudo.class.getName() + ".conteudos");
            createCache(cm, br.edu.up.matrix.domain.Conteudo.class.getName() + ".trilhas");
            createCache(cm, br.edu.up.matrix.domain.Disciplina.class.getName());
            createCache(cm, br.edu.up.matrix.domain.DCN.class.getName());
            createCache(cm, br.edu.up.matrix.domain.DCN.class.getName() + ".diretrizs");
            createCache(cm, br.edu.up.matrix.domain.Diretriz.class.getName());
            createCache(cm, br.edu.up.matrix.domain.Diretriz.class.getName() + ".competencias");
            createCache(cm, br.edu.up.matrix.domain.Habilidade.class.getName());
            createCache(cm, br.edu.up.matrix.domain.Papel.class.getName());
            createCache(cm, br.edu.up.matrix.domain.Papel.class.getName() + ".atividades");
            createCache(cm, br.edu.up.matrix.domain.Papel.class.getName() + ".competencias");
            createCache(cm, br.edu.up.matrix.domain.Perfil.class.getName());
            createCache(cm, br.edu.up.matrix.domain.Perfil.class.getName() + ".papels");
            createCache(cm, br.edu.up.matrix.domain.Perfil.class.getName() + ".cursos");
            createCache(cm, br.edu.up.matrix.domain.Periodo.class.getName());
            createCache(cm, br.edu.up.matrix.domain.Periodo.class.getName() + ".disciplinas");
            createCache(cm, br.edu.up.matrix.domain.Periodo.class.getName() + ".trilhas");
            createCache(cm, br.edu.up.matrix.domain.Trilha.class.getName());
            createCache(cm, br.edu.up.matrix.domain.Trilha.class.getName() + ".disciplinas");
            createCache(cm, br.edu.up.matrix.domain.Trilha.class.getName() + ".unidades");
            // jhipster-needle-ehcache-add-entry
        };
    }

    private void createCache(javax.cache.CacheManager cm, String cacheName) {
        javax.cache.Cache<Object, Object> cache = cm.getCache(cacheName);
        if (cache != null) {
            cache.clear();
        } else {
            cm.createCache(cacheName, jcacheConfiguration);
        }
    }

    @Autowired(required = false)
    public void setGitProperties(GitProperties gitProperties) {
        this.gitProperties = gitProperties;
    }

    @Autowired(required = false)
    public void setBuildProperties(BuildProperties buildProperties) {
        this.buildProperties = buildProperties;
    }

    @Bean
    public KeyGenerator keyGenerator() {
        return new PrefixedKeyGenerator(this.gitProperties, this.buildProperties);
    }
}
