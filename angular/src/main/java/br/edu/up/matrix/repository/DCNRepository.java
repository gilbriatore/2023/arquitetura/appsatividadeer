package br.edu.up.matrix.repository;

import br.edu.up.matrix.domain.DCN;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the DCN entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DCNRepository extends JpaRepository<DCN, Integer> {}
