package br.edu.up.matrix.web.rest;

import br.edu.up.matrix.domain.Diretriz;
import br.edu.up.matrix.repository.DiretrizRepository;
import br.edu.up.matrix.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link br.edu.up.matrix.domain.Diretriz}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class DiretrizResource {

    private final Logger log = LoggerFactory.getLogger(DiretrizResource.class);

    private static final String ENTITY_NAME = "diretriz";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final DiretrizRepository diretrizRepository;

    public DiretrizResource(DiretrizRepository diretrizRepository) {
        this.diretrizRepository = diretrizRepository;
    }

    /**
     * {@code POST  /diretrizs} : Create a new diretriz.
     *
     * @param diretriz the diretriz to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new diretriz, or with status {@code 400 (Bad Request)} if the diretriz has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/diretrizs")
    public ResponseEntity<Diretriz> createDiretriz(@RequestBody Diretriz diretriz) throws URISyntaxException {
        log.debug("REST request to save Diretriz : {}", diretriz);
        if (diretriz.getId() != null) {
            throw new BadRequestAlertException("A new diretriz cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Diretriz result = diretrizRepository.save(diretriz);
        return ResponseEntity
            .created(new URI("/api/diretrizs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /diretrizs/:id} : Updates an existing diretriz.
     *
     * @param id the id of the diretriz to save.
     * @param diretriz the diretriz to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated diretriz,
     * or with status {@code 400 (Bad Request)} if the diretriz is not valid,
     * or with status {@code 500 (Internal Server Error)} if the diretriz couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/diretrizs/{id}")
    public ResponseEntity<Diretriz> updateDiretriz(
        @PathVariable(value = "id", required = false) final Integer id,
        @RequestBody Diretriz diretriz
    ) throws URISyntaxException {
        log.debug("REST request to update Diretriz : {}, {}", id, diretriz);
        if (diretriz.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, diretriz.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!diretrizRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Diretriz result = diretrizRepository.save(diretriz);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, diretriz.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /diretrizs/:id} : Partial updates given fields of an existing diretriz, field will ignore if it is null
     *
     * @param id the id of the diretriz to save.
     * @param diretriz the diretriz to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated diretriz,
     * or with status {@code 400 (Bad Request)} if the diretriz is not valid,
     * or with status {@code 404 (Not Found)} if the diretriz is not found,
     * or with status {@code 500 (Internal Server Error)} if the diretriz couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/diretrizs/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<Diretriz> partialUpdateDiretriz(
        @PathVariable(value = "id", required = false) final Integer id,
        @RequestBody Diretriz diretriz
    ) throws URISyntaxException {
        log.debug("REST request to partial update Diretriz partially : {}, {}", id, diretriz);
        if (diretriz.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, diretriz.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!diretrizRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<Diretriz> result = diretrizRepository
            .findById(diretriz.getId())
            .map(existingDiretriz -> {
                if (diretriz.getDescricao() != null) {
                    existingDiretriz.setDescricao(diretriz.getDescricao());
                }

                return existingDiretriz;
            })
            .map(diretrizRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, diretriz.getId().toString())
        );
    }

    /**
     * {@code GET  /diretrizs} : get all the diretrizs.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of diretrizs in body.
     */
    @GetMapping("/diretrizs")
    public List<Diretriz> getAllDiretrizs() {
        log.debug("REST request to get all Diretrizs");
        return diretrizRepository.findAll();
    }

    /**
     * {@code GET  /diretrizs/:id} : get the "id" diretriz.
     *
     * @param id the id of the diretriz to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the diretriz, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/diretrizs/{id}")
    public ResponseEntity<Diretriz> getDiretriz(@PathVariable Integer id) {
        log.debug("REST request to get Diretriz : {}", id);
        Optional<Diretriz> diretriz = diretrizRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(diretriz);
    }

    /**
     * {@code DELETE  /diretrizs/:id} : delete the "id" diretriz.
     *
     * @param id the id of the diretriz to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/diretrizs/{id}")
    public ResponseEntity<Void> deleteDiretriz(@PathVariable Integer id) {
        log.debug("REST request to delete Diretriz : {}", id);
        diretrizRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
