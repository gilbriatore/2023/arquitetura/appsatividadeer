package br.edu.up.matrix.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Trilha.
 */
@Entity
@Table(name = "trilha")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Trilha implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "trilha")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "periodo", "trilha" }, allowSetters = true)
    private Set<Disciplina> disciplinas = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "trilha")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "atividades", "trilha" }, allowSetters = true)
    private Set<Unidade> unidades = new HashSet<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties(value = { "conteudos", "trilhas", "conhecimento", "geral" }, allowSetters = true)
    private Conteudo conteudo;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties(value = { "disciplinas", "trilhas", "matriz" }, allowSetters = true)
    private Periodo periodo;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Integer getId() {
        return this.id;
    }

    public Trilha id(Integer id) {
        this.setId(id);
        return this;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Set<Disciplina> getDisciplinas() {
        return this.disciplinas;
    }

    public void setDisciplinas(Set<Disciplina> disciplinas) {
        if (this.disciplinas != null) {
            this.disciplinas.forEach(i -> i.setTrilha(null));
        }
        if (disciplinas != null) {
            disciplinas.forEach(i -> i.setTrilha(this));
        }
        this.disciplinas = disciplinas;
    }

    public Trilha disciplinas(Set<Disciplina> disciplinas) {
        this.setDisciplinas(disciplinas);
        return this;
    }

    public Trilha addDisciplina(Disciplina disciplina) {
        this.disciplinas.add(disciplina);
        disciplina.setTrilha(this);
        return this;
    }

    public Trilha removeDisciplina(Disciplina disciplina) {
        this.disciplinas.remove(disciplina);
        disciplina.setTrilha(null);
        return this;
    }

    public Set<Unidade> getUnidades() {
        return this.unidades;
    }

    public void setUnidades(Set<Unidade> unidades) {
        if (this.unidades != null) {
            this.unidades.forEach(i -> i.setTrilha(null));
        }
        if (unidades != null) {
            unidades.forEach(i -> i.setTrilha(this));
        }
        this.unidades = unidades;
    }

    public Trilha unidades(Set<Unidade> unidades) {
        this.setUnidades(unidades);
        return this;
    }

    public Trilha addUnidade(Unidade unidade) {
        this.unidades.add(unidade);
        unidade.setTrilha(this);
        return this;
    }

    public Trilha removeUnidade(Unidade unidade) {
        this.unidades.remove(unidade);
        unidade.setTrilha(null);
        return this;
    }

    public Conteudo getConteudo() {
        return this.conteudo;
    }

    public void setConteudo(Conteudo conteudo) {
        this.conteudo = conteudo;
    }

    public Trilha conteudo(Conteudo conteudo) {
        this.setConteudo(conteudo);
        return this;
    }

    public Periodo getPeriodo() {
        return this.periodo;
    }

    public void setPeriodo(Periodo periodo) {
        this.periodo = periodo;
    }

    public Trilha periodo(Periodo periodo) {
        this.setPeriodo(periodo);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Trilha)) {
            return false;
        }
        return getId() != null && getId().equals(((Trilha) o).getId());
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Trilha{" +
            "id=" + getId() +
            "}";
    }
}
