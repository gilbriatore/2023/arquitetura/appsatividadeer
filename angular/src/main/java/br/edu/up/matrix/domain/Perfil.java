package br.edu.up.matrix.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Perfil.
 */
@Entity
@Table(name = "perfil")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Perfil implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "descricao")
    private String descricao;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "perfil")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "atividades", "competencias", "perfil" }, allowSetters = true)
    private Set<Papel> papels = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "perfil")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "matrizs", "perfil", "modalidade", "escola", "tipoDeCurso" }, allowSetters = true)
    private Set<Curso> cursos = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Integer getId() {
        return this.id;
    }

    public Perfil id(Integer id) {
        this.setId(id);
        return this;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescricao() {
        return this.descricao;
    }

    public Perfil descricao(String descricao) {
        this.setDescricao(descricao);
        return this;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Set<Papel> getPapels() {
        return this.papels;
    }

    public void setPapels(Set<Papel> papels) {
        if (this.papels != null) {
            this.papels.forEach(i -> i.setPerfil(null));
        }
        if (papels != null) {
            papels.forEach(i -> i.setPerfil(this));
        }
        this.papels = papels;
    }

    public Perfil papels(Set<Papel> papels) {
        this.setPapels(papels);
        return this;
    }

    public Perfil addPapel(Papel papel) {
        this.papels.add(papel);
        papel.setPerfil(this);
        return this;
    }

    public Perfil removePapel(Papel papel) {
        this.papels.remove(papel);
        papel.setPerfil(null);
        return this;
    }

    public Set<Curso> getCursos() {
        return this.cursos;
    }

    public void setCursos(Set<Curso> cursos) {
        if (this.cursos != null) {
            this.cursos.forEach(i -> i.setPerfil(null));
        }
        if (cursos != null) {
            cursos.forEach(i -> i.setPerfil(this));
        }
        this.cursos = cursos;
    }

    public Perfil cursos(Set<Curso> cursos) {
        this.setCursos(cursos);
        return this;
    }

    public Perfil addCurso(Curso curso) {
        this.cursos.add(curso);
        curso.setPerfil(this);
        return this;
    }

    public Perfil removeCurso(Curso curso) {
        this.cursos.remove(curso);
        curso.setPerfil(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Perfil)) {
            return false;
        }
        return getId() != null && getId().equals(((Perfil) o).getId());
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Perfil{" +
            "id=" + getId() +
            ", descricao='" + getDescricao() + "'" +
            "}";
    }
}
