package br.edu.up.matrix.repository;

import br.edu.up.matrix.domain.Conhecimento;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the Conhecimento entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ConhecimentoRepository extends JpaRepository<Conhecimento, Integer> {}
