package br.edu.up.matrix.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Diretriz.
 */
@Entity
@Table(name = "diretriz")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Diretriz implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "descricao")
    private String descricao;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "diretriz")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "atitudes", "habilidades", "conhecimentos", "diretriz", "papel" }, allowSetters = true)
    private Set<Competencia> competencias = new HashSet<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties(value = { "diretrizs" }, allowSetters = true)
    private DCN dCN;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Integer getId() {
        return this.id;
    }

    public Diretriz id(Integer id) {
        this.setId(id);
        return this;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescricao() {
        return this.descricao;
    }

    public Diretriz descricao(String descricao) {
        this.setDescricao(descricao);
        return this;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Set<Competencia> getCompetencias() {
        return this.competencias;
    }

    public void setCompetencias(Set<Competencia> competencias) {
        if (this.competencias != null) {
            this.competencias.forEach(i -> i.setDiretriz(null));
        }
        if (competencias != null) {
            competencias.forEach(i -> i.setDiretriz(this));
        }
        this.competencias = competencias;
    }

    public Diretriz competencias(Set<Competencia> competencias) {
        this.setCompetencias(competencias);
        return this;
    }

    public Diretriz addCompetencia(Competencia competencia) {
        this.competencias.add(competencia);
        competencia.setDiretriz(this);
        return this;
    }

    public Diretriz removeCompetencia(Competencia competencia) {
        this.competencias.remove(competencia);
        competencia.setDiretriz(null);
        return this;
    }

    public DCN getDCN() {
        return this.dCN;
    }

    public void setDCN(DCN dCN) {
        this.dCN = dCN;
    }

    public Diretriz dCN(DCN dCN) {
        this.setDCN(dCN);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Diretriz)) {
            return false;
        }
        return getId() != null && getId().equals(((Diretriz) o).getId());
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Diretriz{" +
            "id=" + getId() +
            ", descricao='" + getDescricao() + "'" +
            "}";
    }
}
