package br.edu.up.matrix.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Periodo.
 */
@Entity
@Table(name = "periodo")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Periodo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "descricao")
    private String descricao;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "periodo")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "periodo", "trilha" }, allowSetters = true)
    private Set<Disciplina> disciplinas = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "periodo")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "disciplinas", "unidades", "conteudo", "periodo" }, allowSetters = true)
    private Set<Trilha> trilhas = new HashSet<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties(value = { "periodos", "matrizes" }, allowSetters = true)
    private Matriz matriz;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Integer getId() {
        return this.id;
    }

    public Periodo id(Integer id) {
        this.setId(id);
        return this;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescricao() {
        return this.descricao;
    }

    public Periodo descricao(String descricao) {
        this.setDescricao(descricao);
        return this;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Set<Disciplina> getDisciplinas() {
        return this.disciplinas;
    }

    public void setDisciplinas(Set<Disciplina> disciplinas) {
        if (this.disciplinas != null) {
            this.disciplinas.forEach(i -> i.setPeriodo(null));
        }
        if (disciplinas != null) {
            disciplinas.forEach(i -> i.setPeriodo(this));
        }
        this.disciplinas = disciplinas;
    }

    public Periodo disciplinas(Set<Disciplina> disciplinas) {
        this.setDisciplinas(disciplinas);
        return this;
    }

    public Periodo addDisciplina(Disciplina disciplina) {
        this.disciplinas.add(disciplina);
        disciplina.setPeriodo(this);
        return this;
    }

    public Periodo removeDisciplina(Disciplina disciplina) {
        this.disciplinas.remove(disciplina);
        disciplina.setPeriodo(null);
        return this;
    }

    public Set<Trilha> getTrilhas() {
        return this.trilhas;
    }

    public void setTrilhas(Set<Trilha> trilhas) {
        if (this.trilhas != null) {
            this.trilhas.forEach(i -> i.setPeriodo(null));
        }
        if (trilhas != null) {
            trilhas.forEach(i -> i.setPeriodo(this));
        }
        this.trilhas = trilhas;
    }

    public Periodo trilhas(Set<Trilha> trilhas) {
        this.setTrilhas(trilhas);
        return this;
    }

    public Periodo addTrilha(Trilha trilha) {
        this.trilhas.add(trilha);
        trilha.setPeriodo(this);
        return this;
    }

    public Periodo removeTrilha(Trilha trilha) {
        this.trilhas.remove(trilha);
        trilha.setPeriodo(null);
        return this;
    }

    public Matriz getMatriz() {
        return this.matriz;
    }

    public void setMatriz(Matriz matriz) {
        this.matriz = matriz;
    }

    public Periodo matriz(Matriz matriz) {
        this.setMatriz(matriz);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Periodo)) {
            return false;
        }
        return getId() != null && getId().equals(((Periodo) o).getId());
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Periodo{" +
            "id=" + getId() +
            ", descricao='" + getDescricao() + "'" +
            "}";
    }
}
