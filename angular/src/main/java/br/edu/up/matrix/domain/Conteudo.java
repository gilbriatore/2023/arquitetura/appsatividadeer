package br.edu.up.matrix.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Conteudo.
 */
@Entity
@Table(name = "conteudo")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Conteudo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "descricao")
    private String descricao;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "geral")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "conteudos", "trilhas", "conhecimento", "geral" }, allowSetters = true)
    private Set<Conteudo> conteudos = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "conteudo")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "disciplinas", "unidades", "conteudo", "periodo" }, allowSetters = true)
    private Set<Trilha> trilhas = new HashSet<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties(value = { "conteudos", "competencia", "bloom" }, allowSetters = true)
    private Conhecimento conhecimento;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties(value = { "conteudos", "trilhas", "conhecimento", "geral" }, allowSetters = true)
    private Conteudo geral;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Integer getId() {
        return this.id;
    }

    public Conteudo id(Integer id) {
        this.setId(id);
        return this;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescricao() {
        return this.descricao;
    }

    public Conteudo descricao(String descricao) {
        this.setDescricao(descricao);
        return this;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Set<Conteudo> getConteudos() {
        return this.conteudos;
    }

    public void setConteudos(Set<Conteudo> conteudos) {
        if (this.conteudos != null) {
            this.conteudos.forEach(i -> i.setGeral(null));
        }
        if (conteudos != null) {
            conteudos.forEach(i -> i.setGeral(this));
        }
        this.conteudos = conteudos;
    }

    public Conteudo conteudos(Set<Conteudo> conteudos) {
        this.setConteudos(conteudos);
        return this;
    }

    public Conteudo addConteudo(Conteudo conteudo) {
        this.conteudos.add(conteudo);
        conteudo.setGeral(this);
        return this;
    }

    public Conteudo removeConteudo(Conteudo conteudo) {
        this.conteudos.remove(conteudo);
        conteudo.setGeral(null);
        return this;
    }

    public Set<Trilha> getTrilhas() {
        return this.trilhas;
    }

    public void setTrilhas(Set<Trilha> trilhas) {
        if (this.trilhas != null) {
            this.trilhas.forEach(i -> i.setConteudo(null));
        }
        if (trilhas != null) {
            trilhas.forEach(i -> i.setConteudo(this));
        }
        this.trilhas = trilhas;
    }

    public Conteudo trilhas(Set<Trilha> trilhas) {
        this.setTrilhas(trilhas);
        return this;
    }

    public Conteudo addTrilha(Trilha trilha) {
        this.trilhas.add(trilha);
        trilha.setConteudo(this);
        return this;
    }

    public Conteudo removeTrilha(Trilha trilha) {
        this.trilhas.remove(trilha);
        trilha.setConteudo(null);
        return this;
    }

    public Conhecimento getConhecimento() {
        return this.conhecimento;
    }

    public void setConhecimento(Conhecimento conhecimento) {
        this.conhecimento = conhecimento;
    }

    public Conteudo conhecimento(Conhecimento conhecimento) {
        this.setConhecimento(conhecimento);
        return this;
    }

    public Conteudo getGeral() {
        return this.geral;
    }

    public void setGeral(Conteudo conteudo) {
        this.geral = conteudo;
    }

    public Conteudo geral(Conteudo conteudo) {
        this.setGeral(conteudo);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Conteudo)) {
            return false;
        }
        return getId() != null && getId().equals(((Conteudo) o).getId());
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Conteudo{" +
            "id=" + getId() +
            ", descricao='" + getDescricao() + "'" +
            "}";
    }
}
