package br.edu.up.matrix.web.rest;

import br.edu.up.matrix.domain.Conhecimento;
import br.edu.up.matrix.repository.ConhecimentoRepository;
import br.edu.up.matrix.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link br.edu.up.matrix.domain.Conhecimento}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class ConhecimentoResource {

    private final Logger log = LoggerFactory.getLogger(ConhecimentoResource.class);

    private static final String ENTITY_NAME = "conhecimento";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ConhecimentoRepository conhecimentoRepository;

    public ConhecimentoResource(ConhecimentoRepository conhecimentoRepository) {
        this.conhecimentoRepository = conhecimentoRepository;
    }

    /**
     * {@code POST  /conhecimentos} : Create a new conhecimento.
     *
     * @param conhecimento the conhecimento to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new conhecimento, or with status {@code 400 (Bad Request)} if the conhecimento has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/conhecimentos")
    public ResponseEntity<Conhecimento> createConhecimento(@RequestBody Conhecimento conhecimento) throws URISyntaxException {
        log.debug("REST request to save Conhecimento : {}", conhecimento);
        if (conhecimento.getId() != null) {
            throw new BadRequestAlertException("A new conhecimento cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Conhecimento result = conhecimentoRepository.save(conhecimento);
        return ResponseEntity
            .created(new URI("/api/conhecimentos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /conhecimentos/:id} : Updates an existing conhecimento.
     *
     * @param id the id of the conhecimento to save.
     * @param conhecimento the conhecimento to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated conhecimento,
     * or with status {@code 400 (Bad Request)} if the conhecimento is not valid,
     * or with status {@code 500 (Internal Server Error)} if the conhecimento couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/conhecimentos/{id}")
    public ResponseEntity<Conhecimento> updateConhecimento(
        @PathVariable(value = "id", required = false) final Integer id,
        @RequestBody Conhecimento conhecimento
    ) throws URISyntaxException {
        log.debug("REST request to update Conhecimento : {}, {}", id, conhecimento);
        if (conhecimento.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, conhecimento.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!conhecimentoRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Conhecimento result = conhecimentoRepository.save(conhecimento);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, conhecimento.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /conhecimentos/:id} : Partial updates given fields of an existing conhecimento, field will ignore if it is null
     *
     * @param id the id of the conhecimento to save.
     * @param conhecimento the conhecimento to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated conhecimento,
     * or with status {@code 400 (Bad Request)} if the conhecimento is not valid,
     * or with status {@code 404 (Not Found)} if the conhecimento is not found,
     * or with status {@code 500 (Internal Server Error)} if the conhecimento couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/conhecimentos/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<Conhecimento> partialUpdateConhecimento(
        @PathVariable(value = "id", required = false) final Integer id,
        @RequestBody Conhecimento conhecimento
    ) throws URISyntaxException {
        log.debug("REST request to partial update Conhecimento partially : {}, {}", id, conhecimento);
        if (conhecimento.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, conhecimento.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!conhecimentoRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<Conhecimento> result = conhecimentoRepository
            .findById(conhecimento.getId())
            .map(existingConhecimento -> {
                if (conhecimento.getDescricao() != null) {
                    existingConhecimento.setDescricao(conhecimento.getDescricao());
                }

                return existingConhecimento;
            })
            .map(conhecimentoRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, conhecimento.getId().toString())
        );
    }

    /**
     * {@code GET  /conhecimentos} : get all the conhecimentos.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of conhecimentos in body.
     */
    @GetMapping("/conhecimentos")
    public List<Conhecimento> getAllConhecimentos() {
        log.debug("REST request to get all Conhecimentos");
        return conhecimentoRepository.findAll();
    }

    /**
     * {@code GET  /conhecimentos/:id} : get the "id" conhecimento.
     *
     * @param id the id of the conhecimento to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the conhecimento, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/conhecimentos/{id}")
    public ResponseEntity<Conhecimento> getConhecimento(@PathVariable Integer id) {
        log.debug("REST request to get Conhecimento : {}", id);
        Optional<Conhecimento> conhecimento = conhecimentoRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(conhecimento);
    }

    /**
     * {@code DELETE  /conhecimentos/:id} : delete the "id" conhecimento.
     *
     * @param id the id of the conhecimento to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/conhecimentos/{id}")
    public ResponseEntity<Void> deleteConhecimento(@PathVariable Integer id) {
        log.debug("REST request to delete Conhecimento : {}", id);
        conhecimentoRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
