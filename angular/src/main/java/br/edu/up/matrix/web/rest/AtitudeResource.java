package br.edu.up.matrix.web.rest;

import br.edu.up.matrix.domain.Atitude;
import br.edu.up.matrix.repository.AtitudeRepository;
import br.edu.up.matrix.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link br.edu.up.matrix.domain.Atitude}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class AtitudeResource {

    private final Logger log = LoggerFactory.getLogger(AtitudeResource.class);

    private static final String ENTITY_NAME = "atitude";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AtitudeRepository atitudeRepository;

    public AtitudeResource(AtitudeRepository atitudeRepository) {
        this.atitudeRepository = atitudeRepository;
    }

    /**
     * {@code POST  /atitudes} : Create a new atitude.
     *
     * @param atitude the atitude to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new atitude, or with status {@code 400 (Bad Request)} if the atitude has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/atitudes")
    public ResponseEntity<Atitude> createAtitude(@RequestBody Atitude atitude) throws URISyntaxException {
        log.debug("REST request to save Atitude : {}", atitude);
        if (atitude.getId() != null) {
            throw new BadRequestAlertException("A new atitude cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Atitude result = atitudeRepository.save(atitude);
        return ResponseEntity
            .created(new URI("/api/atitudes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /atitudes/:id} : Updates an existing atitude.
     *
     * @param id the id of the atitude to save.
     * @param atitude the atitude to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated atitude,
     * or with status {@code 400 (Bad Request)} if the atitude is not valid,
     * or with status {@code 500 (Internal Server Error)} if the atitude couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/atitudes/{id}")
    public ResponseEntity<Atitude> updateAtitude(
        @PathVariable(value = "id", required = false) final Integer id,
        @RequestBody Atitude atitude
    ) throws URISyntaxException {
        log.debug("REST request to update Atitude : {}, {}", id, atitude);
        if (atitude.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, atitude.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!atitudeRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Atitude result = atitudeRepository.save(atitude);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, atitude.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /atitudes/:id} : Partial updates given fields of an existing atitude, field will ignore if it is null
     *
     * @param id the id of the atitude to save.
     * @param atitude the atitude to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated atitude,
     * or with status {@code 400 (Bad Request)} if the atitude is not valid,
     * or with status {@code 404 (Not Found)} if the atitude is not found,
     * or with status {@code 500 (Internal Server Error)} if the atitude couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/atitudes/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<Atitude> partialUpdateAtitude(
        @PathVariable(value = "id", required = false) final Integer id,
        @RequestBody Atitude atitude
    ) throws URISyntaxException {
        log.debug("REST request to partial update Atitude partially : {}, {}", id, atitude);
        if (atitude.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, atitude.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!atitudeRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<Atitude> result = atitudeRepository
            .findById(atitude.getId())
            .map(existingAtitude -> {
                if (atitude.getDescricao() != null) {
                    existingAtitude.setDescricao(atitude.getDescricao());
                }

                return existingAtitude;
            })
            .map(atitudeRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, atitude.getId().toString())
        );
    }

    /**
     * {@code GET  /atitudes} : get all the atitudes.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of atitudes in body.
     */
    @GetMapping("/atitudes")
    public List<Atitude> getAllAtitudes() {
        log.debug("REST request to get all Atitudes");
        return atitudeRepository.findAll();
    }

    /**
     * {@code GET  /atitudes/:id} : get the "id" atitude.
     *
     * @param id the id of the atitude to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the atitude, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/atitudes/{id}")
    public ResponseEntity<Atitude> getAtitude(@PathVariable Integer id) {
        log.debug("REST request to get Atitude : {}", id);
        Optional<Atitude> atitude = atitudeRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(atitude);
    }

    /**
     * {@code DELETE  /atitudes/:id} : delete the "id" atitude.
     *
     * @param id the id of the atitude to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/atitudes/{id}")
    public ResponseEntity<Void> deleteAtitude(@PathVariable Integer id) {
        log.debug("REST request to delete Atitude : {}", id);
        atitudeRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
