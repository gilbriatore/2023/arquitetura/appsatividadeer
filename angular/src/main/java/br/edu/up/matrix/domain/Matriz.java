package br.edu.up.matrix.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Matriz.
 */
@Entity
@Table(name = "matriz")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Matriz implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "codigo")
    private String codigo;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "matriz")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "disciplinas", "trilhas", "matriz" }, allowSetters = true)
    private Set<Periodo> periodos = new HashSet<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties(value = { "matrizs", "perfil", "modalidade", "escola", "tipoDeCurso" }, allowSetters = true)
    private Curso matrizes;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Integer getId() {
        return this.id;
    }

    public Matriz id(Integer id) {
        this.setId(id);
        return this;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCodigo() {
        return this.codigo;
    }

    public Matriz codigo(String codigo) {
        this.setCodigo(codigo);
        return this;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Set<Periodo> getPeriodos() {
        return this.periodos;
    }

    public void setPeriodos(Set<Periodo> periodos) {
        if (this.periodos != null) {
            this.periodos.forEach(i -> i.setMatriz(null));
        }
        if (periodos != null) {
            periodos.forEach(i -> i.setMatriz(this));
        }
        this.periodos = periodos;
    }

    public Matriz periodos(Set<Periodo> periodos) {
        this.setPeriodos(periodos);
        return this;
    }

    public Matriz addPeriodo(Periodo periodo) {
        this.periodos.add(periodo);
        periodo.setMatriz(this);
        return this;
    }

    public Matriz removePeriodo(Periodo periodo) {
        this.periodos.remove(periodo);
        periodo.setMatriz(null);
        return this;
    }

    public Curso getMatrizes() {
        return this.matrizes;
    }

    public void setMatrizes(Curso curso) {
        this.matrizes = curso;
    }

    public Matriz matrizes(Curso curso) {
        this.setMatrizes(curso);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Matriz)) {
            return false;
        }
        return getId() != null && getId().equals(((Matriz) o).getId());
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Matriz{" +
            "id=" + getId() +
            ", codigo='" + getCodigo() + "'" +
            "}";
    }
}
