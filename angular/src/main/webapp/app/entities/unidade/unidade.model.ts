import { ITrilha } from 'app/entities/trilha/trilha.model';

export interface IUnidade {
  id: number;
  codigo?: string | null;
  trilha?: Pick<ITrilha, 'id'> | null;
}

export type NewUnidade = Omit<IUnidade, 'id'> & { id: null };
