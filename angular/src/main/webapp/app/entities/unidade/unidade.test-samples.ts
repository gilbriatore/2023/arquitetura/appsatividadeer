import { IUnidade, NewUnidade } from './unidade.model';

export const sampleWithRequiredData: IUnidade = {
  id: 18384,
};

export const sampleWithPartialData: IUnidade = {
  id: 18366,
};

export const sampleWithFullData: IUnidade = {
  id: 7379,
  codigo: 'split woolens',
};

export const sampleWithNewData: NewUnidade = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
