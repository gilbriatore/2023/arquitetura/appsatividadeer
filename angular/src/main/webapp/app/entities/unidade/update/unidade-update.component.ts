import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import SharedModule from 'app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ITrilha } from 'app/entities/trilha/trilha.model';
import { TrilhaService } from 'app/entities/trilha/service/trilha.service';
import { IUnidade } from '../unidade.model';
import { UnidadeService } from '../service/unidade.service';
import { UnidadeFormService, UnidadeFormGroup } from './unidade-form.service';

@Component({
  standalone: true,
  selector: 'jhi-unidade-update',
  templateUrl: './unidade-update.component.html',
  imports: [SharedModule, FormsModule, ReactiveFormsModule],
})
export class UnidadeUpdateComponent implements OnInit {
  isSaving = false;
  unidade: IUnidade | null = null;

  trilhasSharedCollection: ITrilha[] = [];

  editForm: UnidadeFormGroup = this.unidadeFormService.createUnidadeFormGroup();

  constructor(
    protected unidadeService: UnidadeService,
    protected unidadeFormService: UnidadeFormService,
    protected trilhaService: TrilhaService,
    protected activatedRoute: ActivatedRoute,
  ) {}

  compareTrilha = (o1: ITrilha | null, o2: ITrilha | null): boolean => this.trilhaService.compareTrilha(o1, o2);

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ unidade }) => {
      this.unidade = unidade;
      if (unidade) {
        this.updateForm(unidade);
      }

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const unidade = this.unidadeFormService.getUnidade(this.editForm);
    if (unidade.id !== null) {
      this.subscribeToSaveResponse(this.unidadeService.update(unidade));
    } else {
      this.subscribeToSaveResponse(this.unidadeService.create(unidade));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IUnidade>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(unidade: IUnidade): void {
    this.unidade = unidade;
    this.unidadeFormService.resetForm(this.editForm, unidade);

    this.trilhasSharedCollection = this.trilhaService.addTrilhaToCollectionIfMissing<ITrilha>(this.trilhasSharedCollection, unidade.trilha);
  }

  protected loadRelationshipsOptions(): void {
    this.trilhaService
      .query()
      .pipe(map((res: HttpResponse<ITrilha[]>) => res.body ?? []))
      .pipe(map((trilhas: ITrilha[]) => this.trilhaService.addTrilhaToCollectionIfMissing<ITrilha>(trilhas, this.unidade?.trilha)))
      .subscribe((trilhas: ITrilha[]) => (this.trilhasSharedCollection = trilhas));
  }
}
