import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { ITrilha } from 'app/entities/trilha/trilha.model';
import { TrilhaService } from 'app/entities/trilha/service/trilha.service';
import { UnidadeService } from '../service/unidade.service';
import { IUnidade } from '../unidade.model';
import { UnidadeFormService } from './unidade-form.service';

import { UnidadeUpdateComponent } from './unidade-update.component';

describe('Unidade Management Update Component', () => {
  let comp: UnidadeUpdateComponent;
  let fixture: ComponentFixture<UnidadeUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let unidadeFormService: UnidadeFormService;
  let unidadeService: UnidadeService;
  let trilhaService: TrilhaService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([]), UnidadeUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(UnidadeUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(UnidadeUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    unidadeFormService = TestBed.inject(UnidadeFormService);
    unidadeService = TestBed.inject(UnidadeService);
    trilhaService = TestBed.inject(TrilhaService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Trilha query and add missing value', () => {
      const unidade: IUnidade = { id: 456 };
      const trilha: ITrilha = { id: 1220 };
      unidade.trilha = trilha;

      const trilhaCollection: ITrilha[] = [{ id: 25741 }];
      jest.spyOn(trilhaService, 'query').mockReturnValue(of(new HttpResponse({ body: trilhaCollection })));
      const additionalTrilhas = [trilha];
      const expectedCollection: ITrilha[] = [...additionalTrilhas, ...trilhaCollection];
      jest.spyOn(trilhaService, 'addTrilhaToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ unidade });
      comp.ngOnInit();

      expect(trilhaService.query).toHaveBeenCalled();
      expect(trilhaService.addTrilhaToCollectionIfMissing).toHaveBeenCalledWith(
        trilhaCollection,
        ...additionalTrilhas.map(expect.objectContaining),
      );
      expect(comp.trilhasSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const unidade: IUnidade = { id: 456 };
      const trilha: ITrilha = { id: 3122 };
      unidade.trilha = trilha;

      activatedRoute.data = of({ unidade });
      comp.ngOnInit();

      expect(comp.trilhasSharedCollection).toContain(trilha);
      expect(comp.unidade).toEqual(unidade);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IUnidade>>();
      const unidade = { id: 123 };
      jest.spyOn(unidadeFormService, 'getUnidade').mockReturnValue(unidade);
      jest.spyOn(unidadeService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ unidade });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: unidade }));
      saveSubject.complete();

      // THEN
      expect(unidadeFormService.getUnidade).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(unidadeService.update).toHaveBeenCalledWith(expect.objectContaining(unidade));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IUnidade>>();
      const unidade = { id: 123 };
      jest.spyOn(unidadeFormService, 'getUnidade').mockReturnValue({ id: null });
      jest.spyOn(unidadeService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ unidade: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: unidade }));
      saveSubject.complete();

      // THEN
      expect(unidadeFormService.getUnidade).toHaveBeenCalled();
      expect(unidadeService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IUnidade>>();
      const unidade = { id: 123 };
      jest.spyOn(unidadeService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ unidade });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(unidadeService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Compare relationships', () => {
    describe('compareTrilha', () => {
      it('Should forward to trilhaService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(trilhaService, 'compareTrilha');
        comp.compareTrilha(entity, entity2);
        expect(trilhaService.compareTrilha).toHaveBeenCalledWith(entity, entity2);
      });
    });
  });
});
