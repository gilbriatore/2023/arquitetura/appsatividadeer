import { Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { ASC } from 'app/config/navigation.constants';
import { DiretrizComponent } from './list/diretriz.component';
import { DiretrizDetailComponent } from './detail/diretriz-detail.component';
import { DiretrizUpdateComponent } from './update/diretriz-update.component';
import DiretrizResolve from './route/diretriz-routing-resolve.service';

const diretrizRoute: Routes = [
  {
    path: '',
    component: DiretrizComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: DiretrizDetailComponent,
    resolve: {
      diretriz: DiretrizResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: DiretrizUpdateComponent,
    resolve: {
      diretriz: DiretrizResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: DiretrizUpdateComponent,
    resolve: {
      diretriz: DiretrizResolve,
    },
    canActivate: [UserRouteAccessService],
  },
];

export default diretrizRoute;
