import { IDCN } from 'app/entities/dcn/dcn.model';

export interface IDiretriz {
  id: number;
  descricao?: string | null;
  dCN?: Pick<IDCN, 'id'> | null;
}

export type NewDiretriz = Omit<IDiretriz, 'id'> & { id: null };
