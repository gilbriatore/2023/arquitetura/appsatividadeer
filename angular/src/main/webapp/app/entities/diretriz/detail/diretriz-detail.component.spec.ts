import { TestBed } from '@angular/core/testing';
import { provideRouter, withComponentInputBinding } from '@angular/router';
import { RouterTestingHarness, RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { DiretrizDetailComponent } from './diretriz-detail.component';

describe('Diretriz Management Detail Component', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [DiretrizDetailComponent, RouterTestingModule.withRoutes([], { bindToComponentInputs: true })],
      providers: [
        provideRouter(
          [
            {
              path: '**',
              component: DiretrizDetailComponent,
              resolve: { diretriz: () => of({ id: 123 }) },
            },
          ],
          withComponentInputBinding(),
        ),
      ],
    })
      .overrideTemplate(DiretrizDetailComponent, '')
      .compileComponents();
  });

  describe('OnInit', () => {
    it('Should load diretriz on init', async () => {
      const harness = await RouterTestingHarness.create();
      const instance = await harness.navigateByUrl('/', DiretrizDetailComponent);

      // THEN
      expect(instance.diretriz).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
