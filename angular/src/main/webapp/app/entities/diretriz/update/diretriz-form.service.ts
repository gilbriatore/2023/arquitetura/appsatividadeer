import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { IDiretriz, NewDiretriz } from '../diretriz.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts IDiretriz for edit and NewDiretrizFormGroupInput for create.
 */
type DiretrizFormGroupInput = IDiretriz | PartialWithRequiredKeyOf<NewDiretriz>;

type DiretrizFormDefaults = Pick<NewDiretriz, 'id'>;

type DiretrizFormGroupContent = {
  id: FormControl<IDiretriz['id'] | NewDiretriz['id']>;
  descricao: FormControl<IDiretriz['descricao']>;
  dCN: FormControl<IDiretriz['dCN']>;
};

export type DiretrizFormGroup = FormGroup<DiretrizFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class DiretrizFormService {
  createDiretrizFormGroup(diretriz: DiretrizFormGroupInput = { id: null }): DiretrizFormGroup {
    const diretrizRawValue = {
      ...this.getFormDefaults(),
      ...diretriz,
    };
    return new FormGroup<DiretrizFormGroupContent>({
      id: new FormControl(
        { value: diretrizRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        },
      ),
      descricao: new FormControl(diretrizRawValue.descricao),
      dCN: new FormControl(diretrizRawValue.dCN),
    });
  }

  getDiretriz(form: DiretrizFormGroup): IDiretriz | NewDiretriz {
    return form.getRawValue() as IDiretriz | NewDiretriz;
  }

  resetForm(form: DiretrizFormGroup, diretriz: DiretrizFormGroupInput): void {
    const diretrizRawValue = { ...this.getFormDefaults(), ...diretriz };
    form.reset(
      {
        ...diretrizRawValue,
        id: { value: diretrizRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */,
    );
  }

  private getFormDefaults(): DiretrizFormDefaults {
    return {
      id: null,
    };
  }
}
