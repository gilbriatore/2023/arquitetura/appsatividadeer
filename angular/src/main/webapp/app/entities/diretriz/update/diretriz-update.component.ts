import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import SharedModule from 'app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IDCN } from 'app/entities/dcn/dcn.model';
import { DCNService } from 'app/entities/dcn/service/dcn.service';
import { IDiretriz } from '../diretriz.model';
import { DiretrizService } from '../service/diretriz.service';
import { DiretrizFormService, DiretrizFormGroup } from './diretriz-form.service';

@Component({
  standalone: true,
  selector: 'jhi-diretriz-update',
  templateUrl: './diretriz-update.component.html',
  imports: [SharedModule, FormsModule, ReactiveFormsModule],
})
export class DiretrizUpdateComponent implements OnInit {
  isSaving = false;
  diretriz: IDiretriz | null = null;

  dCNSSharedCollection: IDCN[] = [];

  editForm: DiretrizFormGroup = this.diretrizFormService.createDiretrizFormGroup();

  constructor(
    protected diretrizService: DiretrizService,
    protected diretrizFormService: DiretrizFormService,
    protected dCNService: DCNService,
    protected activatedRoute: ActivatedRoute,
  ) {}

  compareDCN = (o1: IDCN | null, o2: IDCN | null): boolean => this.dCNService.compareDCN(o1, o2);

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ diretriz }) => {
      this.diretriz = diretriz;
      if (diretriz) {
        this.updateForm(diretriz);
      }

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const diretriz = this.diretrizFormService.getDiretriz(this.editForm);
    if (diretriz.id !== null) {
      this.subscribeToSaveResponse(this.diretrizService.update(diretriz));
    } else {
      this.subscribeToSaveResponse(this.diretrizService.create(diretriz));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IDiretriz>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(diretriz: IDiretriz): void {
    this.diretriz = diretriz;
    this.diretrizFormService.resetForm(this.editForm, diretriz);

    this.dCNSSharedCollection = this.dCNService.addDCNToCollectionIfMissing<IDCN>(this.dCNSSharedCollection, diretriz.dCN);
  }

  protected loadRelationshipsOptions(): void {
    this.dCNService
      .query()
      .pipe(map((res: HttpResponse<IDCN[]>) => res.body ?? []))
      .pipe(map((dCNS: IDCN[]) => this.dCNService.addDCNToCollectionIfMissing<IDCN>(dCNS, this.diretriz?.dCN)))
      .subscribe((dCNS: IDCN[]) => (this.dCNSSharedCollection = dCNS));
  }
}
