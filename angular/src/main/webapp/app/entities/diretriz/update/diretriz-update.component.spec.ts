import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { IDCN } from 'app/entities/dcn/dcn.model';
import { DCNService } from 'app/entities/dcn/service/dcn.service';
import { DiretrizService } from '../service/diretriz.service';
import { IDiretriz } from '../diretriz.model';
import { DiretrizFormService } from './diretriz-form.service';

import { DiretrizUpdateComponent } from './diretriz-update.component';

describe('Diretriz Management Update Component', () => {
  let comp: DiretrizUpdateComponent;
  let fixture: ComponentFixture<DiretrizUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let diretrizFormService: DiretrizFormService;
  let diretrizService: DiretrizService;
  let dCNService: DCNService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([]), DiretrizUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(DiretrizUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(DiretrizUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    diretrizFormService = TestBed.inject(DiretrizFormService);
    diretrizService = TestBed.inject(DiretrizService);
    dCNService = TestBed.inject(DCNService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call DCN query and add missing value', () => {
      const diretriz: IDiretriz = { id: 456 };
      const dCN: IDCN = { id: 13003 };
      diretriz.dCN = dCN;

      const dCNCollection: IDCN[] = [{ id: 20586 }];
      jest.spyOn(dCNService, 'query').mockReturnValue(of(new HttpResponse({ body: dCNCollection })));
      const additionalDCNS = [dCN];
      const expectedCollection: IDCN[] = [...additionalDCNS, ...dCNCollection];
      jest.spyOn(dCNService, 'addDCNToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ diretriz });
      comp.ngOnInit();

      expect(dCNService.query).toHaveBeenCalled();
      expect(dCNService.addDCNToCollectionIfMissing).toHaveBeenCalledWith(dCNCollection, ...additionalDCNS.map(expect.objectContaining));
      expect(comp.dCNSSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const diretriz: IDiretriz = { id: 456 };
      const dCN: IDCN = { id: 7180 };
      diretriz.dCN = dCN;

      activatedRoute.data = of({ diretriz });
      comp.ngOnInit();

      expect(comp.dCNSSharedCollection).toContain(dCN);
      expect(comp.diretriz).toEqual(diretriz);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IDiretriz>>();
      const diretriz = { id: 123 };
      jest.spyOn(diretrizFormService, 'getDiretriz').mockReturnValue(diretriz);
      jest.spyOn(diretrizService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ diretriz });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: diretriz }));
      saveSubject.complete();

      // THEN
      expect(diretrizFormService.getDiretriz).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(diretrizService.update).toHaveBeenCalledWith(expect.objectContaining(diretriz));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IDiretriz>>();
      const diretriz = { id: 123 };
      jest.spyOn(diretrizFormService, 'getDiretriz').mockReturnValue({ id: null });
      jest.spyOn(diretrizService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ diretriz: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: diretriz }));
      saveSubject.complete();

      // THEN
      expect(diretrizFormService.getDiretriz).toHaveBeenCalled();
      expect(diretrizService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IDiretriz>>();
      const diretriz = { id: 123 };
      jest.spyOn(diretrizService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ diretriz });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(diretrizService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Compare relationships', () => {
    describe('compareDCN', () => {
      it('Should forward to dCNService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(dCNService, 'compareDCN');
        comp.compareDCN(entity, entity2);
        expect(dCNService.compareDCN).toHaveBeenCalledWith(entity, entity2);
      });
    });
  });
});
