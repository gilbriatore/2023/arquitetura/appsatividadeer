import { TestBed } from '@angular/core/testing';

import { sampleWithRequiredData, sampleWithNewData } from '../diretriz.test-samples';

import { DiretrizFormService } from './diretriz-form.service';

describe('Diretriz Form Service', () => {
  let service: DiretrizFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DiretrizFormService);
  });

  describe('Service methods', () => {
    describe('createDiretrizFormGroup', () => {
      it('should create a new form with FormControl', () => {
        const formGroup = service.createDiretrizFormGroup();

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            descricao: expect.any(Object),
            dCN: expect.any(Object),
          }),
        );
      });

      it('passing IDiretriz should create a new form with FormGroup', () => {
        const formGroup = service.createDiretrizFormGroup(sampleWithRequiredData);

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            descricao: expect.any(Object),
            dCN: expect.any(Object),
          }),
        );
      });
    });

    describe('getDiretriz', () => {
      it('should return NewDiretriz for default Diretriz initial value', () => {
        const formGroup = service.createDiretrizFormGroup(sampleWithNewData);

        const diretriz = service.getDiretriz(formGroup) as any;

        expect(diretriz).toMatchObject(sampleWithNewData);
      });

      it('should return NewDiretriz for empty Diretriz initial value', () => {
        const formGroup = service.createDiretrizFormGroup();

        const diretriz = service.getDiretriz(formGroup) as any;

        expect(diretriz).toMatchObject({});
      });

      it('should return IDiretriz', () => {
        const formGroup = service.createDiretrizFormGroup(sampleWithRequiredData);

        const diretriz = service.getDiretriz(formGroup) as any;

        expect(diretriz).toMatchObject(sampleWithRequiredData);
      });
    });

    describe('resetForm', () => {
      it('passing IDiretriz should not enable id FormControl', () => {
        const formGroup = service.createDiretrizFormGroup();
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, sampleWithRequiredData);

        expect(formGroup.controls.id.disabled).toBe(true);
      });

      it('passing NewDiretriz should disable id FormControl', () => {
        const formGroup = service.createDiretrizFormGroup(sampleWithRequiredData);
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, { id: null });

        expect(formGroup.controls.id.disabled).toBe(true);
      });
    });
  });
});
