import { IDiretriz, NewDiretriz } from './diretriz.model';

export const sampleWithRequiredData: IDiretriz = {
  id: 30587,
};

export const sampleWithPartialData: IDiretriz = {
  id: 19723,
};

export const sampleWithFullData: IDiretriz = {
  id: 1461,
  descricao: 'equally farewell',
};

export const sampleWithNewData: NewDiretriz = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
