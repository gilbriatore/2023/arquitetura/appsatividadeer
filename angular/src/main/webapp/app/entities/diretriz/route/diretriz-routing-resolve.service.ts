import { inject } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of, EMPTY, Observable } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IDiretriz } from '../diretriz.model';
import { DiretrizService } from '../service/diretriz.service';

export const diretrizResolve = (route: ActivatedRouteSnapshot): Observable<null | IDiretriz> => {
  const id = route.params['id'];
  if (id) {
    return inject(DiretrizService)
      .find(id)
      .pipe(
        mergeMap((diretriz: HttpResponse<IDiretriz>) => {
          if (diretriz.body) {
            return of(diretriz.body);
          } else {
            inject(Router).navigate(['404']);
            return EMPTY;
          }
        }),
      );
  }
  return of(null);
};

export default diretrizResolve;
