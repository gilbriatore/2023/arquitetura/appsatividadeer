import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { DiretrizService } from '../service/diretriz.service';

import { DiretrizComponent } from './diretriz.component';

describe('Diretriz Management Component', () => {
  let comp: DiretrizComponent;
  let fixture: ComponentFixture<DiretrizComponent>;
  let service: DiretrizService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes([{ path: 'diretriz', component: DiretrizComponent }]),
        HttpClientTestingModule,
        DiretrizComponent,
      ],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            data: of({
              defaultSort: 'id,asc',
            }),
            queryParamMap: of(
              jest.requireActual('@angular/router').convertToParamMap({
                page: '1',
                size: '1',
                sort: 'id,desc',
              }),
            ),
            snapshot: { queryParams: {} },
          },
        },
      ],
    })
      .overrideTemplate(DiretrizComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(DiretrizComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(DiretrizService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        }),
      ),
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.diretrizs?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });

  describe('trackId', () => {
    it('Should forward to diretrizService', () => {
      const entity = { id: 123 };
      jest.spyOn(service, 'getDiretrizIdentifier');
      const id = comp.trackId(0, entity);
      expect(service.getDiretrizIdentifier).toHaveBeenCalledWith(entity);
      expect(id).toBe(entity.id);
    });
  });
});
