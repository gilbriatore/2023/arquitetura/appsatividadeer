import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IDiretriz, NewDiretriz } from '../diretriz.model';

export type PartialUpdateDiretriz = Partial<IDiretriz> & Pick<IDiretriz, 'id'>;

export type EntityResponseType = HttpResponse<IDiretriz>;
export type EntityArrayResponseType = HttpResponse<IDiretriz[]>;

@Injectable({ providedIn: 'root' })
export class DiretrizService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/diretrizs');

  constructor(
    protected http: HttpClient,
    protected applicationConfigService: ApplicationConfigService,
  ) {}

  create(diretriz: NewDiretriz): Observable<EntityResponseType> {
    return this.http.post<IDiretriz>(this.resourceUrl, diretriz, { observe: 'response' });
  }

  update(diretriz: IDiretriz): Observable<EntityResponseType> {
    return this.http.put<IDiretriz>(`${this.resourceUrl}/${this.getDiretrizIdentifier(diretriz)}`, diretriz, { observe: 'response' });
  }

  partialUpdate(diretriz: PartialUpdateDiretriz): Observable<EntityResponseType> {
    return this.http.patch<IDiretriz>(`${this.resourceUrl}/${this.getDiretrizIdentifier(diretriz)}`, diretriz, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IDiretriz>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IDiretriz[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getDiretrizIdentifier(diretriz: Pick<IDiretriz, 'id'>): number {
    return diretriz.id;
  }

  compareDiretriz(o1: Pick<IDiretriz, 'id'> | null, o2: Pick<IDiretriz, 'id'> | null): boolean {
    return o1 && o2 ? this.getDiretrizIdentifier(o1) === this.getDiretrizIdentifier(o2) : o1 === o2;
  }

  addDiretrizToCollectionIfMissing<Type extends Pick<IDiretriz, 'id'>>(
    diretrizCollection: Type[],
    ...diretrizsToCheck: (Type | null | undefined)[]
  ): Type[] {
    const diretrizs: Type[] = diretrizsToCheck.filter(isPresent);
    if (diretrizs.length > 0) {
      const diretrizCollectionIdentifiers = diretrizCollection.map(diretrizItem => this.getDiretrizIdentifier(diretrizItem)!);
      const diretrizsToAdd = diretrizs.filter(diretrizItem => {
        const diretrizIdentifier = this.getDiretrizIdentifier(diretrizItem);
        if (diretrizCollectionIdentifiers.includes(diretrizIdentifier)) {
          return false;
        }
        diretrizCollectionIdentifiers.push(diretrizIdentifier);
        return true;
      });
      return [...diretrizsToAdd, ...diretrizCollection];
    }
    return diretrizCollection;
  }
}
