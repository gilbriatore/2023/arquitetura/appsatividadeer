import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IDiretriz } from '../diretriz.model';
import { sampleWithRequiredData, sampleWithNewData, sampleWithPartialData, sampleWithFullData } from '../diretriz.test-samples';

import { DiretrizService } from './diretriz.service';

const requireRestSample: IDiretriz = {
  ...sampleWithRequiredData,
};

describe('Diretriz Service', () => {
  let service: DiretrizService;
  let httpMock: HttpTestingController;
  let expectedResult: IDiretriz | IDiretriz[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(DiretrizService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should create a Diretriz', () => {
      const diretriz = { ...sampleWithNewData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.create(diretriz).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a Diretriz', () => {
      const diretriz = { ...sampleWithRequiredData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.update(diretriz).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a Diretriz', () => {
      const patchObject = { ...sampleWithPartialData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of Diretriz', () => {
      const returnedFromService = { ...requireRestSample };

      const expected = { ...sampleWithRequiredData };

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toMatchObject([expected]);
    });

    it('should delete a Diretriz', () => {
      const expected = true;

      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult).toBe(expected);
    });

    describe('addDiretrizToCollectionIfMissing', () => {
      it('should add a Diretriz to an empty array', () => {
        const diretriz: IDiretriz = sampleWithRequiredData;
        expectedResult = service.addDiretrizToCollectionIfMissing([], diretriz);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(diretriz);
      });

      it('should not add a Diretriz to an array that contains it', () => {
        const diretriz: IDiretriz = sampleWithRequiredData;
        const diretrizCollection: IDiretriz[] = [
          {
            ...diretriz,
          },
          sampleWithPartialData,
        ];
        expectedResult = service.addDiretrizToCollectionIfMissing(diretrizCollection, diretriz);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a Diretriz to an array that doesn't contain it", () => {
        const diretriz: IDiretriz = sampleWithRequiredData;
        const diretrizCollection: IDiretriz[] = [sampleWithPartialData];
        expectedResult = service.addDiretrizToCollectionIfMissing(diretrizCollection, diretriz);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(diretriz);
      });

      it('should add only unique Diretriz to an array', () => {
        const diretrizArray: IDiretriz[] = [sampleWithRequiredData, sampleWithPartialData, sampleWithFullData];
        const diretrizCollection: IDiretriz[] = [sampleWithRequiredData];
        expectedResult = service.addDiretrizToCollectionIfMissing(diretrizCollection, ...diretrizArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const diretriz: IDiretriz = sampleWithRequiredData;
        const diretriz2: IDiretriz = sampleWithPartialData;
        expectedResult = service.addDiretrizToCollectionIfMissing([], diretriz, diretriz2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(diretriz);
        expect(expectedResult).toContain(diretriz2);
      });

      it('should accept null and undefined values', () => {
        const diretriz: IDiretriz = sampleWithRequiredData;
        expectedResult = service.addDiretrizToCollectionIfMissing([], null, diretriz, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(diretriz);
      });

      it('should return initial array if no Diretriz is added', () => {
        const diretrizCollection: IDiretriz[] = [sampleWithRequiredData];
        expectedResult = service.addDiretrizToCollectionIfMissing(diretrizCollection, undefined, null);
        expect(expectedResult).toEqual(diretrizCollection);
      });
    });

    describe('compareDiretriz', () => {
      it('Should return true if both entities are null', () => {
        const entity1 = null;
        const entity2 = null;

        const compareResult = service.compareDiretriz(entity1, entity2);

        expect(compareResult).toEqual(true);
      });

      it('Should return false if one entity is null', () => {
        const entity1 = { id: 123 };
        const entity2 = null;

        const compareResult1 = service.compareDiretriz(entity1, entity2);
        const compareResult2 = service.compareDiretriz(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey differs', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 456 };

        const compareResult1 = service.compareDiretriz(entity1, entity2);
        const compareResult2 = service.compareDiretriz(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey matches', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 123 };

        const compareResult1 = service.compareDiretriz(entity1, entity2);
        const compareResult2 = service.compareDiretriz(entity2, entity1);

        expect(compareResult1).toEqual(true);
        expect(compareResult2).toEqual(true);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
