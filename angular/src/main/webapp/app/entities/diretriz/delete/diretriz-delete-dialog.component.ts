import { Component } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import SharedModule from 'app/shared/shared.module';
import { ITEM_DELETED_EVENT } from 'app/config/navigation.constants';
import { IDiretriz } from '../diretriz.model';
import { DiretrizService } from '../service/diretriz.service';

@Component({
  standalone: true,
  templateUrl: './diretriz-delete-dialog.component.html',
  imports: [SharedModule, FormsModule],
})
export class DiretrizDeleteDialogComponent {
  diretriz?: IDiretriz;

  constructor(
    protected diretrizService: DiretrizService,
    protected activeModal: NgbActiveModal,
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.diretrizService.delete(id).subscribe(() => {
      this.activeModal.close(ITEM_DELETED_EVENT);
    });
  }
}
