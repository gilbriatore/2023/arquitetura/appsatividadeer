import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { IHabilidade, NewHabilidade } from '../habilidade.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts IHabilidade for edit and NewHabilidadeFormGroupInput for create.
 */
type HabilidadeFormGroupInput = IHabilidade | PartialWithRequiredKeyOf<NewHabilidade>;

type HabilidadeFormDefaults = Pick<NewHabilidade, 'id'>;

type HabilidadeFormGroupContent = {
  id: FormControl<IHabilidade['id'] | NewHabilidade['id']>;
  descricao: FormControl<IHabilidade['descricao']>;
  competencia: FormControl<IHabilidade['competencia']>;
  bloom: FormControl<IHabilidade['bloom']>;
};

export type HabilidadeFormGroup = FormGroup<HabilidadeFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class HabilidadeFormService {
  createHabilidadeFormGroup(habilidade: HabilidadeFormGroupInput = { id: null }): HabilidadeFormGroup {
    const habilidadeRawValue = {
      ...this.getFormDefaults(),
      ...habilidade,
    };
    return new FormGroup<HabilidadeFormGroupContent>({
      id: new FormControl(
        { value: habilidadeRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        },
      ),
      descricao: new FormControl(habilidadeRawValue.descricao),
      competencia: new FormControl(habilidadeRawValue.competencia),
      bloom: new FormControl(habilidadeRawValue.bloom),
    });
  }

  getHabilidade(form: HabilidadeFormGroup): IHabilidade | NewHabilidade {
    return form.getRawValue() as IHabilidade | NewHabilidade;
  }

  resetForm(form: HabilidadeFormGroup, habilidade: HabilidadeFormGroupInput): void {
    const habilidadeRawValue = { ...this.getFormDefaults(), ...habilidade };
    form.reset(
      {
        ...habilidadeRawValue,
        id: { value: habilidadeRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */,
    );
  }

  private getFormDefaults(): HabilidadeFormDefaults {
    return {
      id: null,
    };
  }
}
