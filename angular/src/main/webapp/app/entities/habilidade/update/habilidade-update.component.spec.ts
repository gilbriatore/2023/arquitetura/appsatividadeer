import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { ICompetencia } from 'app/entities/competencia/competencia.model';
import { CompetenciaService } from 'app/entities/competencia/service/competencia.service';
import { IBloom } from 'app/entities/bloom/bloom.model';
import { BloomService } from 'app/entities/bloom/service/bloom.service';
import { IHabilidade } from '../habilidade.model';
import { HabilidadeService } from '../service/habilidade.service';
import { HabilidadeFormService } from './habilidade-form.service';

import { HabilidadeUpdateComponent } from './habilidade-update.component';

describe('Habilidade Management Update Component', () => {
  let comp: HabilidadeUpdateComponent;
  let fixture: ComponentFixture<HabilidadeUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let habilidadeFormService: HabilidadeFormService;
  let habilidadeService: HabilidadeService;
  let competenciaService: CompetenciaService;
  let bloomService: BloomService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([]), HabilidadeUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(HabilidadeUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(HabilidadeUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    habilidadeFormService = TestBed.inject(HabilidadeFormService);
    habilidadeService = TestBed.inject(HabilidadeService);
    competenciaService = TestBed.inject(CompetenciaService);
    bloomService = TestBed.inject(BloomService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Competencia query and add missing value', () => {
      const habilidade: IHabilidade = { id: 456 };
      const competencia: ICompetencia = { id: 4979 };
      habilidade.competencia = competencia;

      const competenciaCollection: ICompetencia[] = [{ id: 12241 }];
      jest.spyOn(competenciaService, 'query').mockReturnValue(of(new HttpResponse({ body: competenciaCollection })));
      const additionalCompetencias = [competencia];
      const expectedCollection: ICompetencia[] = [...additionalCompetencias, ...competenciaCollection];
      jest.spyOn(competenciaService, 'addCompetenciaToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ habilidade });
      comp.ngOnInit();

      expect(competenciaService.query).toHaveBeenCalled();
      expect(competenciaService.addCompetenciaToCollectionIfMissing).toHaveBeenCalledWith(
        competenciaCollection,
        ...additionalCompetencias.map(expect.objectContaining),
      );
      expect(comp.competenciasSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Bloom query and add missing value', () => {
      const habilidade: IHabilidade = { id: 456 };
      const bloom: IBloom = { id: 5732 };
      habilidade.bloom = bloom;

      const bloomCollection: IBloom[] = [{ id: 3398 }];
      jest.spyOn(bloomService, 'query').mockReturnValue(of(new HttpResponse({ body: bloomCollection })));
      const additionalBlooms = [bloom];
      const expectedCollection: IBloom[] = [...additionalBlooms, ...bloomCollection];
      jest.spyOn(bloomService, 'addBloomToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ habilidade });
      comp.ngOnInit();

      expect(bloomService.query).toHaveBeenCalled();
      expect(bloomService.addBloomToCollectionIfMissing).toHaveBeenCalledWith(
        bloomCollection,
        ...additionalBlooms.map(expect.objectContaining),
      );
      expect(comp.bloomsSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const habilidade: IHabilidade = { id: 456 };
      const competencia: ICompetencia = { id: 6449 };
      habilidade.competencia = competencia;
      const bloom: IBloom = { id: 29822 };
      habilidade.bloom = bloom;

      activatedRoute.data = of({ habilidade });
      comp.ngOnInit();

      expect(comp.competenciasSharedCollection).toContain(competencia);
      expect(comp.bloomsSharedCollection).toContain(bloom);
      expect(comp.habilidade).toEqual(habilidade);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IHabilidade>>();
      const habilidade = { id: 123 };
      jest.spyOn(habilidadeFormService, 'getHabilidade').mockReturnValue(habilidade);
      jest.spyOn(habilidadeService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ habilidade });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: habilidade }));
      saveSubject.complete();

      // THEN
      expect(habilidadeFormService.getHabilidade).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(habilidadeService.update).toHaveBeenCalledWith(expect.objectContaining(habilidade));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IHabilidade>>();
      const habilidade = { id: 123 };
      jest.spyOn(habilidadeFormService, 'getHabilidade').mockReturnValue({ id: null });
      jest.spyOn(habilidadeService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ habilidade: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: habilidade }));
      saveSubject.complete();

      // THEN
      expect(habilidadeFormService.getHabilidade).toHaveBeenCalled();
      expect(habilidadeService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IHabilidade>>();
      const habilidade = { id: 123 };
      jest.spyOn(habilidadeService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ habilidade });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(habilidadeService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Compare relationships', () => {
    describe('compareCompetencia', () => {
      it('Should forward to competenciaService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(competenciaService, 'compareCompetencia');
        comp.compareCompetencia(entity, entity2);
        expect(competenciaService.compareCompetencia).toHaveBeenCalledWith(entity, entity2);
      });
    });

    describe('compareBloom', () => {
      it('Should forward to bloomService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(bloomService, 'compareBloom');
        comp.compareBloom(entity, entity2);
        expect(bloomService.compareBloom).toHaveBeenCalledWith(entity, entity2);
      });
    });
  });
});
