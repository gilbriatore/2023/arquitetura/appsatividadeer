import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import SharedModule from 'app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ICompetencia } from 'app/entities/competencia/competencia.model';
import { CompetenciaService } from 'app/entities/competencia/service/competencia.service';
import { IBloom } from 'app/entities/bloom/bloom.model';
import { BloomService } from 'app/entities/bloom/service/bloom.service';
import { HabilidadeService } from '../service/habilidade.service';
import { IHabilidade } from '../habilidade.model';
import { HabilidadeFormService, HabilidadeFormGroup } from './habilidade-form.service';

@Component({
  standalone: true,
  selector: 'jhi-habilidade-update',
  templateUrl: './habilidade-update.component.html',
  imports: [SharedModule, FormsModule, ReactiveFormsModule],
})
export class HabilidadeUpdateComponent implements OnInit {
  isSaving = false;
  habilidade: IHabilidade | null = null;

  competenciasSharedCollection: ICompetencia[] = [];
  bloomsSharedCollection: IBloom[] = [];

  editForm: HabilidadeFormGroup = this.habilidadeFormService.createHabilidadeFormGroup();

  constructor(
    protected habilidadeService: HabilidadeService,
    protected habilidadeFormService: HabilidadeFormService,
    protected competenciaService: CompetenciaService,
    protected bloomService: BloomService,
    protected activatedRoute: ActivatedRoute,
  ) {}

  compareCompetencia = (o1: ICompetencia | null, o2: ICompetencia | null): boolean => this.competenciaService.compareCompetencia(o1, o2);

  compareBloom = (o1: IBloom | null, o2: IBloom | null): boolean => this.bloomService.compareBloom(o1, o2);

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ habilidade }) => {
      this.habilidade = habilidade;
      if (habilidade) {
        this.updateForm(habilidade);
      }

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const habilidade = this.habilidadeFormService.getHabilidade(this.editForm);
    if (habilidade.id !== null) {
      this.subscribeToSaveResponse(this.habilidadeService.update(habilidade));
    } else {
      this.subscribeToSaveResponse(this.habilidadeService.create(habilidade));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IHabilidade>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(habilidade: IHabilidade): void {
    this.habilidade = habilidade;
    this.habilidadeFormService.resetForm(this.editForm, habilidade);

    this.competenciasSharedCollection = this.competenciaService.addCompetenciaToCollectionIfMissing<ICompetencia>(
      this.competenciasSharedCollection,
      habilidade.competencia,
    );
    this.bloomsSharedCollection = this.bloomService.addBloomToCollectionIfMissing<IBloom>(this.bloomsSharedCollection, habilidade.bloom);
  }

  protected loadRelationshipsOptions(): void {
    this.competenciaService
      .query()
      .pipe(map((res: HttpResponse<ICompetencia[]>) => res.body ?? []))
      .pipe(
        map((competencias: ICompetencia[]) =>
          this.competenciaService.addCompetenciaToCollectionIfMissing<ICompetencia>(competencias, this.habilidade?.competencia),
        ),
      )
      .subscribe((competencias: ICompetencia[]) => (this.competenciasSharedCollection = competencias));

    this.bloomService
      .query()
      .pipe(map((res: HttpResponse<IBloom[]>) => res.body ?? []))
      .pipe(map((blooms: IBloom[]) => this.bloomService.addBloomToCollectionIfMissing<IBloom>(blooms, this.habilidade?.bloom)))
      .subscribe((blooms: IBloom[]) => (this.bloomsSharedCollection = blooms));
  }
}
