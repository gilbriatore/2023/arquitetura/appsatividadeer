import { TestBed } from '@angular/core/testing';

import { sampleWithRequiredData, sampleWithNewData } from '../habilidade.test-samples';

import { HabilidadeFormService } from './habilidade-form.service';

describe('Habilidade Form Service', () => {
  let service: HabilidadeFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HabilidadeFormService);
  });

  describe('Service methods', () => {
    describe('createHabilidadeFormGroup', () => {
      it('should create a new form with FormControl', () => {
        const formGroup = service.createHabilidadeFormGroup();

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            descricao: expect.any(Object),
            competencia: expect.any(Object),
            bloom: expect.any(Object),
          }),
        );
      });

      it('passing IHabilidade should create a new form with FormGroup', () => {
        const formGroup = service.createHabilidadeFormGroup(sampleWithRequiredData);

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            descricao: expect.any(Object),
            competencia: expect.any(Object),
            bloom: expect.any(Object),
          }),
        );
      });
    });

    describe('getHabilidade', () => {
      it('should return NewHabilidade for default Habilidade initial value', () => {
        const formGroup = service.createHabilidadeFormGroup(sampleWithNewData);

        const habilidade = service.getHabilidade(formGroup) as any;

        expect(habilidade).toMatchObject(sampleWithNewData);
      });

      it('should return NewHabilidade for empty Habilidade initial value', () => {
        const formGroup = service.createHabilidadeFormGroup();

        const habilidade = service.getHabilidade(formGroup) as any;

        expect(habilidade).toMatchObject({});
      });

      it('should return IHabilidade', () => {
        const formGroup = service.createHabilidadeFormGroup(sampleWithRequiredData);

        const habilidade = service.getHabilidade(formGroup) as any;

        expect(habilidade).toMatchObject(sampleWithRequiredData);
      });
    });

    describe('resetForm', () => {
      it('passing IHabilidade should not enable id FormControl', () => {
        const formGroup = service.createHabilidadeFormGroup();
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, sampleWithRequiredData);

        expect(formGroup.controls.id.disabled).toBe(true);
      });

      it('passing NewHabilidade should disable id FormControl', () => {
        const formGroup = service.createHabilidadeFormGroup(sampleWithRequiredData);
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, { id: null });

        expect(formGroup.controls.id.disabled).toBe(true);
      });
    });
  });
});
