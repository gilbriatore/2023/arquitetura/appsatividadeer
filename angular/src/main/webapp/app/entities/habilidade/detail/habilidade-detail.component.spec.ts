import { TestBed } from '@angular/core/testing';
import { provideRouter, withComponentInputBinding } from '@angular/router';
import { RouterTestingHarness, RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { HabilidadeDetailComponent } from './habilidade-detail.component';

describe('Habilidade Management Detail Component', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HabilidadeDetailComponent, RouterTestingModule.withRoutes([], { bindToComponentInputs: true })],
      providers: [
        provideRouter(
          [
            {
              path: '**',
              component: HabilidadeDetailComponent,
              resolve: { habilidade: () => of({ id: 123 }) },
            },
          ],
          withComponentInputBinding(),
        ),
      ],
    })
      .overrideTemplate(HabilidadeDetailComponent, '')
      .compileComponents();
  });

  describe('OnInit', () => {
    it('Should load habilidade on init', async () => {
      const harness = await RouterTestingHarness.create();
      const instance = await harness.navigateByUrl('/', HabilidadeDetailComponent);

      // THEN
      expect(instance.habilidade).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
