import { IHabilidade, NewHabilidade } from './habilidade.model';

export const sampleWithRequiredData: IHabilidade = {
  id: 26733,
};

export const sampleWithPartialData: IHabilidade = {
  id: 25116,
  descricao: 'pro',
};

export const sampleWithFullData: IHabilidade = {
  id: 13515,
  descricao: 'brr',
};

export const sampleWithNewData: NewHabilidade = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
