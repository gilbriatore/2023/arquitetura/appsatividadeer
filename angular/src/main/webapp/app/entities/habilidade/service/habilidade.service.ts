import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IHabilidade, NewHabilidade } from '../habilidade.model';

export type PartialUpdateHabilidade = Partial<IHabilidade> & Pick<IHabilidade, 'id'>;

export type EntityResponseType = HttpResponse<IHabilidade>;
export type EntityArrayResponseType = HttpResponse<IHabilidade[]>;

@Injectable({ providedIn: 'root' })
export class HabilidadeService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/habilidades');

  constructor(
    protected http: HttpClient,
    protected applicationConfigService: ApplicationConfigService,
  ) {}

  create(habilidade: NewHabilidade): Observable<EntityResponseType> {
    return this.http.post<IHabilidade>(this.resourceUrl, habilidade, { observe: 'response' });
  }

  update(habilidade: IHabilidade): Observable<EntityResponseType> {
    return this.http.put<IHabilidade>(`${this.resourceUrl}/${this.getHabilidadeIdentifier(habilidade)}`, habilidade, {
      observe: 'response',
    });
  }

  partialUpdate(habilidade: PartialUpdateHabilidade): Observable<EntityResponseType> {
    return this.http.patch<IHabilidade>(`${this.resourceUrl}/${this.getHabilidadeIdentifier(habilidade)}`, habilidade, {
      observe: 'response',
    });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IHabilidade>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IHabilidade[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getHabilidadeIdentifier(habilidade: Pick<IHabilidade, 'id'>): number {
    return habilidade.id;
  }

  compareHabilidade(o1: Pick<IHabilidade, 'id'> | null, o2: Pick<IHabilidade, 'id'> | null): boolean {
    return o1 && o2 ? this.getHabilidadeIdentifier(o1) === this.getHabilidadeIdentifier(o2) : o1 === o2;
  }

  addHabilidadeToCollectionIfMissing<Type extends Pick<IHabilidade, 'id'>>(
    habilidadeCollection: Type[],
    ...habilidadesToCheck: (Type | null | undefined)[]
  ): Type[] {
    const habilidades: Type[] = habilidadesToCheck.filter(isPresent);
    if (habilidades.length > 0) {
      const habilidadeCollectionIdentifiers = habilidadeCollection.map(habilidadeItem => this.getHabilidadeIdentifier(habilidadeItem)!);
      const habilidadesToAdd = habilidades.filter(habilidadeItem => {
        const habilidadeIdentifier = this.getHabilidadeIdentifier(habilidadeItem);
        if (habilidadeCollectionIdentifiers.includes(habilidadeIdentifier)) {
          return false;
        }
        habilidadeCollectionIdentifiers.push(habilidadeIdentifier);
        return true;
      });
      return [...habilidadesToAdd, ...habilidadeCollection];
    }
    return habilidadeCollection;
  }
}
