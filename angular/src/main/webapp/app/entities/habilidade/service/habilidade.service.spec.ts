import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IHabilidade } from '../habilidade.model';
import { sampleWithRequiredData, sampleWithNewData, sampleWithPartialData, sampleWithFullData } from '../habilidade.test-samples';

import { HabilidadeService } from './habilidade.service';

const requireRestSample: IHabilidade = {
  ...sampleWithRequiredData,
};

describe('Habilidade Service', () => {
  let service: HabilidadeService;
  let httpMock: HttpTestingController;
  let expectedResult: IHabilidade | IHabilidade[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(HabilidadeService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should create a Habilidade', () => {
      const habilidade = { ...sampleWithNewData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.create(habilidade).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a Habilidade', () => {
      const habilidade = { ...sampleWithRequiredData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.update(habilidade).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a Habilidade', () => {
      const patchObject = { ...sampleWithPartialData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of Habilidade', () => {
      const returnedFromService = { ...requireRestSample };

      const expected = { ...sampleWithRequiredData };

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toMatchObject([expected]);
    });

    it('should delete a Habilidade', () => {
      const expected = true;

      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult).toBe(expected);
    });

    describe('addHabilidadeToCollectionIfMissing', () => {
      it('should add a Habilidade to an empty array', () => {
        const habilidade: IHabilidade = sampleWithRequiredData;
        expectedResult = service.addHabilidadeToCollectionIfMissing([], habilidade);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(habilidade);
      });

      it('should not add a Habilidade to an array that contains it', () => {
        const habilidade: IHabilidade = sampleWithRequiredData;
        const habilidadeCollection: IHabilidade[] = [
          {
            ...habilidade,
          },
          sampleWithPartialData,
        ];
        expectedResult = service.addHabilidadeToCollectionIfMissing(habilidadeCollection, habilidade);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a Habilidade to an array that doesn't contain it", () => {
        const habilidade: IHabilidade = sampleWithRequiredData;
        const habilidadeCollection: IHabilidade[] = [sampleWithPartialData];
        expectedResult = service.addHabilidadeToCollectionIfMissing(habilidadeCollection, habilidade);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(habilidade);
      });

      it('should add only unique Habilidade to an array', () => {
        const habilidadeArray: IHabilidade[] = [sampleWithRequiredData, sampleWithPartialData, sampleWithFullData];
        const habilidadeCollection: IHabilidade[] = [sampleWithRequiredData];
        expectedResult = service.addHabilidadeToCollectionIfMissing(habilidadeCollection, ...habilidadeArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const habilidade: IHabilidade = sampleWithRequiredData;
        const habilidade2: IHabilidade = sampleWithPartialData;
        expectedResult = service.addHabilidadeToCollectionIfMissing([], habilidade, habilidade2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(habilidade);
        expect(expectedResult).toContain(habilidade2);
      });

      it('should accept null and undefined values', () => {
        const habilidade: IHabilidade = sampleWithRequiredData;
        expectedResult = service.addHabilidadeToCollectionIfMissing([], null, habilidade, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(habilidade);
      });

      it('should return initial array if no Habilidade is added', () => {
        const habilidadeCollection: IHabilidade[] = [sampleWithRequiredData];
        expectedResult = service.addHabilidadeToCollectionIfMissing(habilidadeCollection, undefined, null);
        expect(expectedResult).toEqual(habilidadeCollection);
      });
    });

    describe('compareHabilidade', () => {
      it('Should return true if both entities are null', () => {
        const entity1 = null;
        const entity2 = null;

        const compareResult = service.compareHabilidade(entity1, entity2);

        expect(compareResult).toEqual(true);
      });

      it('Should return false if one entity is null', () => {
        const entity1 = { id: 123 };
        const entity2 = null;

        const compareResult1 = service.compareHabilidade(entity1, entity2);
        const compareResult2 = service.compareHabilidade(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey differs', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 456 };

        const compareResult1 = service.compareHabilidade(entity1, entity2);
        const compareResult2 = service.compareHabilidade(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey matches', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 123 };

        const compareResult1 = service.compareHabilidade(entity1, entity2);
        const compareResult2 = service.compareHabilidade(entity2, entity1);

        expect(compareResult1).toEqual(true);
        expect(compareResult2).toEqual(true);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
