import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { HabilidadeService } from '../service/habilidade.service';

import { HabilidadeComponent } from './habilidade.component';

describe('Habilidade Management Component', () => {
  let comp: HabilidadeComponent;
  let fixture: ComponentFixture<HabilidadeComponent>;
  let service: HabilidadeService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes([{ path: 'habilidade', component: HabilidadeComponent }]),
        HttpClientTestingModule,
        HabilidadeComponent,
      ],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            data: of({
              defaultSort: 'id,asc',
            }),
            queryParamMap: of(
              jest.requireActual('@angular/router').convertToParamMap({
                page: '1',
                size: '1',
                sort: 'id,desc',
              }),
            ),
            snapshot: { queryParams: {} },
          },
        },
      ],
    })
      .overrideTemplate(HabilidadeComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(HabilidadeComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(HabilidadeService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        }),
      ),
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.habilidades?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });

  describe('trackId', () => {
    it('Should forward to habilidadeService', () => {
      const entity = { id: 123 };
      jest.spyOn(service, 'getHabilidadeIdentifier');
      const id = comp.trackId(0, entity);
      expect(service.getHabilidadeIdentifier).toHaveBeenCalledWith(entity);
      expect(id).toBe(entity.id);
    });
  });
});
