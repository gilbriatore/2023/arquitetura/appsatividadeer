import { inject } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of, EMPTY, Observable } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IHabilidade } from '../habilidade.model';
import { HabilidadeService } from '../service/habilidade.service';

export const habilidadeResolve = (route: ActivatedRouteSnapshot): Observable<null | IHabilidade> => {
  const id = route.params['id'];
  if (id) {
    return inject(HabilidadeService)
      .find(id)
      .pipe(
        mergeMap((habilidade: HttpResponse<IHabilidade>) => {
          if (habilidade.body) {
            return of(habilidade.body);
          } else {
            inject(Router).navigate(['404']);
            return EMPTY;
          }
        }),
      );
  }
  return of(null);
};

export default habilidadeResolve;
