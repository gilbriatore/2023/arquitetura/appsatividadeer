import { Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { ASC } from 'app/config/navigation.constants';
import { HabilidadeComponent } from './list/habilidade.component';
import { HabilidadeDetailComponent } from './detail/habilidade-detail.component';
import { HabilidadeUpdateComponent } from './update/habilidade-update.component';
import HabilidadeResolve from './route/habilidade-routing-resolve.service';

const habilidadeRoute: Routes = [
  {
    path: '',
    component: HabilidadeComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: HabilidadeDetailComponent,
    resolve: {
      habilidade: HabilidadeResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: HabilidadeUpdateComponent,
    resolve: {
      habilidade: HabilidadeResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: HabilidadeUpdateComponent,
    resolve: {
      habilidade: HabilidadeResolve,
    },
    canActivate: [UserRouteAccessService],
  },
];

export default habilidadeRoute;
