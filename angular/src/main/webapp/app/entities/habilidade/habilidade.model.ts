import { ICompetencia } from 'app/entities/competencia/competencia.model';
import { IBloom } from 'app/entities/bloom/bloom.model';

export interface IHabilidade {
  id: number;
  descricao?: string | null;
  competencia?: Pick<ICompetencia, 'id'> | null;
  bloom?: Pick<IBloom, 'id'> | null;
}

export type NewHabilidade = Omit<IHabilidade, 'id'> & { id: null };
