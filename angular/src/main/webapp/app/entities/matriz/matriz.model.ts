import { ICurso } from 'app/entities/curso/curso.model';

export interface IMatriz {
  id: number;
  codigo?: string | null;
  matrizes?: Pick<ICurso, 'id'> | null;
}

export type NewMatriz = Omit<IMatriz, 'id'> & { id: null };
