import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'atividade',
        data: { pageTitle: 'matrixApp.atividade.home.title' },
        loadChildren: () => import('./atividade/atividade.routes'),
      },
      {
        path: 'bloom',
        data: { pageTitle: 'matrixApp.bloom.home.title' },
        loadChildren: () => import('./bloom/bloom.routes'),
      },
      {
        path: 'curso',
        data: { pageTitle: 'matrixApp.curso.home.title' },
        loadChildren: () => import('./curso/curso.routes'),
      },
      {
        path: 'escola',
        data: { pageTitle: 'matrixApp.escola.home.title' },
        loadChildren: () => import('./escola/escola.routes'),
      },
      {
        path: 'matriz',
        data: { pageTitle: 'matrixApp.matriz.home.title' },
        loadChildren: () => import('./matriz/matriz.routes'),
      },
      {
        path: 'modalidade',
        data: { pageTitle: 'matrixApp.modalidade.home.title' },
        loadChildren: () => import('./modalidade/modalidade.routes'),
      },
      {
        path: 'tipo-de-curso',
        data: { pageTitle: 'matrixApp.tipoDeCurso.home.title' },
        loadChildren: () => import('./tipo-de-curso/tipo-de-curso.routes'),
      },
      {
        path: 'unidade',
        data: { pageTitle: 'matrixApp.unidade.home.title' },
        loadChildren: () => import('./unidade/unidade.routes'),
      },
      {
        path: 'atitude',
        data: { pageTitle: 'matrixApp.atitude.home.title' },
        loadChildren: () => import('./atitude/atitude.routes'),
      },
      {
        path: 'conhecimento',
        data: { pageTitle: 'matrixApp.conhecimento.home.title' },
        loadChildren: () => import('./conhecimento/conhecimento.routes'),
      },
      {
        path: 'competencia',
        data: { pageTitle: 'matrixApp.competencia.home.title' },
        loadChildren: () => import('./competencia/competencia.routes'),
      },
      {
        path: 'conteudo',
        data: { pageTitle: 'matrixApp.conteudo.home.title' },
        loadChildren: () => import('./conteudo/conteudo.routes'),
      },
      {
        path: 'disciplina',
        data: { pageTitle: 'matrixApp.disciplina.home.title' },
        loadChildren: () => import('./disciplina/disciplina.routes'),
      },
      {
        path: 'dcn',
        data: { pageTitle: 'matrixApp.dCN.home.title' },
        loadChildren: () => import('./dcn/dcn.routes'),
      },
      {
        path: 'diretriz',
        data: { pageTitle: 'matrixApp.diretriz.home.title' },
        loadChildren: () => import('./diretriz/diretriz.routes'),
      },
      {
        path: 'habilidade',
        data: { pageTitle: 'matrixApp.habilidade.home.title' },
        loadChildren: () => import('./habilidade/habilidade.routes'),
      },
      {
        path: 'papel',
        data: { pageTitle: 'matrixApp.papel.home.title' },
        loadChildren: () => import('./papel/papel.routes'),
      },
      {
        path: 'perfil',
        data: { pageTitle: 'matrixApp.perfil.home.title' },
        loadChildren: () => import('./perfil/perfil.routes'),
      },
      {
        path: 'periodo',
        data: { pageTitle: 'matrixApp.periodo.home.title' },
        loadChildren: () => import('./periodo/periodo.routes'),
      },
      {
        path: 'trilha',
        data: { pageTitle: 'matrixApp.trilha.home.title' },
        loadChildren: () => import('./trilha/trilha.routes'),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class EntityRoutingModule {}
