import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { IPeriodo } from 'app/entities/periodo/periodo.model';
import { PeriodoService } from 'app/entities/periodo/service/periodo.service';
import { ITrilha } from 'app/entities/trilha/trilha.model';
import { TrilhaService } from 'app/entities/trilha/service/trilha.service';
import { IDisciplina } from '../disciplina.model';
import { DisciplinaService } from '../service/disciplina.service';
import { DisciplinaFormService } from './disciplina-form.service';

import { DisciplinaUpdateComponent } from './disciplina-update.component';

describe('Disciplina Management Update Component', () => {
  let comp: DisciplinaUpdateComponent;
  let fixture: ComponentFixture<DisciplinaUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let disciplinaFormService: DisciplinaFormService;
  let disciplinaService: DisciplinaService;
  let periodoService: PeriodoService;
  let trilhaService: TrilhaService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([]), DisciplinaUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(DisciplinaUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(DisciplinaUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    disciplinaFormService = TestBed.inject(DisciplinaFormService);
    disciplinaService = TestBed.inject(DisciplinaService);
    periodoService = TestBed.inject(PeriodoService);
    trilhaService = TestBed.inject(TrilhaService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Periodo query and add missing value', () => {
      const disciplina: IDisciplina = { id: 456 };
      const periodo: IPeriodo = { id: 27227 };
      disciplina.periodo = periodo;

      const periodoCollection: IPeriodo[] = [{ id: 20415 }];
      jest.spyOn(periodoService, 'query').mockReturnValue(of(new HttpResponse({ body: periodoCollection })));
      const additionalPeriodos = [periodo];
      const expectedCollection: IPeriodo[] = [...additionalPeriodos, ...periodoCollection];
      jest.spyOn(periodoService, 'addPeriodoToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ disciplina });
      comp.ngOnInit();

      expect(periodoService.query).toHaveBeenCalled();
      expect(periodoService.addPeriodoToCollectionIfMissing).toHaveBeenCalledWith(
        periodoCollection,
        ...additionalPeriodos.map(expect.objectContaining),
      );
      expect(comp.periodosSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Trilha query and add missing value', () => {
      const disciplina: IDisciplina = { id: 456 };
      const trilha: ITrilha = { id: 25393 };
      disciplina.trilha = trilha;

      const trilhaCollection: ITrilha[] = [{ id: 17797 }];
      jest.spyOn(trilhaService, 'query').mockReturnValue(of(new HttpResponse({ body: trilhaCollection })));
      const additionalTrilhas = [trilha];
      const expectedCollection: ITrilha[] = [...additionalTrilhas, ...trilhaCollection];
      jest.spyOn(trilhaService, 'addTrilhaToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ disciplina });
      comp.ngOnInit();

      expect(trilhaService.query).toHaveBeenCalled();
      expect(trilhaService.addTrilhaToCollectionIfMissing).toHaveBeenCalledWith(
        trilhaCollection,
        ...additionalTrilhas.map(expect.objectContaining),
      );
      expect(comp.trilhasSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const disciplina: IDisciplina = { id: 456 };
      const periodo: IPeriodo = { id: 11412 };
      disciplina.periodo = periodo;
      const trilha: ITrilha = { id: 3121 };
      disciplina.trilha = trilha;

      activatedRoute.data = of({ disciplina });
      comp.ngOnInit();

      expect(comp.periodosSharedCollection).toContain(periodo);
      expect(comp.trilhasSharedCollection).toContain(trilha);
      expect(comp.disciplina).toEqual(disciplina);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IDisciplina>>();
      const disciplina = { id: 123 };
      jest.spyOn(disciplinaFormService, 'getDisciplina').mockReturnValue(disciplina);
      jest.spyOn(disciplinaService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ disciplina });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: disciplina }));
      saveSubject.complete();

      // THEN
      expect(disciplinaFormService.getDisciplina).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(disciplinaService.update).toHaveBeenCalledWith(expect.objectContaining(disciplina));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IDisciplina>>();
      const disciplina = { id: 123 };
      jest.spyOn(disciplinaFormService, 'getDisciplina').mockReturnValue({ id: null });
      jest.spyOn(disciplinaService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ disciplina: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: disciplina }));
      saveSubject.complete();

      // THEN
      expect(disciplinaFormService.getDisciplina).toHaveBeenCalled();
      expect(disciplinaService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IDisciplina>>();
      const disciplina = { id: 123 };
      jest.spyOn(disciplinaService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ disciplina });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(disciplinaService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Compare relationships', () => {
    describe('comparePeriodo', () => {
      it('Should forward to periodoService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(periodoService, 'comparePeriodo');
        comp.comparePeriodo(entity, entity2);
        expect(periodoService.comparePeriodo).toHaveBeenCalledWith(entity, entity2);
      });
    });

    describe('compareTrilha', () => {
      it('Should forward to trilhaService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(trilhaService, 'compareTrilha');
        comp.compareTrilha(entity, entity2);
        expect(trilhaService.compareTrilha).toHaveBeenCalledWith(entity, entity2);
      });
    });
  });
});
