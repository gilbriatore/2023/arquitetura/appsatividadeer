import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import SharedModule from 'app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IPeriodo } from 'app/entities/periodo/periodo.model';
import { PeriodoService } from 'app/entities/periodo/service/periodo.service';
import { ITrilha } from 'app/entities/trilha/trilha.model';
import { TrilhaService } from 'app/entities/trilha/service/trilha.service';
import { DisciplinaService } from '../service/disciplina.service';
import { IDisciplina } from '../disciplina.model';
import { DisciplinaFormService, DisciplinaFormGroup } from './disciplina-form.service';

@Component({
  standalone: true,
  selector: 'jhi-disciplina-update',
  templateUrl: './disciplina-update.component.html',
  imports: [SharedModule, FormsModule, ReactiveFormsModule],
})
export class DisciplinaUpdateComponent implements OnInit {
  isSaving = false;
  disciplina: IDisciplina | null = null;

  periodosSharedCollection: IPeriodo[] = [];
  trilhasSharedCollection: ITrilha[] = [];

  editForm: DisciplinaFormGroup = this.disciplinaFormService.createDisciplinaFormGroup();

  constructor(
    protected disciplinaService: DisciplinaService,
    protected disciplinaFormService: DisciplinaFormService,
    protected periodoService: PeriodoService,
    protected trilhaService: TrilhaService,
    protected activatedRoute: ActivatedRoute,
  ) {}

  comparePeriodo = (o1: IPeriodo | null, o2: IPeriodo | null): boolean => this.periodoService.comparePeriodo(o1, o2);

  compareTrilha = (o1: ITrilha | null, o2: ITrilha | null): boolean => this.trilhaService.compareTrilha(o1, o2);

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ disciplina }) => {
      this.disciplina = disciplina;
      if (disciplina) {
        this.updateForm(disciplina);
      }

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const disciplina = this.disciplinaFormService.getDisciplina(this.editForm);
    if (disciplina.id !== null) {
      this.subscribeToSaveResponse(this.disciplinaService.update(disciplina));
    } else {
      this.subscribeToSaveResponse(this.disciplinaService.create(disciplina));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IDisciplina>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(disciplina: IDisciplina): void {
    this.disciplina = disciplina;
    this.disciplinaFormService.resetForm(this.editForm, disciplina);

    this.periodosSharedCollection = this.periodoService.addPeriodoToCollectionIfMissing<IPeriodo>(
      this.periodosSharedCollection,
      disciplina.periodo,
    );
    this.trilhasSharedCollection = this.trilhaService.addTrilhaToCollectionIfMissing<ITrilha>(
      this.trilhasSharedCollection,
      disciplina.trilha,
    );
  }

  protected loadRelationshipsOptions(): void {
    this.periodoService
      .query()
      .pipe(map((res: HttpResponse<IPeriodo[]>) => res.body ?? []))
      .pipe(
        map((periodos: IPeriodo[]) => this.periodoService.addPeriodoToCollectionIfMissing<IPeriodo>(periodos, this.disciplina?.periodo)),
      )
      .subscribe((periodos: IPeriodo[]) => (this.periodosSharedCollection = periodos));

    this.trilhaService
      .query()
      .pipe(map((res: HttpResponse<ITrilha[]>) => res.body ?? []))
      .pipe(map((trilhas: ITrilha[]) => this.trilhaService.addTrilhaToCollectionIfMissing<ITrilha>(trilhas, this.disciplina?.trilha)))
      .subscribe((trilhas: ITrilha[]) => (this.trilhasSharedCollection = trilhas));
  }
}
