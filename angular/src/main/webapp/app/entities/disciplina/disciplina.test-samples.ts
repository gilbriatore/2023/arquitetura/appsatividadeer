import { IDisciplina, NewDisciplina } from './disciplina.model';

export const sampleWithRequiredData: IDisciplina = {
  id: 22033,
};

export const sampleWithPartialData: IDisciplina = {
  id: 21519,
  descricao: 'fund qua',
};

export const sampleWithFullData: IDisciplina = {
  id: 21056,
  descricao: 'brr duh worse',
};

export const sampleWithNewData: NewDisciplina = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
