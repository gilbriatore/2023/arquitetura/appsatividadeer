import { IPeriodo } from 'app/entities/periodo/periodo.model';
import { ITrilha } from 'app/entities/trilha/trilha.model';

export interface IDisciplina {
  id: number;
  descricao?: string | null;
  periodo?: Pick<IPeriodo, 'id'> | null;
  trilha?: Pick<ITrilha, 'id'> | null;
}

export type NewDisciplina = Omit<IDisciplina, 'id'> & { id: null };
