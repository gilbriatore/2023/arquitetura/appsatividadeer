import { TestBed } from '@angular/core/testing';
import { provideRouter, withComponentInputBinding } from '@angular/router';
import { RouterTestingHarness, RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { PerfilDetailComponent } from './perfil-detail.component';

describe('Perfil Management Detail Component', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [PerfilDetailComponent, RouterTestingModule.withRoutes([], { bindToComponentInputs: true })],
      providers: [
        provideRouter(
          [
            {
              path: '**',
              component: PerfilDetailComponent,
              resolve: { perfil: () => of({ id: 123 }) },
            },
          ],
          withComponentInputBinding(),
        ),
      ],
    })
      .overrideTemplate(PerfilDetailComponent, '')
      .compileComponents();
  });

  describe('OnInit', () => {
    it('Should load perfil on init', async () => {
      const harness = await RouterTestingHarness.create();
      const instance = await harness.navigateByUrl('/', PerfilDetailComponent);

      // THEN
      expect(instance.perfil).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
