export interface IPerfil {
  id: number;
  descricao?: string | null;
}

export type NewPerfil = Omit<IPerfil, 'id'> & { id: null };
