import { IPerfil, NewPerfil } from './perfil.model';

export const sampleWithRequiredData: IPerfil = {
  id: 21677,
};

export const sampleWithPartialData: IPerfil = {
  id: 15149,
  descricao: 'courage',
};

export const sampleWithFullData: IPerfil = {
  id: 23868,
  descricao: 'unsettle',
};

export const sampleWithNewData: NewPerfil = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
