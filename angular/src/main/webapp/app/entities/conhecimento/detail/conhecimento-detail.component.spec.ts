import { TestBed } from '@angular/core/testing';
import { provideRouter, withComponentInputBinding } from '@angular/router';
import { RouterTestingHarness, RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { ConhecimentoDetailComponent } from './conhecimento-detail.component';

describe('Conhecimento Management Detail Component', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ConhecimentoDetailComponent, RouterTestingModule.withRoutes([], { bindToComponentInputs: true })],
      providers: [
        provideRouter(
          [
            {
              path: '**',
              component: ConhecimentoDetailComponent,
              resolve: { conhecimento: () => of({ id: 123 }) },
            },
          ],
          withComponentInputBinding(),
        ),
      ],
    })
      .overrideTemplate(ConhecimentoDetailComponent, '')
      .compileComponents();
  });

  describe('OnInit', () => {
    it('Should load conhecimento on init', async () => {
      const harness = await RouterTestingHarness.create();
      const instance = await harness.navigateByUrl('/', ConhecimentoDetailComponent);

      // THEN
      expect(instance.conhecimento).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
