import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { ICompetencia } from 'app/entities/competencia/competencia.model';
import { CompetenciaService } from 'app/entities/competencia/service/competencia.service';
import { IBloom } from 'app/entities/bloom/bloom.model';
import { BloomService } from 'app/entities/bloom/service/bloom.service';
import { IConhecimento } from '../conhecimento.model';
import { ConhecimentoService } from '../service/conhecimento.service';
import { ConhecimentoFormService } from './conhecimento-form.service';

import { ConhecimentoUpdateComponent } from './conhecimento-update.component';

describe('Conhecimento Management Update Component', () => {
  let comp: ConhecimentoUpdateComponent;
  let fixture: ComponentFixture<ConhecimentoUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let conhecimentoFormService: ConhecimentoFormService;
  let conhecimentoService: ConhecimentoService;
  let competenciaService: CompetenciaService;
  let bloomService: BloomService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([]), ConhecimentoUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(ConhecimentoUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(ConhecimentoUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    conhecimentoFormService = TestBed.inject(ConhecimentoFormService);
    conhecimentoService = TestBed.inject(ConhecimentoService);
    competenciaService = TestBed.inject(CompetenciaService);
    bloomService = TestBed.inject(BloomService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Competencia query and add missing value', () => {
      const conhecimento: IConhecimento = { id: 456 };
      const competencia: ICompetencia = { id: 13025 };
      conhecimento.competencia = competencia;

      const competenciaCollection: ICompetencia[] = [{ id: 18892 }];
      jest.spyOn(competenciaService, 'query').mockReturnValue(of(new HttpResponse({ body: competenciaCollection })));
      const additionalCompetencias = [competencia];
      const expectedCollection: ICompetencia[] = [...additionalCompetencias, ...competenciaCollection];
      jest.spyOn(competenciaService, 'addCompetenciaToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ conhecimento });
      comp.ngOnInit();

      expect(competenciaService.query).toHaveBeenCalled();
      expect(competenciaService.addCompetenciaToCollectionIfMissing).toHaveBeenCalledWith(
        competenciaCollection,
        ...additionalCompetencias.map(expect.objectContaining),
      );
      expect(comp.competenciasSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Bloom query and add missing value', () => {
      const conhecimento: IConhecimento = { id: 456 };
      const bloom: IBloom = { id: 11692 };
      conhecimento.bloom = bloom;

      const bloomCollection: IBloom[] = [{ id: 24867 }];
      jest.spyOn(bloomService, 'query').mockReturnValue(of(new HttpResponse({ body: bloomCollection })));
      const additionalBlooms = [bloom];
      const expectedCollection: IBloom[] = [...additionalBlooms, ...bloomCollection];
      jest.spyOn(bloomService, 'addBloomToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ conhecimento });
      comp.ngOnInit();

      expect(bloomService.query).toHaveBeenCalled();
      expect(bloomService.addBloomToCollectionIfMissing).toHaveBeenCalledWith(
        bloomCollection,
        ...additionalBlooms.map(expect.objectContaining),
      );
      expect(comp.bloomsSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const conhecimento: IConhecimento = { id: 456 };
      const competencia: ICompetencia = { id: 9115 };
      conhecimento.competencia = competencia;
      const bloom: IBloom = { id: 25008 };
      conhecimento.bloom = bloom;

      activatedRoute.data = of({ conhecimento });
      comp.ngOnInit();

      expect(comp.competenciasSharedCollection).toContain(competencia);
      expect(comp.bloomsSharedCollection).toContain(bloom);
      expect(comp.conhecimento).toEqual(conhecimento);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IConhecimento>>();
      const conhecimento = { id: 123 };
      jest.spyOn(conhecimentoFormService, 'getConhecimento').mockReturnValue(conhecimento);
      jest.spyOn(conhecimentoService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ conhecimento });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: conhecimento }));
      saveSubject.complete();

      // THEN
      expect(conhecimentoFormService.getConhecimento).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(conhecimentoService.update).toHaveBeenCalledWith(expect.objectContaining(conhecimento));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IConhecimento>>();
      const conhecimento = { id: 123 };
      jest.spyOn(conhecimentoFormService, 'getConhecimento').mockReturnValue({ id: null });
      jest.spyOn(conhecimentoService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ conhecimento: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: conhecimento }));
      saveSubject.complete();

      // THEN
      expect(conhecimentoFormService.getConhecimento).toHaveBeenCalled();
      expect(conhecimentoService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IConhecimento>>();
      const conhecimento = { id: 123 };
      jest.spyOn(conhecimentoService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ conhecimento });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(conhecimentoService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Compare relationships', () => {
    describe('compareCompetencia', () => {
      it('Should forward to competenciaService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(competenciaService, 'compareCompetencia');
        comp.compareCompetencia(entity, entity2);
        expect(competenciaService.compareCompetencia).toHaveBeenCalledWith(entity, entity2);
      });
    });

    describe('compareBloom', () => {
      it('Should forward to bloomService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(bloomService, 'compareBloom');
        comp.compareBloom(entity, entity2);
        expect(bloomService.compareBloom).toHaveBeenCalledWith(entity, entity2);
      });
    });
  });
});
