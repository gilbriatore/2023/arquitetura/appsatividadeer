import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import SharedModule from 'app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ICompetencia } from 'app/entities/competencia/competencia.model';
import { CompetenciaService } from 'app/entities/competencia/service/competencia.service';
import { IBloom } from 'app/entities/bloom/bloom.model';
import { BloomService } from 'app/entities/bloom/service/bloom.service';
import { ConhecimentoService } from '../service/conhecimento.service';
import { IConhecimento } from '../conhecimento.model';
import { ConhecimentoFormService, ConhecimentoFormGroup } from './conhecimento-form.service';

@Component({
  standalone: true,
  selector: 'jhi-conhecimento-update',
  templateUrl: './conhecimento-update.component.html',
  imports: [SharedModule, FormsModule, ReactiveFormsModule],
})
export class ConhecimentoUpdateComponent implements OnInit {
  isSaving = false;
  conhecimento: IConhecimento | null = null;

  competenciasSharedCollection: ICompetencia[] = [];
  bloomsSharedCollection: IBloom[] = [];

  editForm: ConhecimentoFormGroup = this.conhecimentoFormService.createConhecimentoFormGroup();

  constructor(
    protected conhecimentoService: ConhecimentoService,
    protected conhecimentoFormService: ConhecimentoFormService,
    protected competenciaService: CompetenciaService,
    protected bloomService: BloomService,
    protected activatedRoute: ActivatedRoute,
  ) {}

  compareCompetencia = (o1: ICompetencia | null, o2: ICompetencia | null): boolean => this.competenciaService.compareCompetencia(o1, o2);

  compareBloom = (o1: IBloom | null, o2: IBloom | null): boolean => this.bloomService.compareBloom(o1, o2);

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ conhecimento }) => {
      this.conhecimento = conhecimento;
      if (conhecimento) {
        this.updateForm(conhecimento);
      }

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const conhecimento = this.conhecimentoFormService.getConhecimento(this.editForm);
    if (conhecimento.id !== null) {
      this.subscribeToSaveResponse(this.conhecimentoService.update(conhecimento));
    } else {
      this.subscribeToSaveResponse(this.conhecimentoService.create(conhecimento));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IConhecimento>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(conhecimento: IConhecimento): void {
    this.conhecimento = conhecimento;
    this.conhecimentoFormService.resetForm(this.editForm, conhecimento);

    this.competenciasSharedCollection = this.competenciaService.addCompetenciaToCollectionIfMissing<ICompetencia>(
      this.competenciasSharedCollection,
      conhecimento.competencia,
    );
    this.bloomsSharedCollection = this.bloomService.addBloomToCollectionIfMissing<IBloom>(this.bloomsSharedCollection, conhecimento.bloom);
  }

  protected loadRelationshipsOptions(): void {
    this.competenciaService
      .query()
      .pipe(map((res: HttpResponse<ICompetencia[]>) => res.body ?? []))
      .pipe(
        map((competencias: ICompetencia[]) =>
          this.competenciaService.addCompetenciaToCollectionIfMissing<ICompetencia>(competencias, this.conhecimento?.competencia),
        ),
      )
      .subscribe((competencias: ICompetencia[]) => (this.competenciasSharedCollection = competencias));

    this.bloomService
      .query()
      .pipe(map((res: HttpResponse<IBloom[]>) => res.body ?? []))
      .pipe(map((blooms: IBloom[]) => this.bloomService.addBloomToCollectionIfMissing<IBloom>(blooms, this.conhecimento?.bloom)))
      .subscribe((blooms: IBloom[]) => (this.bloomsSharedCollection = blooms));
  }
}
