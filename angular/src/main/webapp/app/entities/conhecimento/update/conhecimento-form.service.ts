import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { IConhecimento, NewConhecimento } from '../conhecimento.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts IConhecimento for edit and NewConhecimentoFormGroupInput for create.
 */
type ConhecimentoFormGroupInput = IConhecimento | PartialWithRequiredKeyOf<NewConhecimento>;

type ConhecimentoFormDefaults = Pick<NewConhecimento, 'id'>;

type ConhecimentoFormGroupContent = {
  id: FormControl<IConhecimento['id'] | NewConhecimento['id']>;
  descricao: FormControl<IConhecimento['descricao']>;
  competencia: FormControl<IConhecimento['competencia']>;
  bloom: FormControl<IConhecimento['bloom']>;
};

export type ConhecimentoFormGroup = FormGroup<ConhecimentoFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class ConhecimentoFormService {
  createConhecimentoFormGroup(conhecimento: ConhecimentoFormGroupInput = { id: null }): ConhecimentoFormGroup {
    const conhecimentoRawValue = {
      ...this.getFormDefaults(),
      ...conhecimento,
    };
    return new FormGroup<ConhecimentoFormGroupContent>({
      id: new FormControl(
        { value: conhecimentoRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        },
      ),
      descricao: new FormControl(conhecimentoRawValue.descricao),
      competencia: new FormControl(conhecimentoRawValue.competencia),
      bloom: new FormControl(conhecimentoRawValue.bloom),
    });
  }

  getConhecimento(form: ConhecimentoFormGroup): IConhecimento | NewConhecimento {
    return form.getRawValue() as IConhecimento | NewConhecimento;
  }

  resetForm(form: ConhecimentoFormGroup, conhecimento: ConhecimentoFormGroupInput): void {
    const conhecimentoRawValue = { ...this.getFormDefaults(), ...conhecimento };
    form.reset(
      {
        ...conhecimentoRawValue,
        id: { value: conhecimentoRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */,
    );
  }

  private getFormDefaults(): ConhecimentoFormDefaults {
    return {
      id: null,
    };
  }
}
