import { TestBed } from '@angular/core/testing';

import { sampleWithRequiredData, sampleWithNewData } from '../conhecimento.test-samples';

import { ConhecimentoFormService } from './conhecimento-form.service';

describe('Conhecimento Form Service', () => {
  let service: ConhecimentoFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ConhecimentoFormService);
  });

  describe('Service methods', () => {
    describe('createConhecimentoFormGroup', () => {
      it('should create a new form with FormControl', () => {
        const formGroup = service.createConhecimentoFormGroup();

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            descricao: expect.any(Object),
            competencia: expect.any(Object),
            bloom: expect.any(Object),
          }),
        );
      });

      it('passing IConhecimento should create a new form with FormGroup', () => {
        const formGroup = service.createConhecimentoFormGroup(sampleWithRequiredData);

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            descricao: expect.any(Object),
            competencia: expect.any(Object),
            bloom: expect.any(Object),
          }),
        );
      });
    });

    describe('getConhecimento', () => {
      it('should return NewConhecimento for default Conhecimento initial value', () => {
        const formGroup = service.createConhecimentoFormGroup(sampleWithNewData);

        const conhecimento = service.getConhecimento(formGroup) as any;

        expect(conhecimento).toMatchObject(sampleWithNewData);
      });

      it('should return NewConhecimento for empty Conhecimento initial value', () => {
        const formGroup = service.createConhecimentoFormGroup();

        const conhecimento = service.getConhecimento(formGroup) as any;

        expect(conhecimento).toMatchObject({});
      });

      it('should return IConhecimento', () => {
        const formGroup = service.createConhecimentoFormGroup(sampleWithRequiredData);

        const conhecimento = service.getConhecimento(formGroup) as any;

        expect(conhecimento).toMatchObject(sampleWithRequiredData);
      });
    });

    describe('resetForm', () => {
      it('passing IConhecimento should not enable id FormControl', () => {
        const formGroup = service.createConhecimentoFormGroup();
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, sampleWithRequiredData);

        expect(formGroup.controls.id.disabled).toBe(true);
      });

      it('passing NewConhecimento should disable id FormControl', () => {
        const formGroup = service.createConhecimentoFormGroup(sampleWithRequiredData);
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, { id: null });

        expect(formGroup.controls.id.disabled).toBe(true);
      });
    });
  });
});
