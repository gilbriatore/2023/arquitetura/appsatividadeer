import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { ConhecimentoService } from '../service/conhecimento.service';

import { ConhecimentoComponent } from './conhecimento.component';

describe('Conhecimento Management Component', () => {
  let comp: ConhecimentoComponent;
  let fixture: ComponentFixture<ConhecimentoComponent>;
  let service: ConhecimentoService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes([{ path: 'conhecimento', component: ConhecimentoComponent }]),
        HttpClientTestingModule,
        ConhecimentoComponent,
      ],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            data: of({
              defaultSort: 'id,asc',
            }),
            queryParamMap: of(
              jest.requireActual('@angular/router').convertToParamMap({
                page: '1',
                size: '1',
                sort: 'id,desc',
              }),
            ),
            snapshot: { queryParams: {} },
          },
        },
      ],
    })
      .overrideTemplate(ConhecimentoComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(ConhecimentoComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(ConhecimentoService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        }),
      ),
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.conhecimentos?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });

  describe('trackId', () => {
    it('Should forward to conhecimentoService', () => {
      const entity = { id: 123 };
      jest.spyOn(service, 'getConhecimentoIdentifier');
      const id = comp.trackId(0, entity);
      expect(service.getConhecimentoIdentifier).toHaveBeenCalledWith(entity);
      expect(id).toBe(entity.id);
    });
  });
});
