import { IConhecimento, NewConhecimento } from './conhecimento.model';

export const sampleWithRequiredData: IConhecimento = {
  id: 9159,
};

export const sampleWithPartialData: IConhecimento = {
  id: 30641,
  descricao: 'towards',
};

export const sampleWithFullData: IConhecimento = {
  id: 31379,
  descricao: 'why cacao',
};

export const sampleWithNewData: NewConhecimento = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
