import { inject } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of, EMPTY, Observable } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IConhecimento } from '../conhecimento.model';
import { ConhecimentoService } from '../service/conhecimento.service';

export const conhecimentoResolve = (route: ActivatedRouteSnapshot): Observable<null | IConhecimento> => {
  const id = route.params['id'];
  if (id) {
    return inject(ConhecimentoService)
      .find(id)
      .pipe(
        mergeMap((conhecimento: HttpResponse<IConhecimento>) => {
          if (conhecimento.body) {
            return of(conhecimento.body);
          } else {
            inject(Router).navigate(['404']);
            return EMPTY;
          }
        }),
      );
  }
  return of(null);
};

export default conhecimentoResolve;
