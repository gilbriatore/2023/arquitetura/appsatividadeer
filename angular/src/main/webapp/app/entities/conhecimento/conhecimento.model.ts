import { ICompetencia } from 'app/entities/competencia/competencia.model';
import { IBloom } from 'app/entities/bloom/bloom.model';

export interface IConhecimento {
  id: number;
  descricao?: string | null;
  competencia?: Pick<ICompetencia, 'id'> | null;
  bloom?: Pick<IBloom, 'id'> | null;
}

export type NewConhecimento = Omit<IConhecimento, 'id'> & { id: null };
