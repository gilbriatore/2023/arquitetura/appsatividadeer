import { Component } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import SharedModule from 'app/shared/shared.module';
import { ITEM_DELETED_EVENT } from 'app/config/navigation.constants';
import { IConhecimento } from '../conhecimento.model';
import { ConhecimentoService } from '../service/conhecimento.service';

@Component({
  standalone: true,
  templateUrl: './conhecimento-delete-dialog.component.html',
  imports: [SharedModule, FormsModule],
})
export class ConhecimentoDeleteDialogComponent {
  conhecimento?: IConhecimento;

  constructor(
    protected conhecimentoService: ConhecimentoService,
    protected activeModal: NgbActiveModal,
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.conhecimentoService.delete(id).subscribe(() => {
      this.activeModal.close(ITEM_DELETED_EVENT);
    });
  }
}
