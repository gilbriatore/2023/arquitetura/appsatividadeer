import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IConhecimento, NewConhecimento } from '../conhecimento.model';

export type PartialUpdateConhecimento = Partial<IConhecimento> & Pick<IConhecimento, 'id'>;

export type EntityResponseType = HttpResponse<IConhecimento>;
export type EntityArrayResponseType = HttpResponse<IConhecimento[]>;

@Injectable({ providedIn: 'root' })
export class ConhecimentoService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/conhecimentos');

  constructor(
    protected http: HttpClient,
    protected applicationConfigService: ApplicationConfigService,
  ) {}

  create(conhecimento: NewConhecimento): Observable<EntityResponseType> {
    return this.http.post<IConhecimento>(this.resourceUrl, conhecimento, { observe: 'response' });
  }

  update(conhecimento: IConhecimento): Observable<EntityResponseType> {
    return this.http.put<IConhecimento>(`${this.resourceUrl}/${this.getConhecimentoIdentifier(conhecimento)}`, conhecimento, {
      observe: 'response',
    });
  }

  partialUpdate(conhecimento: PartialUpdateConhecimento): Observable<EntityResponseType> {
    return this.http.patch<IConhecimento>(`${this.resourceUrl}/${this.getConhecimentoIdentifier(conhecimento)}`, conhecimento, {
      observe: 'response',
    });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IConhecimento>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IConhecimento[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getConhecimentoIdentifier(conhecimento: Pick<IConhecimento, 'id'>): number {
    return conhecimento.id;
  }

  compareConhecimento(o1: Pick<IConhecimento, 'id'> | null, o2: Pick<IConhecimento, 'id'> | null): boolean {
    return o1 && o2 ? this.getConhecimentoIdentifier(o1) === this.getConhecimentoIdentifier(o2) : o1 === o2;
  }

  addConhecimentoToCollectionIfMissing<Type extends Pick<IConhecimento, 'id'>>(
    conhecimentoCollection: Type[],
    ...conhecimentosToCheck: (Type | null | undefined)[]
  ): Type[] {
    const conhecimentos: Type[] = conhecimentosToCheck.filter(isPresent);
    if (conhecimentos.length > 0) {
      const conhecimentoCollectionIdentifiers = conhecimentoCollection.map(
        conhecimentoItem => this.getConhecimentoIdentifier(conhecimentoItem)!,
      );
      const conhecimentosToAdd = conhecimentos.filter(conhecimentoItem => {
        const conhecimentoIdentifier = this.getConhecimentoIdentifier(conhecimentoItem);
        if (conhecimentoCollectionIdentifiers.includes(conhecimentoIdentifier)) {
          return false;
        }
        conhecimentoCollectionIdentifiers.push(conhecimentoIdentifier);
        return true;
      });
      return [...conhecimentosToAdd, ...conhecimentoCollection];
    }
    return conhecimentoCollection;
  }
}
