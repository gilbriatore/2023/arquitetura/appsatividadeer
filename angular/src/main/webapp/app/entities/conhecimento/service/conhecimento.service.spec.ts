import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IConhecimento } from '../conhecimento.model';
import { sampleWithRequiredData, sampleWithNewData, sampleWithPartialData, sampleWithFullData } from '../conhecimento.test-samples';

import { ConhecimentoService } from './conhecimento.service';

const requireRestSample: IConhecimento = {
  ...sampleWithRequiredData,
};

describe('Conhecimento Service', () => {
  let service: ConhecimentoService;
  let httpMock: HttpTestingController;
  let expectedResult: IConhecimento | IConhecimento[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(ConhecimentoService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should create a Conhecimento', () => {
      const conhecimento = { ...sampleWithNewData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.create(conhecimento).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a Conhecimento', () => {
      const conhecimento = { ...sampleWithRequiredData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.update(conhecimento).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a Conhecimento', () => {
      const patchObject = { ...sampleWithPartialData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of Conhecimento', () => {
      const returnedFromService = { ...requireRestSample };

      const expected = { ...sampleWithRequiredData };

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toMatchObject([expected]);
    });

    it('should delete a Conhecimento', () => {
      const expected = true;

      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult).toBe(expected);
    });

    describe('addConhecimentoToCollectionIfMissing', () => {
      it('should add a Conhecimento to an empty array', () => {
        const conhecimento: IConhecimento = sampleWithRequiredData;
        expectedResult = service.addConhecimentoToCollectionIfMissing([], conhecimento);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(conhecimento);
      });

      it('should not add a Conhecimento to an array that contains it', () => {
        const conhecimento: IConhecimento = sampleWithRequiredData;
        const conhecimentoCollection: IConhecimento[] = [
          {
            ...conhecimento,
          },
          sampleWithPartialData,
        ];
        expectedResult = service.addConhecimentoToCollectionIfMissing(conhecimentoCollection, conhecimento);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a Conhecimento to an array that doesn't contain it", () => {
        const conhecimento: IConhecimento = sampleWithRequiredData;
        const conhecimentoCollection: IConhecimento[] = [sampleWithPartialData];
        expectedResult = service.addConhecimentoToCollectionIfMissing(conhecimentoCollection, conhecimento);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(conhecimento);
      });

      it('should add only unique Conhecimento to an array', () => {
        const conhecimentoArray: IConhecimento[] = [sampleWithRequiredData, sampleWithPartialData, sampleWithFullData];
        const conhecimentoCollection: IConhecimento[] = [sampleWithRequiredData];
        expectedResult = service.addConhecimentoToCollectionIfMissing(conhecimentoCollection, ...conhecimentoArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const conhecimento: IConhecimento = sampleWithRequiredData;
        const conhecimento2: IConhecimento = sampleWithPartialData;
        expectedResult = service.addConhecimentoToCollectionIfMissing([], conhecimento, conhecimento2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(conhecimento);
        expect(expectedResult).toContain(conhecimento2);
      });

      it('should accept null and undefined values', () => {
        const conhecimento: IConhecimento = sampleWithRequiredData;
        expectedResult = service.addConhecimentoToCollectionIfMissing([], null, conhecimento, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(conhecimento);
      });

      it('should return initial array if no Conhecimento is added', () => {
        const conhecimentoCollection: IConhecimento[] = [sampleWithRequiredData];
        expectedResult = service.addConhecimentoToCollectionIfMissing(conhecimentoCollection, undefined, null);
        expect(expectedResult).toEqual(conhecimentoCollection);
      });
    });

    describe('compareConhecimento', () => {
      it('Should return true if both entities are null', () => {
        const entity1 = null;
        const entity2 = null;

        const compareResult = service.compareConhecimento(entity1, entity2);

        expect(compareResult).toEqual(true);
      });

      it('Should return false if one entity is null', () => {
        const entity1 = { id: 123 };
        const entity2 = null;

        const compareResult1 = service.compareConhecimento(entity1, entity2);
        const compareResult2 = service.compareConhecimento(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey differs', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 456 };

        const compareResult1 = service.compareConhecimento(entity1, entity2);
        const compareResult2 = service.compareConhecimento(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey matches', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 123 };

        const compareResult1 = service.compareConhecimento(entity1, entity2);
        const compareResult2 = service.compareConhecimento(entity2, entity1);

        expect(compareResult1).toEqual(true);
        expect(compareResult2).toEqual(true);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
