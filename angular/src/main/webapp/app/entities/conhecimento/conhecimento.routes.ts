import { Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { ASC } from 'app/config/navigation.constants';
import { ConhecimentoComponent } from './list/conhecimento.component';
import { ConhecimentoDetailComponent } from './detail/conhecimento-detail.component';
import { ConhecimentoUpdateComponent } from './update/conhecimento-update.component';
import ConhecimentoResolve from './route/conhecimento-routing-resolve.service';

const conhecimentoRoute: Routes = [
  {
    path: '',
    component: ConhecimentoComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: ConhecimentoDetailComponent,
    resolve: {
      conhecimento: ConhecimentoResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: ConhecimentoUpdateComponent,
    resolve: {
      conhecimento: ConhecimentoResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: ConhecimentoUpdateComponent,
    resolve: {
      conhecimento: ConhecimentoResolve,
    },
    canActivate: [UserRouteAccessService],
  },
];

export default conhecimentoRoute;
