import { ICompetencia } from 'app/entities/competencia/competencia.model';

export interface IAtitude {
  id: number;
  descricao?: string | null;
  competencia?: Pick<ICompetencia, 'id'> | null;
}

export type NewAtitude = Omit<IAtitude, 'id'> & { id: null };
