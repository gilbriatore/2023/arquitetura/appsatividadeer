import { Component } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import SharedModule from 'app/shared/shared.module';
import { ITEM_DELETED_EVENT } from 'app/config/navigation.constants';
import { IAtitude } from '../atitude.model';
import { AtitudeService } from '../service/atitude.service';

@Component({
  standalone: true,
  templateUrl: './atitude-delete-dialog.component.html',
  imports: [SharedModule, FormsModule],
})
export class AtitudeDeleteDialogComponent {
  atitude?: IAtitude;

  constructor(
    protected atitudeService: AtitudeService,
    protected activeModal: NgbActiveModal,
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.atitudeService.delete(id).subscribe(() => {
      this.activeModal.close(ITEM_DELETED_EVENT);
    });
  }
}
