import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { AtitudeService } from '../service/atitude.service';

import { AtitudeComponent } from './atitude.component';

describe('Atitude Management Component', () => {
  let comp: AtitudeComponent;
  let fixture: ComponentFixture<AtitudeComponent>;
  let service: AtitudeService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes([{ path: 'atitude', component: AtitudeComponent }]),
        HttpClientTestingModule,
        AtitudeComponent,
      ],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            data: of({
              defaultSort: 'id,asc',
            }),
            queryParamMap: of(
              jest.requireActual('@angular/router').convertToParamMap({
                page: '1',
                size: '1',
                sort: 'id,desc',
              }),
            ),
            snapshot: { queryParams: {} },
          },
        },
      ],
    })
      .overrideTemplate(AtitudeComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(AtitudeComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(AtitudeService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        }),
      ),
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.atitudes?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });

  describe('trackId', () => {
    it('Should forward to atitudeService', () => {
      const entity = { id: 123 };
      jest.spyOn(service, 'getAtitudeIdentifier');
      const id = comp.trackId(0, entity);
      expect(service.getAtitudeIdentifier).toHaveBeenCalledWith(entity);
      expect(id).toBe(entity.id);
    });
  });
});
