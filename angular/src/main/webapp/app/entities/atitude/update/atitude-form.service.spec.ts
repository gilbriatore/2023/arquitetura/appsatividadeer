import { TestBed } from '@angular/core/testing';

import { sampleWithRequiredData, sampleWithNewData } from '../atitude.test-samples';

import { AtitudeFormService } from './atitude-form.service';

describe('Atitude Form Service', () => {
  let service: AtitudeFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AtitudeFormService);
  });

  describe('Service methods', () => {
    describe('createAtitudeFormGroup', () => {
      it('should create a new form with FormControl', () => {
        const formGroup = service.createAtitudeFormGroup();

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            descricao: expect.any(Object),
            competencia: expect.any(Object),
          }),
        );
      });

      it('passing IAtitude should create a new form with FormGroup', () => {
        const formGroup = service.createAtitudeFormGroup(sampleWithRequiredData);

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            descricao: expect.any(Object),
            competencia: expect.any(Object),
          }),
        );
      });
    });

    describe('getAtitude', () => {
      it('should return NewAtitude for default Atitude initial value', () => {
        const formGroup = service.createAtitudeFormGroup(sampleWithNewData);

        const atitude = service.getAtitude(formGroup) as any;

        expect(atitude).toMatchObject(sampleWithNewData);
      });

      it('should return NewAtitude for empty Atitude initial value', () => {
        const formGroup = service.createAtitudeFormGroup();

        const atitude = service.getAtitude(formGroup) as any;

        expect(atitude).toMatchObject({});
      });

      it('should return IAtitude', () => {
        const formGroup = service.createAtitudeFormGroup(sampleWithRequiredData);

        const atitude = service.getAtitude(formGroup) as any;

        expect(atitude).toMatchObject(sampleWithRequiredData);
      });
    });

    describe('resetForm', () => {
      it('passing IAtitude should not enable id FormControl', () => {
        const formGroup = service.createAtitudeFormGroup();
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, sampleWithRequiredData);

        expect(formGroup.controls.id.disabled).toBe(true);
      });

      it('passing NewAtitude should disable id FormControl', () => {
        const formGroup = service.createAtitudeFormGroup(sampleWithRequiredData);
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, { id: null });

        expect(formGroup.controls.id.disabled).toBe(true);
      });
    });
  });
});
