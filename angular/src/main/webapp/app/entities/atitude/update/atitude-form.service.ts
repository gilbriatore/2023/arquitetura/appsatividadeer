import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { IAtitude, NewAtitude } from '../atitude.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts IAtitude for edit and NewAtitudeFormGroupInput for create.
 */
type AtitudeFormGroupInput = IAtitude | PartialWithRequiredKeyOf<NewAtitude>;

type AtitudeFormDefaults = Pick<NewAtitude, 'id'>;

type AtitudeFormGroupContent = {
  id: FormControl<IAtitude['id'] | NewAtitude['id']>;
  descricao: FormControl<IAtitude['descricao']>;
  competencia: FormControl<IAtitude['competencia']>;
};

export type AtitudeFormGroup = FormGroup<AtitudeFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class AtitudeFormService {
  createAtitudeFormGroup(atitude: AtitudeFormGroupInput = { id: null }): AtitudeFormGroup {
    const atitudeRawValue = {
      ...this.getFormDefaults(),
      ...atitude,
    };
    return new FormGroup<AtitudeFormGroupContent>({
      id: new FormControl(
        { value: atitudeRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        },
      ),
      descricao: new FormControl(atitudeRawValue.descricao),
      competencia: new FormControl(atitudeRawValue.competencia),
    });
  }

  getAtitude(form: AtitudeFormGroup): IAtitude | NewAtitude {
    return form.getRawValue() as IAtitude | NewAtitude;
  }

  resetForm(form: AtitudeFormGroup, atitude: AtitudeFormGroupInput): void {
    const atitudeRawValue = { ...this.getFormDefaults(), ...atitude };
    form.reset(
      {
        ...atitudeRawValue,
        id: { value: atitudeRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */,
    );
  }

  private getFormDefaults(): AtitudeFormDefaults {
    return {
      id: null,
    };
  }
}
