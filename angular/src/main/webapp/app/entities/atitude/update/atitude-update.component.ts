import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import SharedModule from 'app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ICompetencia } from 'app/entities/competencia/competencia.model';
import { CompetenciaService } from 'app/entities/competencia/service/competencia.service';
import { IAtitude } from '../atitude.model';
import { AtitudeService } from '../service/atitude.service';
import { AtitudeFormService, AtitudeFormGroup } from './atitude-form.service';

@Component({
  standalone: true,
  selector: 'jhi-atitude-update',
  templateUrl: './atitude-update.component.html',
  imports: [SharedModule, FormsModule, ReactiveFormsModule],
})
export class AtitudeUpdateComponent implements OnInit {
  isSaving = false;
  atitude: IAtitude | null = null;

  competenciasSharedCollection: ICompetencia[] = [];

  editForm: AtitudeFormGroup = this.atitudeFormService.createAtitudeFormGroup();

  constructor(
    protected atitudeService: AtitudeService,
    protected atitudeFormService: AtitudeFormService,
    protected competenciaService: CompetenciaService,
    protected activatedRoute: ActivatedRoute,
  ) {}

  compareCompetencia = (o1: ICompetencia | null, o2: ICompetencia | null): boolean => this.competenciaService.compareCompetencia(o1, o2);

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ atitude }) => {
      this.atitude = atitude;
      if (atitude) {
        this.updateForm(atitude);
      }

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const atitude = this.atitudeFormService.getAtitude(this.editForm);
    if (atitude.id !== null) {
      this.subscribeToSaveResponse(this.atitudeService.update(atitude));
    } else {
      this.subscribeToSaveResponse(this.atitudeService.create(atitude));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IAtitude>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(atitude: IAtitude): void {
    this.atitude = atitude;
    this.atitudeFormService.resetForm(this.editForm, atitude);

    this.competenciasSharedCollection = this.competenciaService.addCompetenciaToCollectionIfMissing<ICompetencia>(
      this.competenciasSharedCollection,
      atitude.competencia,
    );
  }

  protected loadRelationshipsOptions(): void {
    this.competenciaService
      .query()
      .pipe(map((res: HttpResponse<ICompetencia[]>) => res.body ?? []))
      .pipe(
        map((competencias: ICompetencia[]) =>
          this.competenciaService.addCompetenciaToCollectionIfMissing<ICompetencia>(competencias, this.atitude?.competencia),
        ),
      )
      .subscribe((competencias: ICompetencia[]) => (this.competenciasSharedCollection = competencias));
  }
}
