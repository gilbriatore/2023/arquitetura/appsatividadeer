import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { ICompetencia } from 'app/entities/competencia/competencia.model';
import { CompetenciaService } from 'app/entities/competencia/service/competencia.service';
import { AtitudeService } from '../service/atitude.service';
import { IAtitude } from '../atitude.model';
import { AtitudeFormService } from './atitude-form.service';

import { AtitudeUpdateComponent } from './atitude-update.component';

describe('Atitude Management Update Component', () => {
  let comp: AtitudeUpdateComponent;
  let fixture: ComponentFixture<AtitudeUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let atitudeFormService: AtitudeFormService;
  let atitudeService: AtitudeService;
  let competenciaService: CompetenciaService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([]), AtitudeUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(AtitudeUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(AtitudeUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    atitudeFormService = TestBed.inject(AtitudeFormService);
    atitudeService = TestBed.inject(AtitudeService);
    competenciaService = TestBed.inject(CompetenciaService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Competencia query and add missing value', () => {
      const atitude: IAtitude = { id: 456 };
      const competencia: ICompetencia = { id: 7912 };
      atitude.competencia = competencia;

      const competenciaCollection: ICompetencia[] = [{ id: 25328 }];
      jest.spyOn(competenciaService, 'query').mockReturnValue(of(new HttpResponse({ body: competenciaCollection })));
      const additionalCompetencias = [competencia];
      const expectedCollection: ICompetencia[] = [...additionalCompetencias, ...competenciaCollection];
      jest.spyOn(competenciaService, 'addCompetenciaToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ atitude });
      comp.ngOnInit();

      expect(competenciaService.query).toHaveBeenCalled();
      expect(competenciaService.addCompetenciaToCollectionIfMissing).toHaveBeenCalledWith(
        competenciaCollection,
        ...additionalCompetencias.map(expect.objectContaining),
      );
      expect(comp.competenciasSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const atitude: IAtitude = { id: 456 };
      const competencia: ICompetencia = { id: 11110 };
      atitude.competencia = competencia;

      activatedRoute.data = of({ atitude });
      comp.ngOnInit();

      expect(comp.competenciasSharedCollection).toContain(competencia);
      expect(comp.atitude).toEqual(atitude);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IAtitude>>();
      const atitude = { id: 123 };
      jest.spyOn(atitudeFormService, 'getAtitude').mockReturnValue(atitude);
      jest.spyOn(atitudeService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ atitude });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: atitude }));
      saveSubject.complete();

      // THEN
      expect(atitudeFormService.getAtitude).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(atitudeService.update).toHaveBeenCalledWith(expect.objectContaining(atitude));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IAtitude>>();
      const atitude = { id: 123 };
      jest.spyOn(atitudeFormService, 'getAtitude').mockReturnValue({ id: null });
      jest.spyOn(atitudeService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ atitude: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: atitude }));
      saveSubject.complete();

      // THEN
      expect(atitudeFormService.getAtitude).toHaveBeenCalled();
      expect(atitudeService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IAtitude>>();
      const atitude = { id: 123 };
      jest.spyOn(atitudeService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ atitude });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(atitudeService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Compare relationships', () => {
    describe('compareCompetencia', () => {
      it('Should forward to competenciaService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(competenciaService, 'compareCompetencia');
        comp.compareCompetencia(entity, entity2);
        expect(competenciaService.compareCompetencia).toHaveBeenCalledWith(entity, entity2);
      });
    });
  });
});
