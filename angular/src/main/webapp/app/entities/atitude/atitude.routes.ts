import { Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { ASC } from 'app/config/navigation.constants';
import { AtitudeComponent } from './list/atitude.component';
import { AtitudeDetailComponent } from './detail/atitude-detail.component';
import { AtitudeUpdateComponent } from './update/atitude-update.component';
import AtitudeResolve from './route/atitude-routing-resolve.service';

const atitudeRoute: Routes = [
  {
    path: '',
    component: AtitudeComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: AtitudeDetailComponent,
    resolve: {
      atitude: AtitudeResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: AtitudeUpdateComponent,
    resolve: {
      atitude: AtitudeResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: AtitudeUpdateComponent,
    resolve: {
      atitude: AtitudeResolve,
    },
    canActivate: [UserRouteAccessService],
  },
];

export default atitudeRoute;
