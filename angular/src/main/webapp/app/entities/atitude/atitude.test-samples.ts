import { IAtitude, NewAtitude } from './atitude.model';

export const sampleWithRequiredData: IAtitude = {
  id: 5852,
};

export const sampleWithPartialData: IAtitude = {
  id: 22173,
  descricao: 'enthusiastically roughly after',
};

export const sampleWithFullData: IAtitude = {
  id: 17972,
  descricao: 'exhausted',
};

export const sampleWithNewData: NewAtitude = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
