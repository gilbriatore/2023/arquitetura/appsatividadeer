import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IAtitude } from '../atitude.model';
import { sampleWithRequiredData, sampleWithNewData, sampleWithPartialData, sampleWithFullData } from '../atitude.test-samples';

import { AtitudeService } from './atitude.service';

const requireRestSample: IAtitude = {
  ...sampleWithRequiredData,
};

describe('Atitude Service', () => {
  let service: AtitudeService;
  let httpMock: HttpTestingController;
  let expectedResult: IAtitude | IAtitude[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(AtitudeService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should create a Atitude', () => {
      const atitude = { ...sampleWithNewData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.create(atitude).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a Atitude', () => {
      const atitude = { ...sampleWithRequiredData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.update(atitude).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a Atitude', () => {
      const patchObject = { ...sampleWithPartialData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of Atitude', () => {
      const returnedFromService = { ...requireRestSample };

      const expected = { ...sampleWithRequiredData };

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toMatchObject([expected]);
    });

    it('should delete a Atitude', () => {
      const expected = true;

      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult).toBe(expected);
    });

    describe('addAtitudeToCollectionIfMissing', () => {
      it('should add a Atitude to an empty array', () => {
        const atitude: IAtitude = sampleWithRequiredData;
        expectedResult = service.addAtitudeToCollectionIfMissing([], atitude);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(atitude);
      });

      it('should not add a Atitude to an array that contains it', () => {
        const atitude: IAtitude = sampleWithRequiredData;
        const atitudeCollection: IAtitude[] = [
          {
            ...atitude,
          },
          sampleWithPartialData,
        ];
        expectedResult = service.addAtitudeToCollectionIfMissing(atitudeCollection, atitude);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a Atitude to an array that doesn't contain it", () => {
        const atitude: IAtitude = sampleWithRequiredData;
        const atitudeCollection: IAtitude[] = [sampleWithPartialData];
        expectedResult = service.addAtitudeToCollectionIfMissing(atitudeCollection, atitude);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(atitude);
      });

      it('should add only unique Atitude to an array', () => {
        const atitudeArray: IAtitude[] = [sampleWithRequiredData, sampleWithPartialData, sampleWithFullData];
        const atitudeCollection: IAtitude[] = [sampleWithRequiredData];
        expectedResult = service.addAtitudeToCollectionIfMissing(atitudeCollection, ...atitudeArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const atitude: IAtitude = sampleWithRequiredData;
        const atitude2: IAtitude = sampleWithPartialData;
        expectedResult = service.addAtitudeToCollectionIfMissing([], atitude, atitude2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(atitude);
        expect(expectedResult).toContain(atitude2);
      });

      it('should accept null and undefined values', () => {
        const atitude: IAtitude = sampleWithRequiredData;
        expectedResult = service.addAtitudeToCollectionIfMissing([], null, atitude, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(atitude);
      });

      it('should return initial array if no Atitude is added', () => {
        const atitudeCollection: IAtitude[] = [sampleWithRequiredData];
        expectedResult = service.addAtitudeToCollectionIfMissing(atitudeCollection, undefined, null);
        expect(expectedResult).toEqual(atitudeCollection);
      });
    });

    describe('compareAtitude', () => {
      it('Should return true if both entities are null', () => {
        const entity1 = null;
        const entity2 = null;

        const compareResult = service.compareAtitude(entity1, entity2);

        expect(compareResult).toEqual(true);
      });

      it('Should return false if one entity is null', () => {
        const entity1 = { id: 123 };
        const entity2 = null;

        const compareResult1 = service.compareAtitude(entity1, entity2);
        const compareResult2 = service.compareAtitude(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey differs', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 456 };

        const compareResult1 = service.compareAtitude(entity1, entity2);
        const compareResult2 = service.compareAtitude(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey matches', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 123 };

        const compareResult1 = service.compareAtitude(entity1, entity2);
        const compareResult2 = service.compareAtitude(entity2, entity1);

        expect(compareResult1).toEqual(true);
        expect(compareResult2).toEqual(true);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
