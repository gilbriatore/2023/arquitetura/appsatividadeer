import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IAtitude, NewAtitude } from '../atitude.model';

export type PartialUpdateAtitude = Partial<IAtitude> & Pick<IAtitude, 'id'>;

export type EntityResponseType = HttpResponse<IAtitude>;
export type EntityArrayResponseType = HttpResponse<IAtitude[]>;

@Injectable({ providedIn: 'root' })
export class AtitudeService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/atitudes');

  constructor(
    protected http: HttpClient,
    protected applicationConfigService: ApplicationConfigService,
  ) {}

  create(atitude: NewAtitude): Observable<EntityResponseType> {
    return this.http.post<IAtitude>(this.resourceUrl, atitude, { observe: 'response' });
  }

  update(atitude: IAtitude): Observable<EntityResponseType> {
    return this.http.put<IAtitude>(`${this.resourceUrl}/${this.getAtitudeIdentifier(atitude)}`, atitude, { observe: 'response' });
  }

  partialUpdate(atitude: PartialUpdateAtitude): Observable<EntityResponseType> {
    return this.http.patch<IAtitude>(`${this.resourceUrl}/${this.getAtitudeIdentifier(atitude)}`, atitude, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IAtitude>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IAtitude[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getAtitudeIdentifier(atitude: Pick<IAtitude, 'id'>): number {
    return atitude.id;
  }

  compareAtitude(o1: Pick<IAtitude, 'id'> | null, o2: Pick<IAtitude, 'id'> | null): boolean {
    return o1 && o2 ? this.getAtitudeIdentifier(o1) === this.getAtitudeIdentifier(o2) : o1 === o2;
  }

  addAtitudeToCollectionIfMissing<Type extends Pick<IAtitude, 'id'>>(
    atitudeCollection: Type[],
    ...atitudesToCheck: (Type | null | undefined)[]
  ): Type[] {
    const atitudes: Type[] = atitudesToCheck.filter(isPresent);
    if (atitudes.length > 0) {
      const atitudeCollectionIdentifiers = atitudeCollection.map(atitudeItem => this.getAtitudeIdentifier(atitudeItem)!);
      const atitudesToAdd = atitudes.filter(atitudeItem => {
        const atitudeIdentifier = this.getAtitudeIdentifier(atitudeItem);
        if (atitudeCollectionIdentifiers.includes(atitudeIdentifier)) {
          return false;
        }
        atitudeCollectionIdentifiers.push(atitudeIdentifier);
        return true;
      });
      return [...atitudesToAdd, ...atitudeCollection];
    }
    return atitudeCollection;
  }
}
