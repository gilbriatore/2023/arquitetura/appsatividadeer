import { inject } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of, EMPTY, Observable } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IAtitude } from '../atitude.model';
import { AtitudeService } from '../service/atitude.service';

export const atitudeResolve = (route: ActivatedRouteSnapshot): Observable<null | IAtitude> => {
  const id = route.params['id'];
  if (id) {
    return inject(AtitudeService)
      .find(id)
      .pipe(
        mergeMap((atitude: HttpResponse<IAtitude>) => {
          if (atitude.body) {
            return of(atitude.body);
          } else {
            inject(Router).navigate(['404']);
            return EMPTY;
          }
        }),
      );
  }
  return of(null);
};

export default atitudeResolve;
