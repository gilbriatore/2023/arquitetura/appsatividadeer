import { TestBed } from '@angular/core/testing';
import { provideRouter, withComponentInputBinding } from '@angular/router';
import { RouterTestingHarness, RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { AtitudeDetailComponent } from './atitude-detail.component';

describe('Atitude Management Detail Component', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AtitudeDetailComponent, RouterTestingModule.withRoutes([], { bindToComponentInputs: true })],
      providers: [
        provideRouter(
          [
            {
              path: '**',
              component: AtitudeDetailComponent,
              resolve: { atitude: () => of({ id: 123 }) },
            },
          ],
          withComponentInputBinding(),
        ),
      ],
    })
      .overrideTemplate(AtitudeDetailComponent, '')
      .compileComponents();
  });

  describe('OnInit', () => {
    it('Should load atitude on init', async () => {
      const harness = await RouterTestingHarness.create();
      const instance = await harness.navigateByUrl('/', AtitudeDetailComponent);

      // THEN
      expect(instance.atitude).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
