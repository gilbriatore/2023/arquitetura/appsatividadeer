import { inject } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of, EMPTY, Observable } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IPapel } from '../papel.model';
import { PapelService } from '../service/papel.service';

export const papelResolve = (route: ActivatedRouteSnapshot): Observable<null | IPapel> => {
  const id = route.params['id'];
  if (id) {
    return inject(PapelService)
      .find(id)
      .pipe(
        mergeMap((papel: HttpResponse<IPapel>) => {
          if (papel.body) {
            return of(papel.body);
          } else {
            inject(Router).navigate(['404']);
            return EMPTY;
          }
        }),
      );
  }
  return of(null);
};

export default papelResolve;
