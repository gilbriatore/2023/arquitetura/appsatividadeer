import { Component } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import SharedModule from 'app/shared/shared.module';
import { ITEM_DELETED_EVENT } from 'app/config/navigation.constants';
import { IPapel } from '../papel.model';
import { PapelService } from '../service/papel.service';

@Component({
  standalone: true,
  templateUrl: './papel-delete-dialog.component.html',
  imports: [SharedModule, FormsModule],
})
export class PapelDeleteDialogComponent {
  papel?: IPapel;

  constructor(
    protected papelService: PapelService,
    protected activeModal: NgbActiveModal,
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.papelService.delete(id).subscribe(() => {
      this.activeModal.close(ITEM_DELETED_EVENT);
    });
  }
}
