import { TestBed } from '@angular/core/testing';
import { provideRouter, withComponentInputBinding } from '@angular/router';
import { RouterTestingHarness, RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { PapelDetailComponent } from './papel-detail.component';

describe('Papel Management Detail Component', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [PapelDetailComponent, RouterTestingModule.withRoutes([], { bindToComponentInputs: true })],
      providers: [
        provideRouter(
          [
            {
              path: '**',
              component: PapelDetailComponent,
              resolve: { papel: () => of({ id: 123 }) },
            },
          ],
          withComponentInputBinding(),
        ),
      ],
    })
      .overrideTemplate(PapelDetailComponent, '')
      .compileComponents();
  });

  describe('OnInit', () => {
    it('Should load papel on init', async () => {
      const harness = await RouterTestingHarness.create();
      const instance = await harness.navigateByUrl('/', PapelDetailComponent);

      // THEN
      expect(instance.papel).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
