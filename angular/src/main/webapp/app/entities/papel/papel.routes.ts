import { Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { ASC } from 'app/config/navigation.constants';
import { PapelComponent } from './list/papel.component';
import { PapelDetailComponent } from './detail/papel-detail.component';
import { PapelUpdateComponent } from './update/papel-update.component';
import PapelResolve from './route/papel-routing-resolve.service';

const papelRoute: Routes = [
  {
    path: '',
    component: PapelComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: PapelDetailComponent,
    resolve: {
      papel: PapelResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: PapelUpdateComponent,
    resolve: {
      papel: PapelResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: PapelUpdateComponent,
    resolve: {
      papel: PapelResolve,
    },
    canActivate: [UserRouteAccessService],
  },
];

export default papelRoute;
