import { IPerfil } from 'app/entities/perfil/perfil.model';

export interface IPapel {
  id: number;
  descricao?: string | null;
  perfil?: Pick<IPerfil, 'id'> | null;
}

export type NewPapel = Omit<IPapel, 'id'> & { id: null };
