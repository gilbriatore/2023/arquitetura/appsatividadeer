import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IPapel } from '../papel.model';
import { sampleWithRequiredData, sampleWithNewData, sampleWithPartialData, sampleWithFullData } from '../papel.test-samples';

import { PapelService } from './papel.service';

const requireRestSample: IPapel = {
  ...sampleWithRequiredData,
};

describe('Papel Service', () => {
  let service: PapelService;
  let httpMock: HttpTestingController;
  let expectedResult: IPapel | IPapel[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(PapelService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should create a Papel', () => {
      const papel = { ...sampleWithNewData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.create(papel).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a Papel', () => {
      const papel = { ...sampleWithRequiredData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.update(papel).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a Papel', () => {
      const patchObject = { ...sampleWithPartialData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of Papel', () => {
      const returnedFromService = { ...requireRestSample };

      const expected = { ...sampleWithRequiredData };

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toMatchObject([expected]);
    });

    it('should delete a Papel', () => {
      const expected = true;

      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult).toBe(expected);
    });

    describe('addPapelToCollectionIfMissing', () => {
      it('should add a Papel to an empty array', () => {
        const papel: IPapel = sampleWithRequiredData;
        expectedResult = service.addPapelToCollectionIfMissing([], papel);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(papel);
      });

      it('should not add a Papel to an array that contains it', () => {
        const papel: IPapel = sampleWithRequiredData;
        const papelCollection: IPapel[] = [
          {
            ...papel,
          },
          sampleWithPartialData,
        ];
        expectedResult = service.addPapelToCollectionIfMissing(papelCollection, papel);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a Papel to an array that doesn't contain it", () => {
        const papel: IPapel = sampleWithRequiredData;
        const papelCollection: IPapel[] = [sampleWithPartialData];
        expectedResult = service.addPapelToCollectionIfMissing(papelCollection, papel);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(papel);
      });

      it('should add only unique Papel to an array', () => {
        const papelArray: IPapel[] = [sampleWithRequiredData, sampleWithPartialData, sampleWithFullData];
        const papelCollection: IPapel[] = [sampleWithRequiredData];
        expectedResult = service.addPapelToCollectionIfMissing(papelCollection, ...papelArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const papel: IPapel = sampleWithRequiredData;
        const papel2: IPapel = sampleWithPartialData;
        expectedResult = service.addPapelToCollectionIfMissing([], papel, papel2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(papel);
        expect(expectedResult).toContain(papel2);
      });

      it('should accept null and undefined values', () => {
        const papel: IPapel = sampleWithRequiredData;
        expectedResult = service.addPapelToCollectionIfMissing([], null, papel, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(papel);
      });

      it('should return initial array if no Papel is added', () => {
        const papelCollection: IPapel[] = [sampleWithRequiredData];
        expectedResult = service.addPapelToCollectionIfMissing(papelCollection, undefined, null);
        expect(expectedResult).toEqual(papelCollection);
      });
    });

    describe('comparePapel', () => {
      it('Should return true if both entities are null', () => {
        const entity1 = null;
        const entity2 = null;

        const compareResult = service.comparePapel(entity1, entity2);

        expect(compareResult).toEqual(true);
      });

      it('Should return false if one entity is null', () => {
        const entity1 = { id: 123 };
        const entity2 = null;

        const compareResult1 = service.comparePapel(entity1, entity2);
        const compareResult2 = service.comparePapel(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey differs', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 456 };

        const compareResult1 = service.comparePapel(entity1, entity2);
        const compareResult2 = service.comparePapel(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey matches', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 123 };

        const compareResult1 = service.comparePapel(entity1, entity2);
        const compareResult2 = service.comparePapel(entity2, entity1);

        expect(compareResult1).toEqual(true);
        expect(compareResult2).toEqual(true);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
