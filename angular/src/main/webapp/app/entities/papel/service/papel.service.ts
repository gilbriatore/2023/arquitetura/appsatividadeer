import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IPapel, NewPapel } from '../papel.model';

export type PartialUpdatePapel = Partial<IPapel> & Pick<IPapel, 'id'>;

export type EntityResponseType = HttpResponse<IPapel>;
export type EntityArrayResponseType = HttpResponse<IPapel[]>;

@Injectable({ providedIn: 'root' })
export class PapelService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/papels');

  constructor(
    protected http: HttpClient,
    protected applicationConfigService: ApplicationConfigService,
  ) {}

  create(papel: NewPapel): Observable<EntityResponseType> {
    return this.http.post<IPapel>(this.resourceUrl, papel, { observe: 'response' });
  }

  update(papel: IPapel): Observable<EntityResponseType> {
    return this.http.put<IPapel>(`${this.resourceUrl}/${this.getPapelIdentifier(papel)}`, papel, { observe: 'response' });
  }

  partialUpdate(papel: PartialUpdatePapel): Observable<EntityResponseType> {
    return this.http.patch<IPapel>(`${this.resourceUrl}/${this.getPapelIdentifier(papel)}`, papel, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IPapel>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IPapel[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getPapelIdentifier(papel: Pick<IPapel, 'id'>): number {
    return papel.id;
  }

  comparePapel(o1: Pick<IPapel, 'id'> | null, o2: Pick<IPapel, 'id'> | null): boolean {
    return o1 && o2 ? this.getPapelIdentifier(o1) === this.getPapelIdentifier(o2) : o1 === o2;
  }

  addPapelToCollectionIfMissing<Type extends Pick<IPapel, 'id'>>(
    papelCollection: Type[],
    ...papelsToCheck: (Type | null | undefined)[]
  ): Type[] {
    const papels: Type[] = papelsToCheck.filter(isPresent);
    if (papels.length > 0) {
      const papelCollectionIdentifiers = papelCollection.map(papelItem => this.getPapelIdentifier(papelItem)!);
      const papelsToAdd = papels.filter(papelItem => {
        const papelIdentifier = this.getPapelIdentifier(papelItem);
        if (papelCollectionIdentifiers.includes(papelIdentifier)) {
          return false;
        }
        papelCollectionIdentifiers.push(papelIdentifier);
        return true;
      });
      return [...papelsToAdd, ...papelCollection];
    }
    return papelCollection;
  }
}
