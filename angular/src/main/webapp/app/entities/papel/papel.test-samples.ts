import { IPapel, NewPapel } from './papel.model';

export const sampleWithRequiredData: IPapel = {
  id: 5091,
};

export const sampleWithPartialData: IPapel = {
  id: 1591,
};

export const sampleWithFullData: IPapel = {
  id: 27258,
  descricao: 'curve far um',
};

export const sampleWithNewData: NewPapel = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
