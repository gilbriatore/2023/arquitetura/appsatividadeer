import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { IPerfil } from 'app/entities/perfil/perfil.model';
import { PerfilService } from 'app/entities/perfil/service/perfil.service';
import { PapelService } from '../service/papel.service';
import { IPapel } from '../papel.model';
import { PapelFormService } from './papel-form.service';

import { PapelUpdateComponent } from './papel-update.component';

describe('Papel Management Update Component', () => {
  let comp: PapelUpdateComponent;
  let fixture: ComponentFixture<PapelUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let papelFormService: PapelFormService;
  let papelService: PapelService;
  let perfilService: PerfilService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([]), PapelUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(PapelUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(PapelUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    papelFormService = TestBed.inject(PapelFormService);
    papelService = TestBed.inject(PapelService);
    perfilService = TestBed.inject(PerfilService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Perfil query and add missing value', () => {
      const papel: IPapel = { id: 456 };
      const perfil: IPerfil = { id: 3043 };
      papel.perfil = perfil;

      const perfilCollection: IPerfil[] = [{ id: 17443 }];
      jest.spyOn(perfilService, 'query').mockReturnValue(of(new HttpResponse({ body: perfilCollection })));
      const additionalPerfils = [perfil];
      const expectedCollection: IPerfil[] = [...additionalPerfils, ...perfilCollection];
      jest.spyOn(perfilService, 'addPerfilToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ papel });
      comp.ngOnInit();

      expect(perfilService.query).toHaveBeenCalled();
      expect(perfilService.addPerfilToCollectionIfMissing).toHaveBeenCalledWith(
        perfilCollection,
        ...additionalPerfils.map(expect.objectContaining),
      );
      expect(comp.perfilsSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const papel: IPapel = { id: 456 };
      const perfil: IPerfil = { id: 17266 };
      papel.perfil = perfil;

      activatedRoute.data = of({ papel });
      comp.ngOnInit();

      expect(comp.perfilsSharedCollection).toContain(perfil);
      expect(comp.papel).toEqual(papel);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IPapel>>();
      const papel = { id: 123 };
      jest.spyOn(papelFormService, 'getPapel').mockReturnValue(papel);
      jest.spyOn(papelService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ papel });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: papel }));
      saveSubject.complete();

      // THEN
      expect(papelFormService.getPapel).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(papelService.update).toHaveBeenCalledWith(expect.objectContaining(papel));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IPapel>>();
      const papel = { id: 123 };
      jest.spyOn(papelFormService, 'getPapel').mockReturnValue({ id: null });
      jest.spyOn(papelService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ papel: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: papel }));
      saveSubject.complete();

      // THEN
      expect(papelFormService.getPapel).toHaveBeenCalled();
      expect(papelService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IPapel>>();
      const papel = { id: 123 };
      jest.spyOn(papelService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ papel });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(papelService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Compare relationships', () => {
    describe('comparePerfil', () => {
      it('Should forward to perfilService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(perfilService, 'comparePerfil');
        comp.comparePerfil(entity, entity2);
        expect(perfilService.comparePerfil).toHaveBeenCalledWith(entity, entity2);
      });
    });
  });
});
