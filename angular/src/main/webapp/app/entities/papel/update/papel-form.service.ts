import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { IPapel, NewPapel } from '../papel.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts IPapel for edit and NewPapelFormGroupInput for create.
 */
type PapelFormGroupInput = IPapel | PartialWithRequiredKeyOf<NewPapel>;

type PapelFormDefaults = Pick<NewPapel, 'id'>;

type PapelFormGroupContent = {
  id: FormControl<IPapel['id'] | NewPapel['id']>;
  descricao: FormControl<IPapel['descricao']>;
  perfil: FormControl<IPapel['perfil']>;
};

export type PapelFormGroup = FormGroup<PapelFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class PapelFormService {
  createPapelFormGroup(papel: PapelFormGroupInput = { id: null }): PapelFormGroup {
    const papelRawValue = {
      ...this.getFormDefaults(),
      ...papel,
    };
    return new FormGroup<PapelFormGroupContent>({
      id: new FormControl(
        { value: papelRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        },
      ),
      descricao: new FormControl(papelRawValue.descricao),
      perfil: new FormControl(papelRawValue.perfil),
    });
  }

  getPapel(form: PapelFormGroup): IPapel | NewPapel {
    return form.getRawValue() as IPapel | NewPapel;
  }

  resetForm(form: PapelFormGroup, papel: PapelFormGroupInput): void {
    const papelRawValue = { ...this.getFormDefaults(), ...papel };
    form.reset(
      {
        ...papelRawValue,
        id: { value: papelRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */,
    );
  }

  private getFormDefaults(): PapelFormDefaults {
    return {
      id: null,
    };
  }
}
