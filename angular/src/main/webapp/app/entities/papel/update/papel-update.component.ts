import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import SharedModule from 'app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IPerfil } from 'app/entities/perfil/perfil.model';
import { PerfilService } from 'app/entities/perfil/service/perfil.service';
import { IPapel } from '../papel.model';
import { PapelService } from '../service/papel.service';
import { PapelFormService, PapelFormGroup } from './papel-form.service';

@Component({
  standalone: true,
  selector: 'jhi-papel-update',
  templateUrl: './papel-update.component.html',
  imports: [SharedModule, FormsModule, ReactiveFormsModule],
})
export class PapelUpdateComponent implements OnInit {
  isSaving = false;
  papel: IPapel | null = null;

  perfilsSharedCollection: IPerfil[] = [];

  editForm: PapelFormGroup = this.papelFormService.createPapelFormGroup();

  constructor(
    protected papelService: PapelService,
    protected papelFormService: PapelFormService,
    protected perfilService: PerfilService,
    protected activatedRoute: ActivatedRoute,
  ) {}

  comparePerfil = (o1: IPerfil | null, o2: IPerfil | null): boolean => this.perfilService.comparePerfil(o1, o2);

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ papel }) => {
      this.papel = papel;
      if (papel) {
        this.updateForm(papel);
      }

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const papel = this.papelFormService.getPapel(this.editForm);
    if (papel.id !== null) {
      this.subscribeToSaveResponse(this.papelService.update(papel));
    } else {
      this.subscribeToSaveResponse(this.papelService.create(papel));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPapel>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(papel: IPapel): void {
    this.papel = papel;
    this.papelFormService.resetForm(this.editForm, papel);

    this.perfilsSharedCollection = this.perfilService.addPerfilToCollectionIfMissing<IPerfil>(this.perfilsSharedCollection, papel.perfil);
  }

  protected loadRelationshipsOptions(): void {
    this.perfilService
      .query()
      .pipe(map((res: HttpResponse<IPerfil[]>) => res.body ?? []))
      .pipe(map((perfils: IPerfil[]) => this.perfilService.addPerfilToCollectionIfMissing<IPerfil>(perfils, this.papel?.perfil)))
      .subscribe((perfils: IPerfil[]) => (this.perfilsSharedCollection = perfils));
  }
}
