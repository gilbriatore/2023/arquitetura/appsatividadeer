import { TestBed } from '@angular/core/testing';

import { sampleWithRequiredData, sampleWithNewData } from '../papel.test-samples';

import { PapelFormService } from './papel-form.service';

describe('Papel Form Service', () => {
  let service: PapelFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PapelFormService);
  });

  describe('Service methods', () => {
    describe('createPapelFormGroup', () => {
      it('should create a new form with FormControl', () => {
        const formGroup = service.createPapelFormGroup();

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            descricao: expect.any(Object),
            perfil: expect.any(Object),
          }),
        );
      });

      it('passing IPapel should create a new form with FormGroup', () => {
        const formGroup = service.createPapelFormGroup(sampleWithRequiredData);

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            descricao: expect.any(Object),
            perfil: expect.any(Object),
          }),
        );
      });
    });

    describe('getPapel', () => {
      it('should return NewPapel for default Papel initial value', () => {
        const formGroup = service.createPapelFormGroup(sampleWithNewData);

        const papel = service.getPapel(formGroup) as any;

        expect(papel).toMatchObject(sampleWithNewData);
      });

      it('should return NewPapel for empty Papel initial value', () => {
        const formGroup = service.createPapelFormGroup();

        const papel = service.getPapel(formGroup) as any;

        expect(papel).toMatchObject({});
      });

      it('should return IPapel', () => {
        const formGroup = service.createPapelFormGroup(sampleWithRequiredData);

        const papel = service.getPapel(formGroup) as any;

        expect(papel).toMatchObject(sampleWithRequiredData);
      });
    });

    describe('resetForm', () => {
      it('passing IPapel should not enable id FormControl', () => {
        const formGroup = service.createPapelFormGroup();
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, sampleWithRequiredData);

        expect(formGroup.controls.id.disabled).toBe(true);
      });

      it('passing NewPapel should disable id FormControl', () => {
        const formGroup = service.createPapelFormGroup(sampleWithRequiredData);
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, { id: null });

        expect(formGroup.controls.id.disabled).toBe(true);
      });
    });
  });
});
