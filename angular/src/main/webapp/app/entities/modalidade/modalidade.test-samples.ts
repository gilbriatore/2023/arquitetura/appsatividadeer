import { IModalidade, NewModalidade } from './modalidade.model';

export const sampleWithRequiredData: IModalidade = {
  id: 21030,
};

export const sampleWithPartialData: IModalidade = {
  id: 13101,
  nome: 'leak',
};

export const sampleWithFullData: IModalidade = {
  id: 28892,
  nome: 'huzzah slowly scarcely',
};

export const sampleWithNewData: NewModalidade = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
