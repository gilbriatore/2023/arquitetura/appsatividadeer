import NavbarItem from 'app/layouts/navbar/navbar-item.model';

export const EntityNavbarItems: NavbarItem[] = [
  {
    name: 'Atividade',
    route: '/atividade',
    translationKey: 'global.menu.entities.atividade',
  },
  {
    name: 'Bloom',
    route: '/bloom',
    translationKey: 'global.menu.entities.bloom',
  },
  {
    name: 'Curso',
    route: '/curso',
    translationKey: 'global.menu.entities.curso',
  },
  {
    name: 'Escola',
    route: '/escola',
    translationKey: 'global.menu.entities.escola',
  },
  {
    name: 'Matriz',
    route: '/matriz',
    translationKey: 'global.menu.entities.matriz',
  },
  {
    name: 'Modalidade',
    route: '/modalidade',
    translationKey: 'global.menu.entities.modalidade',
  },
  {
    name: 'TipoDeCurso',
    route: '/tipo-de-curso',
    translationKey: 'global.menu.entities.tipoDeCurso',
  },
  {
    name: 'Unidade',
    route: '/unidade',
    translationKey: 'global.menu.entities.unidade',
  },
  {
    name: 'Atitude',
    route: '/atitude',
    translationKey: 'global.menu.entities.atitude',
  },
  {
    name: 'Conhecimento',
    route: '/conhecimento',
    translationKey: 'global.menu.entities.conhecimento',
  },
  {
    name: 'Competencia',
    route: '/competencia',
    translationKey: 'global.menu.entities.competencia',
  },
  {
    name: 'Conteudo',
    route: '/conteudo',
    translationKey: 'global.menu.entities.conteudo',
  },
  {
    name: 'Disciplina',
    route: '/disciplina',
    translationKey: 'global.menu.entities.disciplina',
  },
  {
    name: 'DCN',
    route: '/dcn',
    translationKey: 'global.menu.entities.dCN',
  },
  {
    name: 'Diretriz',
    route: '/diretriz',
    translationKey: 'global.menu.entities.diretriz',
  },
  {
    name: 'Habilidade',
    route: '/habilidade',
    translationKey: 'global.menu.entities.habilidade',
  },
  {
    name: 'Papel',
    route: '/papel',
    translationKey: 'global.menu.entities.papel',
  },
  {
    name: 'Perfil',
    route: '/perfil',
    translationKey: 'global.menu.entities.perfil',
  },
  {
    name: 'Periodo',
    route: '/periodo',
    translationKey: 'global.menu.entities.periodo',
  },
  {
    name: 'Trilha',
    route: '/trilha',
    translationKey: 'global.menu.entities.trilha',
  },
];
