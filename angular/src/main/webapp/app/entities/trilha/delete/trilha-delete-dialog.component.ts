import { Component } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import SharedModule from 'app/shared/shared.module';
import { ITEM_DELETED_EVENT } from 'app/config/navigation.constants';
import { ITrilha } from '../trilha.model';
import { TrilhaService } from '../service/trilha.service';

@Component({
  standalone: true,
  templateUrl: './trilha-delete-dialog.component.html',
  imports: [SharedModule, FormsModule],
})
export class TrilhaDeleteDialogComponent {
  trilha?: ITrilha;

  constructor(
    protected trilhaService: TrilhaService,
    protected activeModal: NgbActiveModal,
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.trilhaService.delete(id).subscribe(() => {
      this.activeModal.close(ITEM_DELETED_EVENT);
    });
  }
}
