import { IConteudo } from 'app/entities/conteudo/conteudo.model';
import { IPeriodo } from 'app/entities/periodo/periodo.model';

export interface ITrilha {
  id: number;
  conteudo?: Pick<IConteudo, 'id'> | null;
  periodo?: Pick<IPeriodo, 'id'> | null;
}

export type NewTrilha = Omit<ITrilha, 'id'> & { id: null };
