import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import SharedModule from 'app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IConteudo } from 'app/entities/conteudo/conteudo.model';
import { ConteudoService } from 'app/entities/conteudo/service/conteudo.service';
import { IPeriodo } from 'app/entities/periodo/periodo.model';
import { PeriodoService } from 'app/entities/periodo/service/periodo.service';
import { TrilhaService } from '../service/trilha.service';
import { ITrilha } from '../trilha.model';
import { TrilhaFormService, TrilhaFormGroup } from './trilha-form.service';

@Component({
  standalone: true,
  selector: 'jhi-trilha-update',
  templateUrl: './trilha-update.component.html',
  imports: [SharedModule, FormsModule, ReactiveFormsModule],
})
export class TrilhaUpdateComponent implements OnInit {
  isSaving = false;
  trilha: ITrilha | null = null;

  conteudosSharedCollection: IConteudo[] = [];
  periodosSharedCollection: IPeriodo[] = [];

  editForm: TrilhaFormGroup = this.trilhaFormService.createTrilhaFormGroup();

  constructor(
    protected trilhaService: TrilhaService,
    protected trilhaFormService: TrilhaFormService,
    protected conteudoService: ConteudoService,
    protected periodoService: PeriodoService,
    protected activatedRoute: ActivatedRoute,
  ) {}

  compareConteudo = (o1: IConteudo | null, o2: IConteudo | null): boolean => this.conteudoService.compareConteudo(o1, o2);

  comparePeriodo = (o1: IPeriodo | null, o2: IPeriodo | null): boolean => this.periodoService.comparePeriodo(o1, o2);

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ trilha }) => {
      this.trilha = trilha;
      if (trilha) {
        this.updateForm(trilha);
      }

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const trilha = this.trilhaFormService.getTrilha(this.editForm);
    if (trilha.id !== null) {
      this.subscribeToSaveResponse(this.trilhaService.update(trilha));
    } else {
      this.subscribeToSaveResponse(this.trilhaService.create(trilha));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ITrilha>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(trilha: ITrilha): void {
    this.trilha = trilha;
    this.trilhaFormService.resetForm(this.editForm, trilha);

    this.conteudosSharedCollection = this.conteudoService.addConteudoToCollectionIfMissing<IConteudo>(
      this.conteudosSharedCollection,
      trilha.conteudo,
    );
    this.periodosSharedCollection = this.periodoService.addPeriodoToCollectionIfMissing<IPeriodo>(
      this.periodosSharedCollection,
      trilha.periodo,
    );
  }

  protected loadRelationshipsOptions(): void {
    this.conteudoService
      .query()
      .pipe(map((res: HttpResponse<IConteudo[]>) => res.body ?? []))
      .pipe(
        map((conteudos: IConteudo[]) => this.conteudoService.addConteudoToCollectionIfMissing<IConteudo>(conteudos, this.trilha?.conteudo)),
      )
      .subscribe((conteudos: IConteudo[]) => (this.conteudosSharedCollection = conteudos));

    this.periodoService
      .query()
      .pipe(map((res: HttpResponse<IPeriodo[]>) => res.body ?? []))
      .pipe(map((periodos: IPeriodo[]) => this.periodoService.addPeriodoToCollectionIfMissing<IPeriodo>(periodos, this.trilha?.periodo)))
      .subscribe((periodos: IPeriodo[]) => (this.periodosSharedCollection = periodos));
  }
}
