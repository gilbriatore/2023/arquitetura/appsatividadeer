import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { IConteudo } from 'app/entities/conteudo/conteudo.model';
import { ConteudoService } from 'app/entities/conteudo/service/conteudo.service';
import { IPeriodo } from 'app/entities/periodo/periodo.model';
import { PeriodoService } from 'app/entities/periodo/service/periodo.service';
import { ITrilha } from '../trilha.model';
import { TrilhaService } from '../service/trilha.service';
import { TrilhaFormService } from './trilha-form.service';

import { TrilhaUpdateComponent } from './trilha-update.component';

describe('Trilha Management Update Component', () => {
  let comp: TrilhaUpdateComponent;
  let fixture: ComponentFixture<TrilhaUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let trilhaFormService: TrilhaFormService;
  let trilhaService: TrilhaService;
  let conteudoService: ConteudoService;
  let periodoService: PeriodoService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([]), TrilhaUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(TrilhaUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(TrilhaUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    trilhaFormService = TestBed.inject(TrilhaFormService);
    trilhaService = TestBed.inject(TrilhaService);
    conteudoService = TestBed.inject(ConteudoService);
    periodoService = TestBed.inject(PeriodoService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Conteudo query and add missing value', () => {
      const trilha: ITrilha = { id: 456 };
      const conteudo: IConteudo = { id: 4553 };
      trilha.conteudo = conteudo;

      const conteudoCollection: IConteudo[] = [{ id: 7794 }];
      jest.spyOn(conteudoService, 'query').mockReturnValue(of(new HttpResponse({ body: conteudoCollection })));
      const additionalConteudos = [conteudo];
      const expectedCollection: IConteudo[] = [...additionalConteudos, ...conteudoCollection];
      jest.spyOn(conteudoService, 'addConteudoToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ trilha });
      comp.ngOnInit();

      expect(conteudoService.query).toHaveBeenCalled();
      expect(conteudoService.addConteudoToCollectionIfMissing).toHaveBeenCalledWith(
        conteudoCollection,
        ...additionalConteudos.map(expect.objectContaining),
      );
      expect(comp.conteudosSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Periodo query and add missing value', () => {
      const trilha: ITrilha = { id: 456 };
      const periodo: IPeriodo = { id: 827 };
      trilha.periodo = periodo;

      const periodoCollection: IPeriodo[] = [{ id: 21605 }];
      jest.spyOn(periodoService, 'query').mockReturnValue(of(new HttpResponse({ body: periodoCollection })));
      const additionalPeriodos = [periodo];
      const expectedCollection: IPeriodo[] = [...additionalPeriodos, ...periodoCollection];
      jest.spyOn(periodoService, 'addPeriodoToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ trilha });
      comp.ngOnInit();

      expect(periodoService.query).toHaveBeenCalled();
      expect(periodoService.addPeriodoToCollectionIfMissing).toHaveBeenCalledWith(
        periodoCollection,
        ...additionalPeriodos.map(expect.objectContaining),
      );
      expect(comp.periodosSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const trilha: ITrilha = { id: 456 };
      const conteudo: IConteudo = { id: 11591 };
      trilha.conteudo = conteudo;
      const periodo: IPeriodo = { id: 17863 };
      trilha.periodo = periodo;

      activatedRoute.data = of({ trilha });
      comp.ngOnInit();

      expect(comp.conteudosSharedCollection).toContain(conteudo);
      expect(comp.periodosSharedCollection).toContain(periodo);
      expect(comp.trilha).toEqual(trilha);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ITrilha>>();
      const trilha = { id: 123 };
      jest.spyOn(trilhaFormService, 'getTrilha').mockReturnValue(trilha);
      jest.spyOn(trilhaService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ trilha });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: trilha }));
      saveSubject.complete();

      // THEN
      expect(trilhaFormService.getTrilha).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(trilhaService.update).toHaveBeenCalledWith(expect.objectContaining(trilha));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ITrilha>>();
      const trilha = { id: 123 };
      jest.spyOn(trilhaFormService, 'getTrilha').mockReturnValue({ id: null });
      jest.spyOn(trilhaService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ trilha: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: trilha }));
      saveSubject.complete();

      // THEN
      expect(trilhaFormService.getTrilha).toHaveBeenCalled();
      expect(trilhaService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ITrilha>>();
      const trilha = { id: 123 };
      jest.spyOn(trilhaService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ trilha });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(trilhaService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Compare relationships', () => {
    describe('compareConteudo', () => {
      it('Should forward to conteudoService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(conteudoService, 'compareConteudo');
        comp.compareConteudo(entity, entity2);
        expect(conteudoService.compareConteudo).toHaveBeenCalledWith(entity, entity2);
      });
    });

    describe('comparePeriodo', () => {
      it('Should forward to periodoService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(periodoService, 'comparePeriodo');
        comp.comparePeriodo(entity, entity2);
        expect(periodoService.comparePeriodo).toHaveBeenCalledWith(entity, entity2);
      });
    });
  });
});
