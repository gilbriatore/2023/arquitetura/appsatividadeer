import { TestBed } from '@angular/core/testing';

import { sampleWithRequiredData, sampleWithNewData } from '../trilha.test-samples';

import { TrilhaFormService } from './trilha-form.service';

describe('Trilha Form Service', () => {
  let service: TrilhaFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TrilhaFormService);
  });

  describe('Service methods', () => {
    describe('createTrilhaFormGroup', () => {
      it('should create a new form with FormControl', () => {
        const formGroup = service.createTrilhaFormGroup();

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            conteudo: expect.any(Object),
            periodo: expect.any(Object),
          }),
        );
      });

      it('passing ITrilha should create a new form with FormGroup', () => {
        const formGroup = service.createTrilhaFormGroup(sampleWithRequiredData);

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            conteudo: expect.any(Object),
            periodo: expect.any(Object),
          }),
        );
      });
    });

    describe('getTrilha', () => {
      it('should return NewTrilha for default Trilha initial value', () => {
        const formGroup = service.createTrilhaFormGroup(sampleWithNewData);

        const trilha = service.getTrilha(formGroup) as any;

        expect(trilha).toMatchObject(sampleWithNewData);
      });

      it('should return NewTrilha for empty Trilha initial value', () => {
        const formGroup = service.createTrilhaFormGroup();

        const trilha = service.getTrilha(formGroup) as any;

        expect(trilha).toMatchObject({});
      });

      it('should return ITrilha', () => {
        const formGroup = service.createTrilhaFormGroup(sampleWithRequiredData);

        const trilha = service.getTrilha(formGroup) as any;

        expect(trilha).toMatchObject(sampleWithRequiredData);
      });
    });

    describe('resetForm', () => {
      it('passing ITrilha should not enable id FormControl', () => {
        const formGroup = service.createTrilhaFormGroup();
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, sampleWithRequiredData);

        expect(formGroup.controls.id.disabled).toBe(true);
      });

      it('passing NewTrilha should disable id FormControl', () => {
        const formGroup = service.createTrilhaFormGroup(sampleWithRequiredData);
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, { id: null });

        expect(formGroup.controls.id.disabled).toBe(true);
      });
    });
  });
});
