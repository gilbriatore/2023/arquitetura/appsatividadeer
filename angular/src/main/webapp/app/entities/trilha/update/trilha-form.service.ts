import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { ITrilha, NewTrilha } from '../trilha.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts ITrilha for edit and NewTrilhaFormGroupInput for create.
 */
type TrilhaFormGroupInput = ITrilha | PartialWithRequiredKeyOf<NewTrilha>;

type TrilhaFormDefaults = Pick<NewTrilha, 'id'>;

type TrilhaFormGroupContent = {
  id: FormControl<ITrilha['id'] | NewTrilha['id']>;
  conteudo: FormControl<ITrilha['conteudo']>;
  periodo: FormControl<ITrilha['periodo']>;
};

export type TrilhaFormGroup = FormGroup<TrilhaFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class TrilhaFormService {
  createTrilhaFormGroup(trilha: TrilhaFormGroupInput = { id: null }): TrilhaFormGroup {
    const trilhaRawValue = {
      ...this.getFormDefaults(),
      ...trilha,
    };
    return new FormGroup<TrilhaFormGroupContent>({
      id: new FormControl(
        { value: trilhaRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        },
      ),
      conteudo: new FormControl(trilhaRawValue.conteudo),
      periodo: new FormControl(trilhaRawValue.periodo),
    });
  }

  getTrilha(form: TrilhaFormGroup): ITrilha | NewTrilha {
    return form.getRawValue() as ITrilha | NewTrilha;
  }

  resetForm(form: TrilhaFormGroup, trilha: TrilhaFormGroupInput): void {
    const trilhaRawValue = { ...this.getFormDefaults(), ...trilha };
    form.reset(
      {
        ...trilhaRawValue,
        id: { value: trilhaRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */,
    );
  }

  private getFormDefaults(): TrilhaFormDefaults {
    return {
      id: null,
    };
  }
}
