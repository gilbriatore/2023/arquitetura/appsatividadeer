import { ITrilha, NewTrilha } from './trilha.model';

export const sampleWithRequiredData: ITrilha = {
  id: 30152,
};

export const sampleWithPartialData: ITrilha = {
  id: 31376,
};

export const sampleWithFullData: ITrilha = {
  id: 14936,
};

export const sampleWithNewData: NewTrilha = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
