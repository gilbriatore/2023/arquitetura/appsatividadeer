import { inject } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of, EMPTY, Observable } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { ITrilha } from '../trilha.model';
import { TrilhaService } from '../service/trilha.service';

export const trilhaResolve = (route: ActivatedRouteSnapshot): Observable<null | ITrilha> => {
  const id = route.params['id'];
  if (id) {
    return inject(TrilhaService)
      .find(id)
      .pipe(
        mergeMap((trilha: HttpResponse<ITrilha>) => {
          if (trilha.body) {
            return of(trilha.body);
          } else {
            inject(Router).navigate(['404']);
            return EMPTY;
          }
        }),
      );
  }
  return of(null);
};

export default trilhaResolve;
