import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { ITrilha } from '../trilha.model';
import { sampleWithRequiredData, sampleWithNewData, sampleWithPartialData, sampleWithFullData } from '../trilha.test-samples';

import { TrilhaService } from './trilha.service';

const requireRestSample: ITrilha = {
  ...sampleWithRequiredData,
};

describe('Trilha Service', () => {
  let service: TrilhaService;
  let httpMock: HttpTestingController;
  let expectedResult: ITrilha | ITrilha[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(TrilhaService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should create a Trilha', () => {
      const trilha = { ...sampleWithNewData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.create(trilha).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a Trilha', () => {
      const trilha = { ...sampleWithRequiredData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.update(trilha).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a Trilha', () => {
      const patchObject = { ...sampleWithPartialData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of Trilha', () => {
      const returnedFromService = { ...requireRestSample };

      const expected = { ...sampleWithRequiredData };

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toMatchObject([expected]);
    });

    it('should delete a Trilha', () => {
      const expected = true;

      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult).toBe(expected);
    });

    describe('addTrilhaToCollectionIfMissing', () => {
      it('should add a Trilha to an empty array', () => {
        const trilha: ITrilha = sampleWithRequiredData;
        expectedResult = service.addTrilhaToCollectionIfMissing([], trilha);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(trilha);
      });

      it('should not add a Trilha to an array that contains it', () => {
        const trilha: ITrilha = sampleWithRequiredData;
        const trilhaCollection: ITrilha[] = [
          {
            ...trilha,
          },
          sampleWithPartialData,
        ];
        expectedResult = service.addTrilhaToCollectionIfMissing(trilhaCollection, trilha);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a Trilha to an array that doesn't contain it", () => {
        const trilha: ITrilha = sampleWithRequiredData;
        const trilhaCollection: ITrilha[] = [sampleWithPartialData];
        expectedResult = service.addTrilhaToCollectionIfMissing(trilhaCollection, trilha);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(trilha);
      });

      it('should add only unique Trilha to an array', () => {
        const trilhaArray: ITrilha[] = [sampleWithRequiredData, sampleWithPartialData, sampleWithFullData];
        const trilhaCollection: ITrilha[] = [sampleWithRequiredData];
        expectedResult = service.addTrilhaToCollectionIfMissing(trilhaCollection, ...trilhaArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const trilha: ITrilha = sampleWithRequiredData;
        const trilha2: ITrilha = sampleWithPartialData;
        expectedResult = service.addTrilhaToCollectionIfMissing([], trilha, trilha2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(trilha);
        expect(expectedResult).toContain(trilha2);
      });

      it('should accept null and undefined values', () => {
        const trilha: ITrilha = sampleWithRequiredData;
        expectedResult = service.addTrilhaToCollectionIfMissing([], null, trilha, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(trilha);
      });

      it('should return initial array if no Trilha is added', () => {
        const trilhaCollection: ITrilha[] = [sampleWithRequiredData];
        expectedResult = service.addTrilhaToCollectionIfMissing(trilhaCollection, undefined, null);
        expect(expectedResult).toEqual(trilhaCollection);
      });
    });

    describe('compareTrilha', () => {
      it('Should return true if both entities are null', () => {
        const entity1 = null;
        const entity2 = null;

        const compareResult = service.compareTrilha(entity1, entity2);

        expect(compareResult).toEqual(true);
      });

      it('Should return false if one entity is null', () => {
        const entity1 = { id: 123 };
        const entity2 = null;

        const compareResult1 = service.compareTrilha(entity1, entity2);
        const compareResult2 = service.compareTrilha(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey differs', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 456 };

        const compareResult1 = service.compareTrilha(entity1, entity2);
        const compareResult2 = service.compareTrilha(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey matches', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 123 };

        const compareResult1 = service.compareTrilha(entity1, entity2);
        const compareResult2 = service.compareTrilha(entity2, entity1);

        expect(compareResult1).toEqual(true);
        expect(compareResult2).toEqual(true);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
