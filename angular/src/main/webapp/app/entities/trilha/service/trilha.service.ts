import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { ITrilha, NewTrilha } from '../trilha.model';

export type PartialUpdateTrilha = Partial<ITrilha> & Pick<ITrilha, 'id'>;

export type EntityResponseType = HttpResponse<ITrilha>;
export type EntityArrayResponseType = HttpResponse<ITrilha[]>;

@Injectable({ providedIn: 'root' })
export class TrilhaService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/trilhas');

  constructor(
    protected http: HttpClient,
    protected applicationConfigService: ApplicationConfigService,
  ) {}

  create(trilha: NewTrilha): Observable<EntityResponseType> {
    return this.http.post<ITrilha>(this.resourceUrl, trilha, { observe: 'response' });
  }

  update(trilha: ITrilha): Observable<EntityResponseType> {
    return this.http.put<ITrilha>(`${this.resourceUrl}/${this.getTrilhaIdentifier(trilha)}`, trilha, { observe: 'response' });
  }

  partialUpdate(trilha: PartialUpdateTrilha): Observable<EntityResponseType> {
    return this.http.patch<ITrilha>(`${this.resourceUrl}/${this.getTrilhaIdentifier(trilha)}`, trilha, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ITrilha>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ITrilha[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getTrilhaIdentifier(trilha: Pick<ITrilha, 'id'>): number {
    return trilha.id;
  }

  compareTrilha(o1: Pick<ITrilha, 'id'> | null, o2: Pick<ITrilha, 'id'> | null): boolean {
    return o1 && o2 ? this.getTrilhaIdentifier(o1) === this.getTrilhaIdentifier(o2) : o1 === o2;
  }

  addTrilhaToCollectionIfMissing<Type extends Pick<ITrilha, 'id'>>(
    trilhaCollection: Type[],
    ...trilhasToCheck: (Type | null | undefined)[]
  ): Type[] {
    const trilhas: Type[] = trilhasToCheck.filter(isPresent);
    if (trilhas.length > 0) {
      const trilhaCollectionIdentifiers = trilhaCollection.map(trilhaItem => this.getTrilhaIdentifier(trilhaItem)!);
      const trilhasToAdd = trilhas.filter(trilhaItem => {
        const trilhaIdentifier = this.getTrilhaIdentifier(trilhaItem);
        if (trilhaCollectionIdentifiers.includes(trilhaIdentifier)) {
          return false;
        }
        trilhaCollectionIdentifiers.push(trilhaIdentifier);
        return true;
      });
      return [...trilhasToAdd, ...trilhaCollection];
    }
    return trilhaCollection;
  }
}
