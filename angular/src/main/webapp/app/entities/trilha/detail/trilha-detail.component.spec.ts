import { TestBed } from '@angular/core/testing';
import { provideRouter, withComponentInputBinding } from '@angular/router';
import { RouterTestingHarness, RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { TrilhaDetailComponent } from './trilha-detail.component';

describe('Trilha Management Detail Component', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [TrilhaDetailComponent, RouterTestingModule.withRoutes([], { bindToComponentInputs: true })],
      providers: [
        provideRouter(
          [
            {
              path: '**',
              component: TrilhaDetailComponent,
              resolve: { trilha: () => of({ id: 123 }) },
            },
          ],
          withComponentInputBinding(),
        ),
      ],
    })
      .overrideTemplate(TrilhaDetailComponent, '')
      .compileComponents();
  });

  describe('OnInit', () => {
    it('Should load trilha on init', async () => {
      const harness = await RouterTestingHarness.create();
      const instance = await harness.navigateByUrl('/', TrilhaDetailComponent);

      // THEN
      expect(instance.trilha).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
