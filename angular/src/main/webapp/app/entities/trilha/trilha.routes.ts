import { Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { ASC } from 'app/config/navigation.constants';
import { TrilhaComponent } from './list/trilha.component';
import { TrilhaDetailComponent } from './detail/trilha-detail.component';
import { TrilhaUpdateComponent } from './update/trilha-update.component';
import TrilhaResolve from './route/trilha-routing-resolve.service';

const trilhaRoute: Routes = [
  {
    path: '',
    component: TrilhaComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: TrilhaDetailComponent,
    resolve: {
      trilha: TrilhaResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: TrilhaUpdateComponent,
    resolve: {
      trilha: TrilhaResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: TrilhaUpdateComponent,
    resolve: {
      trilha: TrilhaResolve,
    },
    canActivate: [UserRouteAccessService],
  },
];

export default trilhaRoute;
