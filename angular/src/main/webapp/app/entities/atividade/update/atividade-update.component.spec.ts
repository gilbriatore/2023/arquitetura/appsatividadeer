import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { IPapel } from 'app/entities/papel/papel.model';
import { PapelService } from 'app/entities/papel/service/papel.service';
import { IUnidade } from 'app/entities/unidade/unidade.model';
import { UnidadeService } from 'app/entities/unidade/service/unidade.service';
import { IAtividade } from '../atividade.model';
import { AtividadeService } from '../service/atividade.service';
import { AtividadeFormService } from './atividade-form.service';

import { AtividadeUpdateComponent } from './atividade-update.component';

describe('Atividade Management Update Component', () => {
  let comp: AtividadeUpdateComponent;
  let fixture: ComponentFixture<AtividadeUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let atividadeFormService: AtividadeFormService;
  let atividadeService: AtividadeService;
  let papelService: PapelService;
  let unidadeService: UnidadeService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([]), AtividadeUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(AtividadeUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(AtividadeUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    atividadeFormService = TestBed.inject(AtividadeFormService);
    atividadeService = TestBed.inject(AtividadeService);
    papelService = TestBed.inject(PapelService);
    unidadeService = TestBed.inject(UnidadeService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Papel query and add missing value', () => {
      const atividade: IAtividade = { id: 456 };
      const papel: IPapel = { id: 15992 };
      atividade.papel = papel;

      const papelCollection: IPapel[] = [{ id: 17362 }];
      jest.spyOn(papelService, 'query').mockReturnValue(of(new HttpResponse({ body: papelCollection })));
      const additionalPapels = [papel];
      const expectedCollection: IPapel[] = [...additionalPapels, ...papelCollection];
      jest.spyOn(papelService, 'addPapelToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ atividade });
      comp.ngOnInit();

      expect(papelService.query).toHaveBeenCalled();
      expect(papelService.addPapelToCollectionIfMissing).toHaveBeenCalledWith(
        papelCollection,
        ...additionalPapels.map(expect.objectContaining),
      );
      expect(comp.papelsSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Unidade query and add missing value', () => {
      const atividade: IAtividade = { id: 456 };
      const unidade: IUnidade = { id: 13193 };
      atividade.unidade = unidade;

      const unidadeCollection: IUnidade[] = [{ id: 7817 }];
      jest.spyOn(unidadeService, 'query').mockReturnValue(of(new HttpResponse({ body: unidadeCollection })));
      const additionalUnidades = [unidade];
      const expectedCollection: IUnidade[] = [...additionalUnidades, ...unidadeCollection];
      jest.spyOn(unidadeService, 'addUnidadeToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ atividade });
      comp.ngOnInit();

      expect(unidadeService.query).toHaveBeenCalled();
      expect(unidadeService.addUnidadeToCollectionIfMissing).toHaveBeenCalledWith(
        unidadeCollection,
        ...additionalUnidades.map(expect.objectContaining),
      );
      expect(comp.unidadesSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const atividade: IAtividade = { id: 456 };
      const papel: IPapel = { id: 8582 };
      atividade.papel = papel;
      const unidade: IUnidade = { id: 12346 };
      atividade.unidade = unidade;

      activatedRoute.data = of({ atividade });
      comp.ngOnInit();

      expect(comp.papelsSharedCollection).toContain(papel);
      expect(comp.unidadesSharedCollection).toContain(unidade);
      expect(comp.atividade).toEqual(atividade);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IAtividade>>();
      const atividade = { id: 123 };
      jest.spyOn(atividadeFormService, 'getAtividade').mockReturnValue(atividade);
      jest.spyOn(atividadeService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ atividade });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: atividade }));
      saveSubject.complete();

      // THEN
      expect(atividadeFormService.getAtividade).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(atividadeService.update).toHaveBeenCalledWith(expect.objectContaining(atividade));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IAtividade>>();
      const atividade = { id: 123 };
      jest.spyOn(atividadeFormService, 'getAtividade').mockReturnValue({ id: null });
      jest.spyOn(atividadeService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ atividade: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: atividade }));
      saveSubject.complete();

      // THEN
      expect(atividadeFormService.getAtividade).toHaveBeenCalled();
      expect(atividadeService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IAtividade>>();
      const atividade = { id: 123 };
      jest.spyOn(atividadeService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ atividade });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(atividadeService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Compare relationships', () => {
    describe('comparePapel', () => {
      it('Should forward to papelService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(papelService, 'comparePapel');
        comp.comparePapel(entity, entity2);
        expect(papelService.comparePapel).toHaveBeenCalledWith(entity, entity2);
      });
    });

    describe('compareUnidade', () => {
      it('Should forward to unidadeService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(unidadeService, 'compareUnidade');
        comp.compareUnidade(entity, entity2);
        expect(unidadeService.compareUnidade).toHaveBeenCalledWith(entity, entity2);
      });
    });
  });
});
