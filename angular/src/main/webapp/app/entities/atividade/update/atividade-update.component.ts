import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import SharedModule from 'app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IPapel } from 'app/entities/papel/papel.model';
import { PapelService } from 'app/entities/papel/service/papel.service';
import { IUnidade } from 'app/entities/unidade/unidade.model';
import { UnidadeService } from 'app/entities/unidade/service/unidade.service';
import { AtividadeService } from '../service/atividade.service';
import { IAtividade } from '../atividade.model';
import { AtividadeFormService, AtividadeFormGroup } from './atividade-form.service';

@Component({
  standalone: true,
  selector: 'jhi-atividade-update',
  templateUrl: './atividade-update.component.html',
  imports: [SharedModule, FormsModule, ReactiveFormsModule],
})
export class AtividadeUpdateComponent implements OnInit {
  isSaving = false;
  atividade: IAtividade | null = null;

  papelsSharedCollection: IPapel[] = [];
  unidadesSharedCollection: IUnidade[] = [];

  editForm: AtividadeFormGroup = this.atividadeFormService.createAtividadeFormGroup();

  constructor(
    protected atividadeService: AtividadeService,
    protected atividadeFormService: AtividadeFormService,
    protected papelService: PapelService,
    protected unidadeService: UnidadeService,
    protected activatedRoute: ActivatedRoute,
  ) {}

  comparePapel = (o1: IPapel | null, o2: IPapel | null): boolean => this.papelService.comparePapel(o1, o2);

  compareUnidade = (o1: IUnidade | null, o2: IUnidade | null): boolean => this.unidadeService.compareUnidade(o1, o2);

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ atividade }) => {
      this.atividade = atividade;
      if (atividade) {
        this.updateForm(atividade);
      }

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const atividade = this.atividadeFormService.getAtividade(this.editForm);
    if (atividade.id !== null) {
      this.subscribeToSaveResponse(this.atividadeService.update(atividade));
    } else {
      this.subscribeToSaveResponse(this.atividadeService.create(atividade));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IAtividade>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(atividade: IAtividade): void {
    this.atividade = atividade;
    this.atividadeFormService.resetForm(this.editForm, atividade);

    this.papelsSharedCollection = this.papelService.addPapelToCollectionIfMissing<IPapel>(this.papelsSharedCollection, atividade.papel);
    this.unidadesSharedCollection = this.unidadeService.addUnidadeToCollectionIfMissing<IUnidade>(
      this.unidadesSharedCollection,
      atividade.unidade,
    );
  }

  protected loadRelationshipsOptions(): void {
    this.papelService
      .query()
      .pipe(map((res: HttpResponse<IPapel[]>) => res.body ?? []))
      .pipe(map((papels: IPapel[]) => this.papelService.addPapelToCollectionIfMissing<IPapel>(papels, this.atividade?.papel)))
      .subscribe((papels: IPapel[]) => (this.papelsSharedCollection = papels));

    this.unidadeService
      .query()
      .pipe(map((res: HttpResponse<IUnidade[]>) => res.body ?? []))
      .pipe(map((unidades: IUnidade[]) => this.unidadeService.addUnidadeToCollectionIfMissing<IUnidade>(unidades, this.atividade?.unidade)))
      .subscribe((unidades: IUnidade[]) => (this.unidadesSharedCollection = unidades));
  }
}
