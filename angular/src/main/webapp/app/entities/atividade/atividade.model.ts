import { IPapel } from 'app/entities/papel/papel.model';
import { IUnidade } from 'app/entities/unidade/unidade.model';

export interface IAtividade {
  id: number;
  nome?: string | null;
  papel?: Pick<IPapel, 'id'> | null;
  unidade?: Pick<IUnidade, 'id'> | null;
}

export type NewAtividade = Omit<IAtividade, 'id'> & { id: null };
