import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { IDCN, NewDCN } from '../dcn.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts IDCN for edit and NewDCNFormGroupInput for create.
 */
type DCNFormGroupInput = IDCN | PartialWithRequiredKeyOf<NewDCN>;

type DCNFormDefaults = Pick<NewDCN, 'id'>;

type DCNFormGroupContent = {
  id: FormControl<IDCN['id'] | NewDCN['id']>;
  descricao: FormControl<IDCN['descricao']>;
};

export type DCNFormGroup = FormGroup<DCNFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class DCNFormService {
  createDCNFormGroup(dCN: DCNFormGroupInput = { id: null }): DCNFormGroup {
    const dCNRawValue = {
      ...this.getFormDefaults(),
      ...dCN,
    };
    return new FormGroup<DCNFormGroupContent>({
      id: new FormControl(
        { value: dCNRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        },
      ),
      descricao: new FormControl(dCNRawValue.descricao),
    });
  }

  getDCN(form: DCNFormGroup): IDCN | NewDCN {
    return form.getRawValue() as IDCN | NewDCN;
  }

  resetForm(form: DCNFormGroup, dCN: DCNFormGroupInput): void {
    const dCNRawValue = { ...this.getFormDefaults(), ...dCN };
    form.reset(
      {
        ...dCNRawValue,
        id: { value: dCNRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */,
    );
  }

  private getFormDefaults(): DCNFormDefaults {
    return {
      id: null,
    };
  }
}
