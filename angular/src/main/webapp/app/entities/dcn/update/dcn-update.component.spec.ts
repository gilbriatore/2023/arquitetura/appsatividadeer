import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { DCNService } from '../service/dcn.service';
import { IDCN } from '../dcn.model';
import { DCNFormService } from './dcn-form.service';

import { DCNUpdateComponent } from './dcn-update.component';

describe('DCN Management Update Component', () => {
  let comp: DCNUpdateComponent;
  let fixture: ComponentFixture<DCNUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let dCNFormService: DCNFormService;
  let dCNService: DCNService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([]), DCNUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(DCNUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(DCNUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    dCNFormService = TestBed.inject(DCNFormService);
    dCNService = TestBed.inject(DCNService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should update editForm', () => {
      const dCN: IDCN = { id: 456 };

      activatedRoute.data = of({ dCN });
      comp.ngOnInit();

      expect(comp.dCN).toEqual(dCN);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IDCN>>();
      const dCN = { id: 123 };
      jest.spyOn(dCNFormService, 'getDCN').mockReturnValue(dCN);
      jest.spyOn(dCNService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ dCN });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: dCN }));
      saveSubject.complete();

      // THEN
      expect(dCNFormService.getDCN).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(dCNService.update).toHaveBeenCalledWith(expect.objectContaining(dCN));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IDCN>>();
      const dCN = { id: 123 };
      jest.spyOn(dCNFormService, 'getDCN').mockReturnValue({ id: null });
      jest.spyOn(dCNService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ dCN: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: dCN }));
      saveSubject.complete();

      // THEN
      expect(dCNFormService.getDCN).toHaveBeenCalled();
      expect(dCNService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IDCN>>();
      const dCN = { id: 123 };
      jest.spyOn(dCNService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ dCN });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(dCNService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });
});
