import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import SharedModule from 'app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IDCN } from '../dcn.model';
import { DCNService } from '../service/dcn.service';
import { DCNFormService, DCNFormGroup } from './dcn-form.service';

@Component({
  standalone: true,
  selector: 'jhi-dcn-update',
  templateUrl: './dcn-update.component.html',
  imports: [SharedModule, FormsModule, ReactiveFormsModule],
})
export class DCNUpdateComponent implements OnInit {
  isSaving = false;
  dCN: IDCN | null = null;

  editForm: DCNFormGroup = this.dCNFormService.createDCNFormGroup();

  constructor(
    protected dCNService: DCNService,
    protected dCNFormService: DCNFormService,
    protected activatedRoute: ActivatedRoute,
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ dCN }) => {
      this.dCN = dCN;
      if (dCN) {
        this.updateForm(dCN);
      }
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const dCN = this.dCNFormService.getDCN(this.editForm);
    if (dCN.id !== null) {
      this.subscribeToSaveResponse(this.dCNService.update(dCN));
    } else {
      this.subscribeToSaveResponse(this.dCNService.create(dCN));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IDCN>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(dCN: IDCN): void {
    this.dCN = dCN;
    this.dCNFormService.resetForm(this.editForm, dCN);
  }
}
