import { TestBed } from '@angular/core/testing';

import { sampleWithRequiredData, sampleWithNewData } from '../dcn.test-samples';

import { DCNFormService } from './dcn-form.service';

describe('DCN Form Service', () => {
  let service: DCNFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DCNFormService);
  });

  describe('Service methods', () => {
    describe('createDCNFormGroup', () => {
      it('should create a new form with FormControl', () => {
        const formGroup = service.createDCNFormGroup();

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            descricao: expect.any(Object),
          }),
        );
      });

      it('passing IDCN should create a new form with FormGroup', () => {
        const formGroup = service.createDCNFormGroup(sampleWithRequiredData);

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            descricao: expect.any(Object),
          }),
        );
      });
    });

    describe('getDCN', () => {
      it('should return NewDCN for default DCN initial value', () => {
        const formGroup = service.createDCNFormGroup(sampleWithNewData);

        const dCN = service.getDCN(formGroup) as any;

        expect(dCN).toMatchObject(sampleWithNewData);
      });

      it('should return NewDCN for empty DCN initial value', () => {
        const formGroup = service.createDCNFormGroup();

        const dCN = service.getDCN(formGroup) as any;

        expect(dCN).toMatchObject({});
      });

      it('should return IDCN', () => {
        const formGroup = service.createDCNFormGroup(sampleWithRequiredData);

        const dCN = service.getDCN(formGroup) as any;

        expect(dCN).toMatchObject(sampleWithRequiredData);
      });
    });

    describe('resetForm', () => {
      it('passing IDCN should not enable id FormControl', () => {
        const formGroup = service.createDCNFormGroup();
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, sampleWithRequiredData);

        expect(formGroup.controls.id.disabled).toBe(true);
      });

      it('passing NewDCN should disable id FormControl', () => {
        const formGroup = service.createDCNFormGroup(sampleWithRequiredData);
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, { id: null });

        expect(formGroup.controls.id.disabled).toBe(true);
      });
    });
  });
});
