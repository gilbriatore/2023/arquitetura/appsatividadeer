import { TestBed } from '@angular/core/testing';
import { provideRouter, withComponentInputBinding } from '@angular/router';
import { RouterTestingHarness, RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { DCNDetailComponent } from './dcn-detail.component';

describe('DCN Management Detail Component', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [DCNDetailComponent, RouterTestingModule.withRoutes([], { bindToComponentInputs: true })],
      providers: [
        provideRouter(
          [
            {
              path: '**',
              component: DCNDetailComponent,
              resolve: { dCN: () => of({ id: 123 }) },
            },
          ],
          withComponentInputBinding(),
        ),
      ],
    })
      .overrideTemplate(DCNDetailComponent, '')
      .compileComponents();
  });

  describe('OnInit', () => {
    it('Should load dCN on init', async () => {
      const harness = await RouterTestingHarness.create();
      const instance = await harness.navigateByUrl('/', DCNDetailComponent);

      // THEN
      expect(instance.dCN).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
