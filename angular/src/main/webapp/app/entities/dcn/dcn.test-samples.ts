import { IDCN, NewDCN } from './dcn.model';

export const sampleWithRequiredData: IDCN = {
  id: 22132,
};

export const sampleWithPartialData: IDCN = {
  id: 10156,
};

export const sampleWithFullData: IDCN = {
  id: 24178,
  descricao: 'mmm midst',
};

export const sampleWithNewData: NewDCN = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
