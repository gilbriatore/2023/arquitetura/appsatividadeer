import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IDCN, NewDCN } from '../dcn.model';

export type PartialUpdateDCN = Partial<IDCN> & Pick<IDCN, 'id'>;

export type EntityResponseType = HttpResponse<IDCN>;
export type EntityArrayResponseType = HttpResponse<IDCN[]>;

@Injectable({ providedIn: 'root' })
export class DCNService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/dcns');

  constructor(
    protected http: HttpClient,
    protected applicationConfigService: ApplicationConfigService,
  ) {}

  create(dCN: NewDCN): Observable<EntityResponseType> {
    return this.http.post<IDCN>(this.resourceUrl, dCN, { observe: 'response' });
  }

  update(dCN: IDCN): Observable<EntityResponseType> {
    return this.http.put<IDCN>(`${this.resourceUrl}/${this.getDCNIdentifier(dCN)}`, dCN, { observe: 'response' });
  }

  partialUpdate(dCN: PartialUpdateDCN): Observable<EntityResponseType> {
    return this.http.patch<IDCN>(`${this.resourceUrl}/${this.getDCNIdentifier(dCN)}`, dCN, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IDCN>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IDCN[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getDCNIdentifier(dCN: Pick<IDCN, 'id'>): number {
    return dCN.id;
  }

  compareDCN(o1: Pick<IDCN, 'id'> | null, o2: Pick<IDCN, 'id'> | null): boolean {
    return o1 && o2 ? this.getDCNIdentifier(o1) === this.getDCNIdentifier(o2) : o1 === o2;
  }

  addDCNToCollectionIfMissing<Type extends Pick<IDCN, 'id'>>(dCNCollection: Type[], ...dCNSToCheck: (Type | null | undefined)[]): Type[] {
    const dCNS: Type[] = dCNSToCheck.filter(isPresent);
    if (dCNS.length > 0) {
      const dCNCollectionIdentifiers = dCNCollection.map(dCNItem => this.getDCNIdentifier(dCNItem)!);
      const dCNSToAdd = dCNS.filter(dCNItem => {
        const dCNIdentifier = this.getDCNIdentifier(dCNItem);
        if (dCNCollectionIdentifiers.includes(dCNIdentifier)) {
          return false;
        }
        dCNCollectionIdentifiers.push(dCNIdentifier);
        return true;
      });
      return [...dCNSToAdd, ...dCNCollection];
    }
    return dCNCollection;
  }
}
