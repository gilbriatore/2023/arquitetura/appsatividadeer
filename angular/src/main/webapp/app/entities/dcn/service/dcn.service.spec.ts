import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IDCN } from '../dcn.model';
import { sampleWithRequiredData, sampleWithNewData, sampleWithPartialData, sampleWithFullData } from '../dcn.test-samples';

import { DCNService } from './dcn.service';

const requireRestSample: IDCN = {
  ...sampleWithRequiredData,
};

describe('DCN Service', () => {
  let service: DCNService;
  let httpMock: HttpTestingController;
  let expectedResult: IDCN | IDCN[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(DCNService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should create a DCN', () => {
      const dCN = { ...sampleWithNewData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.create(dCN).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a DCN', () => {
      const dCN = { ...sampleWithRequiredData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.update(dCN).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a DCN', () => {
      const patchObject = { ...sampleWithPartialData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of DCN', () => {
      const returnedFromService = { ...requireRestSample };

      const expected = { ...sampleWithRequiredData };

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toMatchObject([expected]);
    });

    it('should delete a DCN', () => {
      const expected = true;

      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult).toBe(expected);
    });

    describe('addDCNToCollectionIfMissing', () => {
      it('should add a DCN to an empty array', () => {
        const dCN: IDCN = sampleWithRequiredData;
        expectedResult = service.addDCNToCollectionIfMissing([], dCN);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(dCN);
      });

      it('should not add a DCN to an array that contains it', () => {
        const dCN: IDCN = sampleWithRequiredData;
        const dCNCollection: IDCN[] = [
          {
            ...dCN,
          },
          sampleWithPartialData,
        ];
        expectedResult = service.addDCNToCollectionIfMissing(dCNCollection, dCN);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a DCN to an array that doesn't contain it", () => {
        const dCN: IDCN = sampleWithRequiredData;
        const dCNCollection: IDCN[] = [sampleWithPartialData];
        expectedResult = service.addDCNToCollectionIfMissing(dCNCollection, dCN);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(dCN);
      });

      it('should add only unique DCN to an array', () => {
        const dCNArray: IDCN[] = [sampleWithRequiredData, sampleWithPartialData, sampleWithFullData];
        const dCNCollection: IDCN[] = [sampleWithRequiredData];
        expectedResult = service.addDCNToCollectionIfMissing(dCNCollection, ...dCNArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const dCN: IDCN = sampleWithRequiredData;
        const dCN2: IDCN = sampleWithPartialData;
        expectedResult = service.addDCNToCollectionIfMissing([], dCN, dCN2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(dCN);
        expect(expectedResult).toContain(dCN2);
      });

      it('should accept null and undefined values', () => {
        const dCN: IDCN = sampleWithRequiredData;
        expectedResult = service.addDCNToCollectionIfMissing([], null, dCN, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(dCN);
      });

      it('should return initial array if no DCN is added', () => {
        const dCNCollection: IDCN[] = [sampleWithRequiredData];
        expectedResult = service.addDCNToCollectionIfMissing(dCNCollection, undefined, null);
        expect(expectedResult).toEqual(dCNCollection);
      });
    });

    describe('compareDCN', () => {
      it('Should return true if both entities are null', () => {
        const entity1 = null;
        const entity2 = null;

        const compareResult = service.compareDCN(entity1, entity2);

        expect(compareResult).toEqual(true);
      });

      it('Should return false if one entity is null', () => {
        const entity1 = { id: 123 };
        const entity2 = null;

        const compareResult1 = service.compareDCN(entity1, entity2);
        const compareResult2 = service.compareDCN(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey differs', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 456 };

        const compareResult1 = service.compareDCN(entity1, entity2);
        const compareResult2 = service.compareDCN(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey matches', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 123 };

        const compareResult1 = service.compareDCN(entity1, entity2);
        const compareResult2 = service.compareDCN(entity2, entity1);

        expect(compareResult1).toEqual(true);
        expect(compareResult2).toEqual(true);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
