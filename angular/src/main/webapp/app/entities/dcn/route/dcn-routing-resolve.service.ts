import { inject } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of, EMPTY, Observable } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IDCN } from '../dcn.model';
import { DCNService } from '../service/dcn.service';

export const dCNResolve = (route: ActivatedRouteSnapshot): Observable<null | IDCN> => {
  const id = route.params['id'];
  if (id) {
    return inject(DCNService)
      .find(id)
      .pipe(
        mergeMap((dCN: HttpResponse<IDCN>) => {
          if (dCN.body) {
            return of(dCN.body);
          } else {
            inject(Router).navigate(['404']);
            return EMPTY;
          }
        }),
      );
  }
  return of(null);
};

export default dCNResolve;
