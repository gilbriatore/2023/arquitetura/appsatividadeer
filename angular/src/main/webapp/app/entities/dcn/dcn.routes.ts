import { Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { ASC } from 'app/config/navigation.constants';
import { DCNComponent } from './list/dcn.component';
import { DCNDetailComponent } from './detail/dcn-detail.component';
import { DCNUpdateComponent } from './update/dcn-update.component';
import DCNResolve from './route/dcn-routing-resolve.service';

const dCNRoute: Routes = [
  {
    path: '',
    component: DCNComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: DCNDetailComponent,
    resolve: {
      dCN: DCNResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: DCNUpdateComponent,
    resolve: {
      dCN: DCNResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: DCNUpdateComponent,
    resolve: {
      dCN: DCNResolve,
    },
    canActivate: [UserRouteAccessService],
  },
];

export default dCNRoute;
