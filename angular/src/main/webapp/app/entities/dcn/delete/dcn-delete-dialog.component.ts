import { Component } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import SharedModule from 'app/shared/shared.module';
import { ITEM_DELETED_EVENT } from 'app/config/navigation.constants';
import { IDCN } from '../dcn.model';
import { DCNService } from '../service/dcn.service';

@Component({
  standalone: true,
  templateUrl: './dcn-delete-dialog.component.html',
  imports: [SharedModule, FormsModule],
})
export class DCNDeleteDialogComponent {
  dCN?: IDCN;

  constructor(
    protected dCNService: DCNService,
    protected activeModal: NgbActiveModal,
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.dCNService.delete(id).subscribe(() => {
      this.activeModal.close(ITEM_DELETED_EVENT);
    });
  }
}
