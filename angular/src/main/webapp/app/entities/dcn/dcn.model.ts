export interface IDCN {
  id: number;
  descricao?: string | null;
}

export type NewDCN = Omit<IDCN, 'id'> & { id: null };
