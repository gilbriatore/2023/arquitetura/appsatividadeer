import { IPerfil } from 'app/entities/perfil/perfil.model';
import { IModalidade } from 'app/entities/modalidade/modalidade.model';
import { IEscola } from 'app/entities/escola/escola.model';
import { ITipoDeCurso } from 'app/entities/tipo-de-curso/tipo-de-curso.model';

export interface ICurso {
  id: number;
  nome?: string | null;
  cargaHorasMinCurso?: number | null;
  cargaHorasMinEstagio?: number | null;
  percMaxEstagioAC?: number | null;
  percMaxAtividadeDistancia?: number | null;
  perfil?: Pick<IPerfil, 'id'> | null;
  modalidade?: Pick<IModalidade, 'id'> | null;
  escola?: Pick<IEscola, 'id'> | null;
  tipoDeCurso?: Pick<ITipoDeCurso, 'id'> | null;
}

export type NewCurso = Omit<ICurso, 'id'> & { id: null };
