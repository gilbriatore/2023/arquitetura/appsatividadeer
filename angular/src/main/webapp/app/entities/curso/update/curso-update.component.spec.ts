import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { IPerfil } from 'app/entities/perfil/perfil.model';
import { PerfilService } from 'app/entities/perfil/service/perfil.service';
import { IModalidade } from 'app/entities/modalidade/modalidade.model';
import { ModalidadeService } from 'app/entities/modalidade/service/modalidade.service';
import { IEscola } from 'app/entities/escola/escola.model';
import { EscolaService } from 'app/entities/escola/service/escola.service';
import { ITipoDeCurso } from 'app/entities/tipo-de-curso/tipo-de-curso.model';
import { TipoDeCursoService } from 'app/entities/tipo-de-curso/service/tipo-de-curso.service';
import { ICurso } from '../curso.model';
import { CursoService } from '../service/curso.service';
import { CursoFormService } from './curso-form.service';

import { CursoUpdateComponent } from './curso-update.component';

describe('Curso Management Update Component', () => {
  let comp: CursoUpdateComponent;
  let fixture: ComponentFixture<CursoUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let cursoFormService: CursoFormService;
  let cursoService: CursoService;
  let perfilService: PerfilService;
  let modalidadeService: ModalidadeService;
  let escolaService: EscolaService;
  let tipoDeCursoService: TipoDeCursoService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([]), CursoUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(CursoUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(CursoUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    cursoFormService = TestBed.inject(CursoFormService);
    cursoService = TestBed.inject(CursoService);
    perfilService = TestBed.inject(PerfilService);
    modalidadeService = TestBed.inject(ModalidadeService);
    escolaService = TestBed.inject(EscolaService);
    tipoDeCursoService = TestBed.inject(TipoDeCursoService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Perfil query and add missing value', () => {
      const curso: ICurso = { id: 456 };
      const perfil: IPerfil = { id: 4135 };
      curso.perfil = perfil;

      const perfilCollection: IPerfil[] = [{ id: 18404 }];
      jest.spyOn(perfilService, 'query').mockReturnValue(of(new HttpResponse({ body: perfilCollection })));
      const additionalPerfils = [perfil];
      const expectedCollection: IPerfil[] = [...additionalPerfils, ...perfilCollection];
      jest.spyOn(perfilService, 'addPerfilToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ curso });
      comp.ngOnInit();

      expect(perfilService.query).toHaveBeenCalled();
      expect(perfilService.addPerfilToCollectionIfMissing).toHaveBeenCalledWith(
        perfilCollection,
        ...additionalPerfils.map(expect.objectContaining),
      );
      expect(comp.perfilsSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Modalidade query and add missing value', () => {
      const curso: ICurso = { id: 456 };
      const modalidade: IModalidade = { id: 642 };
      curso.modalidade = modalidade;

      const modalidadeCollection: IModalidade[] = [{ id: 7314 }];
      jest.spyOn(modalidadeService, 'query').mockReturnValue(of(new HttpResponse({ body: modalidadeCollection })));
      const additionalModalidades = [modalidade];
      const expectedCollection: IModalidade[] = [...additionalModalidades, ...modalidadeCollection];
      jest.spyOn(modalidadeService, 'addModalidadeToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ curso });
      comp.ngOnInit();

      expect(modalidadeService.query).toHaveBeenCalled();
      expect(modalidadeService.addModalidadeToCollectionIfMissing).toHaveBeenCalledWith(
        modalidadeCollection,
        ...additionalModalidades.map(expect.objectContaining),
      );
      expect(comp.modalidadesSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Escola query and add missing value', () => {
      const curso: ICurso = { id: 456 };
      const escola: IEscola = { id: 22599 };
      curso.escola = escola;

      const escolaCollection: IEscola[] = [{ id: 4446 }];
      jest.spyOn(escolaService, 'query').mockReturnValue(of(new HttpResponse({ body: escolaCollection })));
      const additionalEscolas = [escola];
      const expectedCollection: IEscola[] = [...additionalEscolas, ...escolaCollection];
      jest.spyOn(escolaService, 'addEscolaToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ curso });
      comp.ngOnInit();

      expect(escolaService.query).toHaveBeenCalled();
      expect(escolaService.addEscolaToCollectionIfMissing).toHaveBeenCalledWith(
        escolaCollection,
        ...additionalEscolas.map(expect.objectContaining),
      );
      expect(comp.escolasSharedCollection).toEqual(expectedCollection);
    });

    it('Should call TipoDeCurso query and add missing value', () => {
      const curso: ICurso = { id: 456 };
      const tipoDeCurso: ITipoDeCurso = { id: 9405 };
      curso.tipoDeCurso = tipoDeCurso;

      const tipoDeCursoCollection: ITipoDeCurso[] = [{ id: 10450 }];
      jest.spyOn(tipoDeCursoService, 'query').mockReturnValue(of(new HttpResponse({ body: tipoDeCursoCollection })));
      const additionalTipoDeCursos = [tipoDeCurso];
      const expectedCollection: ITipoDeCurso[] = [...additionalTipoDeCursos, ...tipoDeCursoCollection];
      jest.spyOn(tipoDeCursoService, 'addTipoDeCursoToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ curso });
      comp.ngOnInit();

      expect(tipoDeCursoService.query).toHaveBeenCalled();
      expect(tipoDeCursoService.addTipoDeCursoToCollectionIfMissing).toHaveBeenCalledWith(
        tipoDeCursoCollection,
        ...additionalTipoDeCursos.map(expect.objectContaining),
      );
      expect(comp.tipoDeCursosSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const curso: ICurso = { id: 456 };
      const perfil: IPerfil = { id: 21040 };
      curso.perfil = perfil;
      const modalidade: IModalidade = { id: 12102 };
      curso.modalidade = modalidade;
      const escola: IEscola = { id: 2082 };
      curso.escola = escola;
      const tipoDeCurso: ITipoDeCurso = { id: 30832 };
      curso.tipoDeCurso = tipoDeCurso;

      activatedRoute.data = of({ curso });
      comp.ngOnInit();

      expect(comp.perfilsSharedCollection).toContain(perfil);
      expect(comp.modalidadesSharedCollection).toContain(modalidade);
      expect(comp.escolasSharedCollection).toContain(escola);
      expect(comp.tipoDeCursosSharedCollection).toContain(tipoDeCurso);
      expect(comp.curso).toEqual(curso);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ICurso>>();
      const curso = { id: 123 };
      jest.spyOn(cursoFormService, 'getCurso').mockReturnValue(curso);
      jest.spyOn(cursoService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ curso });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: curso }));
      saveSubject.complete();

      // THEN
      expect(cursoFormService.getCurso).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(cursoService.update).toHaveBeenCalledWith(expect.objectContaining(curso));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ICurso>>();
      const curso = { id: 123 };
      jest.spyOn(cursoFormService, 'getCurso').mockReturnValue({ id: null });
      jest.spyOn(cursoService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ curso: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: curso }));
      saveSubject.complete();

      // THEN
      expect(cursoFormService.getCurso).toHaveBeenCalled();
      expect(cursoService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ICurso>>();
      const curso = { id: 123 };
      jest.spyOn(cursoService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ curso });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(cursoService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Compare relationships', () => {
    describe('comparePerfil', () => {
      it('Should forward to perfilService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(perfilService, 'comparePerfil');
        comp.comparePerfil(entity, entity2);
        expect(perfilService.comparePerfil).toHaveBeenCalledWith(entity, entity2);
      });
    });

    describe('compareModalidade', () => {
      it('Should forward to modalidadeService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(modalidadeService, 'compareModalidade');
        comp.compareModalidade(entity, entity2);
        expect(modalidadeService.compareModalidade).toHaveBeenCalledWith(entity, entity2);
      });
    });

    describe('compareEscola', () => {
      it('Should forward to escolaService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(escolaService, 'compareEscola');
        comp.compareEscola(entity, entity2);
        expect(escolaService.compareEscola).toHaveBeenCalledWith(entity, entity2);
      });
    });

    describe('compareTipoDeCurso', () => {
      it('Should forward to tipoDeCursoService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(tipoDeCursoService, 'compareTipoDeCurso');
        comp.compareTipoDeCurso(entity, entity2);
        expect(tipoDeCursoService.compareTipoDeCurso).toHaveBeenCalledWith(entity, entity2);
      });
    });
  });
});
