import { TestBed } from '@angular/core/testing';
import { provideRouter, withComponentInputBinding } from '@angular/router';
import { RouterTestingHarness, RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { PeriodoDetailComponent } from './periodo-detail.component';

describe('Periodo Management Detail Component', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [PeriodoDetailComponent, RouterTestingModule.withRoutes([], { bindToComponentInputs: true })],
      providers: [
        provideRouter(
          [
            {
              path: '**',
              component: PeriodoDetailComponent,
              resolve: { periodo: () => of({ id: 123 }) },
            },
          ],
          withComponentInputBinding(),
        ),
      ],
    })
      .overrideTemplate(PeriodoDetailComponent, '')
      .compileComponents();
  });

  describe('OnInit', () => {
    it('Should load periodo on init', async () => {
      const harness = await RouterTestingHarness.create();
      const instance = await harness.navigateByUrl('/', PeriodoDetailComponent);

      // THEN
      expect(instance.periodo).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
