import { IMatriz } from 'app/entities/matriz/matriz.model';

export interface IPeriodo {
  id: number;
  descricao?: string | null;
  matriz?: Pick<IMatriz, 'id'> | null;
}

export type NewPeriodo = Omit<IPeriodo, 'id'> & { id: null };
