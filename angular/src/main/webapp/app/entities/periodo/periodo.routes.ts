import { Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { ASC } from 'app/config/navigation.constants';
import { PeriodoComponent } from './list/periodo.component';
import { PeriodoDetailComponent } from './detail/periodo-detail.component';
import { PeriodoUpdateComponent } from './update/periodo-update.component';
import PeriodoResolve from './route/periodo-routing-resolve.service';

const periodoRoute: Routes = [
  {
    path: '',
    component: PeriodoComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: PeriodoDetailComponent,
    resolve: {
      periodo: PeriodoResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: PeriodoUpdateComponent,
    resolve: {
      periodo: PeriodoResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: PeriodoUpdateComponent,
    resolve: {
      periodo: PeriodoResolve,
    },
    canActivate: [UserRouteAccessService],
  },
];

export default periodoRoute;
