import { inject } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of, EMPTY, Observable } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IPeriodo } from '../periodo.model';
import { PeriodoService } from '../service/periodo.service';

export const periodoResolve = (route: ActivatedRouteSnapshot): Observable<null | IPeriodo> => {
  const id = route.params['id'];
  if (id) {
    return inject(PeriodoService)
      .find(id)
      .pipe(
        mergeMap((periodo: HttpResponse<IPeriodo>) => {
          if (periodo.body) {
            return of(periodo.body);
          } else {
            inject(Router).navigate(['404']);
            return EMPTY;
          }
        }),
      );
  }
  return of(null);
};

export default periodoResolve;
