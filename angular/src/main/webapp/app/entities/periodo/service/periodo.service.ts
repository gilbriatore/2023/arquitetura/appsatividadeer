import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IPeriodo, NewPeriodo } from '../periodo.model';

export type PartialUpdatePeriodo = Partial<IPeriodo> & Pick<IPeriodo, 'id'>;

export type EntityResponseType = HttpResponse<IPeriodo>;
export type EntityArrayResponseType = HttpResponse<IPeriodo[]>;

@Injectable({ providedIn: 'root' })
export class PeriodoService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/periodos');

  constructor(
    protected http: HttpClient,
    protected applicationConfigService: ApplicationConfigService,
  ) {}

  create(periodo: NewPeriodo): Observable<EntityResponseType> {
    return this.http.post<IPeriodo>(this.resourceUrl, periodo, { observe: 'response' });
  }

  update(periodo: IPeriodo): Observable<EntityResponseType> {
    return this.http.put<IPeriodo>(`${this.resourceUrl}/${this.getPeriodoIdentifier(periodo)}`, periodo, { observe: 'response' });
  }

  partialUpdate(periodo: PartialUpdatePeriodo): Observable<EntityResponseType> {
    return this.http.patch<IPeriodo>(`${this.resourceUrl}/${this.getPeriodoIdentifier(periodo)}`, periodo, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IPeriodo>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IPeriodo[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getPeriodoIdentifier(periodo: Pick<IPeriodo, 'id'>): number {
    return periodo.id;
  }

  comparePeriodo(o1: Pick<IPeriodo, 'id'> | null, o2: Pick<IPeriodo, 'id'> | null): boolean {
    return o1 && o2 ? this.getPeriodoIdentifier(o1) === this.getPeriodoIdentifier(o2) : o1 === o2;
  }

  addPeriodoToCollectionIfMissing<Type extends Pick<IPeriodo, 'id'>>(
    periodoCollection: Type[],
    ...periodosToCheck: (Type | null | undefined)[]
  ): Type[] {
    const periodos: Type[] = periodosToCheck.filter(isPresent);
    if (periodos.length > 0) {
      const periodoCollectionIdentifiers = periodoCollection.map(periodoItem => this.getPeriodoIdentifier(periodoItem)!);
      const periodosToAdd = periodos.filter(periodoItem => {
        const periodoIdentifier = this.getPeriodoIdentifier(periodoItem);
        if (periodoCollectionIdentifiers.includes(periodoIdentifier)) {
          return false;
        }
        periodoCollectionIdentifiers.push(periodoIdentifier);
        return true;
      });
      return [...periodosToAdd, ...periodoCollection];
    }
    return periodoCollection;
  }
}
