import { Component } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import SharedModule from 'app/shared/shared.module';
import { ITEM_DELETED_EVENT } from 'app/config/navigation.constants';
import { IPeriodo } from '../periodo.model';
import { PeriodoService } from '../service/periodo.service';

@Component({
  standalone: true,
  templateUrl: './periodo-delete-dialog.component.html',
  imports: [SharedModule, FormsModule],
})
export class PeriodoDeleteDialogComponent {
  periodo?: IPeriodo;

  constructor(
    protected periodoService: PeriodoService,
    protected activeModal: NgbActiveModal,
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.periodoService.delete(id).subscribe(() => {
      this.activeModal.close(ITEM_DELETED_EVENT);
    });
  }
}
