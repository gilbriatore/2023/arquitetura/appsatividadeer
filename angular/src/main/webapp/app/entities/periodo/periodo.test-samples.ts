import { IPeriodo, NewPeriodo } from './periodo.model';

export const sampleWithRequiredData: IPeriodo = {
  id: 3260,
};

export const sampleWithPartialData: IPeriodo = {
  id: 12423,
  descricao: 'ah sans',
};

export const sampleWithFullData: IPeriodo = {
  id: 14201,
  descricao: 'noisily',
};

export const sampleWithNewData: NewPeriodo = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
