import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { PeriodoService } from '../service/periodo.service';

import { PeriodoComponent } from './periodo.component';

describe('Periodo Management Component', () => {
  let comp: PeriodoComponent;
  let fixture: ComponentFixture<PeriodoComponent>;
  let service: PeriodoService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes([{ path: 'periodo', component: PeriodoComponent }]),
        HttpClientTestingModule,
        PeriodoComponent,
      ],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            data: of({
              defaultSort: 'id,asc',
            }),
            queryParamMap: of(
              jest.requireActual('@angular/router').convertToParamMap({
                page: '1',
                size: '1',
                sort: 'id,desc',
              }),
            ),
            snapshot: { queryParams: {} },
          },
        },
      ],
    })
      .overrideTemplate(PeriodoComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(PeriodoComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(PeriodoService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        }),
      ),
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.periodos?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });

  describe('trackId', () => {
    it('Should forward to periodoService', () => {
      const entity = { id: 123 };
      jest.spyOn(service, 'getPeriodoIdentifier');
      const id = comp.trackId(0, entity);
      expect(service.getPeriodoIdentifier).toHaveBeenCalledWith(entity);
      expect(id).toBe(entity.id);
    });
  });
});
