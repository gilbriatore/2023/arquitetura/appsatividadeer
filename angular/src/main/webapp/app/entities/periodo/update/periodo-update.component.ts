import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import SharedModule from 'app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IMatriz } from 'app/entities/matriz/matriz.model';
import { MatrizService } from 'app/entities/matriz/service/matriz.service';
import { IPeriodo } from '../periodo.model';
import { PeriodoService } from '../service/periodo.service';
import { PeriodoFormService, PeriodoFormGroup } from './periodo-form.service';

@Component({
  standalone: true,
  selector: 'jhi-periodo-update',
  templateUrl: './periodo-update.component.html',
  imports: [SharedModule, FormsModule, ReactiveFormsModule],
})
export class PeriodoUpdateComponent implements OnInit {
  isSaving = false;
  periodo: IPeriodo | null = null;

  matrizsSharedCollection: IMatriz[] = [];

  editForm: PeriodoFormGroup = this.periodoFormService.createPeriodoFormGroup();

  constructor(
    protected periodoService: PeriodoService,
    protected periodoFormService: PeriodoFormService,
    protected matrizService: MatrizService,
    protected activatedRoute: ActivatedRoute,
  ) {}

  compareMatriz = (o1: IMatriz | null, o2: IMatriz | null): boolean => this.matrizService.compareMatriz(o1, o2);

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ periodo }) => {
      this.periodo = periodo;
      if (periodo) {
        this.updateForm(periodo);
      }

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const periodo = this.periodoFormService.getPeriodo(this.editForm);
    if (periodo.id !== null) {
      this.subscribeToSaveResponse(this.periodoService.update(periodo));
    } else {
      this.subscribeToSaveResponse(this.periodoService.create(periodo));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPeriodo>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(periodo: IPeriodo): void {
    this.periodo = periodo;
    this.periodoFormService.resetForm(this.editForm, periodo);

    this.matrizsSharedCollection = this.matrizService.addMatrizToCollectionIfMissing<IMatriz>(this.matrizsSharedCollection, periodo.matriz);
  }

  protected loadRelationshipsOptions(): void {
    this.matrizService
      .query()
      .pipe(map((res: HttpResponse<IMatriz[]>) => res.body ?? []))
      .pipe(map((matrizs: IMatriz[]) => this.matrizService.addMatrizToCollectionIfMissing<IMatriz>(matrizs, this.periodo?.matriz)))
      .subscribe((matrizs: IMatriz[]) => (this.matrizsSharedCollection = matrizs));
  }
}
