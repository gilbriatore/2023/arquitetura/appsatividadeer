import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { IPeriodo, NewPeriodo } from '../periodo.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts IPeriodo for edit and NewPeriodoFormGroupInput for create.
 */
type PeriodoFormGroupInput = IPeriodo | PartialWithRequiredKeyOf<NewPeriodo>;

type PeriodoFormDefaults = Pick<NewPeriodo, 'id'>;

type PeriodoFormGroupContent = {
  id: FormControl<IPeriodo['id'] | NewPeriodo['id']>;
  descricao: FormControl<IPeriodo['descricao']>;
  matriz: FormControl<IPeriodo['matriz']>;
};

export type PeriodoFormGroup = FormGroup<PeriodoFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class PeriodoFormService {
  createPeriodoFormGroup(periodo: PeriodoFormGroupInput = { id: null }): PeriodoFormGroup {
    const periodoRawValue = {
      ...this.getFormDefaults(),
      ...periodo,
    };
    return new FormGroup<PeriodoFormGroupContent>({
      id: new FormControl(
        { value: periodoRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        },
      ),
      descricao: new FormControl(periodoRawValue.descricao),
      matriz: new FormControl(periodoRawValue.matriz),
    });
  }

  getPeriodo(form: PeriodoFormGroup): IPeriodo | NewPeriodo {
    return form.getRawValue() as IPeriodo | NewPeriodo;
  }

  resetForm(form: PeriodoFormGroup, periodo: PeriodoFormGroupInput): void {
    const periodoRawValue = { ...this.getFormDefaults(), ...periodo };
    form.reset(
      {
        ...periodoRawValue,
        id: { value: periodoRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */,
    );
  }

  private getFormDefaults(): PeriodoFormDefaults {
    return {
      id: null,
    };
  }
}
