import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { IMatriz } from 'app/entities/matriz/matriz.model';
import { MatrizService } from 'app/entities/matriz/service/matriz.service';
import { PeriodoService } from '../service/periodo.service';
import { IPeriodo } from '../periodo.model';
import { PeriodoFormService } from './periodo-form.service';

import { PeriodoUpdateComponent } from './periodo-update.component';

describe('Periodo Management Update Component', () => {
  let comp: PeriodoUpdateComponent;
  let fixture: ComponentFixture<PeriodoUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let periodoFormService: PeriodoFormService;
  let periodoService: PeriodoService;
  let matrizService: MatrizService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([]), PeriodoUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(PeriodoUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(PeriodoUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    periodoFormService = TestBed.inject(PeriodoFormService);
    periodoService = TestBed.inject(PeriodoService);
    matrizService = TestBed.inject(MatrizService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Matriz query and add missing value', () => {
      const periodo: IPeriodo = { id: 456 };
      const matriz: IMatriz = { id: 24314 };
      periodo.matriz = matriz;

      const matrizCollection: IMatriz[] = [{ id: 20014 }];
      jest.spyOn(matrizService, 'query').mockReturnValue(of(new HttpResponse({ body: matrizCollection })));
      const additionalMatrizs = [matriz];
      const expectedCollection: IMatriz[] = [...additionalMatrizs, ...matrizCollection];
      jest.spyOn(matrizService, 'addMatrizToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ periodo });
      comp.ngOnInit();

      expect(matrizService.query).toHaveBeenCalled();
      expect(matrizService.addMatrizToCollectionIfMissing).toHaveBeenCalledWith(
        matrizCollection,
        ...additionalMatrizs.map(expect.objectContaining),
      );
      expect(comp.matrizsSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const periodo: IPeriodo = { id: 456 };
      const matriz: IMatriz = { id: 11607 };
      periodo.matriz = matriz;

      activatedRoute.data = of({ periodo });
      comp.ngOnInit();

      expect(comp.matrizsSharedCollection).toContain(matriz);
      expect(comp.periodo).toEqual(periodo);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IPeriodo>>();
      const periodo = { id: 123 };
      jest.spyOn(periodoFormService, 'getPeriodo').mockReturnValue(periodo);
      jest.spyOn(periodoService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ periodo });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: periodo }));
      saveSubject.complete();

      // THEN
      expect(periodoFormService.getPeriodo).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(periodoService.update).toHaveBeenCalledWith(expect.objectContaining(periodo));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IPeriodo>>();
      const periodo = { id: 123 };
      jest.spyOn(periodoFormService, 'getPeriodo').mockReturnValue({ id: null });
      jest.spyOn(periodoService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ periodo: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: periodo }));
      saveSubject.complete();

      // THEN
      expect(periodoFormService.getPeriodo).toHaveBeenCalled();
      expect(periodoService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IPeriodo>>();
      const periodo = { id: 123 };
      jest.spyOn(periodoService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ periodo });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(periodoService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Compare relationships', () => {
    describe('compareMatriz', () => {
      it('Should forward to matrizService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(matrizService, 'compareMatriz');
        comp.compareMatriz(entity, entity2);
        expect(matrizService.compareMatriz).toHaveBeenCalledWith(entity, entity2);
      });
    });
  });
});
