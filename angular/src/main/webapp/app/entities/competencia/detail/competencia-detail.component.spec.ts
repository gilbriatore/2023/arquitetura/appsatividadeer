import { TestBed } from '@angular/core/testing';
import { provideRouter, withComponentInputBinding } from '@angular/router';
import { RouterTestingHarness, RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { CompetenciaDetailComponent } from './competencia-detail.component';

describe('Competencia Management Detail Component', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [CompetenciaDetailComponent, RouterTestingModule.withRoutes([], { bindToComponentInputs: true })],
      providers: [
        provideRouter(
          [
            {
              path: '**',
              component: CompetenciaDetailComponent,
              resolve: { competencia: () => of({ id: 123 }) },
            },
          ],
          withComponentInputBinding(),
        ),
      ],
    })
      .overrideTemplate(CompetenciaDetailComponent, '')
      .compileComponents();
  });

  describe('OnInit', () => {
    it('Should load competencia on init', async () => {
      const harness = await RouterTestingHarness.create();
      const instance = await harness.navigateByUrl('/', CompetenciaDetailComponent);

      // THEN
      expect(instance.competencia).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
