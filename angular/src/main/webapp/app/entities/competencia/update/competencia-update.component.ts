import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import SharedModule from 'app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IDiretriz } from 'app/entities/diretriz/diretriz.model';
import { DiretrizService } from 'app/entities/diretriz/service/diretriz.service';
import { IPapel } from 'app/entities/papel/papel.model';
import { PapelService } from 'app/entities/papel/service/papel.service';
import { CompetenciaService } from '../service/competencia.service';
import { ICompetencia } from '../competencia.model';
import { CompetenciaFormService, CompetenciaFormGroup } from './competencia-form.service';

@Component({
  standalone: true,
  selector: 'jhi-competencia-update',
  templateUrl: './competencia-update.component.html',
  imports: [SharedModule, FormsModule, ReactiveFormsModule],
})
export class CompetenciaUpdateComponent implements OnInit {
  isSaving = false;
  competencia: ICompetencia | null = null;

  diretrizsSharedCollection: IDiretriz[] = [];
  papelsSharedCollection: IPapel[] = [];

  editForm: CompetenciaFormGroup = this.competenciaFormService.createCompetenciaFormGroup();

  constructor(
    protected competenciaService: CompetenciaService,
    protected competenciaFormService: CompetenciaFormService,
    protected diretrizService: DiretrizService,
    protected papelService: PapelService,
    protected activatedRoute: ActivatedRoute,
  ) {}

  compareDiretriz = (o1: IDiretriz | null, o2: IDiretriz | null): boolean => this.diretrizService.compareDiretriz(o1, o2);

  comparePapel = (o1: IPapel | null, o2: IPapel | null): boolean => this.papelService.comparePapel(o1, o2);

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ competencia }) => {
      this.competencia = competencia;
      if (competencia) {
        this.updateForm(competencia);
      }

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const competencia = this.competenciaFormService.getCompetencia(this.editForm);
    if (competencia.id !== null) {
      this.subscribeToSaveResponse(this.competenciaService.update(competencia));
    } else {
      this.subscribeToSaveResponse(this.competenciaService.create(competencia));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICompetencia>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(competencia: ICompetencia): void {
    this.competencia = competencia;
    this.competenciaFormService.resetForm(this.editForm, competencia);

    this.diretrizsSharedCollection = this.diretrizService.addDiretrizToCollectionIfMissing<IDiretriz>(
      this.diretrizsSharedCollection,
      competencia.diretriz,
    );
    this.papelsSharedCollection = this.papelService.addPapelToCollectionIfMissing<IPapel>(this.papelsSharedCollection, competencia.papel);
  }

  protected loadRelationshipsOptions(): void {
    this.diretrizService
      .query()
      .pipe(map((res: HttpResponse<IDiretriz[]>) => res.body ?? []))
      .pipe(
        map((diretrizs: IDiretriz[]) =>
          this.diretrizService.addDiretrizToCollectionIfMissing<IDiretriz>(diretrizs, this.competencia?.diretriz),
        ),
      )
      .subscribe((diretrizs: IDiretriz[]) => (this.diretrizsSharedCollection = diretrizs));

    this.papelService
      .query()
      .pipe(map((res: HttpResponse<IPapel[]>) => res.body ?? []))
      .pipe(map((papels: IPapel[]) => this.papelService.addPapelToCollectionIfMissing<IPapel>(papels, this.competencia?.papel)))
      .subscribe((papels: IPapel[]) => (this.papelsSharedCollection = papels));
  }
}
