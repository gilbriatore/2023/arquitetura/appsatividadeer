import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { IDiretriz } from 'app/entities/diretriz/diretriz.model';
import { DiretrizService } from 'app/entities/diretriz/service/diretriz.service';
import { IPapel } from 'app/entities/papel/papel.model';
import { PapelService } from 'app/entities/papel/service/papel.service';
import { ICompetencia } from '../competencia.model';
import { CompetenciaService } from '../service/competencia.service';
import { CompetenciaFormService } from './competencia-form.service';

import { CompetenciaUpdateComponent } from './competencia-update.component';

describe('Competencia Management Update Component', () => {
  let comp: CompetenciaUpdateComponent;
  let fixture: ComponentFixture<CompetenciaUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let competenciaFormService: CompetenciaFormService;
  let competenciaService: CompetenciaService;
  let diretrizService: DiretrizService;
  let papelService: PapelService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([]), CompetenciaUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(CompetenciaUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(CompetenciaUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    competenciaFormService = TestBed.inject(CompetenciaFormService);
    competenciaService = TestBed.inject(CompetenciaService);
    diretrizService = TestBed.inject(DiretrizService);
    papelService = TestBed.inject(PapelService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Diretriz query and add missing value', () => {
      const competencia: ICompetencia = { id: 456 };
      const diretriz: IDiretriz = { id: 9051 };
      competencia.diretriz = diretriz;

      const diretrizCollection: IDiretriz[] = [{ id: 27340 }];
      jest.spyOn(diretrizService, 'query').mockReturnValue(of(new HttpResponse({ body: diretrizCollection })));
      const additionalDiretrizs = [diretriz];
      const expectedCollection: IDiretriz[] = [...additionalDiretrizs, ...diretrizCollection];
      jest.spyOn(diretrizService, 'addDiretrizToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ competencia });
      comp.ngOnInit();

      expect(diretrizService.query).toHaveBeenCalled();
      expect(diretrizService.addDiretrizToCollectionIfMissing).toHaveBeenCalledWith(
        diretrizCollection,
        ...additionalDiretrizs.map(expect.objectContaining),
      );
      expect(comp.diretrizsSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Papel query and add missing value', () => {
      const competencia: ICompetencia = { id: 456 };
      const papel: IPapel = { id: 8586 };
      competencia.papel = papel;

      const papelCollection: IPapel[] = [{ id: 17085 }];
      jest.spyOn(papelService, 'query').mockReturnValue(of(new HttpResponse({ body: papelCollection })));
      const additionalPapels = [papel];
      const expectedCollection: IPapel[] = [...additionalPapels, ...papelCollection];
      jest.spyOn(papelService, 'addPapelToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ competencia });
      comp.ngOnInit();

      expect(papelService.query).toHaveBeenCalled();
      expect(papelService.addPapelToCollectionIfMissing).toHaveBeenCalledWith(
        papelCollection,
        ...additionalPapels.map(expect.objectContaining),
      );
      expect(comp.papelsSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const competencia: ICompetencia = { id: 456 };
      const diretriz: IDiretriz = { id: 31762 };
      competencia.diretriz = diretriz;
      const papel: IPapel = { id: 5999 };
      competencia.papel = papel;

      activatedRoute.data = of({ competencia });
      comp.ngOnInit();

      expect(comp.diretrizsSharedCollection).toContain(diretriz);
      expect(comp.papelsSharedCollection).toContain(papel);
      expect(comp.competencia).toEqual(competencia);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ICompetencia>>();
      const competencia = { id: 123 };
      jest.spyOn(competenciaFormService, 'getCompetencia').mockReturnValue(competencia);
      jest.spyOn(competenciaService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ competencia });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: competencia }));
      saveSubject.complete();

      // THEN
      expect(competenciaFormService.getCompetencia).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(competenciaService.update).toHaveBeenCalledWith(expect.objectContaining(competencia));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ICompetencia>>();
      const competencia = { id: 123 };
      jest.spyOn(competenciaFormService, 'getCompetencia').mockReturnValue({ id: null });
      jest.spyOn(competenciaService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ competencia: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: competencia }));
      saveSubject.complete();

      // THEN
      expect(competenciaFormService.getCompetencia).toHaveBeenCalled();
      expect(competenciaService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ICompetencia>>();
      const competencia = { id: 123 };
      jest.spyOn(competenciaService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ competencia });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(competenciaService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Compare relationships', () => {
    describe('compareDiretriz', () => {
      it('Should forward to diretrizService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(diretrizService, 'compareDiretriz');
        comp.compareDiretriz(entity, entity2);
        expect(diretrizService.compareDiretriz).toHaveBeenCalledWith(entity, entity2);
      });
    });

    describe('comparePapel', () => {
      it('Should forward to papelService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(papelService, 'comparePapel');
        comp.comparePapel(entity, entity2);
        expect(papelService.comparePapel).toHaveBeenCalledWith(entity, entity2);
      });
    });
  });
});
