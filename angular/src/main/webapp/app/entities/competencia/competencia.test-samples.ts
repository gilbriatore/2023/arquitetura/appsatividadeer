import { ICompetencia, NewCompetencia } from './competencia.model';

export const sampleWithRequiredData: ICompetencia = {
  id: 4646,
};

export const sampleWithPartialData: ICompetencia = {
  id: 8192,
};

export const sampleWithFullData: ICompetencia = {
  id: 16184,
  descricao: 'often',
};

export const sampleWithNewData: NewCompetencia = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
