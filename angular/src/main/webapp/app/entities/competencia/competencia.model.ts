import { IDiretriz } from 'app/entities/diretriz/diretriz.model';
import { IPapel } from 'app/entities/papel/papel.model';

export interface ICompetencia {
  id: number;
  descricao?: string | null;
  diretriz?: Pick<IDiretriz, 'id'> | null;
  papel?: Pick<IPapel, 'id'> | null;
}

export type NewCompetencia = Omit<ICompetencia, 'id'> & { id: null };
