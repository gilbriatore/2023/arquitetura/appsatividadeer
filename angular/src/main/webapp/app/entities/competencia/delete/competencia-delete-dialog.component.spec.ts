jest.mock('@ng-bootstrap/ng-bootstrap');

import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { CompetenciaService } from '../service/competencia.service';

import { CompetenciaDeleteDialogComponent } from './competencia-delete-dialog.component';

describe('Competencia Management Delete Component', () => {
  let comp: CompetenciaDeleteDialogComponent;
  let fixture: ComponentFixture<CompetenciaDeleteDialogComponent>;
  let service: CompetenciaService;
  let mockActiveModal: NgbActiveModal;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, CompetenciaDeleteDialogComponent],
      providers: [NgbActiveModal],
    })
      .overrideTemplate(CompetenciaDeleteDialogComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(CompetenciaDeleteDialogComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(CompetenciaService);
    mockActiveModal = TestBed.inject(NgbActiveModal);
  });

  describe('confirmDelete', () => {
    it('Should call delete service on confirmDelete', inject(
      [],
      fakeAsync(() => {
        // GIVEN
        jest.spyOn(service, 'delete').mockReturnValue(of(new HttpResponse({ body: {} })));

        // WHEN
        comp.confirmDelete(123);
        tick();

        // THEN
        expect(service.delete).toHaveBeenCalledWith(123);
        expect(mockActiveModal.close).toHaveBeenCalledWith('deleted');
      }),
    ));

    it('Should not call delete service on clear', () => {
      // GIVEN
      jest.spyOn(service, 'delete');

      // WHEN
      comp.cancel();

      // THEN
      expect(service.delete).not.toHaveBeenCalled();
      expect(mockActiveModal.close).not.toHaveBeenCalled();
      expect(mockActiveModal.dismiss).toHaveBeenCalled();
    });
  });
});
