import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { CompetenciaService } from '../service/competencia.service';

import { CompetenciaComponent } from './competencia.component';

describe('Competencia Management Component', () => {
  let comp: CompetenciaComponent;
  let fixture: ComponentFixture<CompetenciaComponent>;
  let service: CompetenciaService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes([{ path: 'competencia', component: CompetenciaComponent }]),
        HttpClientTestingModule,
        CompetenciaComponent,
      ],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            data: of({
              defaultSort: 'id,asc',
            }),
            queryParamMap: of(
              jest.requireActual('@angular/router').convertToParamMap({
                page: '1',
                size: '1',
                sort: 'id,desc',
              }),
            ),
            snapshot: { queryParams: {} },
          },
        },
      ],
    })
      .overrideTemplate(CompetenciaComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(CompetenciaComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(CompetenciaService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        }),
      ),
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.competencias?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });

  describe('trackId', () => {
    it('Should forward to competenciaService', () => {
      const entity = { id: 123 };
      jest.spyOn(service, 'getCompetenciaIdentifier');
      const id = comp.trackId(0, entity);
      expect(service.getCompetenciaIdentifier).toHaveBeenCalledWith(entity);
      expect(id).toBe(entity.id);
    });
  });
});
