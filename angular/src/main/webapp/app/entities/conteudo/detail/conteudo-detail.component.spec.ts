import { TestBed } from '@angular/core/testing';
import { provideRouter, withComponentInputBinding } from '@angular/router';
import { RouterTestingHarness, RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { ConteudoDetailComponent } from './conteudo-detail.component';

describe('Conteudo Management Detail Component', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ConteudoDetailComponent, RouterTestingModule.withRoutes([], { bindToComponentInputs: true })],
      providers: [
        provideRouter(
          [
            {
              path: '**',
              component: ConteudoDetailComponent,
              resolve: { conteudo: () => of({ id: 123 }) },
            },
          ],
          withComponentInputBinding(),
        ),
      ],
    })
      .overrideTemplate(ConteudoDetailComponent, '')
      .compileComponents();
  });

  describe('OnInit', () => {
    it('Should load conteudo on init', async () => {
      const harness = await RouterTestingHarness.create();
      const instance = await harness.navigateByUrl('/', ConteudoDetailComponent);

      // THEN
      expect(instance.conteudo).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
