import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IConteudo } from '../conteudo.model';
import { sampleWithRequiredData, sampleWithNewData, sampleWithPartialData, sampleWithFullData } from '../conteudo.test-samples';

import { ConteudoService } from './conteudo.service';

const requireRestSample: IConteudo = {
  ...sampleWithRequiredData,
};

describe('Conteudo Service', () => {
  let service: ConteudoService;
  let httpMock: HttpTestingController;
  let expectedResult: IConteudo | IConteudo[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(ConteudoService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should create a Conteudo', () => {
      const conteudo = { ...sampleWithNewData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.create(conteudo).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a Conteudo', () => {
      const conteudo = { ...sampleWithRequiredData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.update(conteudo).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a Conteudo', () => {
      const patchObject = { ...sampleWithPartialData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of Conteudo', () => {
      const returnedFromService = { ...requireRestSample };

      const expected = { ...sampleWithRequiredData };

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toMatchObject([expected]);
    });

    it('should delete a Conteudo', () => {
      const expected = true;

      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult).toBe(expected);
    });

    describe('addConteudoToCollectionIfMissing', () => {
      it('should add a Conteudo to an empty array', () => {
        const conteudo: IConteudo = sampleWithRequiredData;
        expectedResult = service.addConteudoToCollectionIfMissing([], conteudo);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(conteudo);
      });

      it('should not add a Conteudo to an array that contains it', () => {
        const conteudo: IConteudo = sampleWithRequiredData;
        const conteudoCollection: IConteudo[] = [
          {
            ...conteudo,
          },
          sampleWithPartialData,
        ];
        expectedResult = service.addConteudoToCollectionIfMissing(conteudoCollection, conteudo);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a Conteudo to an array that doesn't contain it", () => {
        const conteudo: IConteudo = sampleWithRequiredData;
        const conteudoCollection: IConteudo[] = [sampleWithPartialData];
        expectedResult = service.addConteudoToCollectionIfMissing(conteudoCollection, conteudo);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(conteudo);
      });

      it('should add only unique Conteudo to an array', () => {
        const conteudoArray: IConteudo[] = [sampleWithRequiredData, sampleWithPartialData, sampleWithFullData];
        const conteudoCollection: IConteudo[] = [sampleWithRequiredData];
        expectedResult = service.addConteudoToCollectionIfMissing(conteudoCollection, ...conteudoArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const conteudo: IConteudo = sampleWithRequiredData;
        const conteudo2: IConteudo = sampleWithPartialData;
        expectedResult = service.addConteudoToCollectionIfMissing([], conteudo, conteudo2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(conteudo);
        expect(expectedResult).toContain(conteudo2);
      });

      it('should accept null and undefined values', () => {
        const conteudo: IConteudo = sampleWithRequiredData;
        expectedResult = service.addConteudoToCollectionIfMissing([], null, conteudo, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(conteudo);
      });

      it('should return initial array if no Conteudo is added', () => {
        const conteudoCollection: IConteudo[] = [sampleWithRequiredData];
        expectedResult = service.addConteudoToCollectionIfMissing(conteudoCollection, undefined, null);
        expect(expectedResult).toEqual(conteudoCollection);
      });
    });

    describe('compareConteudo', () => {
      it('Should return true if both entities are null', () => {
        const entity1 = null;
        const entity2 = null;

        const compareResult = service.compareConteudo(entity1, entity2);

        expect(compareResult).toEqual(true);
      });

      it('Should return false if one entity is null', () => {
        const entity1 = { id: 123 };
        const entity2 = null;

        const compareResult1 = service.compareConteudo(entity1, entity2);
        const compareResult2 = service.compareConteudo(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey differs', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 456 };

        const compareResult1 = service.compareConteudo(entity1, entity2);
        const compareResult2 = service.compareConteudo(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey matches', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 123 };

        const compareResult1 = service.compareConteudo(entity1, entity2);
        const compareResult2 = service.compareConteudo(entity2, entity1);

        expect(compareResult1).toEqual(true);
        expect(compareResult2).toEqual(true);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
