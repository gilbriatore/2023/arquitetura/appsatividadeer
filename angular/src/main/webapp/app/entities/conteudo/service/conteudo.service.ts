import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IConteudo, NewConteudo } from '../conteudo.model';

export type PartialUpdateConteudo = Partial<IConteudo> & Pick<IConteudo, 'id'>;

export type EntityResponseType = HttpResponse<IConteudo>;
export type EntityArrayResponseType = HttpResponse<IConteudo[]>;

@Injectable({ providedIn: 'root' })
export class ConteudoService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/conteudos');

  constructor(
    protected http: HttpClient,
    protected applicationConfigService: ApplicationConfigService,
  ) {}

  create(conteudo: NewConteudo): Observable<EntityResponseType> {
    return this.http.post<IConteudo>(this.resourceUrl, conteudo, { observe: 'response' });
  }

  update(conteudo: IConteudo): Observable<EntityResponseType> {
    return this.http.put<IConteudo>(`${this.resourceUrl}/${this.getConteudoIdentifier(conteudo)}`, conteudo, { observe: 'response' });
  }

  partialUpdate(conteudo: PartialUpdateConteudo): Observable<EntityResponseType> {
    return this.http.patch<IConteudo>(`${this.resourceUrl}/${this.getConteudoIdentifier(conteudo)}`, conteudo, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IConteudo>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IConteudo[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getConteudoIdentifier(conteudo: Pick<IConteudo, 'id'>): number {
    return conteudo.id;
  }

  compareConteudo(o1: Pick<IConteudo, 'id'> | null, o2: Pick<IConteudo, 'id'> | null): boolean {
    return o1 && o2 ? this.getConteudoIdentifier(o1) === this.getConteudoIdentifier(o2) : o1 === o2;
  }

  addConteudoToCollectionIfMissing<Type extends Pick<IConteudo, 'id'>>(
    conteudoCollection: Type[],
    ...conteudosToCheck: (Type | null | undefined)[]
  ): Type[] {
    const conteudos: Type[] = conteudosToCheck.filter(isPresent);
    if (conteudos.length > 0) {
      const conteudoCollectionIdentifiers = conteudoCollection.map(conteudoItem => this.getConteudoIdentifier(conteudoItem)!);
      const conteudosToAdd = conteudos.filter(conteudoItem => {
        const conteudoIdentifier = this.getConteudoIdentifier(conteudoItem);
        if (conteudoCollectionIdentifiers.includes(conteudoIdentifier)) {
          return false;
        }
        conteudoCollectionIdentifiers.push(conteudoIdentifier);
        return true;
      });
      return [...conteudosToAdd, ...conteudoCollection];
    }
    return conteudoCollection;
  }
}
