import { Component } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import SharedModule from 'app/shared/shared.module';
import { ITEM_DELETED_EVENT } from 'app/config/navigation.constants';
import { IConteudo } from '../conteudo.model';
import { ConteudoService } from '../service/conteudo.service';

@Component({
  standalone: true,
  templateUrl: './conteudo-delete-dialog.component.html',
  imports: [SharedModule, FormsModule],
})
export class ConteudoDeleteDialogComponent {
  conteudo?: IConteudo;

  constructor(
    protected conteudoService: ConteudoService,
    protected activeModal: NgbActiveModal,
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.conteudoService.delete(id).subscribe(() => {
      this.activeModal.close(ITEM_DELETED_EVENT);
    });
  }
}
