import { IConteudo, NewConteudo } from './conteudo.model';

export const sampleWithRequiredData: IConteudo = {
  id: 23060,
};

export const sampleWithPartialData: IConteudo = {
  id: 31570,
  descricao: 'dog venerate',
};

export const sampleWithFullData: IConteudo = {
  id: 18291,
  descricao: 'wrong microblog',
};

export const sampleWithNewData: NewConteudo = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
