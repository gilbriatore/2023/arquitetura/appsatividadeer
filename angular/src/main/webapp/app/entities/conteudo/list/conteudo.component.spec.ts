import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { ConteudoService } from '../service/conteudo.service';

import { ConteudoComponent } from './conteudo.component';

describe('Conteudo Management Component', () => {
  let comp: ConteudoComponent;
  let fixture: ComponentFixture<ConteudoComponent>;
  let service: ConteudoService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes([{ path: 'conteudo', component: ConteudoComponent }]),
        HttpClientTestingModule,
        ConteudoComponent,
      ],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            data: of({
              defaultSort: 'id,asc',
            }),
            queryParamMap: of(
              jest.requireActual('@angular/router').convertToParamMap({
                page: '1',
                size: '1',
                sort: 'id,desc',
              }),
            ),
            snapshot: { queryParams: {} },
          },
        },
      ],
    })
      .overrideTemplate(ConteudoComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(ConteudoComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(ConteudoService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        }),
      ),
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.conteudos?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });

  describe('trackId', () => {
    it('Should forward to conteudoService', () => {
      const entity = { id: 123 };
      jest.spyOn(service, 'getConteudoIdentifier');
      const id = comp.trackId(0, entity);
      expect(service.getConteudoIdentifier).toHaveBeenCalledWith(entity);
      expect(id).toBe(entity.id);
    });
  });
});
