import { IConhecimento } from 'app/entities/conhecimento/conhecimento.model';

export interface IConteudo {
  id: number;
  descricao?: string | null;
  conhecimento?: Pick<IConhecimento, 'id'> | null;
  geral?: Pick<IConteudo, 'id'> | null;
}

export type NewConteudo = Omit<IConteudo, 'id'> & { id: null };
