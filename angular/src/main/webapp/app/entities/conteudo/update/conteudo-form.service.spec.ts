import { TestBed } from '@angular/core/testing';

import { sampleWithRequiredData, sampleWithNewData } from '../conteudo.test-samples';

import { ConteudoFormService } from './conteudo-form.service';

describe('Conteudo Form Service', () => {
  let service: ConteudoFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ConteudoFormService);
  });

  describe('Service methods', () => {
    describe('createConteudoFormGroup', () => {
      it('should create a new form with FormControl', () => {
        const formGroup = service.createConteudoFormGroup();

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            descricao: expect.any(Object),
            conhecimento: expect.any(Object),
            geral: expect.any(Object),
          }),
        );
      });

      it('passing IConteudo should create a new form with FormGroup', () => {
        const formGroup = service.createConteudoFormGroup(sampleWithRequiredData);

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            descricao: expect.any(Object),
            conhecimento: expect.any(Object),
            geral: expect.any(Object),
          }),
        );
      });
    });

    describe('getConteudo', () => {
      it('should return NewConteudo for default Conteudo initial value', () => {
        const formGroup = service.createConteudoFormGroup(sampleWithNewData);

        const conteudo = service.getConteudo(formGroup) as any;

        expect(conteudo).toMatchObject(sampleWithNewData);
      });

      it('should return NewConteudo for empty Conteudo initial value', () => {
        const formGroup = service.createConteudoFormGroup();

        const conteudo = service.getConteudo(formGroup) as any;

        expect(conteudo).toMatchObject({});
      });

      it('should return IConteudo', () => {
        const formGroup = service.createConteudoFormGroup(sampleWithRequiredData);

        const conteudo = service.getConteudo(formGroup) as any;

        expect(conteudo).toMatchObject(sampleWithRequiredData);
      });
    });

    describe('resetForm', () => {
      it('passing IConteudo should not enable id FormControl', () => {
        const formGroup = service.createConteudoFormGroup();
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, sampleWithRequiredData);

        expect(formGroup.controls.id.disabled).toBe(true);
      });

      it('passing NewConteudo should disable id FormControl', () => {
        const formGroup = service.createConteudoFormGroup(sampleWithRequiredData);
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, { id: null });

        expect(formGroup.controls.id.disabled).toBe(true);
      });
    });
  });
});
