import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import SharedModule from 'app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IConhecimento } from 'app/entities/conhecimento/conhecimento.model';
import { ConhecimentoService } from 'app/entities/conhecimento/service/conhecimento.service';
import { IConteudo } from '../conteudo.model';
import { ConteudoService } from '../service/conteudo.service';
import { ConteudoFormService, ConteudoFormGroup } from './conteudo-form.service';

@Component({
  standalone: true,
  selector: 'jhi-conteudo-update',
  templateUrl: './conteudo-update.component.html',
  imports: [SharedModule, FormsModule, ReactiveFormsModule],
})
export class ConteudoUpdateComponent implements OnInit {
  isSaving = false;
  conteudo: IConteudo | null = null;

  conteudosSharedCollection: IConteudo[] = [];
  conhecimentosSharedCollection: IConhecimento[] = [];

  editForm: ConteudoFormGroup = this.conteudoFormService.createConteudoFormGroup();

  constructor(
    protected conteudoService: ConteudoService,
    protected conteudoFormService: ConteudoFormService,
    protected conhecimentoService: ConhecimentoService,
    protected activatedRoute: ActivatedRoute,
  ) {}

  compareConteudo = (o1: IConteudo | null, o2: IConteudo | null): boolean => this.conteudoService.compareConteudo(o1, o2);

  compareConhecimento = (o1: IConhecimento | null, o2: IConhecimento | null): boolean =>
    this.conhecimentoService.compareConhecimento(o1, o2);

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ conteudo }) => {
      this.conteudo = conteudo;
      if (conteudo) {
        this.updateForm(conteudo);
      }

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const conteudo = this.conteudoFormService.getConteudo(this.editForm);
    if (conteudo.id !== null) {
      this.subscribeToSaveResponse(this.conteudoService.update(conteudo));
    } else {
      this.subscribeToSaveResponse(this.conteudoService.create(conteudo));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IConteudo>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(conteudo: IConteudo): void {
    this.conteudo = conteudo;
    this.conteudoFormService.resetForm(this.editForm, conteudo);

    this.conteudosSharedCollection = this.conteudoService.addConteudoToCollectionIfMissing<IConteudo>(
      this.conteudosSharedCollection,
      conteudo.geral,
    );
    this.conhecimentosSharedCollection = this.conhecimentoService.addConhecimentoToCollectionIfMissing<IConhecimento>(
      this.conhecimentosSharedCollection,
      conteudo.conhecimento,
    );
  }

  protected loadRelationshipsOptions(): void {
    this.conteudoService
      .query()
      .pipe(map((res: HttpResponse<IConteudo[]>) => res.body ?? []))
      .pipe(
        map((conteudos: IConteudo[]) => this.conteudoService.addConteudoToCollectionIfMissing<IConteudo>(conteudos, this.conteudo?.geral)),
      )
      .subscribe((conteudos: IConteudo[]) => (this.conteudosSharedCollection = conteudos));

    this.conhecimentoService
      .query()
      .pipe(map((res: HttpResponse<IConhecimento[]>) => res.body ?? []))
      .pipe(
        map((conhecimentos: IConhecimento[]) =>
          this.conhecimentoService.addConhecimentoToCollectionIfMissing<IConhecimento>(conhecimentos, this.conteudo?.conhecimento),
        ),
      )
      .subscribe((conhecimentos: IConhecimento[]) => (this.conhecimentosSharedCollection = conhecimentos));
  }
}
