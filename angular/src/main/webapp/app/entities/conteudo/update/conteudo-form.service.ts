import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { IConteudo, NewConteudo } from '../conteudo.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts IConteudo for edit and NewConteudoFormGroupInput for create.
 */
type ConteudoFormGroupInput = IConteudo | PartialWithRequiredKeyOf<NewConteudo>;

type ConteudoFormDefaults = Pick<NewConteudo, 'id'>;

type ConteudoFormGroupContent = {
  id: FormControl<IConteudo['id'] | NewConteudo['id']>;
  descricao: FormControl<IConteudo['descricao']>;
  conhecimento: FormControl<IConteudo['conhecimento']>;
  geral: FormControl<IConteudo['geral']>;
};

export type ConteudoFormGroup = FormGroup<ConteudoFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class ConteudoFormService {
  createConteudoFormGroup(conteudo: ConteudoFormGroupInput = { id: null }): ConteudoFormGroup {
    const conteudoRawValue = {
      ...this.getFormDefaults(),
      ...conteudo,
    };
    return new FormGroup<ConteudoFormGroupContent>({
      id: new FormControl(
        { value: conteudoRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        },
      ),
      descricao: new FormControl(conteudoRawValue.descricao),
      conhecimento: new FormControl(conteudoRawValue.conhecimento),
      geral: new FormControl(conteudoRawValue.geral),
    });
  }

  getConteudo(form: ConteudoFormGroup): IConteudo | NewConteudo {
    return form.getRawValue() as IConteudo | NewConteudo;
  }

  resetForm(form: ConteudoFormGroup, conteudo: ConteudoFormGroupInput): void {
    const conteudoRawValue = { ...this.getFormDefaults(), ...conteudo };
    form.reset(
      {
        ...conteudoRawValue,
        id: { value: conteudoRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */,
    );
  }

  private getFormDefaults(): ConteudoFormDefaults {
    return {
      id: null,
    };
  }
}
