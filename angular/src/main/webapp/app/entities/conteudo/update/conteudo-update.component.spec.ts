import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { IConhecimento } from 'app/entities/conhecimento/conhecimento.model';
import { ConhecimentoService } from 'app/entities/conhecimento/service/conhecimento.service';
import { ConteudoService } from '../service/conteudo.service';
import { IConteudo } from '../conteudo.model';
import { ConteudoFormService } from './conteudo-form.service';

import { ConteudoUpdateComponent } from './conteudo-update.component';

describe('Conteudo Management Update Component', () => {
  let comp: ConteudoUpdateComponent;
  let fixture: ComponentFixture<ConteudoUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let conteudoFormService: ConteudoFormService;
  let conteudoService: ConteudoService;
  let conhecimentoService: ConhecimentoService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([]), ConteudoUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(ConteudoUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(ConteudoUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    conteudoFormService = TestBed.inject(ConteudoFormService);
    conteudoService = TestBed.inject(ConteudoService);
    conhecimentoService = TestBed.inject(ConhecimentoService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Conteudo query and add missing value', () => {
      const conteudo: IConteudo = { id: 456 };
      const geral: IConteudo = { id: 23871 };
      conteudo.geral = geral;

      const conteudoCollection: IConteudo[] = [{ id: 12889 }];
      jest.spyOn(conteudoService, 'query').mockReturnValue(of(new HttpResponse({ body: conteudoCollection })));
      const additionalConteudos = [geral];
      const expectedCollection: IConteudo[] = [...additionalConteudos, ...conteudoCollection];
      jest.spyOn(conteudoService, 'addConteudoToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ conteudo });
      comp.ngOnInit();

      expect(conteudoService.query).toHaveBeenCalled();
      expect(conteudoService.addConteudoToCollectionIfMissing).toHaveBeenCalledWith(
        conteudoCollection,
        ...additionalConteudos.map(expect.objectContaining),
      );
      expect(comp.conteudosSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Conhecimento query and add missing value', () => {
      const conteudo: IConteudo = { id: 456 };
      const conhecimento: IConhecimento = { id: 32273 };
      conteudo.conhecimento = conhecimento;

      const conhecimentoCollection: IConhecimento[] = [{ id: 11274 }];
      jest.spyOn(conhecimentoService, 'query').mockReturnValue(of(new HttpResponse({ body: conhecimentoCollection })));
      const additionalConhecimentos = [conhecimento];
      const expectedCollection: IConhecimento[] = [...additionalConhecimentos, ...conhecimentoCollection];
      jest.spyOn(conhecimentoService, 'addConhecimentoToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ conteudo });
      comp.ngOnInit();

      expect(conhecimentoService.query).toHaveBeenCalled();
      expect(conhecimentoService.addConhecimentoToCollectionIfMissing).toHaveBeenCalledWith(
        conhecimentoCollection,
        ...additionalConhecimentos.map(expect.objectContaining),
      );
      expect(comp.conhecimentosSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const conteudo: IConteudo = { id: 456 };
      const geral: IConteudo = { id: 11719 };
      conteudo.geral = geral;
      const conhecimento: IConhecimento = { id: 770 };
      conteudo.conhecimento = conhecimento;

      activatedRoute.data = of({ conteudo });
      comp.ngOnInit();

      expect(comp.conteudosSharedCollection).toContain(geral);
      expect(comp.conhecimentosSharedCollection).toContain(conhecimento);
      expect(comp.conteudo).toEqual(conteudo);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IConteudo>>();
      const conteudo = { id: 123 };
      jest.spyOn(conteudoFormService, 'getConteudo').mockReturnValue(conteudo);
      jest.spyOn(conteudoService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ conteudo });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: conteudo }));
      saveSubject.complete();

      // THEN
      expect(conteudoFormService.getConteudo).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(conteudoService.update).toHaveBeenCalledWith(expect.objectContaining(conteudo));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IConteudo>>();
      const conteudo = { id: 123 };
      jest.spyOn(conteudoFormService, 'getConteudo').mockReturnValue({ id: null });
      jest.spyOn(conteudoService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ conteudo: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: conteudo }));
      saveSubject.complete();

      // THEN
      expect(conteudoFormService.getConteudo).toHaveBeenCalled();
      expect(conteudoService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IConteudo>>();
      const conteudo = { id: 123 };
      jest.spyOn(conteudoService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ conteudo });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(conteudoService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Compare relationships', () => {
    describe('compareConteudo', () => {
      it('Should forward to conteudoService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(conteudoService, 'compareConteudo');
        comp.compareConteudo(entity, entity2);
        expect(conteudoService.compareConteudo).toHaveBeenCalledWith(entity, entity2);
      });
    });

    describe('compareConhecimento', () => {
      it('Should forward to conhecimentoService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(conhecimentoService, 'compareConhecimento');
        comp.compareConhecimento(entity, entity2);
        expect(conhecimentoService.compareConhecimento).toHaveBeenCalledWith(entity, entity2);
      });
    });
  });
});
