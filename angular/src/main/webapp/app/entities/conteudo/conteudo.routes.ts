import { Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { ASC } from 'app/config/navigation.constants';
import { ConteudoComponent } from './list/conteudo.component';
import { ConteudoDetailComponent } from './detail/conteudo-detail.component';
import { ConteudoUpdateComponent } from './update/conteudo-update.component';
import ConteudoResolve from './route/conteudo-routing-resolve.service';

const conteudoRoute: Routes = [
  {
    path: '',
    component: ConteudoComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: ConteudoDetailComponent,
    resolve: {
      conteudo: ConteudoResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: ConteudoUpdateComponent,
    resolve: {
      conteudo: ConteudoResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: ConteudoUpdateComponent,
    resolve: {
      conteudo: ConteudoResolve,
    },
    canActivate: [UserRouteAccessService],
  },
];

export default conteudoRoute;
