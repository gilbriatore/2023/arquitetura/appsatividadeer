import { inject } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of, EMPTY, Observable } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IConteudo } from '../conteudo.model';
import { ConteudoService } from '../service/conteudo.service';

export const conteudoResolve = (route: ActivatedRouteSnapshot): Observable<null | IConteudo> => {
  const id = route.params['id'];
  if (id) {
    return inject(ConteudoService)
      .find(id)
      .pipe(
        mergeMap((conteudo: HttpResponse<IConteudo>) => {
          if (conteudo.body) {
            return of(conteudo.body);
          } else {
            inject(Router).navigate(['404']);
            return EMPTY;
          }
        }),
      );
  }
  return of(null);
};

export default conteudoResolve;
