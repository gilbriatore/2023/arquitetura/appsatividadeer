package br.edu.up.matrix.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import br.edu.up.matrix.IntegrationTest;
import br.edu.up.matrix.domain.Atividade;
import br.edu.up.matrix.repository.AtividadeRepository;
import java.util.List;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

/**
 * Integration tests for the {@link AtividadeResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class AtividadeResourceIT {

    private static final String DEFAULT_NOME = "AAAAAAAAAA";
    private static final String UPDATED_NOME = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/atividades";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    @Autowired
    private AtividadeRepository atividadeRepository;

    @Autowired
    private MockMvc restAtividadeMockMvc;

    private Atividade atividade;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Atividade createEntity() {
        Atividade atividade = new Atividade().nome(DEFAULT_NOME);
        return atividade;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Atividade createUpdatedEntity() {
        Atividade atividade = new Atividade().nome(UPDATED_NOME);
        return atividade;
    }

    @BeforeEach
    public void initTest() {
        atividadeRepository.deleteAll();
        atividade = createEntity();
    }

    @Test
    void createAtividade() throws Exception {
        int databaseSizeBeforeCreate = atividadeRepository.findAll().size();
        // Create the Atividade
        restAtividadeMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(atividade)))
            .andExpect(status().isCreated());

        // Validate the Atividade in the database
        List<Atividade> atividadeList = atividadeRepository.findAll();
        assertThat(atividadeList).hasSize(databaseSizeBeforeCreate + 1);
        Atividade testAtividade = atividadeList.get(atividadeList.size() - 1);
        assertThat(testAtividade.getNome()).isEqualTo(DEFAULT_NOME);
    }

    @Test
    void createAtividadeWithExistingId() throws Exception {
        // Create the Atividade with an existing ID
        atividade.setId("existing_id");

        int databaseSizeBeforeCreate = atividadeRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restAtividadeMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(atividade)))
            .andExpect(status().isBadRequest());

        // Validate the Atividade in the database
        List<Atividade> atividadeList = atividadeRepository.findAll();
        assertThat(atividadeList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    void getAllAtividades() throws Exception {
        // Initialize the database
        atividade.setId(UUID.randomUUID().toString());
        atividadeRepository.save(atividade);

        // Get all the atividadeList
        restAtividadeMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(atividade.getId())))
            .andExpect(jsonPath("$.[*].nome").value(hasItem(DEFAULT_NOME)));
    }

    @Test
    void getAtividade() throws Exception {
        // Initialize the database
        atividade.setId(UUID.randomUUID().toString());
        atividadeRepository.save(atividade);

        // Get the atividade
        restAtividadeMockMvc
            .perform(get(ENTITY_API_URL_ID, atividade.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(atividade.getId()))
            .andExpect(jsonPath("$.nome").value(DEFAULT_NOME));
    }

    @Test
    void getNonExistingAtividade() throws Exception {
        // Get the atividade
        restAtividadeMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    void putExistingAtividade() throws Exception {
        // Initialize the database
        atividade.setId(UUID.randomUUID().toString());
        atividadeRepository.save(atividade);

        int databaseSizeBeforeUpdate = atividadeRepository.findAll().size();

        // Update the atividade
        Atividade updatedAtividade = atividadeRepository.findById(atividade.getId()).orElseThrow();
        updatedAtividade.nome(UPDATED_NOME);

        restAtividadeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedAtividade.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedAtividade))
            )
            .andExpect(status().isOk());

        // Validate the Atividade in the database
        List<Atividade> atividadeList = atividadeRepository.findAll();
        assertThat(atividadeList).hasSize(databaseSizeBeforeUpdate);
        Atividade testAtividade = atividadeList.get(atividadeList.size() - 1);
        assertThat(testAtividade.getNome()).isEqualTo(UPDATED_NOME);
    }

    @Test
    void putNonExistingAtividade() throws Exception {
        int databaseSizeBeforeUpdate = atividadeRepository.findAll().size();
        atividade.setId(UUID.randomUUID().toString());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAtividadeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, atividade.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(atividade))
            )
            .andExpect(status().isBadRequest());

        // Validate the Atividade in the database
        List<Atividade> atividadeList = atividadeRepository.findAll();
        assertThat(atividadeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithIdMismatchAtividade() throws Exception {
        int databaseSizeBeforeUpdate = atividadeRepository.findAll().size();
        atividade.setId(UUID.randomUUID().toString());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restAtividadeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, UUID.randomUUID().toString())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(atividade))
            )
            .andExpect(status().isBadRequest());

        // Validate the Atividade in the database
        List<Atividade> atividadeList = atividadeRepository.findAll();
        assertThat(atividadeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithMissingIdPathParamAtividade() throws Exception {
        int databaseSizeBeforeUpdate = atividadeRepository.findAll().size();
        atividade.setId(UUID.randomUUID().toString());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restAtividadeMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(atividade)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Atividade in the database
        List<Atividade> atividadeList = atividadeRepository.findAll();
        assertThat(atividadeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void partialUpdateAtividadeWithPatch() throws Exception {
        // Initialize the database
        atividade.setId(UUID.randomUUID().toString());
        atividadeRepository.save(atividade);

        int databaseSizeBeforeUpdate = atividadeRepository.findAll().size();

        // Update the atividade using partial update
        Atividade partialUpdatedAtividade = new Atividade();
        partialUpdatedAtividade.setId(atividade.getId());

        partialUpdatedAtividade.nome(UPDATED_NOME);

        restAtividadeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedAtividade.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedAtividade))
            )
            .andExpect(status().isOk());

        // Validate the Atividade in the database
        List<Atividade> atividadeList = atividadeRepository.findAll();
        assertThat(atividadeList).hasSize(databaseSizeBeforeUpdate);
        Atividade testAtividade = atividadeList.get(atividadeList.size() - 1);
        assertThat(testAtividade.getNome()).isEqualTo(UPDATED_NOME);
    }

    @Test
    void fullUpdateAtividadeWithPatch() throws Exception {
        // Initialize the database
        atividade.setId(UUID.randomUUID().toString());
        atividadeRepository.save(atividade);

        int databaseSizeBeforeUpdate = atividadeRepository.findAll().size();

        // Update the atividade using partial update
        Atividade partialUpdatedAtividade = new Atividade();
        partialUpdatedAtividade.setId(atividade.getId());

        partialUpdatedAtividade.nome(UPDATED_NOME);

        restAtividadeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedAtividade.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedAtividade))
            )
            .andExpect(status().isOk());

        // Validate the Atividade in the database
        List<Atividade> atividadeList = atividadeRepository.findAll();
        assertThat(atividadeList).hasSize(databaseSizeBeforeUpdate);
        Atividade testAtividade = atividadeList.get(atividadeList.size() - 1);
        assertThat(testAtividade.getNome()).isEqualTo(UPDATED_NOME);
    }

    @Test
    void patchNonExistingAtividade() throws Exception {
        int databaseSizeBeforeUpdate = atividadeRepository.findAll().size();
        atividade.setId(UUID.randomUUID().toString());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAtividadeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, atividade.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(atividade))
            )
            .andExpect(status().isBadRequest());

        // Validate the Atividade in the database
        List<Atividade> atividadeList = atividadeRepository.findAll();
        assertThat(atividadeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithIdMismatchAtividade() throws Exception {
        int databaseSizeBeforeUpdate = atividadeRepository.findAll().size();
        atividade.setId(UUID.randomUUID().toString());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restAtividadeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, UUID.randomUUID().toString())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(atividade))
            )
            .andExpect(status().isBadRequest());

        // Validate the Atividade in the database
        List<Atividade> atividadeList = atividadeRepository.findAll();
        assertThat(atividadeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithMissingIdPathParamAtividade() throws Exception {
        int databaseSizeBeforeUpdate = atividadeRepository.findAll().size();
        atividade.setId(UUID.randomUUID().toString());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restAtividadeMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(atividade))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Atividade in the database
        List<Atividade> atividadeList = atividadeRepository.findAll();
        assertThat(atividadeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void deleteAtividade() throws Exception {
        // Initialize the database
        atividade.setId(UUID.randomUUID().toString());
        atividadeRepository.save(atividade);

        int databaseSizeBeforeDelete = atividadeRepository.findAll().size();

        // Delete the atividade
        restAtividadeMockMvc
            .perform(delete(ENTITY_API_URL_ID, atividade.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Atividade> atividadeList = atividadeRepository.findAll();
        assertThat(atividadeList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
