package br.edu.up.matrix.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import br.edu.up.matrix.IntegrationTest;
import br.edu.up.matrix.domain.Modalidade;
import br.edu.up.matrix.repository.ModalidadeRepository;
import java.util.List;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

/**
 * Integration tests for the {@link ModalidadeResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class ModalidadeResourceIT {

    private static final String DEFAULT_NOME = "AAAAAAAAAA";
    private static final String UPDATED_NOME = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/modalidades";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    @Autowired
    private ModalidadeRepository modalidadeRepository;

    @Autowired
    private MockMvc restModalidadeMockMvc;

    private Modalidade modalidade;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Modalidade createEntity() {
        Modalidade modalidade = new Modalidade().nome(DEFAULT_NOME);
        return modalidade;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Modalidade createUpdatedEntity() {
        Modalidade modalidade = new Modalidade().nome(UPDATED_NOME);
        return modalidade;
    }

    @BeforeEach
    public void initTest() {
        modalidadeRepository.deleteAll();
        modalidade = createEntity();
    }

    @Test
    void createModalidade() throws Exception {
        int databaseSizeBeforeCreate = modalidadeRepository.findAll().size();
        // Create the Modalidade
        restModalidadeMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(modalidade)))
            .andExpect(status().isCreated());

        // Validate the Modalidade in the database
        List<Modalidade> modalidadeList = modalidadeRepository.findAll();
        assertThat(modalidadeList).hasSize(databaseSizeBeforeCreate + 1);
        Modalidade testModalidade = modalidadeList.get(modalidadeList.size() - 1);
        assertThat(testModalidade.getNome()).isEqualTo(DEFAULT_NOME);
    }

    @Test
    void createModalidadeWithExistingId() throws Exception {
        // Create the Modalidade with an existing ID
        modalidade.setId("existing_id");

        int databaseSizeBeforeCreate = modalidadeRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restModalidadeMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(modalidade)))
            .andExpect(status().isBadRequest());

        // Validate the Modalidade in the database
        List<Modalidade> modalidadeList = modalidadeRepository.findAll();
        assertThat(modalidadeList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    void getAllModalidades() throws Exception {
        // Initialize the database
        modalidade.setId(UUID.randomUUID().toString());
        modalidadeRepository.save(modalidade);

        // Get all the modalidadeList
        restModalidadeMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(modalidade.getId())))
            .andExpect(jsonPath("$.[*].nome").value(hasItem(DEFAULT_NOME)));
    }

    @Test
    void getModalidade() throws Exception {
        // Initialize the database
        modalidade.setId(UUID.randomUUID().toString());
        modalidadeRepository.save(modalidade);

        // Get the modalidade
        restModalidadeMockMvc
            .perform(get(ENTITY_API_URL_ID, modalidade.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(modalidade.getId()))
            .andExpect(jsonPath("$.nome").value(DEFAULT_NOME));
    }

    @Test
    void getNonExistingModalidade() throws Exception {
        // Get the modalidade
        restModalidadeMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    void putExistingModalidade() throws Exception {
        // Initialize the database
        modalidade.setId(UUID.randomUUID().toString());
        modalidadeRepository.save(modalidade);

        int databaseSizeBeforeUpdate = modalidadeRepository.findAll().size();

        // Update the modalidade
        Modalidade updatedModalidade = modalidadeRepository.findById(modalidade.getId()).orElseThrow();
        updatedModalidade.nome(UPDATED_NOME);

        restModalidadeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedModalidade.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedModalidade))
            )
            .andExpect(status().isOk());

        // Validate the Modalidade in the database
        List<Modalidade> modalidadeList = modalidadeRepository.findAll();
        assertThat(modalidadeList).hasSize(databaseSizeBeforeUpdate);
        Modalidade testModalidade = modalidadeList.get(modalidadeList.size() - 1);
        assertThat(testModalidade.getNome()).isEqualTo(UPDATED_NOME);
    }

    @Test
    void putNonExistingModalidade() throws Exception {
        int databaseSizeBeforeUpdate = modalidadeRepository.findAll().size();
        modalidade.setId(UUID.randomUUID().toString());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restModalidadeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, modalidade.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(modalidade))
            )
            .andExpect(status().isBadRequest());

        // Validate the Modalidade in the database
        List<Modalidade> modalidadeList = modalidadeRepository.findAll();
        assertThat(modalidadeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithIdMismatchModalidade() throws Exception {
        int databaseSizeBeforeUpdate = modalidadeRepository.findAll().size();
        modalidade.setId(UUID.randomUUID().toString());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restModalidadeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, UUID.randomUUID().toString())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(modalidade))
            )
            .andExpect(status().isBadRequest());

        // Validate the Modalidade in the database
        List<Modalidade> modalidadeList = modalidadeRepository.findAll();
        assertThat(modalidadeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithMissingIdPathParamModalidade() throws Exception {
        int databaseSizeBeforeUpdate = modalidadeRepository.findAll().size();
        modalidade.setId(UUID.randomUUID().toString());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restModalidadeMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(modalidade)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Modalidade in the database
        List<Modalidade> modalidadeList = modalidadeRepository.findAll();
        assertThat(modalidadeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void partialUpdateModalidadeWithPatch() throws Exception {
        // Initialize the database
        modalidade.setId(UUID.randomUUID().toString());
        modalidadeRepository.save(modalidade);

        int databaseSizeBeforeUpdate = modalidadeRepository.findAll().size();

        // Update the modalidade using partial update
        Modalidade partialUpdatedModalidade = new Modalidade();
        partialUpdatedModalidade.setId(modalidade.getId());

        partialUpdatedModalidade.nome(UPDATED_NOME);

        restModalidadeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedModalidade.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedModalidade))
            )
            .andExpect(status().isOk());

        // Validate the Modalidade in the database
        List<Modalidade> modalidadeList = modalidadeRepository.findAll();
        assertThat(modalidadeList).hasSize(databaseSizeBeforeUpdate);
        Modalidade testModalidade = modalidadeList.get(modalidadeList.size() - 1);
        assertThat(testModalidade.getNome()).isEqualTo(UPDATED_NOME);
    }

    @Test
    void fullUpdateModalidadeWithPatch() throws Exception {
        // Initialize the database
        modalidade.setId(UUID.randomUUID().toString());
        modalidadeRepository.save(modalidade);

        int databaseSizeBeforeUpdate = modalidadeRepository.findAll().size();

        // Update the modalidade using partial update
        Modalidade partialUpdatedModalidade = new Modalidade();
        partialUpdatedModalidade.setId(modalidade.getId());

        partialUpdatedModalidade.nome(UPDATED_NOME);

        restModalidadeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedModalidade.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedModalidade))
            )
            .andExpect(status().isOk());

        // Validate the Modalidade in the database
        List<Modalidade> modalidadeList = modalidadeRepository.findAll();
        assertThat(modalidadeList).hasSize(databaseSizeBeforeUpdate);
        Modalidade testModalidade = modalidadeList.get(modalidadeList.size() - 1);
        assertThat(testModalidade.getNome()).isEqualTo(UPDATED_NOME);
    }

    @Test
    void patchNonExistingModalidade() throws Exception {
        int databaseSizeBeforeUpdate = modalidadeRepository.findAll().size();
        modalidade.setId(UUID.randomUUID().toString());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restModalidadeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, modalidade.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(modalidade))
            )
            .andExpect(status().isBadRequest());

        // Validate the Modalidade in the database
        List<Modalidade> modalidadeList = modalidadeRepository.findAll();
        assertThat(modalidadeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithIdMismatchModalidade() throws Exception {
        int databaseSizeBeforeUpdate = modalidadeRepository.findAll().size();
        modalidade.setId(UUID.randomUUID().toString());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restModalidadeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, UUID.randomUUID().toString())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(modalidade))
            )
            .andExpect(status().isBadRequest());

        // Validate the Modalidade in the database
        List<Modalidade> modalidadeList = modalidadeRepository.findAll();
        assertThat(modalidadeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithMissingIdPathParamModalidade() throws Exception {
        int databaseSizeBeforeUpdate = modalidadeRepository.findAll().size();
        modalidade.setId(UUID.randomUUID().toString());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restModalidadeMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(modalidade))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Modalidade in the database
        List<Modalidade> modalidadeList = modalidadeRepository.findAll();
        assertThat(modalidadeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void deleteModalidade() throws Exception {
        // Initialize the database
        modalidade.setId(UUID.randomUUID().toString());
        modalidadeRepository.save(modalidade);

        int databaseSizeBeforeDelete = modalidadeRepository.findAll().size();

        // Delete the modalidade
        restModalidadeMockMvc
            .perform(delete(ENTITY_API_URL_ID, modalidade.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Modalidade> modalidadeList = modalidadeRepository.findAll();
        assertThat(modalidadeList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
