package br.edu.up.matrix.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import br.edu.up.matrix.IntegrationTest;
import br.edu.up.matrix.domain.Matriz;
import br.edu.up.matrix.repository.MatrizRepository;
import java.util.List;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

/**
 * Integration tests for the {@link MatrizResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class MatrizResourceIT {

    private static final String DEFAULT_CODIGO = "AAAAAAAAAA";
    private static final String UPDATED_CODIGO = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/matrizs";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    @Autowired
    private MatrizRepository matrizRepository;

    @Autowired
    private MockMvc restMatrizMockMvc;

    private Matriz matriz;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Matriz createEntity() {
        Matriz matriz = new Matriz().codigo(DEFAULT_CODIGO);
        return matriz;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Matriz createUpdatedEntity() {
        Matriz matriz = new Matriz().codigo(UPDATED_CODIGO);
        return matriz;
    }

    @BeforeEach
    public void initTest() {
        matrizRepository.deleteAll();
        matriz = createEntity();
    }

    @Test
    void createMatriz() throws Exception {
        int databaseSizeBeforeCreate = matrizRepository.findAll().size();
        // Create the Matriz
        restMatrizMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(matriz)))
            .andExpect(status().isCreated());

        // Validate the Matriz in the database
        List<Matriz> matrizList = matrizRepository.findAll();
        assertThat(matrizList).hasSize(databaseSizeBeforeCreate + 1);
        Matriz testMatriz = matrizList.get(matrizList.size() - 1);
        assertThat(testMatriz.getCodigo()).isEqualTo(DEFAULT_CODIGO);
    }

    @Test
    void createMatrizWithExistingId() throws Exception {
        // Create the Matriz with an existing ID
        matriz.setId("existing_id");

        int databaseSizeBeforeCreate = matrizRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restMatrizMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(matriz)))
            .andExpect(status().isBadRequest());

        // Validate the Matriz in the database
        List<Matriz> matrizList = matrizRepository.findAll();
        assertThat(matrizList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    void getAllMatrizs() throws Exception {
        // Initialize the database
        matriz.setId(UUID.randomUUID().toString());
        matrizRepository.save(matriz);

        // Get all the matrizList
        restMatrizMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(matriz.getId())))
            .andExpect(jsonPath("$.[*].codigo").value(hasItem(DEFAULT_CODIGO)));
    }

    @Test
    void getMatriz() throws Exception {
        // Initialize the database
        matriz.setId(UUID.randomUUID().toString());
        matrizRepository.save(matriz);

        // Get the matriz
        restMatrizMockMvc
            .perform(get(ENTITY_API_URL_ID, matriz.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(matriz.getId()))
            .andExpect(jsonPath("$.codigo").value(DEFAULT_CODIGO));
    }

    @Test
    void getNonExistingMatriz() throws Exception {
        // Get the matriz
        restMatrizMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    void putExistingMatriz() throws Exception {
        // Initialize the database
        matriz.setId(UUID.randomUUID().toString());
        matrizRepository.save(matriz);

        int databaseSizeBeforeUpdate = matrizRepository.findAll().size();

        // Update the matriz
        Matriz updatedMatriz = matrizRepository.findById(matriz.getId()).orElseThrow();
        updatedMatriz.codigo(UPDATED_CODIGO);

        restMatrizMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedMatriz.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedMatriz))
            )
            .andExpect(status().isOk());

        // Validate the Matriz in the database
        List<Matriz> matrizList = matrizRepository.findAll();
        assertThat(matrizList).hasSize(databaseSizeBeforeUpdate);
        Matriz testMatriz = matrizList.get(matrizList.size() - 1);
        assertThat(testMatriz.getCodigo()).isEqualTo(UPDATED_CODIGO);
    }

    @Test
    void putNonExistingMatriz() throws Exception {
        int databaseSizeBeforeUpdate = matrizRepository.findAll().size();
        matriz.setId(UUID.randomUUID().toString());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMatrizMockMvc
            .perform(
                put(ENTITY_API_URL_ID, matriz.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(matriz))
            )
            .andExpect(status().isBadRequest());

        // Validate the Matriz in the database
        List<Matriz> matrizList = matrizRepository.findAll();
        assertThat(matrizList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithIdMismatchMatriz() throws Exception {
        int databaseSizeBeforeUpdate = matrizRepository.findAll().size();
        matriz.setId(UUID.randomUUID().toString());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMatrizMockMvc
            .perform(
                put(ENTITY_API_URL_ID, UUID.randomUUID().toString())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(matriz))
            )
            .andExpect(status().isBadRequest());

        // Validate the Matriz in the database
        List<Matriz> matrizList = matrizRepository.findAll();
        assertThat(matrizList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithMissingIdPathParamMatriz() throws Exception {
        int databaseSizeBeforeUpdate = matrizRepository.findAll().size();
        matriz.setId(UUID.randomUUID().toString());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMatrizMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(matriz)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Matriz in the database
        List<Matriz> matrizList = matrizRepository.findAll();
        assertThat(matrizList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void partialUpdateMatrizWithPatch() throws Exception {
        // Initialize the database
        matriz.setId(UUID.randomUUID().toString());
        matrizRepository.save(matriz);

        int databaseSizeBeforeUpdate = matrizRepository.findAll().size();

        // Update the matriz using partial update
        Matriz partialUpdatedMatriz = new Matriz();
        partialUpdatedMatriz.setId(matriz.getId());

        partialUpdatedMatriz.codigo(UPDATED_CODIGO);

        restMatrizMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedMatriz.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedMatriz))
            )
            .andExpect(status().isOk());

        // Validate the Matriz in the database
        List<Matriz> matrizList = matrizRepository.findAll();
        assertThat(matrizList).hasSize(databaseSizeBeforeUpdate);
        Matriz testMatriz = matrizList.get(matrizList.size() - 1);
        assertThat(testMatriz.getCodigo()).isEqualTo(UPDATED_CODIGO);
    }

    @Test
    void fullUpdateMatrizWithPatch() throws Exception {
        // Initialize the database
        matriz.setId(UUID.randomUUID().toString());
        matrizRepository.save(matriz);

        int databaseSizeBeforeUpdate = matrizRepository.findAll().size();

        // Update the matriz using partial update
        Matriz partialUpdatedMatriz = new Matriz();
        partialUpdatedMatriz.setId(matriz.getId());

        partialUpdatedMatriz.codigo(UPDATED_CODIGO);

        restMatrizMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedMatriz.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedMatriz))
            )
            .andExpect(status().isOk());

        // Validate the Matriz in the database
        List<Matriz> matrizList = matrizRepository.findAll();
        assertThat(matrizList).hasSize(databaseSizeBeforeUpdate);
        Matriz testMatriz = matrizList.get(matrizList.size() - 1);
        assertThat(testMatriz.getCodigo()).isEqualTo(UPDATED_CODIGO);
    }

    @Test
    void patchNonExistingMatriz() throws Exception {
        int databaseSizeBeforeUpdate = matrizRepository.findAll().size();
        matriz.setId(UUID.randomUUID().toString());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMatrizMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, matriz.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(matriz))
            )
            .andExpect(status().isBadRequest());

        // Validate the Matriz in the database
        List<Matriz> matrizList = matrizRepository.findAll();
        assertThat(matrizList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithIdMismatchMatriz() throws Exception {
        int databaseSizeBeforeUpdate = matrizRepository.findAll().size();
        matriz.setId(UUID.randomUUID().toString());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMatrizMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, UUID.randomUUID().toString())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(matriz))
            )
            .andExpect(status().isBadRequest());

        // Validate the Matriz in the database
        List<Matriz> matrizList = matrizRepository.findAll();
        assertThat(matrizList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithMissingIdPathParamMatriz() throws Exception {
        int databaseSizeBeforeUpdate = matrizRepository.findAll().size();
        matriz.setId(UUID.randomUUID().toString());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMatrizMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(matriz)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Matriz in the database
        List<Matriz> matrizList = matrizRepository.findAll();
        assertThat(matrizList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void deleteMatriz() throws Exception {
        // Initialize the database
        matriz.setId(UUID.randomUUID().toString());
        matrizRepository.save(matriz);

        int databaseSizeBeforeDelete = matrizRepository.findAll().size();

        // Delete the matriz
        restMatrizMockMvc
            .perform(delete(ENTITY_API_URL_ID, matriz.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Matriz> matrizList = matrizRepository.findAll();
        assertThat(matrizList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
