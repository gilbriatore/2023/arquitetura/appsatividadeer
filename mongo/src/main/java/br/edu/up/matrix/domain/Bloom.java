package br.edu.up.matrix.domain;

import java.io.Serializable;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * A Bloom.
 */
@Document(collection = "bloom")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Bloom implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @Field("nivel")
    private String nivel;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public String getId() {
        return this.id;
    }

    public Bloom id(String id) {
        this.setId(id);
        return this;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNivel() {
        return this.nivel;
    }

    public Bloom nivel(String nivel) {
        this.setNivel(nivel);
        return this;
    }

    public void setNivel(String nivel) {
        this.nivel = nivel;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Bloom)) {
            return false;
        }
        return getId() != null && getId().equals(((Bloom) o).getId());
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Bloom{" +
            "id=" + getId() +
            ", nivel='" + getNivel() + "'" +
            "}";
    }
}
