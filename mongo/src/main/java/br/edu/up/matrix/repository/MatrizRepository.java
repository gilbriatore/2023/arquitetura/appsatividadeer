package br.edu.up.matrix.repository;

import br.edu.up.matrix.domain.Matriz;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data MongoDB repository for the Matriz entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MatrizRepository extends MongoRepository<Matriz, String> {}
