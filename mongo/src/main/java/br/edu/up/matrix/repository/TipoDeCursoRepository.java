package br.edu.up.matrix.repository;

import br.edu.up.matrix.domain.TipoDeCurso;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data MongoDB repository for the TipoDeCurso entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TipoDeCursoRepository extends MongoRepository<TipoDeCurso, String> {}
