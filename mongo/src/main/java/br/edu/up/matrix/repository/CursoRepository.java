package br.edu.up.matrix.repository;

import br.edu.up.matrix.domain.Curso;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data MongoDB repository for the Curso entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CursoRepository extends MongoRepository<Curso, String> {}
