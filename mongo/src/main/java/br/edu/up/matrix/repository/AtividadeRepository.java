package br.edu.up.matrix.repository;

import br.edu.up.matrix.domain.Atividade;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data MongoDB repository for the Atividade entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AtividadeRepository extends MongoRepository<Atividade, String> {}
