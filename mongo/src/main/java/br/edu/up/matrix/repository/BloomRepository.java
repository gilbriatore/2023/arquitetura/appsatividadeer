package br.edu.up.matrix.repository;

import br.edu.up.matrix.domain.Bloom;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data MongoDB repository for the Bloom entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BloomRepository extends MongoRepository<Bloom, String> {}
