package br.edu.up.matrix.repository;

import br.edu.up.matrix.domain.Escola;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data MongoDB repository for the Escola entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EscolaRepository extends MongoRepository<Escola, String> {}
