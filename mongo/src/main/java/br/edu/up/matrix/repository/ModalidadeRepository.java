package br.edu.up.matrix.repository;

import br.edu.up.matrix.domain.Modalidade;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data MongoDB repository for the Modalidade entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ModalidadeRepository extends MongoRepository<Modalidade, String> {}
