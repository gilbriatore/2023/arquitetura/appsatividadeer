package br.edu.up.matrix.repository;

import br.edu.up.matrix.domain.Unidade;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data MongoDB repository for the Unidade entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UnidadeRepository extends MongoRepository<Unidade, String> {}
