import React from 'react';
import { Route } from 'react-router-dom';

import ErrorBoundaryRoutes from 'app/shared/error/error-boundary-routes';

import Matriz from './matriz';
import MatrizDetail from './matriz-detail';
import MatrizUpdate from './matriz-update';
import MatrizDeleteDialog from './matriz-delete-dialog';

const MatrizRoutes = () => (
  <ErrorBoundaryRoutes>
    <Route index element={<Matriz />} />
    <Route path="new" element={<MatrizUpdate />} />
    <Route path=":id">
      <Route index element={<MatrizDetail />} />
      <Route path="edit" element={<MatrizUpdate />} />
      <Route path="delete" element={<MatrizDeleteDialog />} />
    </Route>
  </ErrorBoundaryRoutes>
);

export default MatrizRoutes;
