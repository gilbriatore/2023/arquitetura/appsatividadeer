import React from 'react';
import { Route } from 'react-router-dom';

import ErrorBoundaryRoutes from 'app/shared/error/error-boundary-routes';

import Atividade from './atividade';
import Bloom from './bloom';
import Curso from './curso';
import Escola from './escola';
import Matriz from './matriz';
import Modalidade from './modalidade';
import TipoDeCurso from './tipo-de-curso';
import Unidade from './unidade';
/* jhipster-needle-add-route-import - JHipster will add routes here */

export default () => {
  return (
    <div>
      <ErrorBoundaryRoutes>
        {/* prettier-ignore */}
        <Route path="atividade/*" element={<Atividade />} />
        <Route path="bloom/*" element={<Bloom />} />
        <Route path="curso/*" element={<Curso />} />
        <Route path="escola/*" element={<Escola />} />
        <Route path="matriz/*" element={<Matriz />} />
        <Route path="modalidade/*" element={<Modalidade />} />
        <Route path="tipo-de-curso/*" element={<TipoDeCurso />} />
        <Route path="unidade/*" element={<Unidade />} />
        {/* jhipster-needle-add-route-path - JHipster will add routes here */}
      </ErrorBoundaryRoutes>
    </div>
  );
};
