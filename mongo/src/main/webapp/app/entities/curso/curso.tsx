import React, { useState, useEffect } from 'react';
import { Link, useLocation, useNavigate } from 'react-router-dom';
import { Button, Table } from 'reactstrap';
import { Translate, getSortState } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSort, faSortUp, faSortDown } from '@fortawesome/free-solid-svg-icons';
import { ASC, DESC, SORT } from 'app/shared/util/pagination.constants';
import { overrideSortStateWithQueryParams } from 'app/shared/util/entity-utils';
import { useAppDispatch, useAppSelector } from 'app/config/store';

import { getEntities } from './curso.reducer';

export const Curso = () => {
  const dispatch = useAppDispatch();

  const pageLocation = useLocation();
  const navigate = useNavigate();

  const [sortState, setSortState] = useState(overrideSortStateWithQueryParams(getSortState(pageLocation, 'id'), pageLocation.search));

  const cursoList = useAppSelector(state => state.curso.entities);
  const loading = useAppSelector(state => state.curso.loading);

  const getAllEntities = () => {
    dispatch(
      getEntities({
        sort: `${sortState.sort},${sortState.order}`,
      }),
    );
  };

  const sortEntities = () => {
    getAllEntities();
    const endURL = `?sort=${sortState.sort},${sortState.order}`;
    if (pageLocation.search !== endURL) {
      navigate(`${pageLocation.pathname}${endURL}`);
    }
  };

  useEffect(() => {
    sortEntities();
  }, [sortState.order, sortState.sort]);

  const sort = p => () => {
    setSortState({
      ...sortState,
      order: sortState.order === ASC ? DESC : ASC,
      sort: p,
    });
  };

  const handleSyncList = () => {
    sortEntities();
  };

  const getSortIconByFieldName = (fieldName: string) => {
    const sortFieldName = sortState.sort;
    const order = sortState.order;
    if (sortFieldName !== fieldName) {
      return faSort;
    } else {
      return order === ASC ? faSortUp : faSortDown;
    }
  };

  return (
    <div>
      <h2 id="curso-heading" data-cy="CursoHeading">
        Cursos
        <div className="d-flex justify-content-end">
          <Button className="me-2" color="info" onClick={handleSyncList} disabled={loading}>
            <FontAwesomeIcon icon="sync" spin={loading} /> Atualizar lista
          </Button>
          <Link to="/curso/new" className="btn btn-primary jh-create-entity" id="jh-create-entity" data-cy="entityCreateButton">
            <FontAwesomeIcon icon="plus" />
            &nbsp; Criar novo Curso
          </Link>
        </div>
      </h2>
      <div className="table-responsive">
        {cursoList && cursoList.length > 0 ? (
          <Table responsive>
            <thead>
              <tr>
                <th className="hand" onClick={sort('id')}>
                  Id <FontAwesomeIcon icon={getSortIconByFieldName('id')} />
                </th>
                <th className="hand" onClick={sort('nome')}>
                  Nome <FontAwesomeIcon icon={getSortIconByFieldName('nome')} />
                </th>
                <th className="hand" onClick={sort('cargaHorasMinCurso')}>
                  Carga Horas Min Curso <FontAwesomeIcon icon={getSortIconByFieldName('cargaHorasMinCurso')} />
                </th>
                <th className="hand" onClick={sort('cargaHorasMinEstagio')}>
                  Carga Horas Min Estagio <FontAwesomeIcon icon={getSortIconByFieldName('cargaHorasMinEstagio')} />
                </th>
                <th className="hand" onClick={sort('percMaxEstagioAC')}>
                  Perc Max Estagio AC <FontAwesomeIcon icon={getSortIconByFieldName('percMaxEstagioAC')} />
                </th>
                <th className="hand" onClick={sort('percMaxAtividadeDistancia')}>
                  Perc Max Atividade Distancia <FontAwesomeIcon icon={getSortIconByFieldName('percMaxAtividadeDistancia')} />
                </th>
                <th>
                  Escola <FontAwesomeIcon icon="sort" />
                </th>
                <th>
                  Tipo <FontAwesomeIcon icon="sort" />
                </th>
                <th />
              </tr>
            </thead>
            <tbody>
              {cursoList.map((curso, i) => (
                <tr key={`entity-${i}`} data-cy="entityTable">
                  <td>
                    <Button tag={Link} to={`/curso/${curso.id}`} color="link" size="sm">
                      {curso.id}
                    </Button>
                  </td>
                  <td>{curso.nome}</td>
                  <td>{curso.cargaHorasMinCurso}</td>
                  <td>{curso.cargaHorasMinEstagio}</td>
                  <td>{curso.percMaxEstagioAC}</td>
                  <td>{curso.percMaxAtividadeDistancia}</td>
                  <td>{curso.escola ? <Link to={`/escola/${curso.escola.id}`}>{curso.escola.id}</Link> : ''}</td>
                  <td>{curso.tipo ? <Link to={`/tipo-de-curso/${curso.tipo.id}`}>{curso.tipo.id}</Link> : ''}</td>
                  <td className="text-end">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`/curso/${curso.id}`} color="info" size="sm" data-cy="entityDetailsButton">
                        <FontAwesomeIcon icon="eye" /> <span className="d-none d-md-inline">Visualizar</span>
                      </Button>
                      <Button tag={Link} to={`/curso/${curso.id}/edit`} color="primary" size="sm" data-cy="entityEditButton">
                        <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Editar</span>
                      </Button>
                      <Button tag={Link} to={`/curso/${curso.id}/delete`} color="danger" size="sm" data-cy="entityDeleteButton">
                        <FontAwesomeIcon icon="trash" /> <span className="d-none d-md-inline">Excluir</span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        ) : (
          !loading && <div className="alert alert-warning">Nenhum Curso encontrado</div>
        )}
      </div>
    </div>
  );
};

export default Curso;
