import React, { useState, useEffect } from 'react';
import { Link, useNavigate, useParams } from 'react-router-dom';
import { Button, Row, Col, FormText } from 'reactstrap';
import { isNumber, ValidatedField, ValidatedForm } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';
import { useAppDispatch, useAppSelector } from 'app/config/store';

import { IEscola } from 'app/shared/model/escola.model';
import { getEntities as getEscolas } from 'app/entities/escola/escola.reducer';
import { ITipoDeCurso } from 'app/shared/model/tipo-de-curso.model';
import { getEntities as getTipoDeCursos } from 'app/entities/tipo-de-curso/tipo-de-curso.reducer';
import { ICurso } from 'app/shared/model/curso.model';
import { getEntity, updateEntity, createEntity, reset } from './curso.reducer';

export const CursoUpdate = () => {
  const dispatch = useAppDispatch();

  const navigate = useNavigate();

  const { id } = useParams<'id'>();
  const isNew = id === undefined;

  const escolas = useAppSelector(state => state.escola.entities);
  const tipoDeCursos = useAppSelector(state => state.tipoDeCurso.entities);
  const cursoEntity = useAppSelector(state => state.curso.entity);
  const loading = useAppSelector(state => state.curso.loading);
  const updating = useAppSelector(state => state.curso.updating);
  const updateSuccess = useAppSelector(state => state.curso.updateSuccess);

  const handleClose = () => {
    navigate('/curso');
  };

  useEffect(() => {
    if (isNew) {
      dispatch(reset());
    } else {
      dispatch(getEntity(id));
    }

    dispatch(getEscolas({}));
    dispatch(getTipoDeCursos({}));
  }, []);

  useEffect(() => {
    if (updateSuccess) {
      handleClose();
    }
  }, [updateSuccess]);

  const saveEntity = values => {
    const entity = {
      ...cursoEntity,
      ...values,
      escola: escolas.find(it => it.id.toString() === values.escola.toString()),
      tipo: tipoDeCursos.find(it => it.id.toString() === values.tipo.toString()),
    };

    if (isNew) {
      dispatch(createEntity(entity));
    } else {
      dispatch(updateEntity(entity));
    }
  };

  const defaultValues = () =>
    isNew
      ? {}
      : {
          ...cursoEntity,
          escola: cursoEntity?.escola?.id,
          tipo: cursoEntity?.tipo?.id,
        };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="matrixApp.curso.home.createOrEditLabel" data-cy="CursoCreateUpdateHeading">
            Criar ou editar Curso
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <ValidatedForm defaultValues={defaultValues()} onSubmit={saveEntity}>
              {!isNew ? <ValidatedField name="id" required readOnly id="curso-id" label="Id" validate={{ required: true }} /> : null}
              <ValidatedField label="Nome" id="curso-nome" name="nome" data-cy="nome" type="text" />
              <ValidatedField
                label="Carga Horas Min Curso"
                id="curso-cargaHorasMinCurso"
                name="cargaHorasMinCurso"
                data-cy="cargaHorasMinCurso"
                type="text"
              />
              <ValidatedField
                label="Carga Horas Min Estagio"
                id="curso-cargaHorasMinEstagio"
                name="cargaHorasMinEstagio"
                data-cy="cargaHorasMinEstagio"
                type="text"
              />
              <ValidatedField
                label="Perc Max Estagio AC"
                id="curso-percMaxEstagioAC"
                name="percMaxEstagioAC"
                data-cy="percMaxEstagioAC"
                type="text"
              />
              <ValidatedField
                label="Perc Max Atividade Distancia"
                id="curso-percMaxAtividadeDistancia"
                name="percMaxAtividadeDistancia"
                data-cy="percMaxAtividadeDistancia"
                type="text"
              />
              <ValidatedField id="curso-escola" name="escola" data-cy="escola" label="Escola" type="select">
                <option value="" key="0" />
                {escolas
                  ? escolas.map(otherEntity => (
                      <option value={otherEntity.id} key={otherEntity.id}>
                        {otherEntity.id}
                      </option>
                    ))
                  : null}
              </ValidatedField>
              <ValidatedField id="curso-tipo" name="tipo" data-cy="tipo" label="Tipo" type="select">
                <option value="" key="0" />
                {tipoDeCursos
                  ? tipoDeCursos.map(otherEntity => (
                      <option value={otherEntity.id} key={otherEntity.id}>
                        {otherEntity.id}
                      </option>
                    ))
                  : null}
              </ValidatedField>
              <Button tag={Link} id="cancel-save" data-cy="entityCreateCancelButton" to="/curso" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">Voltar</span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp; Salvar
              </Button>
            </ValidatedForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

export default CursoUpdate;
