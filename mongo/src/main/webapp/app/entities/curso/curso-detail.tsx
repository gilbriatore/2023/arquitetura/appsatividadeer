import React, { useEffect } from 'react';
import { Link, useParams } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import {} from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { useAppDispatch, useAppSelector } from 'app/config/store';

import { getEntity } from './curso.reducer';

export const CursoDetail = () => {
  const dispatch = useAppDispatch();

  const { id } = useParams<'id'>();

  useEffect(() => {
    dispatch(getEntity(id));
  }, []);

  const cursoEntity = useAppSelector(state => state.curso.entity);
  return (
    <Row>
      <Col md="8">
        <h2 data-cy="cursoDetailsHeading">Curso</h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="id">Id</span>
          </dt>
          <dd>{cursoEntity.id}</dd>
          <dt>
            <span id="nome">Nome</span>
          </dt>
          <dd>{cursoEntity.nome}</dd>
          <dt>
            <span id="cargaHorasMinCurso">Carga Horas Min Curso</span>
          </dt>
          <dd>{cursoEntity.cargaHorasMinCurso}</dd>
          <dt>
            <span id="cargaHorasMinEstagio">Carga Horas Min Estagio</span>
          </dt>
          <dd>{cursoEntity.cargaHorasMinEstagio}</dd>
          <dt>
            <span id="percMaxEstagioAC">Perc Max Estagio AC</span>
          </dt>
          <dd>{cursoEntity.percMaxEstagioAC}</dd>
          <dt>
            <span id="percMaxAtividadeDistancia">Perc Max Atividade Distancia</span>
          </dt>
          <dd>{cursoEntity.percMaxAtividadeDistancia}</dd>
          <dt>Escola</dt>
          <dd>{cursoEntity.escola ? cursoEntity.escola.id : ''}</dd>
          <dt>Tipo</dt>
          <dd>{cursoEntity.tipo ? cursoEntity.tipo.id : ''}</dd>
        </dl>
        <Button tag={Link} to="/curso" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Voltar</span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/curso/${cursoEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Editar</span>
        </Button>
      </Col>
    </Row>
  );
};

export default CursoDetail;
