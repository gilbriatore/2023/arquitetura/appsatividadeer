import React from 'react';
import { Route } from 'react-router-dom';

import ErrorBoundaryRoutes from 'app/shared/error/error-boundary-routes';

import Escola from './escola';
import EscolaDetail from './escola-detail';
import EscolaUpdate from './escola-update';
import EscolaDeleteDialog from './escola-delete-dialog';

const EscolaRoutes = () => (
  <ErrorBoundaryRoutes>
    <Route index element={<Escola />} />
    <Route path="new" element={<EscolaUpdate />} />
    <Route path=":id">
      <Route index element={<EscolaDetail />} />
      <Route path="edit" element={<EscolaUpdate />} />
      <Route path="delete" element={<EscolaDeleteDialog />} />
    </Route>
  </ErrorBoundaryRoutes>
);

export default EscolaRoutes;
