import atividade from 'app/entities/atividade/atividade.reducer';
import bloom from 'app/entities/bloom/bloom.reducer';
import curso from 'app/entities/curso/curso.reducer';
import escola from 'app/entities/escola/escola.reducer';
import matriz from 'app/entities/matriz/matriz.reducer';
import modalidade from 'app/entities/modalidade/modalidade.reducer';
import tipoDeCurso from 'app/entities/tipo-de-curso/tipo-de-curso.reducer';
import unidade from 'app/entities/unidade/unidade.reducer';
/* jhipster-needle-add-reducer-import - JHipster will add reducer here */

const entitiesReducers = {
  atividade,
  bloom,
  curso,
  escola,
  matriz,
  modalidade,
  tipoDeCurso,
  unidade,
  /* jhipster-needle-add-reducer-combine - JHipster will add reducer here */
};

export default entitiesReducers;
