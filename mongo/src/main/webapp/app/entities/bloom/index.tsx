import React from 'react';
import { Route } from 'react-router-dom';

import ErrorBoundaryRoutes from 'app/shared/error/error-boundary-routes';

import Bloom from './bloom';
import BloomDetail from './bloom-detail';
import BloomUpdate from './bloom-update';
import BloomDeleteDialog from './bloom-delete-dialog';

const BloomRoutes = () => (
  <ErrorBoundaryRoutes>
    <Route index element={<Bloom />} />
    <Route path="new" element={<BloomUpdate />} />
    <Route path=":id">
      <Route index element={<BloomDetail />} />
      <Route path="edit" element={<BloomUpdate />} />
      <Route path="delete" element={<BloomDeleteDialog />} />
    </Route>
  </ErrorBoundaryRoutes>
);

export default BloomRoutes;
