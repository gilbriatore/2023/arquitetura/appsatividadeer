import React from 'react';

import MenuItem from 'app/shared/layout/menus/menu-item';

const EntitiesMenu = () => {
  return (
    <>
      {/* prettier-ignore */}
      <MenuItem icon="asterisk" to="/atividade">
        Atividade
      </MenuItem>
      <MenuItem icon="asterisk" to="/bloom">
        Bloom
      </MenuItem>
      <MenuItem icon="asterisk" to="/curso">
        Curso
      </MenuItem>
      <MenuItem icon="asterisk" to="/escola">
        Escola
      </MenuItem>
      <MenuItem icon="asterisk" to="/matriz">
        Matriz
      </MenuItem>
      <MenuItem icon="asterisk" to="/modalidade">
        Modalidade
      </MenuItem>
      <MenuItem icon="asterisk" to="/tipo-de-curso">
        Tipo De Curso
      </MenuItem>
      <MenuItem icon="asterisk" to="/unidade">
        Unidade
      </MenuItem>
      {/* jhipster-needle-add-entity-to-menu - JHipster will add entities to the menu here */}
    </>
  );
};

export default EntitiesMenu;
