import React from 'react';
import { Route } from 'react-router-dom';

import ErrorBoundaryRoutes from 'app/shared/error/error-boundary-routes';

import TipoDeCurso from './tipo-de-curso';
import TipoDeCursoDetail from './tipo-de-curso-detail';
import TipoDeCursoUpdate from './tipo-de-curso-update';
import TipoDeCursoDeleteDialog from './tipo-de-curso-delete-dialog';

const TipoDeCursoRoutes = () => (
  <ErrorBoundaryRoutes>
    <Route index element={<TipoDeCurso />} />
    <Route path="new" element={<TipoDeCursoUpdate />} />
    <Route path=":id">
      <Route index element={<TipoDeCursoDetail />} />
      <Route path="edit" element={<TipoDeCursoUpdate />} />
      <Route path="delete" element={<TipoDeCursoDeleteDialog />} />
    </Route>
  </ErrorBoundaryRoutes>
);

export default TipoDeCursoRoutes;
