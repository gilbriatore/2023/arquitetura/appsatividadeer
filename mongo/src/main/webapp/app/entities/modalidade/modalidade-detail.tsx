import React, { useEffect } from 'react';
import { Link, useParams } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import {} from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { useAppDispatch, useAppSelector } from 'app/config/store';

import { getEntity } from './modalidade.reducer';

export const ModalidadeDetail = () => {
  const dispatch = useAppDispatch();

  const { id } = useParams<'id'>();

  useEffect(() => {
    dispatch(getEntity(id));
  }, []);

  const modalidadeEntity = useAppSelector(state => state.modalidade.entity);
  return (
    <Row>
      <Col md="8">
        <h2 data-cy="modalidadeDetailsHeading">Modalidade</h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="id">Id</span>
          </dt>
          <dd>{modalidadeEntity.id}</dd>
          <dt>
            <span id="nome">Nome</span>
          </dt>
          <dd>{modalidadeEntity.nome}</dd>
        </dl>
        <Button tag={Link} to="/modalidade" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Voltar</span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/modalidade/${modalidadeEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Editar</span>
        </Button>
      </Col>
    </Row>
  );
};

export default ModalidadeDetail;
