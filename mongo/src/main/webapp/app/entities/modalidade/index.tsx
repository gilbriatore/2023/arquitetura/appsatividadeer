import React from 'react';
import { Route } from 'react-router-dom';

import ErrorBoundaryRoutes from 'app/shared/error/error-boundary-routes';

import Modalidade from './modalidade';
import ModalidadeDetail from './modalidade-detail';
import ModalidadeUpdate from './modalidade-update';
import ModalidadeDeleteDialog from './modalidade-delete-dialog';

const ModalidadeRoutes = () => (
  <ErrorBoundaryRoutes>
    <Route index element={<Modalidade />} />
    <Route path="new" element={<ModalidadeUpdate />} />
    <Route path=":id">
      <Route index element={<ModalidadeDetail />} />
      <Route path="edit" element={<ModalidadeUpdate />} />
      <Route path="delete" element={<ModalidadeDeleteDialog />} />
    </Route>
  </ErrorBoundaryRoutes>
);

export default ModalidadeRoutes;
