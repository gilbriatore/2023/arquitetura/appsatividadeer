import { IEscola } from 'app/shared/model/escola.model';
import { ITipoDeCurso } from 'app/shared/model/tipo-de-curso.model';

export interface ICurso {
  id?: string;
  nome?: string | null;
  cargaHorasMinCurso?: number | null;
  cargaHorasMinEstagio?: number | null;
  percMaxEstagioAC?: number | null;
  percMaxAtividadeDistancia?: number | null;
  escola?: IEscola | null;
  tipo?: ITipoDeCurso | null;
}

export const defaultValue: Readonly<ICurso> = {};
