export interface IMatriz {
  id?: string;
  codigo?: string | null;
}

export const defaultValue: Readonly<IMatriz> = {};
