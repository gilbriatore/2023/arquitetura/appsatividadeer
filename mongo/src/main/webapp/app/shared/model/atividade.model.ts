export interface IAtividade {
  id?: string;
  nome?: string | null;
}

export const defaultValue: Readonly<IAtividade> = {};
