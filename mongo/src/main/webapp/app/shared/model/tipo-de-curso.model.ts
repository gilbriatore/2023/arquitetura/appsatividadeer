import { ICurso } from 'app/shared/model/curso.model';

export interface ITipoDeCurso {
  id?: string;
  nome?: string | null;
  cursos?: ICurso[] | null;
}

export const defaultValue: Readonly<ITipoDeCurso> = {};
