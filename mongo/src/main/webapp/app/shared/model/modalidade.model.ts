export interface IModalidade {
  id?: string;
  nome?: string | null;
}

export const defaultValue: Readonly<IModalidade> = {};
