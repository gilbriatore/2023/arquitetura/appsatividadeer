export interface IUnidade {
  id?: string;
  codigo?: string | null;
}

export const defaultValue: Readonly<IUnidade> = {};
