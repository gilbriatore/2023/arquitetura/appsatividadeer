import { ICurso } from 'app/shared/model/curso.model';

export interface IEscola {
  id?: string;
  nome?: string | null;
  cursos?: ICurso[] | null;
}

export const defaultValue: Readonly<IEscola> = {};
